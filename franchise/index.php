<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Франчайзинг");
?>

<!-- Content :: start -->
<div class="container">
    <h1 class="text-center"><?= $APPLICATION->ShowTitle(false)?></h1>
    <br>
    <br>
    <p class="text-center">
		<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include_areas/franchise_franchise.php",
				"EDIT_TEMPLATE" => ""
			),
			false,
			array("ACTIVE_COMPONENT" => "Y")
		);?>  
	</p>
    <div style="margin-bottom: 80px;"></div>
</div>

<div class="offer wow fadeIn">
    <div class="container">
        <h2 class="text-center">
		<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include_areas/franchise_offer_title.php",
				"EDIT_TEMPLATE" => ""
			),
			false,
			array("ACTIVE_COMPONENT" => "Y")
		);?> 		
		</h2>

        <div class="offer-table col-md-8 col-sm-10 col-xs-12">
		<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include_areas/franchise_offer.php",
				"EDIT_TEMPLATE" => ""
			),
			false,
			array("ACTIVE_COMPONENT" => "Y")
		);?>
        </div>
    </div>
</div>


<main class="content tariffs">

	<?$APPLICATION->IncludeComponent("bitrix:news.list", "loans", array(
	"IBLOCK_TYPE" => "content",
	"IBLOCK_ID" => "11",
	"NEWS_COUNT" => "200",
	"SORT_BY1" => "ID",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "ASC",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "DETAIL_TEXT",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "MAX_PRICE",
		2 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "3600",
	"CACHE_FILTER" => "Y",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "j F Y",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "N",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>

    <div class="container wow fadeIn">
        <h2 class="text-center head-title">
		<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include_areas/franchise_advantages_title.php",
				"EDIT_TEMPLATE" => ""
			),
			false,
			array("ACTIVE_COMPONENT" => "Y")
		);?> 		
		</h2>
        <p class="text-center">
		<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include_areas/franchise_advantages_description.php",
				"EDIT_TEMPLATE" => ""
			),
			false,
			array("ACTIVE_COMPONENT" => "Y")
		);?> 		
		</p>

        <br>
        <br>
        <br>

        <ol class="ol-list">
		<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include_areas/franchise_advantages.php",
				"EDIT_TEMPLATE" => ""
			),
			false,
			array("ACTIVE_COMPONENT" => "Y")
		);?>
        </ol>

        <br>
        <br>
        <p class="text-center">
		<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include_areas/franchise_call_to_action.php",
				"EDIT_TEMPLATE" => ""
			),
			false,
			array("ACTIVE_COMPONENT" => "Y")
		);?>
		</p>
        <div style="margin-bottom: 80px;"></div>
    </div>

</main>

<!-- Content :: end -->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
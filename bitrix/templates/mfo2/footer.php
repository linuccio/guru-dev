</div>
<div class="footer" align="center">

    <?$APPLICATION->IncludeComponent(
        "bitrix:menu",
        "footer",
        Array(
            "ROOT_MENU_TYPE" => "footer",
            "MAX_LEVEL" => "2",
            "CHILD_MENU_TYPE" => "left",
            "USE_EXT" => "Y",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => Array()
        )
    );?>
    <div class="column">
        <div class="title">
            Сделано в:
        </div>
        <ul>
            <li>
                <a href="http://sitemfo.ru" target="_blank"><img width="150" src="/images/logo_footer.jpg"></a>
            </li>
            <li style="color: #69696a; font-size: 18px">
                2012 - <?=date("Y")?>
            </li>
        </ul>


    </div>
</div>


</div>
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter23373397 = new Ya.Metrika({id:23373397,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/23373397" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>

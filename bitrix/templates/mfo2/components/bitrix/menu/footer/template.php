<?foreach($arResult as $arSection):?>
    <div class="column">
        <div class="title">
            <?=$arSection['title']['TEXT']?>
        </div>
        <ul>
            <?foreach($arSection['items'] as $arItem):?>
            <li>
                <a href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a>
            </li>
            <?endforeach?>
        </ul>
    </div>
<?endforeach?>
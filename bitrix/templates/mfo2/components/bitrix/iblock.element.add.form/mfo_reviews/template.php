<div class="reviews">
    <div>
        <?if (count($arResult["ERRORS"])):?>
            <?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
        <?endif?>
        <?if (strlen($arResult["MESSAGE"]) > 0):?>
            <?=ShowNote($arResult["MESSAGE"])?>
        <?endif?>
    </div>
    <form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" enctype="multipart/form-data" class="form" align="left" method="POST">
        <?=bitrix_sessid_post()?>
        <?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>

        <div>
            <div class="formblock left">
                <?foreach($arResult['leftFields'] as $key => $field):?>
                    <?=$field['NAME']?>
                    <?if($field['REQUIRED']):?>
                        <span class="requiredstar">*</span>
                    <?endif?>
                    :<br/>
                    <input type="text" name="PROPERTY[<?=$key?>][0]" /><br/>
                <?endforeach?>
            </div>
            <div class="formblock right">
                <?foreach($arResult['rightFields'] as $key => $field):?>
                    <?if(!isset($field['NAME'])):?>
                        <?=GetMessage($key)?><br/>
                    <?else:?>
                        <?=$field['NAME']?>
                    <?endif?>
                    <?if($field['REQUIRED']):?>
                        <span class="requiredstar">*</span>
                    <?endif?>
                    :<br/>
                    <textarea name="PROPERTY[<?=$key?>][0]"></textarea><br/>
                <?endforeach?>
            </div>
        </div>
        <div align="center">
            <input type="submit" name="iblock_submit" class="submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
        </div>
    </form>
</div>
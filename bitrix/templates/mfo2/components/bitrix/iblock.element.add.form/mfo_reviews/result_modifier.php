<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 10/11/13
 * Time: 6:47 PM
 * To change this template use File | Settings | File Templates.
 */


foreach($arResult['PROPERTY_LIST'] as $propId)
{
    if(array_search($propId, $arParams['PROPERTY_CODES_REQUIRED']) !== false)
    {
        $arResult['PROPERTY_LIST_FULL'][$propId]['REQUIRED'] = true;
    }

    if(isset($arParams['CUSTOM_TITLE_'.$propId]))
        $arResult['PROPERTY_LIST_FULL'][$propId]['NAME'] = $arParams['CUSTOM_TITLE_'.$propId];

    if($arResult['PROPERTY_LIST_FULL'][$propId]['PROPERTY_TYPE'] == "T")
    {
        $arResult['rightFields'][$propId] = $arResult['PROPERTY_LIST_FULL'][$propId];
    }
    else
    {
        $arResult['leftFields'][$propId] = $arResult['PROPERTY_LIST_FULL'][$propId];
    }
}



<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(isset($arResult['ITEMS'][0])):?>
    <div class="titletext">
        <?=$arResult['ITEMS'][0]['NAME']?>
        <div class="date">
            <?=$arResult['ITEMS'][0]['ACTIVE_FROM']?>
        </div>
    </div>
    <div class="text">
        <?=$arResult['ITEMS'][0]['PREVIEW_TEXT']?>
    </div>
    <div class="link">
        <a href="news.php" style="color: gray;">Смотреть больше новостей</a>
    </div>
<?else:?>
    <?=GetMessage("no_items");?>
<?endif?>
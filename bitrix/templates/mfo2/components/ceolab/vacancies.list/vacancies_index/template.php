<div class="right">
    <?if(isset($arResult['DATA'][0])):?>
        <div class="titletext">
            <?=$arResult['DATA'][0]['NAME']?>
            <div class="date">
                <?=$arResult['DATA'][0]['ACTIVE_FROM']?>
            </div>
        </div>
        <div class="text">
            <?=$arResult['DATA'][0]['PREVIEW_TEXT']?>
        </div>
        <div class="link">
            <a href="/jobs/vacancies.php"><?=GetMessage("all_items")?></a>
        </div>
    <?else:?>
        <?=GetMessage("no_items")?>
    <?endif?>
</div>

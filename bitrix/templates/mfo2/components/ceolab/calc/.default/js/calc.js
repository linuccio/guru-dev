/**
 * Created with JetBrains WebStorm.
 * User: evgenius
 * Date: 12/4/13
 * Time: 5:28 PM
 * To change this template use File | Settings | File Templates.
 */
function merge(obj1, obj2)
{
    for(var i in obj2)
    {
        obj1[i] = obj2[i];
    }
    return obj1;
}


function ceolab_calcmfo(domObject)
{
    this.baseObject = $(domObject);

    this.percent = 2.5;

    this.term = {
        min: 1,
        max: 5,
        step: 1,
        field: this.baseObject.find("input[name=\"fieldTerm\"]"),
        value: 30,
        certain: 2,
        type: "soft"
    };

    this.current = {
        summ: 0,
        term: 0,
        summPercent: 0,
        toPay: 0
    }

    this.summ = {
        min: 1000,
        max: 15000,
        step: 1000,
        field: this.baseObject.find("input[name=\"fieldSumm\"]"),
        value: 30,
        certain: 2,
        type: "soft"
    }

    this.slider_summ = new vg_slider();

    this.slider_summ.bind(
        "setval",
        this,
        function(val, data)
        {
            val = val.toString().replace(" ", "")*1;

            return val;
        }
    )


    this.slider_summ.lastStep = 0;
    this.slider_summ.bind(
        "slide",
        this,
        function(val, data)
        {
            var steps = parseInt((data.slider_summ.baseBlock_Width) / data.slider_summ.step);
            var currentStep = (val / data.summ.step)-2;

            data.slider_summ.baseBlock.parent(".slider").find(".scale").find(".step").removeClass("activeStep");

            data.slider_summ.baseBlock.parent(".slider").find(".scale").find(".step").each(
                function()
                {

                }
            )

            val = parseInt(val);
            data.current.summ = val;
            val = formatPrice(val);
            data.render.setData("spanSumm", val);



            return val;
        }
    )

    this.slider_summ.bind(
        "down",
        this,
        function(val, data)
        {
            data.render.setData("spanToPay", "--");
            data.render.setData("spanSummPercent", "--");
        }
    )

    this.slider_summ.bind(
        "up",
        this,
        function(val, data)
        {
            data.calculate();
        }
    )

    this.slider_term = new vg_slider();

    this.slider_term.lastStep = 0;
    this.slider_term.bind(
        "slide",
        this,
        function(val, data)
        {
            var steps = parseInt((data.slider_term.baseBlock_Width) / data.slider_term.step);
            var currentStep = parseInt((val / data.term.step)-2);

            /*
            if(data.slider_term.lastStep && currentStep > data.slider_term.lastStep)
                data.slider_term.baseBlock.parent(".slider").find(".scale").find(".step").eq(currentStep-1).addClass("activeStep");
            else
                data.slider_term.baseBlock.parent(".slider").find(".scale").find(".step").eq(currentStep).removeClass("activeStep");
            */

            data.slider_term.lastStep = currentStep;

            val = parseInt(val);
            data.current.term = val;
            data.render.setData("spanTerm", val);

            return val;
        }
    )

    this.slider_term.bind(
        "down",
        this,
        function(val, data)
        {
            data.render.setData("spanToPay", "--");
            data.render.setData("spanSummPercent", "--");
        }
    )

    this.slider_term.bind(
        "up",
        this,
        function(val, data)
        {
            data.calculate();
        }
    )

    this.init = function(data)
    {
        if(data)
        {
            if(data.summ)
                this.summ = merge(this.summ, data.summ);

            if(data.term)
                this.term = merge(this.term, data.term);

            if(data.percent)
                this.percent  = data.percent;
        }

        this.initSliders();

        this.baseObject.find("div[name=\"scale_min_summ\"]").text(formatPrice(this.summ.min));

        this.baseObject.find("div[name=\"scale_max_summ\"]").text(formatPrice(this.summ.max));
        this.baseObject.find("div[name=\"scale_max_summ\"]").css("marginRight", -this.baseObject.find("div[name=\"scale_max_summ\"]").width()/2);

        this.baseObject.find("div[name=\"scale_min_term\"]").text(formatPrice(this.term.min));
        this.baseObject.find("div[name=\"scale_max_term\"]").text(formatPrice(this.term.max));
        this.baseObject.find("div[name=\"scale_max_term\"]").css("marginRight", -this.baseObject.find("div[name=\"scale_max_term\"]").width()/2);



        this.scale_summ = this.baseObject.find("div[name=\"scale_summ\"]");
        this.initScale(this.slider_summ, this.scale_summ);

        this.scale_term = this.baseObject.find("div[name=\"scale_term\"]");
        this.initScale(this.slider_term, this.scale_term);
    }


    this.initSliders = function()
    {
        this.slider_summ.init(this.baseObject.find("input[name=\"slider_summ\"]"),this.summ);
        this.slider_summ.setValue(this.summ.min);
        this.slider_summ.stepFix();
        this.p = (this.summ.max - this.summ.min)/100;
        this.slider_term.init(this.baseObject.find("input[name=\"slider_term\"]"),this.term);
        this.slider_term.setValue(this.term.min);
        this.slider_term.stepFix();
        this.calculate();
        //this.rend();
    }

    this.initScale = function(slider, objectScale)
    {
        objectScale.empty();
        var stepLength = slider.step;
        var steps = parseInt((slider.baseBlock_Width) / stepLength);




        var maxSteps = 30;
        if(steps > maxSteps)
        {
           stepLength = stepLength * (steps/maxSteps);
            steps = maxSteps;
        }
        var stepScale;
        for(var i = 0; i<=steps+1; i++)
        {
           if(stepLength*i <= 340)
            stepScale = $("<div class=\"step\"></div>").appendTo(objectScale).css("left", stepLength*i);
        }
    }

    this.rend = function()
    {
        this.render.setData("spanSummPercent", this.current.summPercent);
        this.render.setData("spanToPay", this.current.toPay);


        this.bag.setSize((this.current.summ-this.summ.min)/this.p);
    }

    this.calculate = function(summ, term)
    {
        this.current.summPercent = ((this.current.summ/100)*this.percent)*this.current.term;
        this.current.summPercent = this.current.summPercent.toFixed(2)*1;
        this.current.toPay = this.current.summ+this.current.summPercent;
        this.current.toPay = this.current.toPay.toFixed(2);
        //return;
        this.rend();
    }



    this.initRender = function()
    {
        this.render = new ceolab_calcmfo_render();
        this.render.addObject('spanSummPercent', this.baseObject.find("span[name=\"summPercent\"]"));
        this.render.addObject('spanToPay', this.baseObject.find("span[name=\"toPay\"]"));
        this.render.addObject("spanSumm", this.baseObject.find("span[name=\"summ\"]"));
        this.render.addObject("spanTerm", this.baseObject.find("span[name=\"term\"]"));


        this.bag = new ceolab_calcmfo_bag(this.baseObject.find("img[name=\"moneyBag\"]"));
    }


    this.initRender();
}

function ceolab_calcmfo_render()
{
    this.objects = {};

    this.setData = function(name, text)
    {
        this.objects[name].text(text);
    }

    this.addObject = function(name, object)
    {
        this.objects[name] = object;
    }
}

function ceolab_calcmfo_bag(object)
{
    this.size = {
        min: 122,
        max: 177
    };

    this.p2 = (this.size.max-this.size.min)/100;

    this.object = $(object);

    this.setSize = function(p)
    {
        var val = this.p2*p;

        this.object.animate(
            {
                height: this.size.min+val,
                top: -val
            },
            500
        )

        //this.object.attr("height", this.size.min+val);
        //this.object.css("top", -val);
    }
}

function formatPrice(price)
{
    price = price.toString().split(".");
    var l = price[0].length;
    var a = 3;
    var result = "";
    do
    {
        l-=3;
        if(l<0)
        {
            a+=l;
            l=0;
        }

        result = price[0].substr(l, a)+" "+result;
    }while(l);
    if(price[1])
        result+="."+price[1];
    return result;
}
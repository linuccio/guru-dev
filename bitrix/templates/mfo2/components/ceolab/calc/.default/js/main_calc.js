/**
 * Created with JetBrains WebStorm.
 * User: evgenius
 * Date: 12/7/13
 * Time: 10:50 PM
 * To change this template use File | Settings | File Templates.
 */

function main_calc(products)
{
    this.baseBlock = $("#ceolab_calcmfo");
    this.switcherBlock = this.baseBlock.find("*[name=\"loan_switcher\"]");
    this.textBlock = this.baseBlock.find("*[name=\"loan_description\"]");

    this.products = products;
    this.items = [];

    this.calc = new ceolab_calcmfo("#ceolab_calcmfo");

    this.setActive = function(i)
    {
        this.calc.init(this.products[i]);
        this.textBlock.text(this.products[i].text);
        this.switcherBlock.find("*").removeClass("itemActive");
        this.items[i].addClass("itemActive");
        this.setAnketId(this.products[i].anket);

        $("#ceolab_calcmfo input[name=\"product\"]").val(products[i].name);


        if(this.products[i].type == "day")
        {
            console.debug(123);
            $("#ceolab_calcmfo span[name=\"typeLoan\"]").text("дней");
        }
        else
        {
            $("#ceolab_calcmfo span[name=\"typeLoan\"]").text("месяцев");
        }

    }

    this.rendSwitcher = function()
    {
        var item;
        for(var i in this.products)
        {
            item = $("<li></li>").appendTo(this.switcherBlock);
            item.text(this.products[i].name);
            item.bind(
                "click",
                [this, i],
                function(event)
                {
                    event.data[0].setActive(event.data[1]);
                }
            )
            this.items.push(item);
        }
    }

    this.setAnketId = function(id)
    {
        this.baseBlock.find("input[name=\"anket\"]").val(id);
    }

    this.rendSwitcher();

}

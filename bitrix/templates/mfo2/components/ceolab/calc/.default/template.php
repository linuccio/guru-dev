<script>
    $("document").ready(
        function()
        {
            var data = eval('('+$('#calcJsonData').val()+')');
            var products = [];
            for(var i in data.calcs)
            {
                var calc = data.calcs[i].calc;
                products.push({
                    name: calc.name,
                    text: calc.additionInfo,
                    percent : calc.percent,
                    type : calc.type_percent,
                    summ: {
                        min : calc.min_summ,
                        max : calc.max_summ,
                        step: calc.step
                    },
                    term : {
                        min : calc.min_term,
                        max : calc.max_term,
                        step: calc.step_term
                    },
                    anket:59
                });


            }
            /*
             var products = [
             {
             name: "До зарплаты",
             text: "qwerty 123",
             summ: {
             min: 1000,
             max: 5000,
             step: 1000
             },
             percent: 1.5,
             term: {
             min: 1,
             max: 15,
             step: 1
             },
             anket: 59
             },
             {
             name: "Большие деньги",
             text: "qwerty asdasd",
             summ: {
             min: 100000,
             max: 500000,
             step: 1000
             },
             percent: 1.5,
             term: {
             min: 1,
             max: 15,
             step: 1
             },
             anket: 2
             }
             ];*/
            calc = new main_calc(products);
            calc.setActive(0);

            function testPerformance()
            {
                var startTime = (new Date()).getTime();
                for(var i = 0; i<10000; i++)
                    calc.calc.slider_summ.pointerPos(i);
                var endTime = (new Date()).getTime() - startTime;
                return endTime;
            }

            $("#testPerformance").click(
                function()
                {
                    var text = $("#testPerformance").val();

                    $("#testPerformance").val("Testing...");
                    $("#testPerformance").attr("disabled", "disabled");
                    $("#resultPerformance").text("---");

                    var val = testPerformance();

                    $("#testPerformance").removeAttr("disabled");
                    $("#resultPerformance").text(val);
                    $("#testPerformance").val(text);
                }
            )

        }
    )
</script>
<input id="calcJsonData" type="hidden" value="<?=$arParams['PRODUCTS']?>"/>

<form class="calc_mfo" id="ceolab_calcmfo" method="post" action="anket.php">
    <input type="hidden" name="anket"/>
    <input type="hidden" name="product"/>
    <input type="hidden" name="type">
    <input type="hidden" name="currency">
    <input name="fieldSumm" type="hidden"/>
    <input name="fieldTerm" type="hidden"/>
    <div class="head">
        <div class="switchWrap">
            <div class="switch">
                Вид займа:
                <ul name="loan_switcher">

                </ul>
            </div>
        </div>

        <div class="info" name="loan_description">
            Для работающих заемщиков:паспорт (прописка в г. Ижевске или Завьяловском районе), ИНН или водительское удостоверение или пенсионное страховое свидетельство (один из трех документов на выбор).Для заемщиков – пенсионеров: пенсионное удостоверение, документ, подтверждающий размер пенсии.
        </div>
    </div>
    <div class="middle">
        <div class="sliderWrap left">
            Какая сумма Вам нужна?
            <div class="slider">
                <div class="scale_numbers">
                    <div class="first" name="scale_min_summ">
                        1000
                    </div>
                    <div class="last" name="scale_max_summ">
                        5000
                    </div>
                </div>
                <div class="scale" name="scale_summ">

                </div>
                <input type="slider" name="slider_summ"/>
            </div>
        </div>
        <div class="sliderWrap right">
            На сколько дней Вы хотели бы ее получить?
            <div class="slider">
                <div class="scale_numbers">
                    <div class="first" name="scale_min_term">
                        1
                    </div>
                    <div class="last" name="scale_max_term">
                        15
                    </div>
                </div>
                <div class="scale" name="scale_term">

                </div>
                <input type="slider" name="slider_term"/>
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="result" align="center">
            <div class="p">
                <div class="text">
                    Вы берете
                </div>
                <span class="d" name="summ">4 500</span> руб.
            </div>
            <div class="p term">
                <div class="text">
                    На
                </div>
                <span class="d" name="term">5</span> <span name="typeLoan"></span>
            </div>
            <div class="p total">
                <div class="text">
                    Возвращаете
                </div>
                <span class="d2" name="toPay">5 320</span> руб.
            </div>
        </div>
        <div class="btnWrap">
            <input type="submit" class="calcBtn" value="ОФОРМИТЬ ЗАЯВКУ">
        </div>
    </div>
</form>
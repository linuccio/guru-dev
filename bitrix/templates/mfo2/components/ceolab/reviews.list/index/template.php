<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(isset($arResult[0])):?>
    <?
    if($arResult[0]['ACTIVE'] != "Y")
        $APPLICATION->setEditArea($this->getEditAreaId($arResult[0]['ID']), array(
            array(
                "URL" => "?active=".$arResult[0]['ID'],
                "TITLE" => "Активировать отзыв",
                "ICON" => "bx-context-toolbar-edit-icon"
            )
        ));            ?>
    <div class="textwrap" id="<?=$this->GetEditAreaId($arResult[0]['ID']);?>">
        <div class="titletext">
            <?=$arResult[0]['NAME']?>
            <div class="date">
                <?=$arResult[0]['ACTIVE_FROM']?>
            </div>
        </div>
        <div class="text">
            <?=$arResult[0]['PREVIEW_TEXT']?>
        </div>
    </div>
    <div class="link">
        <a style="color: gray;" href="reviews.php">Смотреть больше отзывов</a>
    </div>
<?else:?>
<p>
    Отзывов нет
</p>
<div>
    <a style="color: gray;" href="reviews.php">Добавить отзыв</a>
</div>
<?endif?>



<!DOCTYPE html>
<html>
<head>
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<div align="center">
    <div class="content">
        <div class="head" align="center">
            <div class="logo">
                <a href="/"><img src="/bitrix/templates/mfo2/logo.jpg"></a>
                <div class="slogan">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => "/include_areas/slogan.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false,
                        array(
                            "ACTIVE_COMPONENT" => "Y"
                        )
                    );?>
                </div>
            </div>
            <div class="text" align="left">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/include_areas/header_info.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false,
                    array(
                        "ACTIVE_COMPONENT" => "Y"
                    )
                );?>
            </div>
            <div class="contacts" align="right">
                <div class="address">
                    <img src="/images/geo.jpg">
                    <a href="/address.php">Адреса офисов</a>
                </div>
                <div class="phone">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => "/include_areas/phone.php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false,
                        array(
                            "ACTIVE_COMPONENT" => "Y"
                        )
                    );?>
                </div>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/include_areas/icons.php",
                "EDIT_TEMPLATE" => ""
            ),
            false,
            array(
                "ACTIVE_COMPONENT" => "Y"
            )
        );?>
        <div class="contentWrap" align="left">

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle()?></title>

</head>
<body>
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <div>
        <div align="center">
            <div class="headerLinks" align="left">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "mfo_header",
                    Array(
                        "ROOT_MENU_TYPE" => "top",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "top",
                        "USE_EXT" => "Y",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => Array()
                    )
                );?>
            </div>
        </div>
        <div align="center" class="headerAdres">
            <table  width="900px">
                <td align="left">
                    <a href="/">
                        <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."logo.php",
                            "EDIT_TEMPLATE" => ""
                        )
                    )?>
                    </a>
                </td>
                <td align="right">
                    <?$APPLICATION->IncludeComponent("ceolab:office.selectcity", "header_mfo", array(
	"IBLOCK_TYPE" => "regions",
	"IBLOCK_ID" => "1",
	"REDIRECT_URL" => "",
	"SET_SESSION" => "REGION_ID",
	"GET_SESSION" => "REGION_ID",
	"DEFAULT_CITY" => "1",
	"SET_GET" => "region",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600"
	),
	false
);?>
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DIR."contacts.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false,
                    array(
                        "ACTIVE_COMPONENT" => "Y"
                    )
                );?>
                </td>
            </table>
        </div>
    </div>

    <div align="center" style="min-height: 60%; padding-top: 50px;">
        <div class="content" align="left">
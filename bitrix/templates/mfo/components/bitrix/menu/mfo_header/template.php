<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <? $first = true; ?>
    <? foreach($arResult as $arItem): ?>
        <? if(!$first): ?>
        &bull;
        <? endif ?>
        <a href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a>
        <? $first = false; ?>
    <? endforeach ?>
<?endif?>
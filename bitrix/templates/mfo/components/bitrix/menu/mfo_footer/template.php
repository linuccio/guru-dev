<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

    <table class="footerLinks">
        <tr>
            <? foreach($arResult as $arItem):?>
                <? if($arItem['DEPTH_LEVEL'] == 1):?>
                    <th>
                        <?=$arItem['TEXT'] ?>
                    </th>
                <? endif ?>
            <? endforeach ?>
        </tr>
        <tr>
            <? $tagClosed = true; ?>
            <? foreach($arResult as $arItem):?>
                <? if($arItem['DEPTH_LEVEL'] == 1):?>
                    <? if(!$tagClosed): ?>
                        </td>
                    <? endif ?>
                    <td valign="top" nowrap>
                     <? $tagClosed = false ?>
                <? else: ?>
                    <div><a href="<?=$arItem['LINK'] ?>"><?=$arItem['TEXT'] ?></a></div>
                <? endif ?>
            <? endforeach ?>
            <? if(!$tagClosed): ?>
                        </td>
            <? endif ?>
        </tr>
    </table>
<?endif?>
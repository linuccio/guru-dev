<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?><!doctype html>
<html lang="<?= LANGUAGE_ID?>">
<head>
    <?$APPLICATION->ShowHead();?>
	<meta charset="<?= LANG_CHARSET;?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH?>/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">
    <title><?$APPLICATION->ShowTitle()?></title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400&subset=latin,cyrillic">
	<?
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH . '/css/grid.css');
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH . '/css/main.css');
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH . '/css/additional.css');
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH . '/css/animate.min.css');
	$APPLICATION->SetAdditionalCSS (SITE_TEMPLATE_PATH . '/css/validationEngine.jquery.css');
	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/materialmenu.jquery.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/background.cycle.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/wow.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.placeholder.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.formToWizard.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.validationEngine.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.validationEngine-ru.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/main.js');
	?>
        <script src="<?= SITE_TEMPLATE_PATH; ?>/js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH; ?>/js/arcticmodal/jquery.arcticmodal-0.3.css">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH; ?>/js/arcticmodal/themes/simple.css">
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH; ?>/js/jquery.maskedinput.js"></script>
        <script type="text/javascript">
        jQuery(function(){
            jQuery(".mask").mask("+7(999)999-99-99");
            jQuery(".pasp").mask("99 99 999-999");
        });
        </script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<header class="header">
    <div class="container">
        <div class="row">
        <!--  Logo :: start -->
        <div class="col-md-2 col-sm-6 col-xs-5">
            <div class="logo">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_TEMPLATE_PATH . "/include_areas/logo.php",
					"EDIT_TEMPLATE" => ""
				),
				false,
				array("ACTIVE_COMPONENT" => "Y")
			);?>
            </div>
        </div>
        <!--  Logo :: end -->
		
		<div class="col-xs-2 visible-sm visible-xs button-show-menu pull-right">
            <div class="material-menu-button">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

		<!-- Navigation menu :: start -->
		<div class="col-md-7">
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"top",
			Array(
				"ROOT_MENU_TYPE" => "top",
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "",
				"USE_EXT" => "Y",
				"MENU_CACHE_TYPE" => "A",
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => Array()
			)
		);?>
		</div>

        <!-- Header phone :: start -->
        <div class="col-md-3 col-sm-4 col-xs-5">
            <div class="top-phone">
				<div id="top-phone">
					<?
					if ($_REQUEST["ajax_action"] == "Y") {
						$APPLICATION->RestartBuffer();
						$_SESSION["CITY_INFO"] = $_REQUEST["CITY_INFO"];
					}
					if (!empty ($_SESSION["CITY_INFO"]["TELEPHONE"])):?>
						<a href="tel:<?= $_SESSION["CITY_INFO"]["TELEPHONE"]?>" class="phone"><?= $_SESSION["CITY_INFO"]["TELEPHONE"]?></a>
					<?else:?>				
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH . "/include_areas/telephone.php",
							"EDIT_TEMPLATE" => ""
						),
						false,
						array("ACTIVE_COMPONENT" => "Y")
					);?>
					<?
					endif;
					if ($_REQUEST["ajax_action"] == "Y") {
						die();
					}
					?>	
				</div>				
					<?/*$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH . "/include_areas/city.php",
							"EDIT_TEMPLATE" => ""
						),
						false,
						array("HIDE_ICONS" => "Y")
					);*/?>
            </div>
        </div>
        <!-- Header phone :: end -->
    </div>
</div>
</header>

<?if (SITE_DIR == $APPLICATION->GetCurPage(false)):?>
	<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "slider", array(
	"IBLOCK_TYPE" => "content",
	"IBLOCK_ID" => "8",
	"SECTION_ID" => "2",
	"SECTION_CODE" => "",
	"SECTION_USER_FIELDS" => array(
		0 => "UF_IMAGE",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => "id",
	"ELEMENT_SORT_ORDER" => "asc",
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "N",
	"PAGE_ELEMENT_COUNT" => "3",
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"OFFERS_LIMIT" => "5",
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "N",
	"PRICE_CODE" => array(
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "N",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
<?elseif ($APPLICATION->GetCurPage(true) <> "/loans-online.php"):?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "sect", 
			"AREA_FILE_SUFFIX" => "breadcrumb", 
			"AREA_FILE_RECURSIVE" => "Y", 
			"EDIT_MODE" => "html", 
			"EDIT_TEMPLATE" => "" 
		),
		false,
		Array()
	);?>
<?endif?>
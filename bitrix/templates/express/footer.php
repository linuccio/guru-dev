<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!-- Bottom links :: start -->
<div class="bottom-links wow fadeIn">
    <div class="container">
        <div class="block">
            <p>
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"bottom",
				Array(
					"ROOT_MENU_TYPE" => "bottom",
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "",
					"USE_EXT" => "Y",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => Array()
				)
			);?>
            </p>
            <p class="light-grey">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_TEMPLATE_PATH . "/include_areas/description.php",
					"EDIT_TEMPLATE" => ""
				),
				false,
				array("ACTIVE_COMPONENT" => "Y")
			);?>
            </p>
        </div>
    </div>
</div>
<!-- Bottom links :: end -->

<!-- Footer :: start -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-4 col-xs-12">
                <p>
				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_TEMPLATE_PATH . "/include_areas/copyright.php",
						"EDIT_TEMPLATE" => ""
					),
					false,
					array("ACTIVE_COMPONENT" => "Y")
				);?>				
				</p>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-12">
                <ul class="social-links">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_TEMPLATE_PATH . "/include_areas/social.php",
						"EDIT_TEMPLATE" => ""
					),
					false,
					array("ACTIVE_COMPONENT" => "Y")
				);?>
                </ul>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-12">
                <p>
					<nobr>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH . "/include_areas/devepobed_by.php",
							"EDIT_TEMPLATE" => ""
						),
						false,
						array("ACTIVE_COMPONENT" => "Y")
					);?>				
					</nobr>
				</p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer :: end -->

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"modal",
	Array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "regions",
		"IBLOCK_ID" => "1",
		"NEWS_COUNT" => "200",
		"SORT_BY1" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(),
		"PROPERTY_CODE" => array("TELEPHONE"),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	)
);?>
<div style="display: none;">
            <div class="box-modal" id="Modal-zak">
                <div class="box-modal_close arcticmodal-close"><img src="<?= SITE_TEMPLATE_PATH; ?>/img/close.png" alt="" /></div>
                <h3>Оплатить займ</h3>
                <form method="post" action="" id="form1">
                    <div class="name-bg"><input type="text" class='pasp' name="name" placeholder="Паспортные данные" /></div>
                    <div class="phone-bg"><input type="text" class="mask" name="phone" placeholder="Ваш телефон" /></div>
                    <div class="submit"><span><input class="butt" type="submit" value="Оплатить" /></span></div>
                </form>
            </div>
    	</div>
</body>
</html>
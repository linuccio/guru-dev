wow = new WOW({
    offset: 50
});
wow.init();

$(document).ready( function() {
    // Responsive menu
    $('.material-menu nav > ul').materialmenu({
        mobileWidth:991
    });

//    Select city
    $(function () {
        var cityButton = $('.select-city-button'),
            modalSelectCity = $('#select-city');

        modalSelectCity.find('a').on('click', function () {
            cityButton.text($(this).text());
            $('#myModal').modal('hide');
        });
    });
});


// Equal height of columns
function setEqualHeight(columns){
    var tallestcolumn = 0;
    columns.each(function(){
        currentHeight = $(this).height();
        if(currentHeight > tallestcolumn){
            tallestcolumn = currentHeight;
        }
    });
    columns.height(tallestcolumn);
    console.log(tallestcolumn);
}




$(function() {
    setEqualHeight($(".services .row > div"));
    setEqualHeight($(".calc > div"));
    $(window).resize(function(){
        setEqualHeight($(".calc > div"));
    });
});

function GetAjaxContent(domID, url) {
	$("body").addClass("load");
	$.get(url, "", function(data){
		$("#" + domID).html(data);
		$("body").removeClass("load");
	})
}












//function responsivecheck(size){
//    var compare = $(".container").width();
//    if(size != compare){
//        reload();
//    } else {
//        setTimeout(
//            function(){responsivecheck(compare)},
//            5000
//        );
//    }
//}
//// ! executed after rhinoslider initialization !
//function reload() {
//    window.location.href = window.location.href;
//}
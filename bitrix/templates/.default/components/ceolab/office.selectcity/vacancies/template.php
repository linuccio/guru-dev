<div class="address">
    <div>
        <span class="mfo_vacancy_header"><?=GetMessage("vacancies")?>&nbsp;<?=$arResult["CURRENT_CITY"]['NAME']?></span>
        <a href="javascript:onclick()" class="selectCityAddress" onclick="selectRegionShow('select_region_vacancies')"><?=GetMessage("select_city")?></a>
        <div id="select_region_vacancies" class="selectRegion_vacancies_wrapper">
            <div class="selectRegion_vacancies">
                <div align="right">
                    <a href="javascript:onclick()" onclick="selectRegionHide('select_region_vacancies')">[x]</a>
                </div>
                <div class="regionList">
                    <div style="display: inline-block;">
                        <?foreach($arResult['CITY'] as $lether => $citys):?>
                        <div class="selectRegion_vacancies_block">
                            <div align="left" style="font-size: 30px;">
                                <?=$lether?>
                            </div>
                            <?foreach($citys as $cityId => $city):?>
                                <a href="?city=<?=$cityId?>"><?=$city['NAME']?></a><br/>
                            <?endforeach?>
                        </div>
                        <?endforeach?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
$address = current($arResult["ITEMS"]);
$address = $address[0];
if (!isset($_GET['city']) && empty($_GET['city'])) {
    header( "location:/address.php?city=" . $arResult["DATA"]["0"]["CITY"] );
}
?>    <script type="text/javascript">
        ymaps.ready(init);
        var myMap;

        function init(){
			myMap = new ymaps.Map ("map", {
				center: [<?=$arResult["DATA"][0]["COORD"]?>],
				zoom: 15
			});

			myMap.controls.add(
                new ymaps.control.ZoomControl()
);

            <?foreach($arResult['DATA'] as $res):?>
				myMap.geoObjects.add(new ymaps.Placemark([<?=$res['COORD'];?>], {
					content: '',
					balloonContent: ''}, {
					iconLayout: 'default#image',
					iconImageHref: '/bitrix/templates/.default/components/ceolab/office.address/express/images/myicon.gif',
iconImageSize: [30, 33]}));
            <? endforeach ?>
				setObject('<?=$address['COORD']?>', '<?=$address['ADDRESS']?>', '<?=$address['PHONE']?>', '<?=$address['EMAIL']?>', '<?=implode("<br/>", $address['SCHEDULE'])?>');
}

        function setObject(coord, address, phone, email, schedule)
        {
            var coord = coord.split(",");

            $("*[name=address]").text(address);
            $("*[name=phone]").text(phone);
            $("*[name=email]").text(email);
            $("*[name=schedule]").html(schedule.replace(",", "<br/>"));

            if(coord.length == 2)
            {
                myMap.setCenter(coord);
            }
        }


    </script>


<?if($arResult['ITEMS']):?>	
	<div class="adress">
		<div class="row">
		<?foreach($arResult['ITEMS'] as $destinct => $item):?>
			<?foreach($item as $address):
				$this->AddEditAction($address['ID'], $address['EDIT_LINK'], CIBlock::GetArrayByID($address["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($address['ID'], $address['DELETE_LINK'], CIBlock::GetArrayByID($address["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				
				$result = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => "2", "PROPERTY_ADDRESS" => $address["ADDRESS"]), false, false, array());
				$arCity = $result->GetNext();
				?>
				<div id="<?=$this->GetEditAreaId($address['ID']);?>" class="col-md-3 col-sm-6 col-xs-12 adress-item">
					<div class="color-red" style="cursor: pointer;" onclick="setObject('<?=$address['COORD']?>', '<?=$address['ADDRESS']?>', '<?=$address['PHONE']?>', '<?=$address['EMAIL']?>', '<?=implode("<br/>", $address['SCHEDULE'])?>')"><strong><?= $arCity["NAME"]?></strong></div>
					<p><?=$address['ADDRESS']?></p>
					<p>время работы: <?= implode("<br/>", $address['SCHEDULE'])?></p>
				</div>		
			<?endforeach?>		
		<?endforeach?>
		</div>
	</div>
</div>
</div>

<!-- Yandex.Map :: start -->
<div class="container-fluid">
	<div class="row">
		<div id="map" style="height: 450px;"></div>
	</div>
</div>

<!-- Yandex.Map :: end -->	
<?else:?>
    <p align="center">
        <?= GetMessage("no_data")?>
    </p>
<?endif?>
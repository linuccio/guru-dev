/**
 * Created with JetBrains PhpStorm.
 * User: evgenius
 * Date: 12/9/13
 * Time: 6:09 PM
 * To change this template use File | Settings | File Templates.
 */

$(function() {
	$('input, textarea').placeholder();

	// Form anket
	var $orderForm = $( '#form-slider' );

	$orderForm.validationEngine();

	$orderForm.formToWizard({
		submitButton: 'submit-form',
		showProgress: false, //default value for showProgress is also true
		nextBtnName: 'Следующий шаг',
		prevBtnName: 'Предыдущий шаг',
		showStepNo: false,
		validateBeforeNext: function() {
			return $orderForm.validationEngine( 'validate' );
		}
	});
	// remove previos step button
	$('.step-prev').remove();

});




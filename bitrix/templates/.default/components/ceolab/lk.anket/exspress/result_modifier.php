<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 12/10/13
 * Time: 10:48 AM
 * To change this template use File | Settings | File Templates.
 */


$APPLICATION->AddHeadString("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js\"></script>");
$APPLICATION->AddHeadString("<script src=\"/bitrix/components/ceolab/lk.anket/templates/.default/js/calendar.js\"></script>");
$APPLICATION->AddHeadString("<script src=\"/bitrix/components/ceolab/lk.anket/templates/.default/js/validator.js\"></script>");
$APPLICATION->AddHeadString("<link rel=\"stylesheet\" href=\"/bitrix/components/ceolab/lk.anket/templates/.default/css/jquery.kladr.min.css\"/>");
$APPLICATION->AddHeadString("<script src=\"/bitrix/components/ceolab/lk.anket/templates/.default/js/jquery.kladr.min.js\"></script>");
$APPLICATION->AddHeadString("<script src=\"/bitrix/components/ceolab/lk.anket/templates/.default/js/mask.js\"></script>");

//	отправляем письмо
if ($_REQUEST["ajax_submit"] == "Y") {
	foreach ($_REQUEST as $name => $value) {
		$arMailFields[strtoupper($name)] = $value;
	}
	CEvent::Send($arParams["EVENT_TYPE"], 's1', $arMailFields);	
}

if(!function_exists('BITGetDeclNum')) {
    /**
     * Возврат окончания слова при склонении
     *
     * Функция возвращает окончание слова, в зависимости от примененного к ней числа
     * Например: 5 дней, 1 день, 3 дня
     *
     * @param int $value - число, к которому необходимо применить склонение
     * @param array $status - массив возможных окончаний
     * @return mixed
     */
	function BITGetDeclNum($value = 1, $status = array('день', 'дня', 'дней')) {
		$array =array(2,0,1,1,1,2);
		return $status[($value%100 > 4 && $value % 100 < 20) ? 2 : $array[($value%10 < 5) ? $value%10 : 5]];
	}
}
?>


<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 12/9/13
 * Time: 5:11 PM
 * To change this template use File | Settings | File Templates.
 */
?>

<div id="submitForm">
<?if(!isset($arResult['STATUS']) && $_REQUEST["ajax_submit"] != "Y"):?>
    <script>
        $("document").ready(
            function()
            {
			$("#form-slider").submit(function(){
				if ($(this).find(".formError").html()) {
					return false;
				}
				$("body").addClass("load");
				$.post("<?= $APPLICATION->GetCurPage()?>", $(this).serialize(), function (data){
					$("#submitForm").html(data);
					$("body").removeClass("load");
				});
				return false;
			});				
				var kladr1 = $();
                var fields = [
                    {
                        selector: $("input[name=reg_city]"),
                        type: $.kladr.type.city
                    },
                    {
                        selector: $("input[name=reg_street]"),
                        type: $.kladr.type.street
                    },
                    {
                        selector: $("input[name=reg_house]"),
                        type: $.kladr.type.building
                    },
                    {
                        selector: $("input[name=reg_region]"),
                        type: $.kladr.type.region
                    }
                ];

                
                for(var i in fields)
                {
                    kladr1.add(fields[i].selector);
                    fields[i].selector.kladr(
                        {
                            type : fields[i].type
                        }
                    )
                }

                kladr1.kladr(
                    {
                        parentInput: $("#live_address"),
                        verify: true
                    }
                );


                $("input[name=live_post_index]").kladrZip("#live_address");


                var fields = [
                    {
                        selector: $("input[name=live_city]"),
                        type: $.kladr.type.city
                    },
                    {
                        selector: $("input[name=live_street]"),
                        type: $.kladr.type.street
                    },
                    {
                        selector: $("input[name=live_house]"),
                        type: $.kladr.type.building
                    },
                    {
                        selector: $("input[name=region]"),
                        type: $.kladr.type.region
                    }
                ];

                var kladr1 = $();
                for(var i in fields)
                {
                    kladr1.add(fields[i].selector);
                    fields[i].selector.kladr(
                        {
                            type : fields[i].type
                        }
                    )
                }

                kladr1.kladr(
                    {
                        parentInput: $("#live_address"),
                        verify: true
                    }
                );


                $("input[name=live_post_index]").kladrZip("#live_address");



                smsConfirm = new function()
                {
                    this.resend = true;
                    this.isChecked = false;
                    this.showForm = function()
                    {
                        var top = ($("#modalWrap").height() / 2) - ($("#modal").height() / 2) - 50;
                        var left = ($("#modalWrap").width() / 2) - ($("#modal").width() / 2);
                        $("#modal").css(
                            {
                                top: top,
                                left: left
                            }
                        );

                        this.phone = $("#mfoAnketForm input[name=mobile_phone]").val();
                        $("#modal span.phone").text(this.phone);

                        $("#modalWrap").show();

                        this.sendCode();
                    }


                    this.checkCode = function()
                    {
                        var code = $("#modal").find("input.phonecode").val();

                        if(code && this.phone)
                        {
                            $.post(
                                "/bitrix/modules/ceolab.lkmfo/controllers/smsconfirm.php",
                                {
                                    phone : this.phone,
                                    code : code
                                },
                                function(data)
                                {
                                    if(data == "ok")
                                    {
                                        smsConfirm.closeForm();
                                        smsConfirm.isChecked = true;
                                        $("#mfoAnketForm").submit();
                                    }
                                    else if(data == "fail")
                                    {
                                        smsConfirm.showError("Неверный код");
                                    }
                                }
                            )
                        }

                    }

                    this.sendCode = function()
                    {
                        if(this.phone && this.resend)
                        {
                            $.post(
                                "/bitrix/modules/ceolab.lkmfo/controllers/smsconfirm.php",
                                {
                                    phone : this.phone
                                },
                                function(data)
                                {
                                    if(data != "ok")
                                    {
                                        //smsConfirm.showError("Отправка не удалась");
                                    }
                                }
                            )
                            this.resend = false;
                            this.timerResendStart();
                        }
                    }

                    this.timerResendStart = function()
                    {
                        $("#resendsms").addClass("disabledLink");


                        sendSmsTime = new Date();
                        sendSmsTime = sendSmsTime.getTime();
                        sendSmsmInterval = setInterval(
                            function()
                            {
                                var time = new Date();
                                time = 60 - parseInt((time.getTime() - sendSmsTime) / 1000);

                                if(time <= 0)
                                {
                                    clearInterval(sendSmsmInterval);
                                    $("#resendsms").removeClass("disabledLink");
                                    $("#resendsms").text("Отправить код повторно");
                                    smsConfirm.resend = true;
                                }
                                else
                                {
                                    $("#resendsms").text("Повторная отправка через ("+time+")");
                                }
                            },
                            1000
                        );
                    }

                    this.closeForm = function()
                    {
                        $("#modalWrap").hide();
                    }

                    this.showError = function(text)
                    {
                        $("#modal .error").text(text);
                        $("#modal .error").show();
                    }

                    this.hideError = function()
                    {
                        $("#modal .error").hide();
                    }



                    $("#modal").find("input.btn").bind(
                        "click",
                        this,
                        function(event)
                        {
                            event.data.checkCode();
                            event.data.hideError();
                        }
                    );

                    $("#resendsms").bind(
                        "click",
                        this,
                        function(event)
                        {
                            event.data.sendCode();
                        }
                    )


                    $("#modal").find("input.phonecode").bind(
                        "keyup",
                        this,
                        function(event)
                        {
                            if($(this).val().length == $(this).attr("maxlength"))
                            {
                                $("#modal").find("input.btn").removeAttr("disabled");
                                event.data.hideError();
                            }
                            else
                            {
                                $("#modal").find("input.btn").attr("disabled", "disabled");
                            }
                        }
                    );
                    $("#modal").find("input.phonecode").keyup();
                }
            }
        );
    </script>
	
<form class="anket" align="center" method="POST" id="form-slider" enctype="multipart/form-data">
    <input type="hidden" name="anket" value="<?=$arResult['anketID']?>"/>
    <input type="hidden" name="product" value="<?=$arResult['product']?>"/>
    <input type="hidden" name="summ" value="<?=$arResult['summ']?>"/>
    <input type="hidden" name="term" value="<?=$arResult['term']?>"/>
    <input type="hidden" name="ajax_submit" value="Y"/>
	<!--  Step 1 :: start -->
	<fieldset class="form-step col-md-8 col-sm-10 col-xs-12">
		<div class="row">
			<div class="form-step-title col-xs-12">
				<h3>Общая информация</h3>
				<div class="step">Шаг <span class="current-step">1</span> из <span class="count-step">4</span></div>
				<div class="id-step"></div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<input name="lastname" id="lastname" type="text" class="input-text validate[required]" required="" placeholder="Фамилия *" tabindex="1">
				<input name="patronymic" id="patronymic" type="text" class="input-text validate[required]" placeholder="Отчество *" tabindex="3">
				<input name="email" id="email" type="email" class="input-text validate[custom[email],required]" placeholder="Email" tabindex="5">
			</div>
			<div class="col-sm-6 col-xs-12">
				<input name="firstname" id="firstname" type="text" class="input-text validate[required]" placeholder="Имя *" tabindex="2">
				<input name="date_birth" id="date_birth" type="date" class="input-text calendar validate[required]" placeholder="Дата рождения *" tabindex="4">
				<input name="phone" id="phone" type="text" class="input-text validate[required]" placeholder="Номер телефона *" tabindex="6">
			</div>

			<div class="row">
				<div class="col-xs-12">
					<span class="next-step"></span>
				</div>
			</div>
		</div>
	</fieldset>
	<!--  Step 1 :: end -->

	<!--  Step 2 :: start -->
	<fieldset class="form-step col-md-8 col-sm-10 col-xs-12">
		<div class="row">
			<div class="form-step-title col-xs-12">
				<h3>Паспортные данные</h3>
				<div class="step">Шаг <span class="current-step">2</span> из <span class="count-step">4</span></div>
				<div class="id-step"></div>
			</div>
			<div class="col-sm-6 col-xs-12 columns-two">
				<input name="passport_serial" type="text" class="input-text inp-first validate[required]" placeholder="Серия *">
				<input name="passport_number" type="text" class="input-text inp-second validate[required]" placeholder="Номер паспорта *">
			</div>
			<div class="col-sm-6 col-xs-12">
				<input name="passport_date" type="date" class="input-text calendar validate[required]" placeholder="Дата выдачи паспорта *">
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<input name="passport_issued" type="text" class="input-text validate[required]" placeholder="Кем выдан паспорт *">
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<input name="address_register" type="text" class="input-text validate[required]" placeholder="Адрес регистрации *">
			</div>
			<div class="col-sm-6 col-xs-12">
				<input name="address_home" type="text" class="input-text validate[required]" placeholder="Адрес фактического проживания *">
			</div>
		</div>
	</fieldset>
	<!-- Step 2 :: end -->

	<!--  Step 3 :: start -->
	<fieldset class="form-step col-md-8 col-sm-10 col-xs-12">
		<div class="row">
			<div class="form-step-title col-xs-12">
				<h3>Информация о трудоустройстве</h3>
				<div class="step">Шаг <span class="current-step">3</span> из <span class="count-step">4</span></div>
				<div class="id-step"></div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<select name="employment" class="select">
					<option value="employment_0" selected="" disabled="">Трудоустройство</option>
					<option value="Есть работа">Есть работа</option>
					<option value="Безработный">Безработный</option>
				</select>
				<input name="firm_address" type="text" class="input-text" placeholder="Факт. адрес организации">
				<input name="firm_position" type="text" class="input-text" placeholder="Должность">
			</div>
			<div class="col-sm-6 col-xs-12">
			   <input name="firm_name" type="text" class="input-text" placeholder="Название организации">
			   <input name="firm_phone" type="text" class="input-text" placeholder="Рабочий телефон">
			   <input name="average_income" type="text" class="input-text" placeholder="Среднемесячный доход">
			</div>
		</div>
	</fieldset>
	<!--  Step 3 :: end -->

	<!--  Step 4 :: start -->
	<fieldset class="form-step col-md-8 col-sm-10 col-xs-12">
		<div class="row">
			<div class="form-step-title col-xs-12">
				<h3>Информация о займе</h3>
				<div class="step">Шаг <span class="current-step">4</span> из <span class="count-step">4</span></div>
				<div class="id-step"></div>
			</div>
			<div class="col-xs-12">
				<div class="pair loan-type">
					<div class="label">Вид займа:</div>
					<input name="type_loan" type="text" class="input-text" value="<?= $arResult['product']?>" style="width: 80%;">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<div class="pair loan-summ">
					<div class="label">Сумма займа:</div>
					<input name="loan_summ" type="text" class="input-text validate[required]" placeholder="8 000р." value="<?= $arResult["summ"]?>">
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="pair loan-time">
					<div class="label">Срок займа:</div>
					<?
					if ((int)$arResult["term"] > 0) {
						$arResult["term"] = $arResult["term"] . " " . BITGetDeclNum($arResult["term"]);
					}
						
					?>
					<input name="loan_time" type="text" class="input-text validate[required]" placeholder="4 дня" value="<?= $arResult["term"]?>">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
			   <input type="checkbox" class="input-checkbox" name="agree" id="agree" value="Да">
			   <label for="agree"><span><i></i></span> Согласен с SMS-рассылкой</label>
			</div>
		</div>

		<div style="margin-bottom: 60px;"></div>

		<div class="row">
			<div class="col-xs-12">
				<input name="submit" class="button solid red submit-form" type="submit" value="Оформить займ">
			</div>
		</div>
	</fieldset>
	<!--  Step 1 :: start -->

</form>
<?else:?>
<?
if ($_REQUEST["ajax_submit"] == "Y")
	$APPLICATION->RestartBuffer();
?>
<h2>Ваша заявка успешно отправлена</h2>
<?
if ($_REQUEST["ajax_submit"] == "Y")
	die();
?>
<?endif?>
</div>
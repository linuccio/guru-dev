/**
 * Created with JetBrains WebStorm.
 * User: evgenius
 * Date: 12/1/13
 * Time: 7:17 PM
 * To change this template use File | Settings | File Templates.
 */

function vg_slider()
{
    this.init = function(selector, data, fn)
    {
        this.object = $(selector);
        this.move = false;

        this.object.attr("style", "display: none");
        if(!this.baseBlock)
            this.baseBlock = $("<div class=\"vg_slider\" align=\"left\"></div>").insertAfter(this.object);
        if(!this.backgroundBlock)
            this.backgroundBlock = $("<div class=\"vg_background\"></div>").appendTo(this.baseBlock);
        if(!this.pointerBlock)
            this.pointerBlock = $("<div class=\"vg_pointer\"></div>").appendTo(this.baseBlock);

        this.pointerBlock_Width = this.pointerBlock.width();
        this.baseBlock_Width = this.baseBlock.width();
        this.baseBlock_posLeft = this.baseBlock.offset().left;

        this.data = data;
        this.data.max = (Math.ceil((this.data.max-this.data.min)/this.data.step)*this.data.step)+this.data.min;

        this.fn = fn;
        this.field = $(data.field);

        this.roundingType = {
            ceil : Math.ceil,
            floor: Math.floor,
            round: Math.round
        }

        if(data.rounding && this.roundingType[data.rounding])
            this.rounding = this.roundingType[data.rounding];
        else
            this.rounding = this.roundingType.round;


        this.multiplier = (this.data.max-this.data.min)/(this.baseBlock_Width-this.pointerBlock_Width);

	if(!this.multiplier)
		this.multiplier = 0;

        if(data.type == "soft")
            this.data.type = data.type;
        else
            this.data.type = "hard";



        this.step = data.step / this.multiplier;

        this.data.min = data.min;

        var b = $(window).bind(
            "touchmove mousemove",
            this,
            function(event) {
                if (event.type == "touchmove") {
                    var touch = event.originalEvent.touches[0] || event.originalEvent.changedTouches[0];
                    var x = (touch.clientX - event.data.baseBlock_posLeft);
                }
                else {
                    var x = event.clientX - event.data.baseBlock_posLeft;
                }

                if (event.data.move)
                {
                    event.data.pointerPos(x);
                }



            }
        );

        this.baseBlock.unbind("touchstart mousedown");
        this.baseBlock.bind(
            "touchstart mousedown",
            this,
            function(event)
            {
                $("*").onselectstart = function() {  return false };
                $("*").css({
                    '-moz-user-select': 'none'
                    ,'-o-user-select': 'none'
                    ,'-khtml-user-select': 'none'
                    ,'-webkit-user-select': 'none'
                    ,'-ms-user-select': 'none'
                    ,'user-select': 'none'
                });


                if(event.type == "touchstart")
                {
                    var touch = event.originalEvent.touches[0] || event.originalEvent.changedTouches[0];
                    var x = (touch.clientX - event.data.baseBlock_posLeft);
                }
                else
                {
                    var x = event.clientX - event.data.baseBlock_posLeft;
                }
                event.data.move = true;

                event.data.pointerPos(x);

                if(event.data.typeFunc.down.func)
                {
                    event.data.typeFunc.down.func(event.data, event.data.typeFunc.down.data);
                }

            }
        )

        if(!this.mouseupBind)
        {
            $(window).bind(
                "mouseup touchend",
                this,
                function(event)
                {
                    $("*").onselectstart = function() {  return true };
                    $("*").css({
                        '-moz-user-select': ''
                        ,'-o-user-select': ''
                        ,'-khtml-user-select': ''
                        ,'-webkit-user-select': ''
                        ,'-ms-user-select': ''
                        ,'user-select': ''
                    });


                    event.data.move = false;
                    event.data.stepFix();

                    if(event.data.typeFunc.up.func)
                    {
                        event.data.typeFunc.up.func(event.data, event.data.typeFunc.up.data);
                    }
                }
            )
            this.mouseupBind = true;
        }


        this.field.bind(
            "change",
            this,
            function(event)
            {
                event.data.setValue($(this).val());
            }
        )

        ///this.setValue(100);
    }

    this.pointerPos = function(x, c)
    {
        if(x >= (this.baseBlock_Width-(this.pointerBlock_Width/2)))
            x = this.baseBlock_Width-this.pointerBlock_Width;
        else if(!c)
            x = x-(this.pointerBlock_Width/2);

        if(x < 0)
        {
            x = 0;
        }

        if(this.data.type == "hard")
            x = this.rounding(x/this.step)*this.step;

        this.pointerBlock.css("left", x);
        this.backgroundBlock.css("width", x);
        this.calculate(x);
    }

    this.stepFix = function()
    {
        var x = this.rounding(this.pointerBlock.position().left/this.step)*this.step;

        if(x > this.baseBlock_Width)
            x = this.baseBlock_Width;

        var speed = 200;
        this.pointerBlock.animate(
            {
                left:x
            },
            speed,
            function()
            {

            }
        )
        this.backgroundBlock.animate(
            {
                width:x
            },
            speed
        )

        this.calculate(x);
    }



    this.calculate = function(x)
    {
	if(!x)
		x=0;

        var val = (x*this.multiplier)+this.data.min;

	
        if(!this.data.certain)
        {
            val = parseInt(val);

        }
        else
        {
            val = val.toFixed(this.data.certain)*1;
            if(val == parseInt(val))
                val = parseInt(val);
        }

        if(this.typeFunc.slide.func)
        {
            val = this.typeFunc.slide.func(val, this.typeFunc.slide.data);
        }

        this.object.val(val);
        this.field.val(val);

    }

    this.bind = function(type, data, func)
    {
        this.typeFunc[type].func = func;
        this.typeFunc[type].data = data;
    }

    this.typeFunc = {
        slide: {
            func: false,
            data: {}
        },
        down:{
            func: false,
            data: {}
        },
        up:{
            func: false,
            data: {}
        },
        setval:{
            func: false,
            data: {}
        }
    }


    this.setValue = function(val)
    {
        if(this.typeFunc.setval.func)
        {
            val = this.typeFunc.setval.func(val, this.typeFunc.setval.data);
        }

        this.pointerPos((val/this.multiplier) - (this.data.min/this.multiplier), true);
        this.stepFix();
    }




}



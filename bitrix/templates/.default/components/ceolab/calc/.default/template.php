<script>
    $("document").ready(
        function()
        {
            var data = eval('('+$('#calcJsonData').val()+')');
            var products = [];
            for(var i in data.calcs)
            {
                var calc = data.calcs[i].calc;

                products.push({
                    name: calc.name,
                    text: calc.additionInfo,
					slider_image: calc.slider_image,
                    percent : calc.percent,
                    type : calc.type_percent,
                    rate_table: calc.rate_table,
                    type_calc: calc.type_calc,
                    count_pay: calc['count_pay'] ? calc['count_pay'] : null,
                    summ: {
                        min : calc.min_summ,
                        max : calc.max_summ,
                        step: calc.step
                    },
                    term : {
                        min : calc.min_term,
                        max : calc.max_term,
                        step: calc.step_term
                    },
                    anket:59
                });


            }

            calc = new main_calc(products);
            calc.setActive(0);
        }
    )
	
</script>
<form class="calc_mfo_" id="ceolab_calcmfo" method="post" action="/anketa/">
	<section class="slideshow slideshow-about">
		<div class="container title">
			<div class="row">
				<div class="col-sm-7 col-xs-12">
					<h1 class="wow fadeInDown"><strong name="item_name"></strong></h1>
					<p name="loan_description" style="text-shadow: 0 0 50px white"></p>
				</div>
			</div>
		</div>
	</section>
	
	<section class="wrap">
		<main class="calculator-loans">
			
				<div id="tab-container" class="tab-container tab-calc">

				<input id="calcJsonData" type="hidden" value="<?=$arParams['PRODUCTS']?>"/>
				<input type="hidden" name="anket"/>
				<input type="hidden" name="product"/>
				<input type="hidden" name="type">
				<input type="hidden" name="currency">
				<input name="fieldSumm" id="fieldSumm" type="hidden"/>
				<input name="fieldTerm" id="fieldTerm" type="hidden"/>

				<div class="container">
					<ul class="etabs clearfix" name="loan_switcher">

					</ul>
				</div>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="loan-express">
						<div class="container">
							<div class="calc" id="calc-loan-express">
								<div class="calc-left col-md-8">

								
									<div class="slider calc-row clearfix">
										<h5><strong>На какую сумму хотите оформить займ?</strong></h5>
										<div class="scale_numbers">
											<div class="first" name="scale_min_summ">
												
											</div>
											<div class="last" name="scale_max_summ">
												
											</div>
										</div>
										<div class="scale" name="scale_summ">
										</div>
										<input type="slider" name="slider_summ"/>
										<div class="calc-value"><span name="toResultSumm"></span> &#8381;</div>
									</div>

									<div class="slider calc-row clearfix">
										<h5><strong>На сколько дней</strong></h5>
										<div class="scale_numbers">
											<div class="first" name="scale_min_term">
												
											</div>
											<div class="last" name="scale_max_term">
												
											</div>
										</div>
										<div class="scale" name="scale_term">

										</div>
										<input type="slider" name="slider_term"/>
										<div class="calc-value"><span name="toResultDate"></span> дн.</div>
									</div>
								</div>

								<div class="calc-right col-md-4">
									<div class="calc-row">
										<div class="calc-cell">Плата за займ:</div>
										<div class="calc-cell"><b><span name="totalPay"></span> &#8381;</b></div>
									</div>

									<div class="calc-row">
										<div class="calc-cell">Итого к оплате:</div>
										<div class="calc-cell"><b><span name="toPay"></span> &#8381;</b></div>
									</div>

									<div class="calc-row">
										<div class="calc-cell">Погасить включительно до:</div>
										<div class="calc-cell"><b><span name="toDate"></span></b></div>
									</div>

									<div class="calc-row">
										<span><input type="submit" href="/anketa/" value="Получить займ" class="button solid red" /></span>
										<br>
										<span>или <a href="#">связаться с менеджером</a> <br> для уточнения деталей</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?$APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"loans",
			Array(
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "N",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "content",
				"IBLOCK_ID" => "11",
				"NEWS_COUNT" => "200",
				"SORT_BY1" => "ID",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => array("DETAIL_TEXT"),
				"PROPERTY_CODE" => array("MIN_PRICE", "MAX_PRICE"),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => "3600",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N"
			)
		);?>
		</main>
	</section>
</form>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
	<div class="material-menu">
		<nav>
			<ul>
				<?foreach($arResult as $arItem): ?>
				<li><a class="<?= $arItem["SELECTED"] ? "selected" : ""?>" href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a></li>
				<?endforeach ?>
			</ul>
		</nav>
	</div>
<!-- Navigation menu :: end -->
<?endif?>
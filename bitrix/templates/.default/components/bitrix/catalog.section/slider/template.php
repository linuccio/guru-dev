<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><!-- Slideshow && services :: start -->
<section id="slideshow-home" class="slideshow">

    <!-- Title page :: start-->
    <div class="container title">
        <div class="row">
            <div class="col-sm-7 col-xs-12">
                <h1 class="wow fadeInDown" style="margin-bottom:150px;"><?= $arResult["~DESCRIPTION"]?></h1>
                <!-- <a href="/loans-online.php" class="button wow fadeInUp" data-wow-delay="0.2s"><?= GetMessage("SLIDER_BUTTON")?></a> -->
            </div>
        </div>
    </div>
    <!-- Title page :: end-->

<div class="services wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-10 center-block fut_auto">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="services-block">
                                <h2 class="title"><a href="/loans-online.php">Оформить займ</a></h2>
                                <p>Заполнить анкету на займ и получить займ в ближайшем офисе организации</p>
                                <a href="/loans-online.php" class="button">Далее <span class="arrow"></span></a>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="services-block">
                                <h2 class="title"><a href="#">Погасить займ</a></h2>
                                <p>Погасить ранее оформленный займ не выходя из дома или в любом офисе организации</p>
                                <a href="#more" class="button">Далее <span class="arrow"></span></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="steps wow fadeIn">
        <div class="container">
            <div class="row">
				<?foreach($arResult["ITEMS"] as $cell => $arElement):?>
				<?
				$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
				?>
                <div id="<?=$this->GetEditAreaId($arElement['ID']);?>" class="col-sm-4 step">
                    <div class="services-block">
						<?if (!empty ($arElement["PREVIEW_PICTURE"]["SRC"])):?>
							<img src="<?= $arElement["PREVIEW_PICTURE"]["SRC"]?>" alt="<?= $arElement["NAME"]?>">
						<?endif?>
                        <div class="title"><?= $arElement["NAME"]?></div>
                        <p><?= $arElement["PREVIEW_TEXT"]?></p>
                    </div>
                </div>
				<?endforeach?>
            </div>
        </div>
    </div>
</section>
<!-- Slideshow && services :: end -->

<?if (count ($arResult["UF_IMAGE"]) > 0):?>
<script>
    // Slideshow at home page
    $("#slideshow-home").backgroundCycle({
        imageUrls: [
			<?foreach ($arResult["UF_IMAGE"] as $key => $imageID):?>
            '<?= CFile::GetPath($imageID)?>'<?= (($key + 1) != count($arResult["UF_IMAGE"])) ? "," : "";?>
			<?endforeach?>
        ],
        //fadeSpeed: 1000,
        duration: 5000,
        backgroundSize: SCALING_MODE_COVER
    });
</script>
<?endif?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="actions-list news-list">
      <div class="row"> 
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

<div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="col-sm-4 news-item"> 
    <div class="element relative">
        <div class="img-wrapper relative">
<div class="img-wrapper relative"> <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img class="radius" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="323" height="197"  /></a></div>
<br>
<div> <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" ><?echo $arItem["NAME"]?></a> </div>
<br>
			<div class="text gray"><?echo $arItem["PREVIEW_TEXT"];?></div>
</div>
 </div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
</div>


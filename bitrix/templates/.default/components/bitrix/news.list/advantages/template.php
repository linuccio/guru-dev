<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="advantages clearfix">
	<div class="container">
		<h5 class="add-color text-center"><strong><?= $arResult["NAME"]?></strong></h5>
		<br>
		<div class="row">
			<?foreach($arResult["ITEMS"] as $arItem):
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="col-sm-4" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="item">
				<?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
					<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?= $arItem["NAME"]?>">
				<?endif?>
					<strong><?= $arItem["NAME"]?></strong>
					<small><?= $arItem["PREVIEW_TEXT"]?></small>
				</div>
			</div>
			<?endforeach?>
		</div>
	</div>
</div>
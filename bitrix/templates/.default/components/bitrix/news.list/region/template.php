<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="region">
	<span class="label">Ваш регион:</span>

	<form action="" id="region-selection">
		<select name="region" id="region" class="region-selection" onchange="GetAjaxContent('top-phone', '/?ajax_action=Y&' + $(this).val());<?if ($APPLICATION->GetCurPage(true) == "/address.php"):?>window.location.href='/address.php'<?endif?>">
			<?
			foreach($arResult["ITEMS"] as $arItem):
				$arCity["CITY_INFO"]["NAME"] = $arItem["NAME"];
				$arCity["CITY_INFO"]["TELEPHONE"] = $arItem["PROPERTIES"]["TELEPHONE"]["VALUE"];		
				$arCity["CITY_INFO"]["ID"] = $arItem["ID"];		
			?>
			<option 
				value="<?= http_build_query($arCity)?>"
				data-id="<?= $arItem["ID"]?>"
				<?= $_SESSION["CITY_INFO"]["NAME"] == $arItem["NAME"] ? "selected" : "";?>
				><?= $arItem["NAME"]?></option>
			<?endforeach?>
		</select>
	</form>
</div>

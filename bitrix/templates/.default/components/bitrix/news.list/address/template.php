<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="row">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="col-md-3 col-sm-6 col-xs-12 adress-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="color-red"><strong><?= $arItem["NAME"]?></strong></div>
		<?foreach ($arItem["DISPLAY_PROPERTIES"] as $property):?>
		<p><?= $property["VALUE"]?></p>
		<?endforeach?>
	</div>
<?endforeach?>
</div>


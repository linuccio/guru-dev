<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><div class="tariffs">
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
	<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
<div class="tariffs-row wow fadeInUp">
	<div class="container">
		<div class="row">
			<div id="<?= $this->GetEditAreaId($arItem['ID']);?>" class="row wow fadeInUp">
				<?if (($key + 1) % 2 != 0):?>
				<div class="col-sm-6 col-xs-12">
					<div class="tariff-img">
						<?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
							<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?= $arItem["NAME"]?>">
						<?endif?>
					</div>	
				</div>
				<?endif?>
				<div class="col-sm-6 col-xs-12">
					<h3>«<?= $arItem["NAME"]?>»</h3>
					<p><?= $arItem["PREVIEW_TEXT"]?></p>
					<p><a href="javascript:void(0)" onclick="SelectLoan('<?= $arItem["NAME"]?>')" class="button"><?= GetMessage("DETAIL")?></a></p>
				</div>
				<?if (($key + 1) % 2 == 0):?>
				<div class="col-sm-6 col-xs-12">
					<div class="tariff-img">
						<?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
							<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?= $arItem["NAME"]?>">
						<?endif?>
					</div>	
				</div>
				<?endif?>					
			</div>
		</div>
	</div>				
</div>
<? endforeach ?>
</div>
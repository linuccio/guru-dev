<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!-- Partners :: start -->
<section class="partners wow fadeIn"> 
	<div class="container"> 
		<div class="head-title"><?= $arResult["NAME"]?></div>

		<ul class="partners-list">
		<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>	
		<li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
			<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"]?>" />
			<?endif?>
		</li>
		<? endforeach ?>
		</ul>
	</div>
</section> 
<!-- Partners :: end -->
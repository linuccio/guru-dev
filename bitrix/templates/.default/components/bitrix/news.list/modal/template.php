<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= GetMessage("SELECT_CITY")?></h4>
            </div>
            <div class="modal-body">
                <ul id="select-city">
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
						$arCity["CITY_INFO"]["NAME"] = $arItem["NAME"];
						$arCity["CITY_INFO"]["TELEPHONE"] = $arItem["PROPERTIES"]["TELEPHONE"]["VALUE"];
						$arCity["CITY_INFO"]["ID"] = $arItem["ID"];
					?>
                    <li>
						<a 
							href="#" 
							onclick="GetAjaxContent('top-phone', '/?ajax_action=Y&<?= http_build_query($arCity)?>');<?if ($APPLICATION->GetCurPage(true) == "/address.php") echo "window.location.href='/address.php?city=" . $arItem["ID"] . "';"?>" 
							id="<?=$this->GetEditAreaId($arItem['ID']);?>"
							>
							<?= $arItem['NAME']?>
						</a>
					</li>
					<?endforeach?>
                </ul>
            </div>
        </div>
    </div>
</div>

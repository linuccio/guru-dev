

-- --------------------------------------------------------
-- 
-- Table structure for table `b_admin_notify`
-- 




DROP TABLE IF EXISTS `b_admin_notify`;
CREATE TABLE `b_admin_notify` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `ENABLE_CLOSE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IX_AD_TAG` (`TAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_admin_notify_lang`
-- 




DROP TABLE IF EXISTS `b_admin_notify_lang`;
CREATE TABLE `b_admin_notify_lang` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `NOTIFY_ID` int(18) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_ADM_NTFY_LANG` (`NOTIFY_ID`,`LID`),
  KEY `IX_ADM_NTFY_LID` (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_agent`
-- 




DROP TABLE IF EXISTS `b_agent`;
CREATE TABLE `b_agent` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(18) NOT NULL DEFAULT '100',
  `NAME` text COLLATE utf8_unicode_ci,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `LAST_EXEC` datetime DEFAULT NULL,
  `NEXT_EXEC` datetime NOT NULL,
  `DATE_CHECK` datetime DEFAULT NULL,
  `AGENT_INTERVAL` int(18) DEFAULT '86400',
  `IS_PERIOD` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `USER_ID` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_act_next_exec` (`ACTIVE`,`NEXT_EXEC`),
  KEY `ix_agent_user_id` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_agent`
-- 


INSERT INTO `b_agent` VALUES (1,'main',100,'CEvent::CleanUpAgent();','Y','2015-10-19 10:29:25','2015-10-20 00:00:00',NULL,86400,'Y',NULL);
INSERT INTO `b_agent` VALUES (2,'main',100,'CUser::CleanUpHitAuthAgent();','Y','2015-10-19 10:29:25','2015-10-20 00:00:00',NULL,86400,'Y',NULL);
INSERT INTO `b_agent` VALUES (3,'main',100,'CCaptchaAgent::DeleteOldCaptcha(3600);','Y','2015-10-19 21:00:48','2015-10-19 22:00:48',NULL,3600,'N',NULL);
INSERT INTO `b_agent` VALUES (4,'main',100,'CUndo::CleanUpOld();','Y','2015-10-19 10:29:25','2015-10-20 00:00:00',NULL,86400,'Y',NULL);
INSERT INTO `b_agent` VALUES (5,'search',10,'CSearchSuggest::CleanUpAgent();','Y','2015-10-19 21:00:48','2015-10-20 21:00:48',NULL,86400,'N',NULL);
INSERT INTO `b_agent` VALUES (6,'search',10,'CSearchStatistic::CleanUpAgent();','Y','2015-10-19 21:00:48','2015-10-20 21:00:48',NULL,86400,'N',NULL);
INSERT INTO `b_agent` VALUES (9,'main',100,'CEventLog::CleanUpAgent();','Y','2015-10-19 21:00:48','2015-10-20 21:00:48',NULL,86400,'N',NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_bitrixcloud_option`
-- 




DROP TABLE IF EXISTS `b_bitrixcloud_option`;
CREATE TABLE `b_bitrixcloud_option` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL,
  `PARAM_KEY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PARAM_VALUE` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_bitrixcloud_option_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=12121 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_bitrixcloud_option`
-- 


INSERT INTO `b_bitrixcloud_option` VALUES (12118,'backup_quota',0,'0','0');
INSERT INTO `b_bitrixcloud_option` VALUES (12119,'backup_total_size',0,'0','0');
INSERT INTO `b_bitrixcloud_option` VALUES (12120,'backup_last_backup_time',0,'0','0');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_cache_tag`
-- 




DROP TABLE IF EXISTS `b_cache_tag`;
CREATE TABLE `b_cache_tag` (
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CACHE_SALT` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RELATIVE_PATH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAG` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `ix_b_cache_tag_0` (`SITE_ID`,`CACHE_SALT`,`RELATIVE_PATH`(50)),
  KEY `ix_b_cache_tag_1` (`TAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_cache_tag`
-- 


INSERT INTO `b_cache_tag` VALUES (NULL,NULL,'0:1445143575','**');
INSERT INTO `b_cache_tag` VALUES ('s1','/872','/s1/bitrix/news.list/872','iblock_id_3');
INSERT INTO `b_cache_tag` VALUES ('s1','/e37','/s1/bitrix/news.detail/e37','iblock_id_3');
INSERT INTO `b_cache_tag` VALUES ('s1','/693','/s1/ceolab/vacancies.detail/693','iblock_id_5');
INSERT INTO `b_cache_tag` VALUES ('s1','/693','/s1/ceolab/vacancies.list/693','iblock_id_5');
INSERT INTO `b_cache_tag` VALUES ('s1','/bf6','/s1/bitrix/catalog.section/bf6','iblock_id_8');
INSERT INTO `b_cache_tag` VALUES ('s1','/e25','/s1/bitrix/catalog.section/06f','iblock_id_8');
INSERT INTO `b_cache_tag` VALUES ('s1','/3e3','/s1/bitrix/news.list/345','iblock_id_1');
INSERT INTO `b_cache_tag` VALUES ('s1','/315','/s1/bitrix/news.list/345','iblock_id_1');
INSERT INTO `b_cache_tag` VALUES ('s1','/e25','/s1/bitrix/news.list/345','iblock_id_1');
INSERT INTO `b_cache_tag` VALUES ('s1','/e64','/s1/bitrix/news.list/345','iblock_id_1');
INSERT INTO `b_cache_tag` VALUES ('s1','/e25','/s1/bitrix/news.list/e25','iblock_id_9');
INSERT INTO `b_cache_tag` VALUES ('s1','/fb5','/s1/bitrix/news.list/345','iblock_id_1');
INSERT INTO `b_cache_tag` VALUES ('s1','/fb5','/s1/bitrix/news.list/fb5','iblock_id_12');
INSERT INTO `b_cache_tag` VALUES ('s1','/bf6','/s1/bitrix/news.list/345','iblock_id_1');
INSERT INTO `b_cache_tag` VALUES ('s1','/d99','/s1/bitrix/news.list/345','iblock_id_1');
INSERT INTO `b_cache_tag` VALUES ('s1','/cf5','/s1/bitrix/news.list/345','iblock_id_1');
INSERT INTO `b_cache_tag` VALUES ('s1','/315','/s1/bitrix/news.list/315','iblock_id_1');
INSERT INTO `b_cache_tag` VALUES ('s1','/cf5','/s1/bitrix/news.list/cf5','iblock_id_1');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_captcha`
-- 




DROP TABLE IF EXISTS `b_captcha`;
CREATE TABLE `b_captcha` (
  `ID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `CODE` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IP` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  UNIQUE KEY `UX_B_CAPTCHA` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_ceolab_calcmfo_anket`
-- 




DROP TABLE IF EXISTS `b_ceolab_calcmfo_anket`;
CREATE TABLE `b_ceolab_calcmfo_anket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iblockType` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iblock` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_ceolab_calcmfo_anket`
-- 


INSERT INTO `b_ceolab_calcmfo_anket` VALUES (59,'first','requests','7');
INSERT INTO `b_ceolab_calcmfo_anket` VALUES (60,'first','news','3');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_ceolab_calcmfo_anket_forms`
-- 




DROP TABLE IF EXISTS `b_ceolab_calcmfo_anket_forms`;
CREATE TABLE `b_ceolab_calcmfo_anket_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anket` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `iblockType` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iblock` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_ceolab_calcmfo_anket_forms`
-- 


INSERT INTO `b_ceolab_calcmfo_anket_forms` VALUES (58,59,'Адрес прописки',2,NULL,NULL);
INSERT INTO `b_ceolab_calcmfo_anket_forms` VALUES (59,59,'Личная информация',1,NULL,NULL);
INSERT INTO `b_ceolab_calcmfo_anket_forms` VALUES (60,59,'Адрес фактического проживания',3,NULL,NULL);
INSERT INTO `b_ceolab_calcmfo_anket_forms` VALUES (61,59,'Паспортные данные',4,NULL,NULL);
INSERT INTO `b_ceolab_calcmfo_anket_forms` VALUES (62,59,'Контактные данные',5,NULL,NULL);
INSERT INTO `b_ceolab_calcmfo_anket_forms` VALUES (63,59,'Работа',6,NULL,NULL);
INSERT INTO `b_ceolab_calcmfo_anket_forms` VALUES (64,59,'Документы',7,NULL,NULL);
INSERT INTO `b_ceolab_calcmfo_anket_forms` VALUES (65,60,'First form',1,NULL,NULL);
INSERT INTO `b_ceolab_calcmfo_anket_forms` VALUES (66,59,'Доп. информация',8,NULL,NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_ceolab_calcmfo_anket_forms_fields`
-- 




DROP TABLE IF EXISTS `b_ceolab_calcmfo_anket_forms_fields`;
CREATE TABLE `b_ceolab_calcmfo_anket_forms_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form` int(11) DEFAULT NULL,
  `type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `attr` text CHARACTER SET utf8,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `require` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_ceolab_calcmfo_anket_forms_fields`
-- 


INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (106,58,'input','Город',NULL,'reg_city',1,1);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (107,59,'input','Имя','a:1:{s:10:\"iblockProp\";s:9:\"FIRSTNAME\";}','firstname',1,1);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (108,59,'input','Фамилия','a:1:{s:10:\"iblockProp\";s:8:\"LASTNAME\";}','lastname',1,2);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (112,59,'input','Отчество','a:1:{s:10:\"iblockProp\";s:10:\"MIDDLENAME\";}','middlename',1,3);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (113,59,'date','Дата рождения',NULL,'birth_date',1,4);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (114,58,'input','Улица',NULL,'reg_street',1,2);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (115,58,'input','Дом',NULL,'reg_build',1,3);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (116,58,'input','Квартира',NULL,'reg_room',1,4);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (117,60,'input','Город',NULL,'live_city',1,1);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (118,60,'input','Улица',NULL,'live_street',1,2);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (119,60,'input','Дом',NULL,'live_build',1,3);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (120,60,'input','Квартира',NULL,'live_room',1,4);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (121,61,'input','Серия',NULL,'pass_seria',1,1);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (122,61,'input','Номер',NULL,'pass_number',1,2);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (123,61,'input','Кем выдан',NULL,'pass_who_issued',1,3);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (124,61,'date','Дата выдачи','a:1:{s:16:\"defaultMonthYear\";s:0:\"\";}','pass_date_issued',1,4);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (125,62,'input','Мобильный',NULL,'mobile_phone',1,1);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (126,62,'input','Домашний',NULL,'phone',1,2);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (127,63,'input','Название организации',NULL,'job_org',1,1);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (128,63,'input','Адрес организации',NULL,'job_address',1,2);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (129,63,'input','Рабочий телефон',NULL,'job_phone',1,3);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (130,63,'input','Должность',NULL,'job_position',1,4);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (131,63,'input','Стаж работы',NULL,'job_exp',1,5);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (132,63,'input','ФИО руговодителя',NULL,'job_chief',1,6);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (133,63,'input','Телефон отдела кадров',NULL,'job_phone_staff',1,7);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (134,63,'input','Средний доход за месяц',NULL,'job_income',1,8);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (135,64,'file','Файл',NULL,'file1',NULL,1);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (136,64,'file','Файл',NULL,'file2',NULL,2);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (137,64,'file','Файл',NULL,'file3',NULL,3);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (139,65,'input','Текстовое поле','a:1:{s:10:\"iblockProp\";s:4:\"CITY\";}',NULL,1,1);
INSERT INTO `b_ceolab_calcmfo_anket_forms_fields` VALUES (142,66,'select','Повторный займ','a:1:{s:6:\"values\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"name\";s:4:\"Да\";s:5:\"value\";s:4:\"Да\";}i:1;O:8:\"stdClass\":2:{s:4:\"name\";s:6:\"Нет\";s:5:\"value\";s:6:\"Нет\";}}}','repeated',1,1);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_checklist`
-- 




DROP TABLE IF EXISTS `b_checklist`;
CREATE TABLE `b_checklist` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_CREATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TESTER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMPANY_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PICTURE` int(11) DEFAULT NULL,
  `TOTAL` int(11) DEFAULT NULL,
  `SUCCESS` int(11) DEFAULT NULL,
  `FAILED` int(11) DEFAULT NULL,
  `PENDING` int(11) DEFAULT NULL,
  `SKIP` int(11) DEFAULT NULL,
  `STATE` longtext COLLATE utf8_unicode_ci,
  `REPORT_COMMENT` text COLLATE utf8_unicode_ci,
  `REPORT` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `EMAIL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHONE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SENDED_TO_BITRIX` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `HIDDEN` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_clouds_file_bucket`
-- 




DROP TABLE IF EXISTS `b_clouds_file_bucket`;
CREATE TABLE `b_clouds_file_bucket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `SORT` int(11) DEFAULT '500',
  `READ_ONLY` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `SERVICE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUCKET` varchar(63) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOCATION` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CNAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILE_COUNT` int(11) DEFAULT '0',
  `FILE_SIZE` float DEFAULT '0',
  `LAST_FILE_ID` int(11) DEFAULT NULL,
  `PREFIX` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SETTINGS` text COLLATE utf8_unicode_ci,
  `FILE_RULES` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_clouds_file_resize`
-- 




DROP TABLE IF EXISTS `b_clouds_file_resize`;
CREATE TABLE `b_clouds_file_resize` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ERROR_CODE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `FILE_ID` int(11) DEFAULT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  `FROM_PATH` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_PATH` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_file_resize_ts` (`TIMESTAMP_X`),
  KEY `ix_b_file_resize_path` (`TO_PATH`(100)),
  KEY `ix_b_file_resize_file` (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_clouds_file_upload`
-- 




DROP TABLE IF EXISTS `b_clouds_file_upload`;
CREATE TABLE `b_clouds_file_upload` (
  `ID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FILE_PATH` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `TMP_FILE` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUCKET_ID` int(11) NOT NULL,
  `PART_SIZE` int(11) NOT NULL,
  `PART_NO` int(11) NOT NULL,
  `PART_FAIL_COUNTER` int(11) NOT NULL,
  `NEXT_STEP` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_component_params`
-- 




DROP TABLE IF EXISTS `b_component_params`;
CREATE TABLE `b_component_params` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `COMPONENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TEMPLATE_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REAL_PATH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SEF_MODE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SEF_FOLDER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `START_CHAR` int(11) NOT NULL,
  `END_CHAR` int(11) NOT NULL,
  `PARAMETERS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_comp_params_name` (`COMPONENT_NAME`),
  KEY `ix_comp_params_path` (`SITE_ID`,`REAL_PATH`),
  KEY `ix_comp_params_sname` (`SITE_ID`,`COMPONENT_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_culture`
-- 




DROP TABLE IF EXISTS `b_culture`;
CREATE TABLE `b_culture` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_DATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_DATETIME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEEK_START` int(1) DEFAULT '1',
  `CHARSET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIRECTION` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_event`
-- 




DROP TABLE IF EXISTS `b_event`;
CREATE TABLE `b_event` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `EVENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE_ID` int(18) DEFAULT NULL,
  `LID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `C_FIELDS` longtext COLLATE utf8_unicode_ci,
  `DATE_INSERT` datetime DEFAULT NULL,
  `DATE_EXEC` datetime DEFAULT NULL,
  `SUCCESS_EXEC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DUPLICATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `ix_success` (`SUCCESS_EXEC`),
  KEY `ix_b_event_date_exec` (`DATE_EXEC`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_event`
-- 


INSERT INTO `b_event` VALUES (1,'ANKETA',NULL,'s1','ANKET=&PRODUCT=&SUMM=&TERM=&AJAX_SUBMIT=Y&LASTNAME=1&PATRONYMIC=1&EMAIL=1@mail.ru&FIRSTNAME=1&DATE_BIRTH=2015-10-03&PHONE=111&PASSPORT_SERIAL=1&PASSPORT_NUMBER=1&PASSPORT_DATE=2015-10-09&PASSPORT_ISSUED=1&ADDRESS_REGISTER=1&ADDRESS_HOME=1&EMPLOYMENT=Безработный&FIRM_ADDRESS=1&FIRM_POSITION=1&FIRM_NAME=1&FIRM_PHONE=1&AVERAGE_INCOME=1&TYPE_LOAN=Клубный&LOAN_SUMM=11&LOAN_TIME=1&AGREE=Да&_GA=GA1.2.1184310465.1434529567&BITRIX_SM_SOUND_LOGIN_PLAYED=Y&PHPSESSID=efd4b9fc95ee094629894b50ec53c346&BITRIX_SM_LOGIN=admin&BITRIX_SM_LAST_SETTINGS=','2015-10-12 15:20:15','2015-10-12 15:23:12','F','Y');
INSERT INTO `b_event` VALUES (2,'ANKETA',NULL,'s1','ANKET=57&PRODUCT=ЭКСПРЕСС&SUMM=5 000 &TERM=3&AJAX_SUBMIT=Y&LASTNAME=Тестов&PATRONYMIC=Тестович&EMAIL=MihailPolushkin@gmail.com&FIRSTNAME=Тест&DATE_BIRTH=2011-01-22&PHONE=88002022020&PASSPORT_SERIAL=1231&PASSPORT_NUMBER=123123&PASSPORT_DATE=1231-12-12&PASSPORT_ISSUED=Паспорт выдан НКВД&ADDRESS_REGISTER=Зарегистрирован по месту жительства&ADDRESS_HOME=Живу по месту проживания&EMPLOYMENT=Есть работа&FIRM_ADDRESS=Серверный полюс&FIRM_POSITION=Самый главный&FIRM_NAME= ООО \"Цифровые люди\"&FIRM_PHONE=88008080800&AVERAGE_INCOME=02938019283&TYPE_LOAN=ЭКСПРЕСС&LOAN_SUMM=8000&LOAN_TIME=4&PHPSESSID=74fbe9eae35932714b85be3c9170f857&BITRIX_SM_LOGIN=admin&BITRIX_SM_SOUND_LOGIN_PLAYED=Y&BITRIX_SM_LAST_SETTINGS=','2015-10-13 04:33:45','2015-10-13 04:35:05','F','Y');
INSERT INTO `b_event` VALUES (3,'ANKETA',NULL,'s1','ANKET=59&PRODUCT=До зарплаты&SUMM=60000&TERM=12&AJAX_SUBMIT=Y&LASTNAME=В&PATRONYMIC=1&EMAIL=timur-ildusovich@mail.ru&FIRSTNAME=2&DATE_BIRTH=2015-10-10&PHONE=dfs&PASSPORT_SERIAL=df&PASSPORT_NUMBER=sdf&PASSPORT_DATE=2015-10-10&PASSPORT_ISSUED=sd&ADDRESS_REGISTER=sdf&ADDRESS_HOME=sd&FIRM_ADDRESS=&FIRM_POSITION=&FIRM_NAME=&FIRM_PHONE=&AVERAGE_INCOME=&TYPE_LOAN=До зарплаты&LOAN_SUMM=60000&LOAN_TIME=12 дней&_GA=GA1.2.1157273837.1443722050&PHPSESSID=8581fa3a6e360cbb181f2d0f97884953&BITRIX_SM_LOGIN=admin&BITRIX_SM_SOUND_LOGIN_PLAYED=Y','2015-10-14 01:19:53','2015-10-14 01:21:54','F','Y');
INSERT INTO `b_event` VALUES (4,'ANKETA',NULL,'s1','ANKET=59&PRODUCT=До зарплаты&SUMM=80000&TERM=22&AJAX_SUBMIT=Y&LASTNAME=Полушкин&PATRONYMIC=Владимирович&EMAIL=MihailPolushkin@gmail.com&FIRSTNAME=Михаил&DATE_BIRTH=1986-08-22&PHONE=+7 (917) 206-06-88&PASSPORT_SERIAL=1233&PASSPORT_NUMBER=123123&PASSPORT_DATE=12312-03-12&PASSPORT_ISSUED=Кем выдан тем выдан&ADDRESS_REGISTER=мегаполис Советский, дистрикт Дергачёвский, галактика Млечный путь&ADDRESS_HOME=тот же&EMPLOYMENT=Есть работа&FIRM_ADDRESS=г. Саратов&FIRM_POSITION=самый главный (почти...)&FIRM_NAME=ООО Цифровые Люди&FIRM_PHONE=123123&AVERAGE_INCOME=123123123&TYPE_LOAN=До зарплаты&LOAN_SUMM=80000&LOAN_TIME=22 дня&BITRIX_SM_SOUND_LOGIN_PLAYED=Y&PHPSESSID=b91815721694b22280bb7a7de9315032&BITRIX_SM_LOGIN=admin&BITRIX_SM_LAST_SETTINGS=','2015-10-14 09:19:32','2015-10-14 09:20:37','F','Y');
INSERT INTO `b_event` VALUES (5,'ANKETA',NULL,'s1','ANKET=&PRODUCT=&SUMM=&TERM=&AJAX_SUBMIT=Y&LASTNAME=asdasd&PATRONYMIC=asdasdasd&EMAIL=aksjdh@laskd.com&FIRSTNAME=asdasdasd&DATE_BIRTH=2015-10-08&PHONE=1&PASSPORT_SERIAL=12123&PASSPORT_NUMBER=2131123123&PASSPORT_DATE=2015-10-16&PASSPORT_ISSUED=qweqweqwe&ADDRESS_REGISTER=qweqwe&ADDRESS_HOME=qweqwe&EMPLOYMENT=Есть работа&FIRM_ADDRESS=weqw&FIRM_POSITION=qweqw&FIRM_NAME=qweqwe&FIRM_PHONE=qweqweq&AVERAGE_INCOME=eqweqw&TYPE_LOAN=&LOAN_SUMM=&LOAN_TIME=&BITRIX_SM_SOUND_LOGIN_PLAYED=Y&BITRIX_SM_LOGIN=admin&BITRIX_SM_LAST_SETTINGS=&PHPSESSID=80b42fa35a666c8e0c344b7b0119fc9d','2015-10-17 10:52:25','2015-10-17 10:52:39','F','Y');
INSERT INTO `b_event` VALUES (6,'ANKETA',NULL,'s1','ANKET=59&PRODUCT=До зарплаты&SUMM=50000&TERM=1&AJAX_SUBMIT=Y&LASTNAME=12312312&PATRONYMIC=231231&EMAIL=asdasd@asdasdasd.asd&FIRSTNAME=31231231&DATE_BIRTH=31231-12-23&PHONE=123123&PASSPORT_SERIAL=12312&PASSPORT_NUMBER=123123&PASSPORT_DATE=2015-10-08&PASSPORT_ISSUED=123123&ADDRESS_REGISTER=123123&ADDRESS_HOME=12312&FIRM_ADDRESS=&FIRM_POSITION=&FIRM_NAME=&FIRM_PHONE=&AVERAGE_INCOME=&TYPE_LOAN=&LOAN_SUMM=&LOAN_TIME=&BITRIX_SM_SOUND_LOGIN_PLAYED=Y&BITRIX_SM_LOGIN=admin&BITRIX_SM_LAST_SETTINGS=&PHPSESSID=80b42fa35a666c8e0c344b7b0119fc9d','2015-10-17 10:53:38','2015-10-17 10:53:57','F','Y');
INSERT INTO `b_event` VALUES (7,'ANKETA',NULL,'s1','ANKET=&PRODUCT=&SUMM=&TERM=&AJAX_SUBMIT=Y&LASTNAME=dfssdf&PATRONYMIC=sdf&EMAIL=sdf@mail.ru&FIRSTNAME=asdf&DATE_BIRTH=2015-10-10&PHONE=22132&PASSPORT_SERIAL=sdf&PASSPORT_NUMBER=sdf&PASSPORT_DATE=2015-10-10&PASSPORT_ISSUED=sdf&ADDRESS_REGISTER=sdf&ADDRESS_HOME=sdf&FIRM_ADDRESS=&FIRM_POSITION=&FIRM_NAME=&FIRM_PHONE=&AVERAGE_INCOME=&TYPE_LOAN=&LOAN_SUMM=&LOAN_TIME=','2015-10-17 23:57:26','2015-10-17 23:58:25','Y','Y');
INSERT INTO `b_event` VALUES (8,'ANKETA',NULL,'s1','ANKET=&PRODUCT=&SUMM=&TERM=&AJAX_SUBMIT=Y&LASTNAME=sf&PATRONYMIC=sdfsdf&EMAIL=sss@mail.ry&FIRSTNAME=sdf&DATE_BIRTH=2015-10-17&PHONE=s34324&PASSPORT_SERIAL=sdf&PASSPORT_NUMBER=sdf&PASSPORT_DATE=2015-10-17&PASSPORT_ISSUED=sdf&ADDRESS_REGISTER=sdf&ADDRESS_HOME=sdf&FIRM_ADDRESS=&FIRM_POSITION=&FIRM_NAME=&FIRM_PHONE=&AVERAGE_INCOME=&TYPE_LOAN=&LOAN_SUMM=4444&LOAN_TIME=444','2015-10-18 00:04:54','2015-10-18 00:06:22','Y','Y');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_event_log`
-- 




DROP TABLE IF EXISTS `b_event_log`;
CREATE TABLE `b_event_log` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SEVERITY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `AUDIT_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `REMOTE_ADDR` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_AGENT` text COLLATE utf8_unicode_ci,
  `REQUEST_URI` text COLLATE utf8_unicode_ci,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_ID` int(18) DEFAULT NULL,
  `GUEST_ID` int(18) DEFAULT NULL,
  `DESCRIPTION` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_event_log_time` (`TIMESTAMP_X`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_event_log`
-- 


INSERT INTO `b_event_log` VALUES (71,'2015-10-14 01:01:19','UNKNOWN','MENU_EDIT','main','UNKNOWN','85.26.233.111','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36','/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&lang=ru&site=s1&back_url=%2Floans-online.php&path=%2F&name=top',NULL,1,NULL,'a:2:{s:9:\"menu_name\";s:23:\"Верхнее меню\";s:4:\"path\";b:0;}');
INSERT INTO `b_event_log` VALUES (72,'2015-10-14 16:09:55','UNKNOWN','SITE_CHECKER_ERROR','main','/home/c/cy25011/public_html','213.80.223.250','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/44.0.2403.89 Chrome/44.0.2403.89 Safari/537.36','/bitrix/admin/site_checker.php?HTTP_HOST=cy25011.tmweb.ru&SERVER_PORT=&HTTPS=&test_start=Y&lang=ru&step=41&global_test_vars=YTozOntzOjIwOiJzaXRlX2NoZWNrZXJfc3VjY2VzcyI7czoxOiJOIjtzOjE1OiJkYl9jaGFyc2V0X2ZhaWwiO2I6MTtzOjEwOiJsYXN0X3ZhbHVlIjtzOjE5OiJ1cGRhdGVyX3BhcnRuZXIubG9nIjt9',NULL,1,NULL,'2015-Oct-14 16:09:42 Работа с сокетами (check_socket): Ok\nConnection to cy25011.tmweb.ru:80\n2015-Oct-14 16:09:45 Фактическое ограничение памяти (check_memory_limit): Ok\n85% done\n\n== Request ==\nGET /bitrix/admin/site_checker.php?test_type=memory_test&unique_id=ec51b1e0a8eeaf5096b240be4447516f&max=511 HTTP/1.1\r\nHost: cy25011.tmweb.ru\r\n\r\n\n== Response ==\nHTTP/1.1 200 OK\r\nServer: nginx/1.6.3\r\nDate: Wed, 14 Oct 2015 13:09:45 GMT\r\nContent-Type: text/html; charset=utf-8\r\nContent-Length: 217\r\nConnection: keep-alive\r\nX-Powered-By: PHP/5.3.29\r\n\n== Body ==\n\nFatal error:  Allowed memory size of 524288000 bytes exhausted (tried to allocate 1048577 bytes) in /home/c/cy25011/public_html/bitrix/modules/main/admin/site_checker.php on line 111\n\n==========\n2015-Oct-14 16:09:47 Кодировка соединения (check_mysql_connection_charset): Ok\ncharacter_set_connection=utf8, collation_connection=utf8_unicode_ci, character_set_results=utf8\n2015-Oct-14 16:09:47 Кодировка базы данных (check_mysql_db_charset): Fail\nCHARSET=utf8, COLLATION=utf8_general_ci\n');
INSERT INTO `b_event_log` VALUES (73,'2015-10-17 22:48:05','UNKNOWN','PAGE_EDIT','main','UNKNOWN','127.0.0.1','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36','/bitrix/admin/public_file_edit_src.php?bxsender=core_window_cdialog&site=s1&path=%2Fbitrix%2Ftemplates%2F.default%2Fcomponents%2Fbitrix%2Fcatalog.section%2Fslider%2Ftemplate.php&back_url=%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY&lang=ru','s1',1,NULL,'a:1:{s:4:\"path\";s:79:\"bitrix/templates/.default/components/bitrix/catalog.section/slider/template.php\";}');
INSERT INTO `b_event_log` VALUES (74,'2015-10-18 07:31:12','UNKNOWN','MENU_EDIT','main','UNKNOWN','127.0.0.1','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36','/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&lang=ru&site=s1&back_url=%2Faddress.php%3Fcity%3D17&path=%2F&name=top',NULL,1,NULL,'a:2:{s:9:\"menu_name\";s:23:\"Верхнее меню\";s:4:\"path\";b:0;}');
INSERT INTO `b_event_log` VALUES (75,'2015-10-18 07:33:05','UNKNOWN','MENU_EDIT','main','UNKNOWN','127.0.0.1','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36','/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&lang=ru&site=s1&back_url=%2Faddress.php%3Fcity%3D17%26clear_cache%3DY&path=%2F&name=bottom',NULL,1,NULL,'a:2:{s:9:\"menu_name\";s:21:\"Нижнее меню\";s:4:\"path\";b:0;}');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_event_message`
-- 




DROP TABLE IF EXISTS `b_event_message`;
CREATE TABLE `b_event_message` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EVENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `EMAIL_FROM` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#EMAIL_FROM#',
  `EMAIL_TO` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#EMAIL_TO#',
  `SUBJECT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `BODY_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `BCC` text COLLATE utf8_unicode_ci,
  `REPLY_TO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IN_REPLY_TO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRIORITY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD1_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD1_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD2_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD2_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_event_message_name` (`EVENT_NAME`(50))
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_event_message`
-- 


INSERT INTO `b_event_message` VALUES (1,'2013-09-26 10:01:06','NEW_USER','s1','Y','#DEFAULT_EMAIL_FROM#','#DEFAULT_EMAIL_FROM#','#SITE_NAME#: Зарегистрировался новый пользователь','Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nНа сайте #SERVER_NAME# успешно зарегистрирован новый пользователь.\n\nДанные пользователя:\nID пользователя: #USER_ID#\n\nИмя: #NAME#\nФамилия: #LAST_NAME#\nE-Mail: #EMAIL#\n\nLogin: #LOGIN#\n\nПисьмо сгенерировано автоматически.','text',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_event_message` VALUES (2,'2013-09-26 10:01:06','USER_INFO','s1','Y','#DEFAULT_EMAIL_FROM#','#EMAIL#','#SITE_NAME#: Регистрационная информация','Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n#NAME# #LAST_NAME#,\n\n#MESSAGE#\n\nВаша регистрационная информация:\n\nID пользователя: #USER_ID#\nСтатус профиля: #STATUS#\nLogin: #LOGIN#\n\nВы можете изменить пароль, перейдя по следующей ссылке:\nhttp://#SERVER_NAME#/auth/index.php?change_password=yes&lang=ru&USER_CHECKWORD=#CHECKWORD#&USER_LOGIN=#LOGIN#\n\nСообщение сгенерировано автоматически.','text',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_event_message` VALUES (3,'2013-09-26 10:01:06','USER_PASS_REQUEST','s1','Y','#DEFAULT_EMAIL_FROM#','#EMAIL#','#SITE_NAME#: Запрос на смену пароля','Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n#NAME# #LAST_NAME#,\n\n#MESSAGE#\n\nДля смены пароля перейдите по следующей ссылке:\nhttp://#SERVER_NAME#/auth/index.php?change_password=yes&lang=ru&USER_CHECKWORD=#CHECKWORD#&USER_LOGIN=#LOGIN#\n\nВаша регистрационная информация:\n\nID пользователя: #USER_ID#\nСтатус профиля: #STATUS#\nLogin: #LOGIN#\n\nСообщение сгенерировано автоматически.','text',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_event_message` VALUES (4,'2013-09-26 10:01:07','USER_PASS_CHANGED','s1','Y','#DEFAULT_EMAIL_FROM#','#EMAIL#','#SITE_NAME#: Подтверждение смены пароля','Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n#NAME# #LAST_NAME#,\n\n#MESSAGE#\n\nВаша регистрационная информация:\n\nID пользователя: #USER_ID#\nСтатус профиля: #STATUS#\nLogin: #LOGIN#\n\nСообщение сгенерировано автоматически.','text',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_event_message` VALUES (5,'2013-09-26 10:01:07','NEW_USER_CONFIRM','s1','Y','#DEFAULT_EMAIL_FROM#','#EMAIL#','#SITE_NAME#: Подтверждение регистрации нового пользователя','Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nЗдравствуйте,\n\nВы получили это сообщение, так как ваш адрес был использован при регистрации нового пользователя на сервере #SERVER_NAME#.\n\nВаш код для подтверждения регистрации: #CONFIRM_CODE#\n\nДля подтверждения регистрации перейдите по следующей ссылке:\nhttp://#SERVER_NAME#/auth/index.php?confirm_registration=yes&confirm_user_id=#USER_ID#&confirm_code=#CONFIRM_CODE#\n\nВы также можете ввести код для подтверждения регистрации на странице:\nhttp://#SERVER_NAME#/auth/index.php?confirm_registration=yes&confirm_user_id=#USER_ID#\n\nВнимание! Ваш профиль не будет активным, пока вы не подтвердите свою регистрацию.\n\n---------------------------------------------------------------------\n\nСообщение сгенерировано автоматически.','text',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_event_message` VALUES (6,'2013-09-26 10:01:07','USER_INVITE','s1','Y','#DEFAULT_EMAIL_FROM#','#EMAIL#','#SITE_NAME#: Приглашение на сайт','Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\nЗдравствуйте, #NAME# #LAST_NAME#!\n\nАдминистратором сайта вы добавлены в число зарегистрированных пользователей.\n\nПриглашаем Вас на наш сайт.\n\nВаша регистрационная информация:\n\nID пользователя: #ID#\nLogin: #LOGIN#\n\nРекомендуем вам сменить установленный автоматически пароль.\n\nДля смены пароля перейдите по следующей ссылке:\nhttp://#SERVER_NAME#/auth.php?change_password=yes&USER_LOGIN=#URL_LOGIN#&USER_CHECKWORD=#CHECKWORD#\n','text',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_event_message` VALUES (7,'2013-09-26 10:01:07','FEEDBACK_FORM','s1','Y','#DEFAULT_EMAIL_FROM#','#EMAIL_TO#','#SITE_NAME#: Сообщение из формы обратной связи','Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nВам было отправлено сообщение через форму обратной связи\n\nАвтор: #AUTHOR#\nE-mail автора: #AUTHOR_EMAIL#\n\nТекст сообщения:\n#TEXT#\n\nСообщение сгенерировано автоматически.','text',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_event_message` VALUES (8,'2014-11-10 11:19:43','SEND_REQUEST','s1','Y','request@sitemfo.ru','ryabikind@gmail.com','Заявка на кредит','        <p>Идентификационные данные<br/>\r\nФамилия: #lastname#<br/>\r\nИмя: #firstname#<br/>\r\nОтчество: #middlename#<br/>\r\nДата рождения: #birth_date#<br/>\r\nПолных лет: #age#<br/>\r\nМесто рождения: #birth_place#<br/>\r\nПол: #gender#<br/>\r\nМенялась ли ФИО: #change_lastname#<br/>\r\nПрежняя фамилия: #prev_lastname#<br/>\r\n</p>\r\n\r\n<p>Паспортные данные<br/>\r\nСерия: #pass_seria#<br/>\r\nНомер: #pass_number#<br/>\r\nКод подразделения: #pass_code#<br/>\r\nКем выдан: #pass_who_issued#<br/>\r\nДата выдачи: #pass_date_issued#<br/>\r\nСогл. на пред ПД третьим лицам: #confirm_data#<br/>\r\n</p>\r\n\r\n<p>Второй документ или пенсионное свидетельство<br/>\r\nВид: #second_doc#<br/>\r\nСерия: #second_doc_seria#<br/>\r\nНомер: #second_doc_num#<br/>\r\nДата выдачи: #second_doc_date#<br/>\r\nКем выдан: #second_doc_issued#<br/>\r\n</p>\r\n\r\n<p>Полис ОМС<br/>\r\nНомер: #health_insurance#<br/>\r\n</p>\r\n\r\n<p>Контактные данные<br/>\r\nАдрес регистрации: #reg_address#<br/>\r\nТелефон: #reg_phone#<br/>\r\nАдрес проживания: #live_address#<br/>\r\nТелефон: #live_phone#<br/>\r\nМобильный телефон: #mobile_num#<br/>\r\nЭл. почта: #email#<br/>\r\n</p>\r\n\r\n<p>Контактное лицо (кроме супруга)<br/>\r\nФамилия: #contact_lastname#<br/>\r\nИмя: #contact_firstname#<br/>\r\nОтчество: #contact_middlename#<br/>\r\nДата рождения: #contact_birthdate#<br/>\r\nПол: #contact_gender#<br/>\r\nСтатус: #contact_status#<br/>\r\nМоб. тел.: #contactperson_phone#<br/>\r\n</p>\r\n\r\n<p>Сведения о работе<br/>\r\nНазвание организации: #job_org#<br/>\r\nПосл. стаж: #job_exp#<br/>\r\nФИО руговодителя: #job_chief#<br/>\r\nАдрес фактический: #job_address#<br/>\r\nРабочий телефон: #job_phone#<br/>\r\n</p>\r\n\r\n<p>Доходы / расходы<br/>\r\nЕжемесячные доходы: #income#<br/>\r\nЕжемесячные расходы: #expense#<br/>\r\n</p>\r\n\r\n<p>Имеющиеся обязательства по кредитам<br/>\r\nНазвание организации: #credit_org#<br/>\r\nВид: #type_credit#<br/>\r\nСумма: #credit_summ#<br/>\r\nДолг: #debt_credit#<br/>\r\nПлатеж: #credit_pay#<br/>\r\n</p>\r\n\r\n<p>Семейное положение и сведения о супруге<br/>\r\nСемейное положение: #family_status#<br/>\r\nДетей на иждевении: #family_childs#<br/>\r\nДетей не на иждевении: #family_childsdep#<br/>\r\nДругие иждивенцы: #family_otherdep#<br/>\r\nФамилия: #family_lastname#<br/>\r\nИмя: #family_firstname#<br/>\r\nОтчество: #family_middlename#<br/>\r\nДата рождения: #family_birthdate#<br/>\r\nПол: #family_gender#<br/>\r\nТелефон: #family_phone#<br/>\r\nЗанятость: #family_employment#<br/>\r\n</p>\r\n\r\n<p>Личный автотранспорт<br/>\r\nМарка: #auto_brand#<br/>\r\nМодель: #auto_model#<br/>\r\nГос. №: #auto_num#<br/>\r\nГод выпуска: #auto_year#<br/>\r\n</p>\r\n\r\n<p>Каналы рекламы<br/>\r\nОткуда Вы узнали о нас: #adv#<br/>\r\n</p>\r\n\r\n<p>Выбор отделения<br/>\r\nГород: #city_issued#<br/>\r\nОтделение: #depart_issued#<br/>\r\n</p>\r\n\r\n    ','html','','','','','','','','','');
INSERT INTO `b_event_message` VALUES (10,'2015-10-14 09:16:03','ANKETA','s1','Y','#DEFAULT_EMAIL_FROM#','MihailPolushkin@gmail.com','Заполнена анкета на получение займа на сайте #SITE_NAME# ','Заполнена анкета на получение займа на сайте #SITE_NAME# \r\n_____________________________________________\r\n\r\n*** Поля из калькулятора ***\r\nКредитный продукт: #PRODUCT#\r\nСумма: #SUMM#\r\nСрок кредитования: #TERM#\r\n\r\n*** Общая информация ***\r\nФамилия: #LASTNAME#\r\nОтчество: #PATRONYMIC#\r\nEmail: #EMAIL#\r\nИмя: #FIRSTNAME#\r\nДата рождения: #DATE_BIRTH#\r\nТелефон: #PHONE#\r\n\r\n*** Паспортные данные ***\r\nСерия: #PASSPORT_SERIAL#\r\nНомер паспорта: #PASSPORT_NUMBER#\r\nДата выдачи паспорта: #PASSPORT_DATE#\r\nКем выдан паспорт: #PASSPORT_ISSUED#\r\nАдрес фактического проживания: #ADDRESS_HOME#\r\n\r\n*** Информация о трудоустройстве ***\r\nТрудоустройство: #EMPLOYMENT#\r\nФакт. адрес организации: #FIRM_ADDRESS#\r\nДолжность: #FIRM_POSITION#\r\nНазвание организации: #FIRM_NAME#\r\nРабочий телефон: #FIRM_PHONE#\r\nСреднемесячный доход: #AVERAGE_INCOME#\r\n\r\n*** Информация о займе ***\r\nВид займа: #TYPE_LOAN#\r\nСумма займа: #LOAN_SUMM#\r\nСрок займа: #LOAN_TIME#\r\n\r\n*** Дополнительная информация ***\r\nСогласен с SMS-рассылкой: #AGREE#','text','','','','','','','','','');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_event_message_site`
-- 




DROP TABLE IF EXISTS `b_event_message_site`;
CREATE TABLE `b_event_message_site` (
  `EVENT_MESSAGE_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`EVENT_MESSAGE_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_event_message_site`
-- 


INSERT INTO `b_event_message_site` VALUES (1,'s1');
INSERT INTO `b_event_message_site` VALUES (2,'s1');
INSERT INTO `b_event_message_site` VALUES (3,'s1');
INSERT INTO `b_event_message_site` VALUES (4,'s1');
INSERT INTO `b_event_message_site` VALUES (5,'s1');
INSERT INTO `b_event_message_site` VALUES (6,'s1');
INSERT INTO `b_event_message_site` VALUES (7,'s1');
INSERT INTO `b_event_message_site` VALUES (8,'s1');
INSERT INTO `b_event_message_site` VALUES (10,'s1');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_event_type`
-- 




DROP TABLE IF EXISTS `b_event_type`;
CREATE TABLE `b_event_type` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `EVENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `SORT` int(18) NOT NULL DEFAULT '150',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_1` (`EVENT_NAME`,`LID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_event_type`
-- 


INSERT INTO `b_event_type` VALUES (1,'ru','NEW_USER','Зарегистрировался новый пользователь','\n\n#USER_ID# - ID пользователя\n#LOGIN# - Логин\n#EMAIL# - EMail\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#USER_IP# - IP пользователя\n#USER_HOST# - Хост пользователя\n',1);
INSERT INTO `b_event_type` VALUES (2,'ru','USER_INFO','Информация о пользователе','\n\n#USER_ID# - ID пользователя\n#STATUS# - Статус логина\n#MESSAGE# - Сообщение пользователю\n#LOGIN# - Логин\n#CHECKWORD# - Контрольная строка для смены пароля\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#EMAIL# - E-Mail пользователя\n',2);
INSERT INTO `b_event_type` VALUES (3,'ru','NEW_USER_CONFIRM','Подтверждение регистрации нового пользователя','\n\n\n#USER_ID# - ID пользователя\n#LOGIN# - Логин\n#EMAIL# - EMail\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#USER_IP# - IP пользователя\n#USER_HOST# - Хост пользователя\n#CONFIRM_CODE# - Код подтверждения\n',3);
INSERT INTO `b_event_type` VALUES (4,'ru','USER_PASS_REQUEST','Запрос на смену пароля','\n\n#USER_ID# - ID пользователя\n#STATUS# - Статус логина\n#MESSAGE# - Сообщение пользователю\n#LOGIN# - Логин\n#CHECKWORD# - Контрольная строка для смены пароля\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#EMAIL# - E-Mail пользователя\n',4);
INSERT INTO `b_event_type` VALUES (5,'ru','USER_PASS_CHANGED','Подтверждение смены пароля','\n\n#USER_ID# - ID пользователя\n#STATUS# - Статус логина\n#MESSAGE# - Сообщение пользователю\n#LOGIN# - Логин\n#CHECKWORD# - Контрольная строка для смены пароля\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#EMAIL# - E-Mail пользователя\n',5);
INSERT INTO `b_event_type` VALUES (6,'ru','USER_INVITE','Приглашение на сайт нового пользователя','#ID# - ID пользователя\n#LOGIN# - Логин\n#URL_LOGIN# - Логин, закодированный для использования в URL\n#EMAIL# - EMail\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#PASSWORD# - пароль пользователя \n#CHECKWORD# - Контрольная строка для смены пароля\n#XML_ID# - ID пользователя для связи с внешними источниками\n',6);
INSERT INTO `b_event_type` VALUES (7,'ru','FEEDBACK_FORM','Отправка сообщения через форму обратной связи','#AUTHOR# - Автор сообщения\n#AUTHOR_EMAIL# - Email автора сообщения\n#TEXT# - Текст сообщения\n#EMAIL_FROM# - Email отправителя письма\n#EMAIL_TO# - Email получателя письма',7);
INSERT INTO `b_event_type` VALUES (8,'en','NEW_USER','New user was registered','\n\n#USER_ID# - User ID\n#LOGIN# - Login\n#EMAIL# - EMail\n#NAME# - Name\n#LAST_NAME# - Last Name\n#USER_IP# - User IP\n#USER_HOST# - User Host\n',1);
INSERT INTO `b_event_type` VALUES (9,'en','USER_INFO','Account Information','\n\n#USER_ID# - User ID\n#STATUS# - Account status\n#MESSAGE# - Message for user\n#LOGIN# - Login\n#CHECKWORD# - Check string for password change\n#NAME# - Name\n#LAST_NAME# - Last Name\n#EMAIL# - User E-Mail\n',2);
INSERT INTO `b_event_type` VALUES (10,'en','NEW_USER_CONFIRM','New user registration confirmation','\n\n#USER_ID# - User ID\n#LOGIN# - Login\n#EMAIL# - E-mail\n#NAME# - First name\n#LAST_NAME# - Last name\n#USER_IP# - User IP\n#USER_HOST# - User host\n#CONFIRM_CODE# - Confirmation code\n',3);
INSERT INTO `b_event_type` VALUES (11,'en','USER_PASS_REQUEST','Password Change Request','\n\n#USER_ID# - User ID\n#STATUS# - Account status\n#MESSAGE# - Message for user\n#LOGIN# - Login\n#CHECKWORD# - Check string for password change\n#NAME# - Name\n#LAST_NAME# - Last Name\n#EMAIL# - User E-Mail\n',4);
INSERT INTO `b_event_type` VALUES (12,'en','USER_PASS_CHANGED','Password Change Confirmation','\n\n#USER_ID# - User ID\n#STATUS# - Account status\n#MESSAGE# - Message for user\n#LOGIN# - Login\n#CHECKWORD# - Check string for password change\n#NAME# - Name\n#LAST_NAME# - Last Name\n#EMAIL# - User E-Mail\n',5);
INSERT INTO `b_event_type` VALUES (13,'en','USER_INVITE','Invitation of a new site user','#ID# - User ID\n#LOGIN# - Login\n#URL_LOGIN# - Encoded login for use in URL\n#EMAIL# - EMail\n#NAME# - Name\n#LAST_NAME# - Last Name\n#PASSWORD# - User password \n#CHECKWORD# - Password check string\n#XML_ID# - User ID to link with external data sources\n\n',6);
INSERT INTO `b_event_type` VALUES (14,'en','FEEDBACK_FORM','Sending a message using a feedback form','#AUTHOR# - Message author\n#AUTHOR_EMAIL# - Author\'s e-mail address\n#TEXT# - Message text\n#EMAIL_FROM# - Sender\'s e-mail address\n#EMAIL_TO# - Recipient\'s e-mail address',7);
INSERT INTO `b_event_type` VALUES (15,'ru','SEND_REQUEST','Отправка заявки','',150);
INSERT INTO `b_event_type` VALUES (16,'en','SEND_REQUEST','','',150);
INSERT INTO `b_event_type` VALUES (17,'ru','ANKETA','Заполнена анкета на получение займа','',150);
INSERT INTO `b_event_type` VALUES (18,'en','ANKETA','Fill in the questionnaire for a loan','',150);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_favorite`
-- 




DROP TABLE IF EXISTS `b_favorite`;
CREATE TABLE `b_favorite` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` text COLLATE utf8_unicode_ci,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `CODE_ID` int(18) DEFAULT NULL,
  `COMMON` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `MENU_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_file`
-- 




DROP TABLE IF EXISTS `b_file`;
CREATE TABLE `b_file` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HEIGHT` int(18) DEFAULT NULL,
  `WIDTH` int(18) DEFAULT NULL,
  `FILE_SIZE` int(18) NOT NULL,
  `CONTENT_TYPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'IMAGE',
  `SUBDIR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILE_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ORIGINAL_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HANDLER_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_file`
-- 


INSERT INTO `b_file` VALUES (5,'2015-10-11 14:29:33','main',720,1400,260496,'image/jpeg','uf/b61','b614b7e4df8fa3a68d3eb5804ed9b391.jpg','slide-1.jpg','',NULL);
INSERT INTO `b_file` VALUES (6,'2015-10-11 14:29:33','main',720,1400,260496,'image/jpeg','uf/2d8','2d87f178d1c855dec50b6883e3762d6e.jpg','slide-1.jpg','',NULL);
INSERT INTO `b_file` VALUES (7,'2015-10-11 14:29:33','main',720,1400,244571,'image/jpeg','uf/d1c','d1ccf59d93589875a14c5f07c0efb75e.jpg','slide-2.jpg','',NULL);
INSERT INTO `b_file` VALUES (8,'2015-10-11 14:29:33','main',720,1400,263461,'image/jpeg','uf/f2f','f2f1c2fe588380b3dc699b2821ffee4b.jpg','slide-3.jpg','',NULL);
INSERT INTO `b_file` VALUES (9,'2015-10-11 14:29:33','main',720,1400,326723,'image/jpeg','uf/805','80571829b9bdd381745a8e4f96b6665f.jpg','slide-4.jpg','',NULL);
INSERT INTO `b_file` VALUES (10,'2015-10-11 14:32:17','iblock',64,64,5689,'image/png','iblock/d5a','d5a5aea896ce4625dd6e6a6c7555ff30.png','icon-1.png','',NULL);
INSERT INTO `b_file` VALUES (11,'2015-10-11 14:33:25','iblock',64,64,5104,'image/png','iblock/786','786a29b1c4a477180769c1e4fb90e2af.png','icon-2.png','',NULL);
INSERT INTO `b_file` VALUES (12,'2015-10-11 14:34:04','iblock',64,64,5127,'image/png','iblock/28f','28ff5420a6b4462e2f09cf875ce06ce9.png','icon-3.png','',NULL);
INSERT INTO `b_file` VALUES (14,'2015-10-11 15:22:18','iblock',35,61,3002,'image/jpeg','iblock/ebb','ebb72b11a023d456fb226e7c00fe9ae4.jpg','partner-1.jpg','',NULL);
INSERT INTO `b_file` VALUES (15,'2015-10-11 15:22:40','iblock',35,42,1790,'image/jpeg','iblock/8f5','8f533fdbbefb70e62133c067f5395236.jpg','partner-2.jpg','',NULL);
INSERT INTO `b_file` VALUES (16,'2015-10-11 15:23:09','iblock',35,65,3866,'image/jpeg','iblock/85f','85f1028c5f0946c676e9d7c8d562aa24.jpg','partner-3.jpg','',NULL);
INSERT INTO `b_file` VALUES (17,'2015-10-11 15:23:35','iblock',35,73,2598,'image/jpeg','iblock/edd','edd9da41d7733f1c449d049c7837f97b.jpg','partner-4.jpg','',NULL);
INSERT INTO `b_file` VALUES (18,'2015-10-11 15:23:56','iblock',35,38,1511,'image/jpeg','iblock/d87','d874456f6e1219371d1a0cfa2441d812.jpg','partner-5.jpg','',NULL);
INSERT INTO `b_file` VALUES (19,'2015-10-11 15:24:23','iblock',35,55,2429,'image/jpeg','iblock/b26','b26038b9a04835d0b91264f78ea0fdef.jpg','partner-6.jpg','',NULL);
INSERT INTO `b_file` VALUES (20,'2015-10-11 20:23:32','iblock',256,410,20312,'image/jpeg','iblock/f72','f728c8c35a54214d801c151923c625ba.jpg','tariff-1.jpg','',NULL);
INSERT INTO `b_file` VALUES (21,'2015-10-11 20:26:16','iblock',256,410,40069,'image/jpeg','iblock/b2b','b2b66aa0d2739ef431a01280eac287a2.jpg','tariff-2.jpg','',NULL);
INSERT INTO `b_file` VALUES (22,'2015-10-11 23:01:28','iblock',256,410,20953,'image/jpeg','iblock/a05','a0560e1145e1d38cd269e80eea97a7f7.jpg','tariff-3 (1).jpg','',NULL);
INSERT INTO `b_file` VALUES (23,'2015-10-11 23:13:52','iblock',256,410,38783,'image/jpeg','iblock/a06','a06c9e6a1f3da350bf1c9cf1dc544342.jpg','tariff-4.jpg','',NULL);
INSERT INTO `b_file` VALUES (24,'2015-10-11 23:15:29','iblock',256,410,31449,'image/jpeg','iblock/e34','e3429ad0564fd3cbbf7e50009792eaf4.jpg','tariff-5.jpg','',NULL);
INSERT INTO `b_file` VALUES (25,'2015-10-11 23:16:43','iblock',256,410,18538,'image/jpeg','iblock/0e5','0e50dc871ab9d79e42c266084b13bac8.jpg','tariff-6.jpg','',NULL);
INSERT INTO `b_file` VALUES (26,'2015-10-12 11:19:24','iblock',74,74,2857,'image/png','iblock/02b','02bce355af966ce2e9dbeb018671d756.png','icon-10.png','',NULL);
INSERT INTO `b_file` VALUES (27,'2015-10-12 11:19:35','iblock',74,74,2857,'image/png','iblock/b62','b6282d0b87763a69929451a666692ef4.png','icon-10.png','',NULL);
INSERT INTO `b_file` VALUES (28,'2015-10-12 11:19:50','iblock',74,74,2857,'image/png','iblock/ed2','ed28a28c5ae3353fc4ef098800a16468.png','icon-10.png','',NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_file_search`
-- 




DROP TABLE IF EXISTS `b_file_search`;
CREATE TABLE `b_file_search` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SESS_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `F_PATH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `B_DIR` int(11) NOT NULL DEFAULT '0',
  `F_SIZE` int(11) NOT NULL DEFAULT '0',
  `F_TIME` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_filters`
-- 




DROP TABLE IF EXISTS `b_filters`;
CREATE TABLE `b_filters` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) DEFAULT NULL,
  `FILTER_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FIELDS` text COLLATE utf8_unicode_ci NOT NULL,
  `COMMON` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRESET` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRESET_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(18) DEFAULT NULL,
  `SORT_FIELD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_group`
-- 




DROP TABLE IF EXISTS `b_group`;
CREATE TABLE `b_group` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `ANONYMOUS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECURITY_POLICY` text COLLATE utf8_unicode_ci,
  `STRING_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_group`
-- 


INSERT INTO `b_group` VALUES (1,'2013-09-26 10:01:00','Y',1,'N','Администраторы','Полный доступ к управлению сайтом.',NULL,NULL);
INSERT INTO `b_group` VALUES (2,'2013-09-26 10:01:00','Y',2,'Y','Все пользователи (в том числе неавторизованные)','Все пользователи, включая неавторизованных.',NULL,NULL);
INSERT INTO `b_group` VALUES (3,'2015-10-13 09:46:18','Y',3,'N','Пользователи, имеющие право голосовать за рейтинг','В эту группу пользователи добавляются автоматически.','a:0:{}','RATING_VOTE');
INSERT INTO `b_group` VALUES (4,'2013-09-26 10:01:00','Y',4,'N','Пользователи имеющие право голосовать за авторитет','В эту группу пользователи добавляются автоматически.',NULL,'RATING_VOTE_AUTHORITY');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_group_collection_task`
-- 




DROP TABLE IF EXISTS `b_group_collection_task`;
CREATE TABLE `b_group_collection_task` (
  `GROUP_ID` int(11) NOT NULL,
  `TASK_ID` int(11) NOT NULL,
  `COLLECTION_ID` int(11) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`TASK_ID`,`COLLECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_group_subordinate`
-- 




DROP TABLE IF EXISTS `b_group_subordinate`;
CREATE TABLE `b_group_subordinate` (
  `ID` int(18) NOT NULL,
  `AR_SUBGROUP_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_group_task`
-- 




DROP TABLE IF EXISTS `b_group_task`;
CREATE TABLE `b_group_task` (
  `GROUP_ID` int(18) NOT NULL,
  `TASK_ID` int(18) NOT NULL,
  `EXTERNAL_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`GROUP_ID`,`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_group_task`
-- 


INSERT INTO `b_group_task` VALUES (3,6,'');
INSERT INTO `b_group_task` VALUES (3,15,'');
INSERT INTO `b_group_task` VALUES (3,18,'');
INSERT INTO `b_group_task` VALUES (3,36,'');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_hot_keys`
-- 




DROP TABLE IF EXISTS `b_hot_keys`;
CREATE TABLE `b_hot_keys` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `KEYS_STRING` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CODE_ID` int(18) NOT NULL,
  `USER_ID` int(18) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_b_hot_keys_co_u` (`CODE_ID`,`USER_ID`),
  KEY `ix_hot_keys_code` (`CODE_ID`),
  KEY `ix_hot_keys_user` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_hot_keys`
-- 


INSERT INTO `b_hot_keys` VALUES (1,'Ctrl+Alt+85',139,0);
INSERT INTO `b_hot_keys` VALUES (2,'Ctrl+Alt+80',17,0);
INSERT INTO `b_hot_keys` VALUES (3,'Ctrl+Alt+70',120,0);
INSERT INTO `b_hot_keys` VALUES (4,'Ctrl+Alt+68',117,0);
INSERT INTO `b_hot_keys` VALUES (5,'Ctrl+Alt+81',3,0);
INSERT INTO `b_hot_keys` VALUES (6,'Ctrl+Alt+75',106,0);
INSERT INTO `b_hot_keys` VALUES (7,'Ctrl+Alt+79',133,0);
INSERT INTO `b_hot_keys` VALUES (8,'Ctrl+Alt+70',121,0);
INSERT INTO `b_hot_keys` VALUES (9,'Ctrl+Alt+69',118,0);
INSERT INTO `b_hot_keys` VALUES (10,'Ctrl+Shift+83',87,0);
INSERT INTO `b_hot_keys` VALUES (11,'Ctrl+Shift+88',88,0);
INSERT INTO `b_hot_keys` VALUES (12,'Ctrl+Shift+76',89,0);
INSERT INTO `b_hot_keys` VALUES (13,'Ctrl+Alt+85',139,1);
INSERT INTO `b_hot_keys` VALUES (14,'Ctrl+Alt+80',17,1);
INSERT INTO `b_hot_keys` VALUES (15,'Ctrl+Alt+70',120,1);
INSERT INTO `b_hot_keys` VALUES (16,'Ctrl+Alt+68',117,1);
INSERT INTO `b_hot_keys` VALUES (17,'Ctrl+Alt+81',3,1);
INSERT INTO `b_hot_keys` VALUES (18,'Ctrl+Alt+75',106,1);
INSERT INTO `b_hot_keys` VALUES (19,'Ctrl+Alt+79',133,1);
INSERT INTO `b_hot_keys` VALUES (20,'Ctrl+Alt+70',121,1);
INSERT INTO `b_hot_keys` VALUES (21,'Ctrl+Alt+69',118,1);
INSERT INTO `b_hot_keys` VALUES (22,'Ctrl+Shift+83',87,1);
INSERT INTO `b_hot_keys` VALUES (23,'Ctrl+Shift+88',88,1);
INSERT INTO `b_hot_keys` VALUES (24,'Ctrl+Shift+76',89,1);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_hot_keys_code`
-- 




DROP TABLE IF EXISTS `b_hot_keys_code`;
CREATE TABLE `b_hot_keys_code` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CLASS_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMMENTS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TITLE_OBJ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_CUSTOM` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `ix_hot_keys_code_cn` (`CLASS_NAME`),
  KEY `ix_hot_keys_code_url` (`URL`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_hot_keys_code`
-- 


INSERT INTO `b_hot_keys_code` VALUES (3,'CAdminTabControl','NextTab();','HK_DB_CADMINTC','HK_DB_CADMINTC_C','tab-container','',0);
INSERT INTO `b_hot_keys_code` VALUES (5,'btn_new','var d=BX (\'btn_new\'); if (d) location.href = d.href;','HK_DB_BUT_ADD','HK_DB_BUT_ADD_C','btn_new','',0);
INSERT INTO `b_hot_keys_code` VALUES (6,'btn_excel','var d=BX(\'btn_excel\'); if (d) location.href = d.href;','HK_DB_BUT_EXL','HK_DB_BUT_EXL_C','btn_excel','',0);
INSERT INTO `b_hot_keys_code` VALUES (7,'btn_settings','var d=BX(\'btn_settings\'); if (d) location.href = d.href;','HK_DB_BUT_OPT','HK_DB_BUT_OPT_C','btn_settings','',0);
INSERT INTO `b_hot_keys_code` VALUES (8,'btn_list','var d=BX(\'btn_list\'); if (d) location.href = d.href;','HK_DB_BUT_LST','HK_DB_BUT_LST_C','btn_list','',0);
INSERT INTO `b_hot_keys_code` VALUES (9,'Edit_Save_Button','var d=BX .findChild(document, {attribute: {\'name\': \'save\'}}, true );  if (d) d.click();','HK_DB_BUT_SAVE','HK_DB_BUT_SAVE_C','Edit_Save_Button','',0);
INSERT INTO `b_hot_keys_code` VALUES (10,'btn_delete','var d=BX(\'btn_delete\'); if (d) location.href = d.href;','HK_DB_BUT_DEL','HK_DB_BUT_DEL_C','btn_delete','',0);
INSERT INTO `b_hot_keys_code` VALUES (12,'CAdminFilter','var d=BX .findChild(document, {attribute: {\'name\': \'find\'}}, true ); if (d) d.focus();','HK_DB_FLT_FND','HK_DB_FLT_FND_C','find','',0);
INSERT INTO `b_hot_keys_code` VALUES (13,'CAdminFilter','var d=BX .findChild(document, {attribute: {\'name\': \'set_filter\'}}, true );  if (d) d.click();','HK_DB_FLT_BUT_F','HK_DB_FLT_BUT_F_C','set_filter','',0);
INSERT INTO `b_hot_keys_code` VALUES (14,'CAdminFilter','var d=BX .findChild(document, {attribute: {\'name\': \'del_filter\'}}, true );  if (d) d.click();','HK_DB_FLT_BUT_CNL','HK_DB_FLT_BUT_CNL_C','del_filter','',0);
INSERT INTO `b_hot_keys_code` VALUES (15,'bx-panel-admin-button-help-icon-id','var d=BX(\'bx-panel-admin-button-help-icon-id\'); if (d) location.href = d.href;','HK_DB_BUT_HLP','HK_DB_BUT_HLP_C','bx-panel-admin-button-help-icon-id','',0);
INSERT INTO `b_hot_keys_code` VALUES (17,'Global','BXHotKeys.ShowSettings();','HK_DB_SHW_L','HK_DB_SHW_L_C','bx-panel-hotkeys','',0);
INSERT INTO `b_hot_keys_code` VALUES (19,'Edit_Apply_Button','var d=BX .findChild(document, {attribute: {\'name\': \'apply\'}}, true );  if (d) d.click();','HK_DB_BUT_APPL','HK_DB_BUT_APPL_C','Edit_Apply_Button','',0);
INSERT INTO `b_hot_keys_code` VALUES (20,'Edit_Cancel_Button','var d=BX .findChild(document, {attribute: {\'name\': \'cancel\'}}, true );  if (d) d.click();','HK_DB_BUT_CANCEL','HK_DB_BUT_CANCEL_C','Edit_Cancel_Button','',0);
INSERT INTO `b_hot_keys_code` VALUES (54,'top_panel_org_fav','','-=AUTONAME=-',NULL,'top_panel_org_fav',NULL,0);
INSERT INTO `b_hot_keys_code` VALUES (55,'top_panel_module_settings','','-=AUTONAME=-',NULL,'top_panel_module_settings','',0);
INSERT INTO `b_hot_keys_code` VALUES (56,'top_panel_interface_settings','','-=AUTONAME=-',NULL,'top_panel_interface_settings','',0);
INSERT INTO `b_hot_keys_code` VALUES (57,'top_panel_help','','-=AUTONAME=-',NULL,'top_panel_help','',0);
INSERT INTO `b_hot_keys_code` VALUES (58,'top_panel_bizproc_tasks','','-=AUTONAME=-',NULL,'top_panel_bizproc_tasks','',0);
INSERT INTO `b_hot_keys_code` VALUES (59,'top_panel_add_fav','','-=AUTONAME=-',NULL,'top_panel_add_fav',NULL,0);
INSERT INTO `b_hot_keys_code` VALUES (60,'top_panel_create_page','','-=AUTONAME=-',NULL,'top_panel_create_page','',0);
INSERT INTO `b_hot_keys_code` VALUES (62,'top_panel_create_folder','','-=AUTONAME=-',NULL,'top_panel_create_folder','',0);
INSERT INTO `b_hot_keys_code` VALUES (63,'top_panel_edit_page','','-=AUTONAME=-',NULL,'top_panel_edit_page','',0);
INSERT INTO `b_hot_keys_code` VALUES (64,'top_panel_page_prop','','-=AUTONAME=-',NULL,'top_panel_page_prop','',0);
INSERT INTO `b_hot_keys_code` VALUES (65,'top_panel_edit_page_html','','-=AUTONAME=-',NULL,'top_panel_edit_page_html','',0);
INSERT INTO `b_hot_keys_code` VALUES (67,'top_panel_edit_page_php','','-=AUTONAME=-',NULL,'top_panel_edit_page_php','',0);
INSERT INTO `b_hot_keys_code` VALUES (68,'top_panel_del_page','','-=AUTONAME=-',NULL,'top_panel_del_page','',0);
INSERT INTO `b_hot_keys_code` VALUES (69,'top_panel_folder_prop','','-=AUTONAME=-',NULL,'top_panel_folder_prop','',0);
INSERT INTO `b_hot_keys_code` VALUES (70,'top_panel_access_folder_new','','-=AUTONAME=-',NULL,'top_panel_access_folder_new','',0);
INSERT INTO `b_hot_keys_code` VALUES (71,'main_top_panel_struct_panel','','-=AUTONAME=-',NULL,'main_top_panel_struct_panel','',0);
INSERT INTO `b_hot_keys_code` VALUES (72,'top_panel_cache_page','','-=AUTONAME=-',NULL,'top_panel_cache_page','',0);
INSERT INTO `b_hot_keys_code` VALUES (73,'top_panel_cache_comp','','-=AUTONAME=-',NULL,'top_panel_cache_comp','',0);
INSERT INTO `b_hot_keys_code` VALUES (74,'top_panel_cache_not','','-=AUTONAME=-',NULL,'top_panel_cache_not','',0);
INSERT INTO `b_hot_keys_code` VALUES (75,'top_panel_edit_mode','','-=AUTONAME=-',NULL,'top_panel_edit_mode','',0);
INSERT INTO `b_hot_keys_code` VALUES (76,'top_panel_templ_site_css','','-=AUTONAME=-',NULL,'top_panel_templ_site_css','',0);
INSERT INTO `b_hot_keys_code` VALUES (77,'top_panel_templ_templ_css','','-=AUTONAME=-',NULL,'top_panel_templ_templ_css','',0);
INSERT INTO `b_hot_keys_code` VALUES (78,'top_panel_templ_site','','-=AUTONAME=-',NULL,'top_panel_templ_site','',0);
INSERT INTO `b_hot_keys_code` VALUES (81,'top_panel_debug_time','','-=AUTONAME=-',NULL,'top_panel_debug_time','',0);
INSERT INTO `b_hot_keys_code` VALUES (82,'top_panel_debug_incl','','-=AUTONAME=-',NULL,'top_panel_debug_incl','',0);
INSERT INTO `b_hot_keys_code` VALUES (83,'top_panel_debug_sql','','-=AUTONAME=-',NULL,'top_panel_debug_sql',NULL,0);
INSERT INTO `b_hot_keys_code` VALUES (84,'top_panel_debug_compr','','-=AUTONAME=-',NULL,'top_panel_debug_compr','',0);
INSERT INTO `b_hot_keys_code` VALUES (85,'MTP_SHORT_URI1','','-=AUTONAME=-',NULL,'MTP_SHORT_URI1','',0);
INSERT INTO `b_hot_keys_code` VALUES (86,'MTP_SHORT_URI_LIST','','-=AUTONAME=-',NULL,'MTP_SHORT_URI_LIST','',0);
INSERT INTO `b_hot_keys_code` VALUES (87,'FMST_PANEL_STICKER_ADD','','-=AUTONAME=-',NULL,'FMST_PANEL_STICKER_ADD','',0);
INSERT INTO `b_hot_keys_code` VALUES (88,'FMST_PANEL_STICKERS_SHOW','','-=AUTONAME=-',NULL,'FMST_PANEL_STICKERS_SHOW','',0);
INSERT INTO `b_hot_keys_code` VALUES (89,'FMST_PANEL_CUR_STICKER_LIST','','-=AUTONAME=-',NULL,'FMST_PANEL_CUR_STICKER_LIST','',0);
INSERT INTO `b_hot_keys_code` VALUES (90,'FMST_PANEL_ALL_STICKER_LIST','','-=AUTONAME=-',NULL,'FMST_PANEL_ALL_STICKER_LIST','',0);
INSERT INTO `b_hot_keys_code` VALUES (91,'top_panel_menu','var d=BX(\"bx-panel-menu\"); if (d) d.click();','-=AUTONAME=-',NULL,'bx-panel-menu','',0);
INSERT INTO `b_hot_keys_code` VALUES (92,'top_panel_admin','var d=BX(\'bx-panel-admin-tab\'); if (d) location.href = d.href;','-=AUTONAME=-',NULL,'bx-panel-admin-tab','',0);
INSERT INTO `b_hot_keys_code` VALUES (93,'admin_panel_site','var d=BX(\'bx-panel-view-tab\'); if (d) location.href = d.href;','-=AUTONAME=-',NULL,'bx-panel-view-tab','',0);
INSERT INTO `b_hot_keys_code` VALUES (94,'admin_panel_admin','var d=BX(\'bx-panel-admin-tab\'); if (d) location.href = d.href;','-=AUTONAME=-',NULL,'bx-panel-admin-tab','',0);
INSERT INTO `b_hot_keys_code` VALUES (96,'top_panel_folder_prop_new','','-=AUTONAME=-',NULL,'top_panel_folder_prop_new','',0);
INSERT INTO `b_hot_keys_code` VALUES (97,'main_top_panel_structure','','-=AUTONAME=-',NULL,'main_top_panel_structure','',0);
INSERT INTO `b_hot_keys_code` VALUES (98,'top_panel_clear_cache','','-=AUTONAME=-',NULL,'top_panel_clear_cache','',0);
INSERT INTO `b_hot_keys_code` VALUES (99,'top_panel_templ','','-=AUTONAME=-',NULL,'top_panel_templ','',0);
INSERT INTO `b_hot_keys_code` VALUES (100,'top_panel_debug','','-=AUTONAME=-',NULL,'top_panel_debug','',0);
INSERT INTO `b_hot_keys_code` VALUES (101,'MTP_SHORT_URI','','-=AUTONAME=-',NULL,'MTP_SHORT_URI','',0);
INSERT INTO `b_hot_keys_code` VALUES (102,'FMST_PANEL_STICKERS','','-=AUTONAME=-',NULL,'FMST_PANEL_STICKERS','',0);
INSERT INTO `b_hot_keys_code` VALUES (103,'top_panel_settings','','-=AUTONAME=-',NULL,'top_panel_settings','',0);
INSERT INTO `b_hot_keys_code` VALUES (104,'top_panel_fav','','-=AUTONAME=-',NULL,'top_panel_fav','',0);
INSERT INTO `b_hot_keys_code` VALUES (106,'Global','location.href=\'/bitrix/admin/hot_keys_list.php?lang=ru\';','HK_DB_SHW_HK','','','',0);
INSERT INTO `b_hot_keys_code` VALUES (107,'top_panel_edit_new','','-=AUTONAME=-',NULL,'top_panel_edit_new','',0);
INSERT INTO `b_hot_keys_code` VALUES (108,'FLOW_PANEL_CREATE_WITH_WF','','-=AUTONAME=-',NULL,'FLOW_PANEL_CREATE_WITH_WF','',0);
INSERT INTO `b_hot_keys_code` VALUES (109,'FLOW_PANEL_EDIT_WITH_WF','','-=AUTONAME=-',NULL,'FLOW_PANEL_EDIT_WITH_WF','',0);
INSERT INTO `b_hot_keys_code` VALUES (110,'FLOW_PANEL_HISTORY','','-=AUTONAME=-',NULL,'FLOW_PANEL_HISTORY','',0);
INSERT INTO `b_hot_keys_code` VALUES (111,'top_panel_create_new','','-=AUTONAME=-',NULL,'top_panel_create_new','',0);
INSERT INTO `b_hot_keys_code` VALUES (112,'top_panel_create_folder_new','','-=AUTONAME=-',NULL,'top_panel_create_folder_new','',0);
INSERT INTO `b_hot_keys_code` VALUES (116,'bx-panel-toggle','','-=AUTONAME=-',NULL,'bx-panel-toggle','',0);
INSERT INTO `b_hot_keys_code` VALUES (117,'bx-panel-small-toggle','','-=AUTONAME=-',NULL,'bx-panel-small-toggle','',0);
INSERT INTO `b_hot_keys_code` VALUES (118,'bx-panel-expander','var d=BX(\'bx-panel-expander\'); if (d) BX.fireEvent(d, \'click\');','-=AUTONAME=-',NULL,'bx-panel-expander','',0);
INSERT INTO `b_hot_keys_code` VALUES (119,'bx-panel-hider','var d=BX(\'bx-panel-hider\'); if (d) d.click();','-=AUTONAME=-',NULL,'bx-panel-hider','',0);
INSERT INTO `b_hot_keys_code` VALUES (120,'search-textbox-input','var d=BX(\'search-textbox-input\'); if (d) { d.click(); d.focus();}','-=AUTONAME=-','','search','',0);
INSERT INTO `b_hot_keys_code` VALUES (121,'bx-search-input','var d=BX(\'bx-search-input\'); if (d) { d.click(); d.focus(); }','-=AUTONAME=-','','bx-search-input','',0);
INSERT INTO `b_hot_keys_code` VALUES (133,'bx-panel-logout','var d=BX(\'bx-panel-logout\'); if (d) location.href = d.href;','-=AUTONAME=-','','bx-panel-logout','',0);
INSERT INTO `b_hot_keys_code` VALUES (135,'CDialog','var d=BX(\'cancel\'); if (d) d.click();','HK_DB_D_CANCEL','','cancel','',0);
INSERT INTO `b_hot_keys_code` VALUES (136,'CDialog','var d=BX(\'close\'); if (d) d.click();','HK_DB_D_CLOSE','','close','',0);
INSERT INTO `b_hot_keys_code` VALUES (137,'CDialog','var d=BX(\'savebtn\'); if (d) d.click();','HK_DB_D_SAVE','','savebtn','',0);
INSERT INTO `b_hot_keys_code` VALUES (138,'CDialog','var d=BX(\'btn_popup_save\'); if (d) d.click();','HK_DB_D_EDIT_SAVE','','btn_popup_save','',0);
INSERT INTO `b_hot_keys_code` VALUES (139,'Global','location.href=\'/bitrix/admin/user_admin.php?lang=\'+phpVars.LANGUAGE_ID;','HK_DB_SHW_U','','','',0);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock`
-- 




DROP TABLE IF EXISTS `b_iblock`;
CREATE TABLE `b_iblock` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IBLOCK_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `LIST_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DETAIL_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTION_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PICTURE` int(18) DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DESCRIPTION_TYPE` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `RSS_TTL` int(11) NOT NULL DEFAULT '24',
  `RSS_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `RSS_FILE_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `RSS_FILE_LIMIT` int(11) DEFAULT NULL,
  `RSS_FILE_DAYS` int(11) DEFAULT NULL,
  `RSS_YANDEX_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INDEX_ELEMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `INDEX_SECTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `WORKFLOW` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `BIZPROC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SECTION_CHOOSER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIST_MODE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RIGHTS_MODE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTION_PROPERTY` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(11) NOT NULL DEFAULT '1',
  `LAST_CONV_ELEMENT` int(11) NOT NULL DEFAULT '0',
  `SOCNET_GROUP_ID` int(18) DEFAULT NULL,
  `EDIT_FILE_BEFORE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDIT_FILE_AFTER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTIONS_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTION_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ELEMENTS_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ELEMENT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock` (`IBLOCK_TYPE_ID`,`LID`,`ACTIVE`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock`
-- 


INSERT INTO `b_iblock` VALUES (1,'2015-10-11 17:49:29','regions','s1','regions','Регионы','Y',500,'#SITE_DIR#/regions/index.php?ID=#IBLOCK_ID#','#SITE_DIR#/regions/detail.php?ID=#ID#','#SITE_DIR#/regions/list.php?SECTION_ID=#ID#',NULL,'','text',24,'Y','N',NULL,NULL,'N','23',NULL,'Y','Y','N','N','L','','S',NULL,1,0,NULL,'','','Разделы','Раздел','Регионы','Регион');
INSERT INTO `b_iblock` VALUES (2,'2014-10-27 12:45:08','address','s1','offices','Офисы','Y',500,'#SITE_DIR#/address/index.php?ID=#IBLOCK_ID#','#SITE_DIR#/address/detail.php?ID=#ID#','#SITE_DIR#/address/list.php?SECTION_ID=#ID#',NULL,'','text',24,'Y','N',NULL,NULL,'N','19',NULL,'Y','Y','N','N','L','','S','N',1,0,NULL,'','','Разделы','Раздел','Офисы','Офис');
INSERT INTO `b_iblock` VALUES (3,'2013-10-10 06:02:37','news','s1','news','Новости','Y',500,'#SITE_DIR#/news.php?ID=#IBLOCK_ID#','#SITE_DIR#/news.php?ID=#ID#','#SITE_DIR#/news.php?SECTION_ID=#ID#',NULL,'','text',24,'Y','N',NULL,NULL,'N','12','e9b688b9c6c1c5574597a69e69938250','Y','Y','N','N','L','','S','N',1,0,NULL,'','','Разделы','Раздел','Новости','Нововсть');
INSERT INTO `b_iblock` VALUES (4,'2013-10-10 11:38:32','reviews','s1','reviews','Отзывы','Y',500,'#SITE_DIR#/reviews.php?ID=#IBLOCK_ID#','#SITE_DIR#/reviews.php?ID=#ID#','#SITE_DIR#/reviews.php?SECTION_ID=#ID#',NULL,'Отзывы','html',24,'Y','N',NULL,NULL,'N','13',NULL,'Y','Y','N','N','L','','E','N',1,0,NULL,'','','Разделы','Раздел','Отзывы','Отзыв');
INSERT INTO `b_iblock` VALUES (5,'2013-09-26 10:37:41','vacancy','s1','vacancies','Вакансии','Y',500,'#SITE_DIR#/vacanies/index.php?ID=#IBLOCK_ID#','#SITE_DIR#/vacanies/detail.php?ID=#ID#','#SITE_DIR#/vacanies/list.php?SECTION_ID=#ID#',NULL,NULL,'text',24,'Y','N',NULL,NULL,'N','20',NULL,'Y','Y','N','N','L',NULL,'S','Y',1,0,NULL,NULL,NULL,'Разделы','Раздел','Вакансии','Вакансия');
INSERT INTO `b_iblock` VALUES (6,'2013-09-26 10:38:05','affiliatted','s1','affiliated','Аффилированные лица','Y',500,'#SITE_DIR#/affiliatted/index.php?ID=#IBLOCK_ID#','#SITE_DIR#/affiliatted/detail.php?ID=#ID#','#SITE_DIR#/affiliatted/list.php?SECTION_ID=#ID#',NULL,NULL,'text',24,'Y','N',NULL,NULL,'N','22',NULL,'Y','Y','N','N','L',NULL,'S','Y',1,0,NULL,NULL,NULL,'Разделы','Раздел','Элементы','Элемент');
INSERT INTO `b_iblock` VALUES (7,'2013-10-15 07:39:57','requests','s1','product1','Продукт 1','Y',500,'#SITE_DIR#/requests/index.php?ID=#IBLOCK_ID#','#SITE_DIR#/requests/detail.php?ID=#ID#','#SITE_DIR#/requests/list.php?SECTION_ID=#ID#',NULL,'','text',24,'Y','N',NULL,NULL,'N',NULL,NULL,'Y','Y','N','N','L','','S','N',1,0,NULL,'','','Разделы','Раздел','Элементы','Элемент');
INSERT INTO `b_iblock` VALUES (8,'2015-10-11 14:29:33','content','s1','','Слайдеры','Y',500,'','','',NULL,'','text',24,'Y','N',NULL,NULL,'N',NULL,'61938f21db5d1b6fc0d79f183cd331a1','Y','Y','N','N','L','','S','N',1,0,NULL,'','','Слайдеры','Слайдер','Иконки','Иконка');
INSERT INTO `b_iblock` VALUES (9,'2015-10-11 15:16:28','content','s1','partners','Наши партнеры','Y',500,'','','',NULL,'','text',24,'Y','N',NULL,NULL,'N',NULL,NULL,'Y','Y','N','N','L','','S','N',1,0,NULL,'','','Разделы','Раздел','Партнеры','Партнер');
INSERT INTO `b_iblock` VALUES (11,'2015-10-14 01:14:49','content','s1','','Займы','Y',500,'','','',NULL,'','text',24,'Y','N',NULL,NULL,'N',NULL,NULL,'Y','Y','N','N','L','','S','N',1,0,NULL,'','','Разделы','Раздел','Займы','Займ');
INSERT INTO `b_iblock` VALUES (12,'2015-10-12 11:18:46','content','s1','','Наши преимущества','Y',500,'','','',NULL,'','text',24,'Y','N',NULL,NULL,'N',NULL,NULL,'Y','Y','N','N','L','','S','N',1,0,NULL,'','','Разделы','Раздел','Преимущества','Преимущество');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_cache`
-- 




DROP TABLE IF EXISTS `b_iblock_cache`;
CREATE TABLE `b_iblock_cache` (
  `CACHE_KEY` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `CACHE` longtext COLLATE utf8_unicode_ci NOT NULL,
  `CACHE_DATE` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`CACHE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_element`
-- 




DROP TABLE IF EXISTS `b_iblock_element`;
CREATE TABLE `b_iblock_element` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `IBLOCK_ID` int(11) NOT NULL DEFAULT '0',
  `IBLOCK_SECTION_ID` int(11) DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ACTIVE_FROM` datetime DEFAULT NULL,
  `ACTIVE_TO` datetime DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PREVIEW_PICTURE` int(18) DEFAULT NULL,
  `PREVIEW_TEXT` text COLLATE utf8_unicode_ci,
  `PREVIEW_TEXT_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `DETAIL_PICTURE` int(18) DEFAULT NULL,
  `DETAIL_TEXT` longtext COLLATE utf8_unicode_ci,
  `DETAIL_TEXT_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `SEARCHABLE_CONTENT` text COLLATE utf8_unicode_ci,
  `WF_STATUS_ID` int(18) DEFAULT '1',
  `WF_PARENT_ELEMENT_ID` int(11) DEFAULT NULL,
  `WF_NEW` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WF_LOCKED_BY` int(18) DEFAULT NULL,
  `WF_DATE_LOCK` datetime DEFAULT NULL,
  `WF_COMMENTS` text COLLATE utf8_unicode_ci,
  `IN_SECTIONS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAGS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WF_LAST_HISTORY_ID` int(11) DEFAULT NULL,
  `SHOW_COUNTER` int(18) DEFAULT NULL,
  `SHOW_COUNTER_START` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_element_1` (`IBLOCK_ID`,`IBLOCK_SECTION_ID`),
  KEY `ix_iblock_element_4` (`IBLOCK_ID`,`XML_ID`,`WF_PARENT_ELEMENT_ID`),
  KEY `ix_iblock_element_3` (`WF_PARENT_ELEMENT_ID`),
  KEY `ix_iblock_element_code` (`IBLOCK_ID`,`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_element`
-- 


INSERT INTO `b_iblock_element` VALUES (7,'2014-10-30 00:35:22',1,'2013-09-26 18:38:05',1,6,NULL,'Y',NULL,NULL,500,'Смирнов Алексей Вадимович',NULL,'','text',NULL,'','text','СМИРНОВ АЛЕКСЕЙ ВАДИМОВИЧ\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','429','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (17,'2015-10-11 17:56:12',1,'2014-03-21 20:50:09',1,1,NULL,'Y',NULL,NULL,500,'Москва',NULL,'','text',NULL,'','text','Москва\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','17','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (18,'2014-10-30 00:05:09',1,'2014-03-21 20:54:38',1,2,NULL,'Y',NULL,NULL,500,'1.2. ОП «ЛЮБЛИНО»',NULL,'','text',NULL,'','text','1.2. ОП «ЛЮБЛИНО»\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','18','','','0',NULL,11,'2014-04-18 15:20:54');
INSERT INTO `b_iblock_element` VALUES (21,'2014-07-18 16:40:43',1,'2014-07-18 16:40:43',1,3,NULL,'Y','2014-07-18 16:40:12',NULL,500,'Тестовая новость',NULL,'','text',NULL,'','text','Тестовая новость\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','21','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (22,'2014-07-18 16:41:27',1,'2014-07-18 16:41:27',1,3,1,'Y','2014-07-18 16:41:13',NULL,500,'ываываыва',NULL,'','text',NULL,'ываываыва ыв ыв аыва ыв ','text','ываываыва\r\n\r\nываываыва ыв ыв аыва ыв ',1,NULL,NULL,NULL,NULL,NULL,'Y','22','','','0',NULL,10,'2014-07-30 07:12:36');
INSERT INTO `b_iblock_element` VALUES (24,'2014-10-30 00:03:02',1,'2014-10-27 23:21:55',1,2,NULL,'Y',NULL,NULL,500,'1.1. ОП «МЕНДЕЛЕЕВСКАЯ»',NULL,'','text',NULL,'','text','1.1. ОП «МЕНДЕЛЕЕВСКАЯ»\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','24','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (25,'2014-10-30 00:06:25',1,'2014-10-29 23:05:43',1,2,NULL,'Y',NULL,NULL,500,'ОП «ЭЛЕКТРОЗАВОДСКАЯ»',NULL,'','text',NULL,'','text','ОП «ЭЛЕКТРОЗАВОДСКАЯ»\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','25','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (26,'2014-10-30 00:35:43',1,'2014-10-30 00:35:43',1,6,NULL,'Y',NULL,NULL,500,'Сахарова Олеся Дмитриевна',NULL,'','text',NULL,'','text','САХАРОВА ОЛЕСЯ ДМИТРИЕВНА\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','26','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (27,'2014-10-30 01:30:46',1,'2014-10-30 00:52:14',1,5,NULL,'Y','2014-10-30 00:00:00',NULL,500,'Финансовый специалист / Кредитный специалист',NULL,'','text',NULL,' 	 	 	 	 \r\n<style type=\"text/css\">h2 { margin-top: 0in; margin-bottom: 0in; direction: ltr; line-height: 100%; text-align: left; widows: 2; orphans: 2; page-break-after: auto; }h2.western { font-family: &quot;Calibri&quot;,serif; font-size: 11pt; font-weight: normal; }h2.cjk { font-family: &quot;Droid Sans Fallback&quot;; font-size: 11pt; font-weight: normal; }h2.ctl { font-family: &quot;Calibri&quot;; font-size: 11pt; font-weight: normal; }p { margin-bottom: 0.1in; direction: ltr; line-height: 120%; text-align: left; widows: 2; orphans: 2; }a:link { color: rgb(0, 0, 255); }</style>\r\n \r\n<h2 class=\"western\" style=\"margin-top: 0.19in; margin-bottom: 0.19in;\"> <font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Должностные обязанности</font></font></h2>\r\n \r\n<ul> \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Консультация 	потенциальных клиентов по условиям 	предоставления и погашения займов;</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Первичная 	оценка платежеспособности клиента;</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Оформление 	договора займа с клиентом в офисе 	продаж;</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Выдача/прием 	денежных средств по обязательствам 	договора займа;</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Полное 	сопровождение клиентов от момента 	подачи заявки до погашения займа;</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Ведение 	внутреннего документооборота и 	отчетности;</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Выполнение 	плановых показателей и поставленных 	задач.</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Контроль 	за финансовой дисциплиной: движение 	(пополнение, инкассация) денежных 	средств.</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Сверки 	и сбор документов по поставщикам</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Материальная 	ответственность за денежные средства.</font></font></font></font></font></p>\r\n   </li>\r\n </ul>\r\n \r\n<br />\r\n \r\n<h2 class=\"western\" style=\"margin-top: 0.19in; margin-bottom: 0.19in;\"> <font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Требуется</font></font></font></h2>\r\n \r\n<ul> \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Возраст: 	от 23 до 43 лет </font></font></font></font></font> 	</p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Образование: 	высшее.</font></font></font></font></font></p>\r\n   </li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Знание 	ПК</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Как 	преимущество опыт работы:</font></font></font></font></font></p>\r\n   </li>\r\n </ul>\r\n \r\n<p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> <font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">- в области активных продаж;</font></font></font></font></font></p>\r\n \r\n<p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> <font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">- банковской или страховой сфере.</font></font></font></font></font></p>\r\n \r\n<ul> \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Желание 	зарабатывать и развиваться в Финансовой 	сфере.</font></font></font></font></font></p>\r\n   </li>\r\n </ul>\r\n \r\n<table cellspacing=\"0\" width=\"544\" height=\"1\" cellpadding=\"0\" bgcolor=\"#ffffff\"> 	<colgroup><col width=\"128*\"></col> 	<col width=\"128*\"></col> 	</colgroup> \r\n  <tbody> \r\n    <tr valign=\"top\"> 		<td width=\"50%\" bgcolor=\"#ffffff\" style=\"border: medium none; padding: 0in;\"></td><td width=\"50%\" bgcolor=\"#ffffff\" style=\"border: medium none; padding: 0in;\"> \r\n        <br />\r\n       </td></tr>\r\n   </tbody>\r\n </table>\r\n \r\n<h2 class=\"western\" style=\"margin-top: 0.19in; margin-bottom: 0.19in;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Мы предлагаем</font></font></font></h2>\r\n \r\n<ul> \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Место 	работы: офисы продаж</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Оборудованное 	рабочее место по стандартам компании;</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">График 	работы 2/2 (с 9:00-21:00);</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Доход: 	оклад+ премия от 23 000 до 56 000 руб. 	(фиксированный оклад + ежемесячная 	премия);</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Соблюдение 	ТК РФ, &laquo;белая&raquo; заработная плата;</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Испытательный 	срок 3 месяца</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Обучение 	работе, профессиональная поддержка;</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Развитие 	и карьерный рост;</font></font></font></font></font></p>\r\n   	</li>\r\n \r\n  <li> \r\n    <p style=\"margin-top: 0.19in; margin-bottom: 0.19in; background: none repeat scroll 0% 0% rgb(255, 255, 255); line-height: 100%;\"> 	<font face=\"Times New Roman, serif\"><font size=\"3\" style=\"font-size: 12pt;\"><font color=\"#000000\"><font face=\"Arial, serif\"><font size=\"1\" style=\"font-size: 8pt;\">Работа 	в дружном и сплоченном коллективе.</font></font></font></font></font></p>\r\n   </li>\r\n </ul>\r\n \r\n<h2 class=\"western\" style=\"margin-top: 0.19in; margin-bottom: 0.19in; font-weight: normal;\"> \r\n  <br />\r\n \r\n  <br />\r\n </h2>\r\n \r\n<p style=\"margin-bottom: 0in; line-height: 100%;\"> \r\n  <br />\r\n </p>\r\n \r\n<p style=\"margin-bottom: 0in; line-height: 100%;\"> \r\n  <br />\r\n </p>\r\n ','html','ФИНАНСОВЫЙ СПЕЦИАЛИСТ / КРЕДИТНЫЙ СПЕЦИАЛИСТ\r\n\r\nДОЛЖНОСТНЫЕ ОБЯЗАННОСТИ \r\n\r\n- \r\n\r\nКОНСУЛЬТАЦИЯ ПОТЕНЦИАЛЬНЫХ КЛИЕНТОВ ПО \r\nУСЛОВИЯМ ПРЕДОСТАВЛЕНИЯ И ПОГАШЕНИЯ ЗАЙМОВ; \r\n- \r\n\r\nПЕРВИЧНАЯ ОЦЕНКА ПЛАТЕЖЕСПОСОБНОСТИ КЛИЕНТА; \r\n- \r\n\r\nОФОРМЛЕНИЕ ДОГОВОРА ЗАЙМА С КЛИЕНТОМ В \r\nОФИСЕ ПРОДАЖ; \r\n- \r\n\r\nВЫДАЧА/ПРИЕМ ДЕНЕЖНЫХ СРЕДСТВ ПО ОБЯЗАТЕЛЬСТВАМ \r\nДОГОВОРА ЗАЙМА; \r\n- \r\n\r\nПОЛНОЕ СОПРОВОЖДЕНИЕ КЛИЕНТОВ ОТ МОМЕНТА \r\nПОДАЧИ ЗАЯВКИ ДО ПОГАШЕНИЯ ЗАЙМА; \r\n- \r\n\r\nВЕДЕНИЕ ВНУТРЕННЕГО ДОКУМЕНТООБОРОТА \r\nИ ОТЧЕТНОСТИ; \r\n- \r\n\r\nВЫПОЛНЕНИЕ ПЛАНОВЫХ ПОКАЗАТЕЛЕЙ И ПОСТАВЛЕННЫХ \r\nЗАДАЧ. \r\n- \r\n\r\nКОНТРОЛЬ ЗА ФИНАНСОВОЙ ДИСЦИПЛИНОЙ: ДВИЖЕНИЕ \r\n(ПОПОЛНЕНИЕ, ИНКАССАЦИЯ) ДЕНЕЖНЫХ СРЕДСТВ. \r\n- \r\n\r\nСВЕРКИ И СБОР ДОКУМЕНТОВ ПО ПОСТАВЩИКАМ \r\n- \r\n\r\nМАТЕРИАЛЬНАЯ ОТВЕТСТВЕННОСТЬ ЗА ДЕНЕЖНЫЕ \r\nСРЕДСТВА. \r\n ТРЕБУЕТСЯ \r\n\r\n- \r\n\r\nВОЗРАСТ: ОТ 23 ДО 43 ЛЕТ \r\n- \r\n\r\nОБРАЗОВАНИЕ: ВЫСШЕЕ. \r\n- \r\n\r\nЗНАНИЕ ПК \r\n- \r\n\r\nКАК ПРЕИМУЩЕСТВО ОПЫТ РАБОТЫ: \r\n\r\n- В ОБЛАСТИ АКТИВНЫХ ПРОДАЖ; \r\n\r\n- БАНКОВСКОЙ ИЛИ СТРАХОВОЙ СФЕРЕ. \r\n\r\n- \r\n\r\nЖЕЛАНИЕ ЗАРАБАТЫВАТЬ И РАЗВИВАТЬСЯ В ФИНАНСОВОЙ \r\nСФЕРЕ. \r\n      \r\n    \r\n МЫ ПРЕДЛАГАЕМ \r\n\r\n- \r\n\r\nМЕСТО РАБОТЫ: ОФИСЫ ПРОДАЖ \r\n- \r\n\r\nОБОРУДОВАННОЕ РАБОЧЕЕ МЕСТО ПО СТАНДАРТАМ \r\nКОМПАНИИ; \r\n- \r\n\r\nГРАФИК РАБОТЫ 2/2 (С 9:00-21:00); \r\n- \r\n\r\nДОХОД: ОКЛАД+ ПРЕМИЯ ОТ 23 000 ДО 56 000 РУБ. \r\n(ФИКСИРОВАННЫЙ ОКЛАД + ЕЖЕМЕСЯЧНАЯ ПРЕМИЯ); \r\n- \r\n\r\nСОБЛЮДЕНИЕ ТК РФ, &LAQUO;БЕЛАЯ&RAQUO; ЗАРАБОТНАЯ \r\nПЛАТА; \r\n- \r\n\r\nИСПЫТАТЕЛЬНЫЙ СРОК 3 МЕСЯЦА \r\n- \r\n\r\nОБУЧЕНИЕ РАБОТЕ, ПРОФЕССИОНАЛЬНАЯ ПОДДЕРЖКА; \r\n- \r\n\r\nРАЗВИТИЕ И КАРЬЕРНЫЙ РОСТ; \r\n- \r\n\r\nРАБОТА В ДРУЖНОМ И СПЛОЧЕННОМ КОЛЛЕКТИВЕ.',1,NULL,NULL,NULL,NULL,NULL,'N','27','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (28,'2015-01-22 05:50:07',0,'2015-01-22 05:50:07',0,4,NULL,'N','2015-01-22 14:50:07',NULL,500,'Иванов',NULL,'Тест','text',NULL,NULL,'text','Иванов\r\nТест',1,NULL,NULL,NULL,NULL,NULL,'N','28',NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (29,'2015-03-13 08:26:57',0,'2015-03-13 08:26:57',0,4,NULL,'N','2015-03-13 16:26:57',NULL,500,'Дыркин',NULL,'Жопа. Из долгов не выбраться. Не берите.','text',NULL,NULL,'text','Дыркин\r\nЖопа. Из долгов не выбраться. Не берите.',1,NULL,NULL,NULL,NULL,NULL,'N','29',NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (30,'2015-03-27 19:17:51',0,'2015-03-27 19:17:51',0,4,NULL,'N','2015-03-28 03:17:51',NULL,500,'asddas',NULL,'ssadssa','text',NULL,NULL,'text','ASDDAS\r\nSSADSSA',1,NULL,NULL,NULL,NULL,NULL,'N','30',NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (31,'2015-04-08 06:09:09',0,'2015-04-08 06:09:09',0,4,NULL,'N','2015-04-08 14:09:09',NULL,500,'Инкогнито',NULL,'Кредит не выдаёт, не ведитесь товарищи, это лажа!!!','text',NULL,NULL,'text','Инкогнито\r\nКредит не выдаёт, не ведитесь товарищи, это лажа!!!',1,NULL,NULL,NULL,NULL,NULL,'N','31',NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (32,'2015-04-29 03:50:21',0,'2015-04-29 03:50:21',0,4,NULL,'N','2015-04-29 11:50:21',NULL,500,'WillisHorb',NULL,'atlanta medical center jobs  <a href=http://mayk.org.uk/levitra-uk.html>Levitra UK</a>  medicine for irritable bowel syndrome ','text',NULL,NULL,'text','WILLISHORB\r\nATLANTA MEDICAL CENTER JOBS  <A HREF=HTTP://MAYK.ORG.UK/LEVITRA-UK.HTML>LEVITRA UK</A>  MEDICINE FOR IRRITABLE BOWEL SYNDROME ',1,NULL,NULL,NULL,NULL,NULL,'N','32',NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (33,'2015-04-29 23:07:52',0,'2015-04-29 23:07:52',0,4,NULL,'N','2015-04-30 07:07:52',NULL,500,'WillisHorb',NULL,'texas tech university school of medicine  <a href=http://mayk.org.uk/levitra-uk.html>levitra online</a>  lahey clinic burlington doctors ','text',NULL,NULL,'text','WILLISHORB\r\nTEXAS TECH UNIVERSITY SCHOOL OF MEDICINE  <A HREF=HTTP://MAYK.ORG.UK/LEVITRA-UK.HTML>LEVITRA ONLINE</A>  LAHEY CLINIC BURLINGTON DOCTORS ',1,NULL,NULL,NULL,NULL,NULL,'N','33',NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (34,'2015-05-04 09:39:18',0,'2015-05-04 09:39:18',0,4,NULL,'N','2015-05-04 17:39:18',NULL,500,'Peter',NULL,'Furrealz? That\'s mavlelousry good to know.','text',NULL,NULL,'text','PETER\r\nFURREALZ? THAT\'S MAVLELOUSRY GOOD TO KNOW.',1,NULL,NULL,NULL,NULL,NULL,'N','34',NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (35,'2015-05-20 21:33:00',0,'2015-05-20 21:33:00',0,4,NULL,'N','2015-05-21 05:33:00',NULL,500,'Carol',NULL,'You\'ve imseseprd us all with that posting!','text',NULL,NULL,'text','CAROL\r\nYOU\'VE IMSESEPRD US ALL WITH THAT POSTING!',1,NULL,NULL,NULL,NULL,NULL,'N','35',NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (36,'2015-07-10 04:58:33',0,'2015-07-10 04:58:33',0,4,NULL,'N','2015-07-10 12:58:33',NULL,500,'LenardDiz',NULL,'wells fargo auto refinance  <a href=http://ambris.pl/kolekcje/cialis.html>cialis bez recepty w aptece</a>  ashtabula county medical center ','text',NULL,NULL,'text','LENARDDIZ\r\nWELLS FARGO AUTO REFINANCE  <A HREF=HTTP://AMBRIS.PL/KOLEKCJE/CIALIS.HTML>CIALIS BEZ RECEPTY W APTECE</A>  ASHTABULA COUNTY MEDICAL CENTER ',1,NULL,NULL,NULL,NULL,NULL,'N','36',NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (37,'2015-07-18 18:16:12',0,'2015-07-18 18:16:12',0,4,NULL,'N','2015-07-19 01:16:12',NULL,500,'LenardDiz',NULL,'i need a doctor video  <a href=http://placa.hr/viagra-cijena.html>viagra online</a>  new drug for diabetes ','text',NULL,NULL,'text','LENARDDIZ\r\nI NEED A DOCTOR VIDEO  <A HREF=HTTP://PLACA.HR/VIAGRA-CIJENA.HTML>VIAGRA ONLINE</A>  NEW DRUG FOR DIABETES ',1,NULL,NULL,NULL,NULL,NULL,'N','37',NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (42,'2015-10-11 14:32:17',1,'2015-10-11 14:32:17',1,8,2,'Y',NULL,NULL,500,'Оформите заявку',10,'У Вас это займет не больше 10 минут','text',NULL,'','text','Оформите заявку\r\nУ Вас это займет не больше 10 минут\r\n',1,NULL,NULL,NULL,NULL,NULL,'Y','42','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (43,'2015-10-11 14:37:23',1,'2015-10-11 14:33:25',1,8,2,'Y',NULL,NULL,500,'Получите ответ',11,'Мы дадим ответ в течение <br> <strong>20 минут</strong>','html',NULL,'','text','Получите ответ\r\nМы дадим ответ в течение \r\n20 минут\r\n',1,NULL,NULL,NULL,NULL,NULL,'Y','43','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (44,'2015-10-11 14:34:04',1,'2015-10-11 14:34:04',1,8,2,'Y',NULL,NULL,500,'Мгновенно получите деньги',12,'Любым удобным для вас способом!','text',NULL,'','text','Мгновенно получите деньги\r\nЛюбым удобным для вас способом!\r\n',1,NULL,NULL,NULL,NULL,NULL,'Y','44','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (46,'2015-10-11 15:22:18',1,'2015-10-11 15:22:18',1,9,NULL,'Y',NULL,NULL,500,'Visa',14,'','text',NULL,'','text','VISA\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','46','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (47,'2015-10-11 15:22:40',1,'2015-10-11 15:22:40',1,9,NULL,'Y',NULL,NULL,500,'MasterCard',15,'','text',NULL,'','text','MASTERCARD\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','47','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (48,'2015-10-11 15:23:09',1,'2015-10-11 15:23:09',1,9,NULL,'Y',NULL,NULL,500,'Qiwi',16,'','text',NULL,'','text','QIWI\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','48','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (49,'2015-10-11 15:23:35',1,'2015-10-11 15:23:35',1,9,NULL,'Y',NULL,NULL,500,'Мир',17,'','text',NULL,'','text','Мир\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','49','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (50,'2015-10-11 15:23:56',1,'2015-10-11 15:23:56',1,9,NULL,'Y',NULL,NULL,500,'NA',18,'','text',NULL,'','text','NA\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','50','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (51,'2015-10-11 15:24:23',1,'2015-10-11 15:24:23',1,9,NULL,'Y',NULL,NULL,500,'Contact',19,'','text',NULL,'','text','CONTACT\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','51','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (52,'2015-10-11 17:50:01',1,'2015-10-11 17:50:01',1,1,NULL,'Y',NULL,NULL,500,'Казань',NULL,'','text',NULL,'','text','Казань\r\n\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','52','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (57,'2015-10-11 20:24:49',1,'2015-10-11 20:22:21',1,11,NULL,'Y',NULL,NULL,100,'ЭКСПРЕСС',20,'от 1000 до 5 000 рублей для клиентов количество полученных займов которых составляет от 1 до 3','text',NULL,'Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Loreum 1','text','ЭКСПРЕСС\r\nот 1000 до 5 000 рублей для клиентов количество полученных займов которых составляет от 1 до 3\r\nДалеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. LOREUM 1',1,NULL,NULL,NULL,NULL,NULL,'N','57','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (58,'2015-10-11 20:26:16',1,'2015-10-11 20:25:30',1,11,NULL,'Y',NULL,NULL,500,'До зарплаты',21,'от 1000 до 10000 рублей для клиентов количество полученных займов которых составляет от 4 до 5','text',NULL,'2 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана.','text','До зарплаты\r\nот 1000 до 10000 рублей для клиентов количество полученных займов которых составляет от 4 до 5\r\n2 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана.',1,NULL,NULL,NULL,NULL,NULL,'N','58','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (59,'2015-10-11 23:01:28',1,'2015-10-11 20:27:01',1,11,NULL,'Y',NULL,NULL,500,'Клубный',22,'от 1000 до 20 000 рублей для постоянных клиентов количество полученных займов которых составляет от 6 и более','text',NULL,'3 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана.','text','Клубный\r\nот 1000 до 20 000 рублей для постоянных клиентов количество полученных займов которых составляет от 6 и более\r\n3 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана.',1,NULL,NULL,NULL,NULL,NULL,'N','59','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (60,'2015-10-11 23:13:52',1,'2015-10-11 23:13:52',1,11,NULL,'Y',NULL,NULL,500,'Офицерский',23,'от 1 000 до 25 000 на срок до одного года','text',NULL,'Приглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас. ','text','Офицерский\r\nот 1 000 до 25 000 на срок до одного года\r\nПриглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас. ',1,NULL,NULL,NULL,NULL,NULL,'N','60','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (61,'2015-10-11 23:17:46',1,'2015-10-11 23:14:48',1,11,NULL,'Y',NULL,NULL,500,'Пенсионный',24,'от 1000 до 10 000 на срок до 6 месяцев','text',NULL,'5. Приглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас. ','text','Пенсионный\r\nот 1000 до 10 000 на срок до 6 месяцев\r\n5. Приглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас. ',1,NULL,NULL,NULL,NULL,NULL,'N','61','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (62,'2015-10-11 23:17:56',1,'2015-10-11 23:16:43',1,11,NULL,'Y',NULL,NULL,500,'Деньги на дом',25,'микрофинансирование от 1000 до 20 000 рублей','text',NULL,'6. Приглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас. ','text','Деньги на дом\r\nмикрофинансирование от 1000 до 20 000 рублей\r\n6. Приглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас. ',1,NULL,NULL,NULL,NULL,NULL,'N','62','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (63,'2015-10-12 11:19:24',1,'2015-10-12 11:19:24',1,12,NULL,'Y',NULL,NULL,500,'Преимущество 1',26,'краткое описание <br> преимущества','html',NULL,'','text','ПРЕИМУЩЕСТВО 1\r\nКРАТКОЕ ОПИСАНИЕ \r\nПРЕИМУЩЕСТВА\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','63','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (64,'2015-10-12 11:19:35',1,'2015-10-12 11:19:35',1,12,NULL,'Y',NULL,NULL,500,'Преимущество 1',27,'краткое описание <br> преимущества','html',NULL,'','text','ПРЕИМУЩЕСТВО 1\r\nКРАТКОЕ ОПИСАНИЕ \r\nПРЕИМУЩЕСТВА\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','64','','','0',NULL,NULL,NULL);
INSERT INTO `b_iblock_element` VALUES (65,'2015-10-12 11:19:50',1,'2015-10-12 11:19:50',1,12,NULL,'Y',NULL,NULL,500,'Преимущество 1',28,'краткое описание <br> преимущества','html',NULL,'','text','ПРЕИМУЩЕСТВО 1\r\nКРАТКОЕ ОПИСАНИЕ \r\nПРЕИМУЩЕСТВА\r\n',1,NULL,NULL,NULL,NULL,NULL,'N','65','','','0',NULL,NULL,NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_element_lock`
-- 




DROP TABLE IF EXISTS `b_iblock_element_lock`;
CREATE TABLE `b_iblock_element_lock` (
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `DATE_LOCK` datetime DEFAULT NULL,
  `LOCKED_BY` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_element_property`
-- 




DROP TABLE IF EXISTS `b_iblock_element_property`;
CREATE TABLE `b_iblock_element_property` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_PROPERTY_ID` int(11) NOT NULL,
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `VALUE_TYPE` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `VALUE_ENUM` int(11) DEFAULT NULL,
  `VALUE_NUM` decimal(18,4) DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_element_property_1` (`IBLOCK_ELEMENT_ID`,`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_element_property_2` (`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_element_prop_enum` (`VALUE_ENUM`,`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_element_prop_num` (`VALUE_NUM`,`IBLOCK_PROPERTY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_element_property`
-- 


INSERT INTO `b_iblock_element_property` VALUES (33,15,7,'-','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (34,16,7,'Долевой участник','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (35,17,7,'-','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (36,18,7,'-','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (49,1,18,'17','text',NULL,17.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (51,3,18,'109559, Москва, Совхозная улица, д.16 ','text',NULL,109559.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (52,4,18,'55.679031243829,37.763801373016','text',NULL,55.6790,'');
INSERT INTO `b_iblock_element_property` VALUES (71,21,18,'lublino@pluskredit.ru','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (72,22,18,'8(499)600-5877 доб. 222 / 223','text',NULL,8.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (74,7,18,'Ежедневно. 9.00-21.00','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (87,1,24,'17','text',NULL,17.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (89,3,24,'127055, г. Москва, ул. Новослободская, д. 14/19 стр.1, офис 26','text',NULL,127055.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (90,4,24,'55.78133428788,37.600431971247','text',NULL,55.7813,'');
INSERT INTO `b_iblock_element_property` VALUES (91,7,24,'Ежедневно. 9.00-21.00','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (93,21,24,'mendeleevskaya@pluskredit.ru','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (94,22,24,'8(499)600-5877 доб. 220','text',NULL,8.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (98,1,25,'17','text',NULL,17.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (99,3,25,'107023, Москва, Большая Семеновская, д. 28 ','text',NULL,107023.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (100,4,25,'55.781941292712,37.707166784061','text',NULL,55.7819,'');
INSERT INTO `b_iblock_element_property` VALUES (101,7,25,'Ежедневно. 9.00-21.00','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (102,21,25,'elektrozavodskaya@pluskredit.ru','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (103,22,25,'8(499)600-5877 доб. 224 / 225','text',NULL,8.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (104,15,26,'-','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (105,16,26,'Долевой участник','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (106,17,26,'-','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (107,18,26,'-','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (108,10,27,'от 23 000 до 56 000 руб. (фиксированный оклад + ежемесячная премия)','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (109,11,27,'17','text',NULL,17.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (110,12,27,'Как преимущество','text',NULL,0.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (111,13,27,'2/2 (с 9:00-21:00)','text',NULL,2.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (115,14,27,'7','text',7,NULL,NULL);
INSERT INTO `b_iblock_element_property` VALUES (116,19,28,'Иван','text',NULL,0.0000,NULL);
INSERT INTO `b_iblock_element_property` VALUES (117,19,29,'Вася','text',NULL,0.0000,NULL);
INSERT INTO `b_iblock_element_property` VALUES (118,19,30,'sasdsa','text',NULL,0.0000,NULL);
INSERT INTO `b_iblock_element_property` VALUES (119,19,31,'Матьего','text',NULL,0.0000,NULL);
INSERT INTO `b_iblock_element_property` VALUES (120,19,32,'WillisHorbSH','text',NULL,0.0000,NULL);
INSERT INTO `b_iblock_element_property` VALUES (121,19,33,'WillisHorbSH','text',NULL,0.0000,NULL);
INSERT INTO `b_iblock_element_property` VALUES (122,19,34,'EAAZnStH','text',NULL,0.0000,NULL);
INSERT INTO `b_iblock_element_property` VALUES (123,19,35,'R7bvuMTYhKfk','text',NULL,0.0000,NULL);
INSERT INTO `b_iblock_element_property` VALUES (124,19,36,'LenardDizXS','text',NULL,0.0000,NULL);
INSERT INTO `b_iblock_element_property` VALUES (125,19,37,'LenardDizXS','text',NULL,0.0000,NULL);
INSERT INTO `b_iblock_element_property` VALUES (126,23,17,'8-499-333-81-82','text',NULL,8.0000,'');
INSERT INTO `b_iblock_element_property` VALUES (127,23,52,'8-843-333-81-82','text',NULL,8.0000,'');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_element_right`
-- 




DROP TABLE IF EXISTS `b_iblock_element_right`;
CREATE TABLE `b_iblock_element_right` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `ELEMENT_ID` int(11) NOT NULL,
  `RIGHT_ID` int(11) NOT NULL,
  `IS_INHERITED` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RIGHT_ID`,`ELEMENT_ID`,`SECTION_ID`),
  KEY `ix_b_iblock_element_right_1` (`ELEMENT_ID`,`IBLOCK_ID`),
  KEY `ix_b_iblock_element_right_2` (`IBLOCK_ID`,`RIGHT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_fields`
-- 




DROP TABLE IF EXISTS `b_iblock_fields`;
CREATE TABLE `b_iblock_fields` (
  `IBLOCK_ID` int(18) NOT NULL,
  `FIELD_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `IS_REQUIRED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEFAULT_VALUE` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`IBLOCK_ID`,`FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_group`
-- 




DROP TABLE IF EXISTS `b_iblock_group`;
CREATE TABLE `b_iblock_group` (
  `IBLOCK_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `PERMISSION` char(1) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `ux_iblock_group_1` (`IBLOCK_ID`,`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_group`
-- 


INSERT INTO `b_iblock_group` VALUES (1,1,'X');
INSERT INTO `b_iblock_group` VALUES (1,2,'R');
INSERT INTO `b_iblock_group` VALUES (2,1,'X');
INSERT INTO `b_iblock_group` VALUES (2,2,'R');
INSERT INTO `b_iblock_group` VALUES (3,1,'X');
INSERT INTO `b_iblock_group` VALUES (3,2,'R');
INSERT INTO `b_iblock_group` VALUES (5,1,'X');
INSERT INTO `b_iblock_group` VALUES (5,2,'R');
INSERT INTO `b_iblock_group` VALUES (6,1,'X');
INSERT INTO `b_iblock_group` VALUES (6,2,'R');
INSERT INTO `b_iblock_group` VALUES (7,1,'X');
INSERT INTO `b_iblock_group` VALUES (8,1,'X');
INSERT INTO `b_iblock_group` VALUES (8,2,'R');
INSERT INTO `b_iblock_group` VALUES (9,1,'X');
INSERT INTO `b_iblock_group` VALUES (9,2,'R');
INSERT INTO `b_iblock_group` VALUES (11,1,'X');
INSERT INTO `b_iblock_group` VALUES (11,2,'R');
INSERT INTO `b_iblock_group` VALUES (12,1,'X');
INSERT INTO `b_iblock_group` VALUES (12,2,'R');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_messages`
-- 




DROP TABLE IF EXISTS `b_iblock_messages`;
CREATE TABLE `b_iblock_messages` (
  `IBLOCK_ID` int(18) NOT NULL,
  `MESSAGE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`MESSAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_messages`
-- 


INSERT INTO `b_iblock_messages` VALUES (1,'ELEMENT_ADD','Добавить регион');
INSERT INTO `b_iblock_messages` VALUES (1,'ELEMENT_DELETE','Удалить регион');
INSERT INTO `b_iblock_messages` VALUES (1,'ELEMENT_EDIT','Изменить регион');
INSERT INTO `b_iblock_messages` VALUES (1,'ELEMENT_NAME','Регион');
INSERT INTO `b_iblock_messages` VALUES (1,'ELEMENTS_NAME','Регионы');
INSERT INTO `b_iblock_messages` VALUES (1,'SECTION_ADD','Добавить раздел');
INSERT INTO `b_iblock_messages` VALUES (1,'SECTION_DELETE','Удалить раздел');
INSERT INTO `b_iblock_messages` VALUES (1,'SECTION_EDIT','Изменить раздел');
INSERT INTO `b_iblock_messages` VALUES (1,'SECTION_NAME','Раздел');
INSERT INTO `b_iblock_messages` VALUES (1,'SECTIONS_NAME','Разделы');
INSERT INTO `b_iblock_messages` VALUES (2,'ELEMENT_ADD','Добавить офис');
INSERT INTO `b_iblock_messages` VALUES (2,'ELEMENT_DELETE','Удалить элемент');
INSERT INTO `b_iblock_messages` VALUES (2,'ELEMENT_EDIT','Изменить элемент');
INSERT INTO `b_iblock_messages` VALUES (2,'ELEMENT_NAME','Офис');
INSERT INTO `b_iblock_messages` VALUES (2,'ELEMENTS_NAME','Офисы');
INSERT INTO `b_iblock_messages` VALUES (2,'SECTION_ADD','Добавить раздел');
INSERT INTO `b_iblock_messages` VALUES (2,'SECTION_DELETE','Удалить раздел');
INSERT INTO `b_iblock_messages` VALUES (2,'SECTION_EDIT','Изменить раздел');
INSERT INTO `b_iblock_messages` VALUES (2,'SECTION_NAME','Раздел');
INSERT INTO `b_iblock_messages` VALUES (2,'SECTIONS_NAME','Разделы');
INSERT INTO `b_iblock_messages` VALUES (3,'ELEMENT_ADD','Добавить новость');
INSERT INTO `b_iblock_messages` VALUES (3,'ELEMENT_DELETE','Удалить новость');
INSERT INTO `b_iblock_messages` VALUES (3,'ELEMENT_EDIT','Изменить новость');
INSERT INTO `b_iblock_messages` VALUES (3,'ELEMENT_NAME','Нововсть');
INSERT INTO `b_iblock_messages` VALUES (3,'ELEMENTS_NAME','Новости');
INSERT INTO `b_iblock_messages` VALUES (3,'SECTION_ADD','Добавить раздел');
INSERT INTO `b_iblock_messages` VALUES (3,'SECTION_DELETE','Удалить раздел');
INSERT INTO `b_iblock_messages` VALUES (3,'SECTION_EDIT','Изменить раздел');
INSERT INTO `b_iblock_messages` VALUES (3,'SECTION_NAME','Раздел');
INSERT INTO `b_iblock_messages` VALUES (3,'SECTIONS_NAME','Разделы');
INSERT INTO `b_iblock_messages` VALUES (4,'ELEMENT_ADD','Добавить отзыв');
INSERT INTO `b_iblock_messages` VALUES (4,'ELEMENT_DELETE','Удалить отзыв');
INSERT INTO `b_iblock_messages` VALUES (4,'ELEMENT_EDIT','Изменить отзыв');
INSERT INTO `b_iblock_messages` VALUES (4,'ELEMENT_NAME','Отзыв');
INSERT INTO `b_iblock_messages` VALUES (4,'ELEMENTS_NAME','Отзывы');
INSERT INTO `b_iblock_messages` VALUES (4,'SECTION_ADD','Добавить раздел');
INSERT INTO `b_iblock_messages` VALUES (4,'SECTION_DELETE','Удалить раздел');
INSERT INTO `b_iblock_messages` VALUES (4,'SECTION_EDIT','Изменить раздел');
INSERT INTO `b_iblock_messages` VALUES (4,'SECTION_NAME','Раздел');
INSERT INTO `b_iblock_messages` VALUES (4,'SECTIONS_NAME','Разделы');
INSERT INTO `b_iblock_messages` VALUES (5,'ELEMENT_ADD','Добавить вакансию');
INSERT INTO `b_iblock_messages` VALUES (5,'ELEMENT_DELETE','Удалить вакансию');
INSERT INTO `b_iblock_messages` VALUES (5,'ELEMENT_EDIT','Изменить вакансию');
INSERT INTO `b_iblock_messages` VALUES (5,'ELEMENT_NAME','Вакансия');
INSERT INTO `b_iblock_messages` VALUES (5,'ELEMENTS_NAME','Вакансии');
INSERT INTO `b_iblock_messages` VALUES (5,'SECTION_ADD','Добавить раздел');
INSERT INTO `b_iblock_messages` VALUES (5,'SECTION_DELETE','Удалить раздел');
INSERT INTO `b_iblock_messages` VALUES (5,'SECTION_EDIT','Изменить раздел');
INSERT INTO `b_iblock_messages` VALUES (5,'SECTION_NAME','Раздел');
INSERT INTO `b_iblock_messages` VALUES (5,'SECTIONS_NAME','Разделы');
INSERT INTO `b_iblock_messages` VALUES (6,'ELEMENT_ADD','Добавить элемент');
INSERT INTO `b_iblock_messages` VALUES (6,'ELEMENT_DELETE','Удалить элемент');
INSERT INTO `b_iblock_messages` VALUES (6,'ELEMENT_EDIT','Изменить элемент');
INSERT INTO `b_iblock_messages` VALUES (6,'ELEMENT_NAME','Элемент');
INSERT INTO `b_iblock_messages` VALUES (6,'ELEMENTS_NAME','Элементы');
INSERT INTO `b_iblock_messages` VALUES (6,'SECTION_ADD','Добавить раздел');
INSERT INTO `b_iblock_messages` VALUES (6,'SECTION_DELETE','Удалить раздел');
INSERT INTO `b_iblock_messages` VALUES (6,'SECTION_EDIT','Изменить раздел');
INSERT INTO `b_iblock_messages` VALUES (6,'SECTION_NAME','Раздел');
INSERT INTO `b_iblock_messages` VALUES (6,'SECTIONS_NAME','Разделы');
INSERT INTO `b_iblock_messages` VALUES (7,'ELEMENT_ADD','Добавить элемент');
INSERT INTO `b_iblock_messages` VALUES (7,'ELEMENT_DELETE','Удалить элемент');
INSERT INTO `b_iblock_messages` VALUES (7,'ELEMENT_EDIT','Изменить элемент');
INSERT INTO `b_iblock_messages` VALUES (7,'ELEMENT_NAME','Элемент');
INSERT INTO `b_iblock_messages` VALUES (7,'ELEMENTS_NAME','Элементы');
INSERT INTO `b_iblock_messages` VALUES (7,'SECTION_ADD','Добавить раздел');
INSERT INTO `b_iblock_messages` VALUES (7,'SECTION_DELETE','Удалить раздел');
INSERT INTO `b_iblock_messages` VALUES (7,'SECTION_EDIT','Изменить раздел');
INSERT INTO `b_iblock_messages` VALUES (7,'SECTION_NAME','Раздел');
INSERT INTO `b_iblock_messages` VALUES (7,'SECTIONS_NAME','Разделы');
INSERT INTO `b_iblock_messages` VALUES (8,'ELEMENT_ADD','Добавить иконку');
INSERT INTO `b_iblock_messages` VALUES (8,'ELEMENT_DELETE','Удалить иконку');
INSERT INTO `b_iblock_messages` VALUES (8,'ELEMENT_EDIT','Изменить иконку');
INSERT INTO `b_iblock_messages` VALUES (8,'ELEMENT_NAME','Иконка');
INSERT INTO `b_iblock_messages` VALUES (8,'ELEMENTS_NAME','Иконки');
INSERT INTO `b_iblock_messages` VALUES (8,'SECTION_ADD','Добавить слайдер');
INSERT INTO `b_iblock_messages` VALUES (8,'SECTION_DELETE','Удалить слайдер');
INSERT INTO `b_iblock_messages` VALUES (8,'SECTION_EDIT','Изменить слайдер');
INSERT INTO `b_iblock_messages` VALUES (8,'SECTION_NAME','Слайдер');
INSERT INTO `b_iblock_messages` VALUES (8,'SECTIONS_NAME','Слайдеры');
INSERT INTO `b_iblock_messages` VALUES (9,'ELEMENT_ADD','Добавить партнера');
INSERT INTO `b_iblock_messages` VALUES (9,'ELEMENT_DELETE','Удалить партнера');
INSERT INTO `b_iblock_messages` VALUES (9,'ELEMENT_EDIT','Изменить партнера');
INSERT INTO `b_iblock_messages` VALUES (9,'ELEMENT_NAME','Партнер');
INSERT INTO `b_iblock_messages` VALUES (9,'ELEMENTS_NAME','Партнеры');
INSERT INTO `b_iblock_messages` VALUES (9,'SECTION_ADD','Добавить раздел');
INSERT INTO `b_iblock_messages` VALUES (9,'SECTION_DELETE','Удалить раздел');
INSERT INTO `b_iblock_messages` VALUES (9,'SECTION_EDIT','Изменить раздел');
INSERT INTO `b_iblock_messages` VALUES (9,'SECTION_NAME','Раздел');
INSERT INTO `b_iblock_messages` VALUES (9,'SECTIONS_NAME','Разделы');
INSERT INTO `b_iblock_messages` VALUES (11,'ELEMENT_ADD','Добавить займ');
INSERT INTO `b_iblock_messages` VALUES (11,'ELEMENT_DELETE','Удалить займ');
INSERT INTO `b_iblock_messages` VALUES (11,'ELEMENT_EDIT','Изменить займ');
INSERT INTO `b_iblock_messages` VALUES (11,'ELEMENT_NAME','Займ');
INSERT INTO `b_iblock_messages` VALUES (11,'ELEMENTS_NAME','Займы');
INSERT INTO `b_iblock_messages` VALUES (11,'SECTION_ADD','Добавить раздел');
INSERT INTO `b_iblock_messages` VALUES (11,'SECTION_DELETE','Удалить раздел');
INSERT INTO `b_iblock_messages` VALUES (11,'SECTION_EDIT','Изменить раздел');
INSERT INTO `b_iblock_messages` VALUES (11,'SECTION_NAME','Раздел');
INSERT INTO `b_iblock_messages` VALUES (11,'SECTIONS_NAME','Разделы');
INSERT INTO `b_iblock_messages` VALUES (12,'ELEMENT_ADD','Добавить преимущество');
INSERT INTO `b_iblock_messages` VALUES (12,'ELEMENT_DELETE','Удалить преимущество');
INSERT INTO `b_iblock_messages` VALUES (12,'ELEMENT_EDIT','Изменить преимущество');
INSERT INTO `b_iblock_messages` VALUES (12,'ELEMENT_NAME','Преимущество');
INSERT INTO `b_iblock_messages` VALUES (12,'ELEMENTS_NAME','Преимущества');
INSERT INTO `b_iblock_messages` VALUES (12,'SECTION_ADD','Добавить раздел');
INSERT INTO `b_iblock_messages` VALUES (12,'SECTION_DELETE','Удалить раздел');
INSERT INTO `b_iblock_messages` VALUES (12,'SECTION_EDIT','Изменить раздел');
INSERT INTO `b_iblock_messages` VALUES (12,'SECTION_NAME','Раздел');
INSERT INTO `b_iblock_messages` VALUES (12,'SECTIONS_NAME','Разделы');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_offers_tmp`
-- 




DROP TABLE IF EXISTS `b_iblock_offers_tmp`;
CREATE TABLE `b_iblock_offers_tmp` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PRODUCT_IBLOCK_ID` int(11) unsigned NOT NULL,
  `OFFERS_IBLOCK_ID` int(11) unsigned NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_property`
-- 




DROP TABLE IF EXISTS `b_iblock_property`;
CREATE TABLE `b_iblock_property` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IBLOCK_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEFAULT_VALUE` text COLLATE utf8_unicode_ci,
  `PROPERTY_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `ROW_COUNT` int(11) NOT NULL DEFAULT '1',
  `COL_COUNT` int(11) NOT NULL DEFAULT '30',
  `LIST_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'L',
  `MULTIPLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `XML_ID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILE_TYPE` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MULTIPLE_CNT` int(11) DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LINK_IBLOCK_ID` int(18) DEFAULT NULL,
  `WITH_DESCRIPTION` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEARCHABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `FILTRABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_REQUIRED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(11) NOT NULL DEFAULT '1',
  `USER_TYPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_TYPE_SETTINGS` text COLLATE utf8_unicode_ci,
  `HINT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_property_1` (`IBLOCK_ID`),
  KEY `ix_iblock_property_3` (`LINK_IBLOCK_ID`),
  KEY `ix_iblock_property_2` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_property`
-- 


INSERT INTO `b_iblock_property` VALUES (1,'2013-09-26 10:36:55',2,'Город','Y',100,'CITY','','E',1,30,'L','N','19',NULL,5,NULL,1,'N','N','N','Y',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (2,'2014-10-29 14:56:50',2,'Район','Y',200,'DESTINCT','','L',1,30,'L','N','20',NULL,5,NULL,0,'N','N','N','N',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (3,'2014-10-27 12:45:08',2,'Адрес','Y',300,'ADDRESS','','S',1,30,'L','N','21',NULL,5,NULL,0,'N','N','N','Y',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (4,'2014-10-27 12:45:08',2,'Координаты на карте','Y',400,'COORD',NULL,'S',1,30,'L','N','22',NULL,5,NULL,0,'N','N','N','N',1,'map_yandex',NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (7,'2014-10-27 14:59:16',2,'Режим работы','Y',600,'SCHEDULE','Круглосуточно','S',1,30,'L','Y','25',NULL,5,NULL,0,'N','N','N','N',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (8,'2013-09-26 10:37:08',3,'Регион','Y',500,'CITY','','E',1,30,'L','N','37',NULL,5,NULL,1,'N','N','N','N',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (9,'2013-10-11 12:51:38',4,'E-mail','Y',200,'EMAIL','','S',1,30,'L','N','1',NULL,5,NULL,0,'N','N','N','N',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (10,'2013-09-26 10:37:41',5,'Зар. плата','Y',500,'SALARY','','S',1,30,'L','N','28',NULL,5,NULL,NULL,'N','N','N','Y',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (11,'2013-09-26 10:37:41',5,'Город','Y',500,'CITY','','E',1,30,'L','N','29',NULL,5,NULL,1,'N','N','N','Y',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (12,'2013-09-26 10:37:41',5,'Опыт работы','Y',500,'EXPERIENCE','','S',1,30,'L','N','30',NULL,5,NULL,NULL,'N','N','N','Y',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (13,'2013-09-26 10:37:41',5,'График','Y',500,'SHEDULE','','S',1,30,'C','N','31',NULL,5,NULL,NULL,'N','N','N','Y',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (14,'2013-09-26 10:37:41',5,'Соискатели','Y',500,'APPLICANTS','','L',1,30,'L','N','32',NULL,5,NULL,NULL,'N','N','N','Y',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (15,'2013-09-26 10:38:04',6,'Место нахождения/жительства','Y',500,'ADDRESS','','S',1,30,'L','N','33',NULL,5,NULL,NULL,'N','N','N','Y',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (16,'2013-09-26 10:38:05',6,'Основание аффилированности','Y',500,'AFFILIATION','','S',1,30,'L','N','34',NULL,5,NULL,NULL,'N','N','N','Y',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (17,'2013-09-26 10:38:05',6,'Дата наступления основания ','Y',500,'DATE','','S',1,30,'L','N','35',NULL,5,NULL,NULL,'N','N','N','Y',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (18,'2013-09-26 10:38:05',6,'Доля участия лица в уставном капитале','Y',500,'SHARE','','S',1,30,'L','N','36',NULL,5,NULL,NULL,'N','N','N','Y',1,NULL,NULL,NULL);
INSERT INTO `b_iblock_property` VALUES (19,'2013-10-11 10:13:46',4,'Имя','Y',500,'','','S',1,30,'L','N',NULL,'',5,NULL,0,'N','N','N','N',1,NULL,NULL,'');
INSERT INTO `b_iblock_property` VALUES (20,'2013-10-11 10:13:46',4,'Фамилия','Y',500,'','','S',1,30,'L','N',NULL,'',5,NULL,0,'N','N','N','N',1,NULL,NULL,'');
INSERT INTO `b_iblock_property` VALUES (21,'2014-10-27 12:45:08',2,'Email','Y',500,'EMAIL','','S',1,30,'L','N',NULL,'',5,NULL,0,'N','N','N','N',1,NULL,NULL,'');
INSERT INTO `b_iblock_property` VALUES (22,'2014-10-27 12:45:08',2,'Телефон','Y',500,'PHONE','','S',1,30,'L','N',NULL,'',5,NULL,0,'N','N','N','N',1,NULL,NULL,'');
INSERT INTO `b_iblock_property` VALUES (23,'2015-10-11 17:49:29',1,'Телефон','Y',500,'TELEPHONE','','S',1,30,'L','N',NULL,'',5,NULL,0,'N','N','N','N',1,NULL,NULL,'');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_property_enum`
-- 




DROP TABLE IF EXISTS `b_iblock_property_enum`;
CREATE TABLE `b_iblock_property_enum` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROPERTY_ID` int(11) NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `XML_ID` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_iblock_property_enum` (`PROPERTY_ID`,`XML_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_property_enum`
-- 


INSERT INTO `b_iblock_property_enum` VALUES (1,2,'Волжский район','N',500,'94833c43716ff640af94208fae3f5252',NULL);
INSERT INTO `b_iblock_property_enum` VALUES (2,2,'Кировский район','N',500,'8cc65275586bd85be3eadaa259abd82a',NULL);
INSERT INTO `b_iblock_property_enum` VALUES (3,2,'Ленинский район','N',500,'e2ca3cf596fc8186cb2f91ea67787177',NULL);
INSERT INTO `b_iblock_property_enum` VALUES (4,2,'Фрунзенский район','N',500,'80eaa2dc5a2bd3763084e1c2df10ed0b',NULL);
INSERT INTO `b_iblock_property_enum` VALUES (5,2,'Октябрьский район','N',500,'d7fc6be08aca0315954a2de46395de4f',NULL);
INSERT INTO `b_iblock_property_enum` VALUES (6,2,'Заводской район','N',500,'385960497a630acc341c215e5740e2f9',NULL);
INSERT INTO `b_iblock_property_enum` VALUES (7,14,'рассматриваются соискатели только из указанного города','Y',500,'e3c871547e7fc44d48c64ecf8d156ea8',NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_right`
-- 




DROP TABLE IF EXISTS `b_iblock_right`;
CREATE TABLE `b_iblock_right` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ID` int(11) NOT NULL,
  `GROUP_CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_TYPE` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `DO_INHERIT` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `TASK_ID` int(11) NOT NULL,
  `OP_SREAD` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `OP_EREAD` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `XML_ID` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_iblock_right_iblock_id` (`IBLOCK_ID`,`ENTITY_TYPE`,`ENTITY_ID`),
  KEY `ix_b_iblock_right_group_code` (`GROUP_CODE`,`IBLOCK_ID`),
  KEY `ix_b_iblock_right_op_eread` (`ID`,`OP_EREAD`,`GROUP_CODE`),
  KEY `ix_b_iblock_right_op_sread` (`ID`,`OP_SREAD`,`GROUP_CODE`),
  KEY `ix_b_iblock_right_task_id` (`TASK_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_right`
-- 


INSERT INTO `b_iblock_right` VALUES (1,4,'G2','iblock',4,'Y',29,'Y','Y',NULL);
INSERT INTO `b_iblock_right` VALUES (2,4,'G1','iblock',4,'Y',33,'Y','Y',NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_rss`
-- 




DROP TABLE IF EXISTS `b_iblock_rss`;
CREATE TABLE `b_iblock_rss` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ID` int(11) NOT NULL,
  `NODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NODE_VALUE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_section`
-- 




DROP TABLE IF EXISTS `b_iblock_section`;
CREATE TABLE `b_iblock_section` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `IBLOCK_ID` int(11) NOT NULL,
  `IBLOCK_SECTION_ID` int(11) DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `GLOBAL_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PICTURE` int(18) DEFAULT NULL,
  `LEFT_MARGIN` int(18) DEFAULT NULL,
  `RIGHT_MARGIN` int(18) DEFAULT NULL,
  `DEPTH_LEVEL` int(18) DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DESCRIPTION_TYPE` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `SEARCHABLE_CONTENT` text COLLATE utf8_unicode_ci,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DETAIL_PICTURE` int(18) DEFAULT NULL,
  `SOCNET_GROUP_ID` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_section_1` (`IBLOCK_ID`,`IBLOCK_SECTION_ID`),
  KEY `ix_iblock_section_depth_level` (`IBLOCK_ID`,`DEPTH_LEVEL`),
  KEY `ix_iblock_section_left_margin` (`IBLOCK_ID`,`LEFT_MARGIN`,`RIGHT_MARGIN`),
  KEY `ix_iblock_section_right_margin` (`IBLOCK_ID`,`RIGHT_MARGIN`,`LEFT_MARGIN`),
  KEY `ix_iblock_section_code` (`IBLOCK_ID`,`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_section`
-- 


INSERT INTO `b_iblock_section` VALUES (1,'2013-09-26 10:37:08',1,'2013-09-26 18:37:08',1,3,NULL,'Y','Y',500,'qqq',NULL,1,2,1,NULL,'text','QQQ\r\n',NULL,'1',NULL,NULL,NULL);
INSERT INTO `b_iblock_section` VALUES (2,'2015-10-11 14:29:33',1,'2015-10-11 14:26:50',1,8,NULL,'Y','Y',500,'Слайдер на главной',NULL,1,2,1,'Займы на любые <br/> случаи жизни!','html','Слайдер на главной\r\nЗаймы на любые \r\nслучаи жизни!','',NULL,NULL,NULL,NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_section_element`
-- 




DROP TABLE IF EXISTS `b_iblock_section_element`;
CREATE TABLE `b_iblock_section_element` (
  `IBLOCK_SECTION_ID` int(11) NOT NULL,
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `ADDITIONAL_PROPERTY_ID` int(18) DEFAULT NULL,
  UNIQUE KEY `ux_iblock_section_element` (`IBLOCK_SECTION_ID`,`IBLOCK_ELEMENT_ID`,`ADDITIONAL_PROPERTY_ID`),
  KEY `UX_IBLOCK_SECTION_ELEMENT2` (`IBLOCK_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_section_element`
-- 


INSERT INTO `b_iblock_section_element` VALUES (1,22,NULL);
INSERT INTO `b_iblock_section_element` VALUES (2,42,NULL);
INSERT INTO `b_iblock_section_element` VALUES (2,43,NULL);
INSERT INTO `b_iblock_section_element` VALUES (2,44,NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_section_property`
-- 




DROP TABLE IF EXISTS `b_iblock_section_property`;
CREATE TABLE `b_iblock_section_property` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `PROPERTY_ID` int(11) NOT NULL,
  `SMART_FILTER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`SECTION_ID`,`PROPERTY_ID`),
  KEY `ix_b_iblock_section_property_1` (`PROPERTY_ID`),
  KEY `ix_b_iblock_section_property_2` (`SECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_section_property`
-- 


INSERT INTO `b_iblock_section_property` VALUES (5,0,10,'N');
INSERT INTO `b_iblock_section_property` VALUES (5,0,11,'N');
INSERT INTO `b_iblock_section_property` VALUES (5,0,12,'N');
INSERT INTO `b_iblock_section_property` VALUES (5,0,13,'N');
INSERT INTO `b_iblock_section_property` VALUES (5,0,14,'N');
INSERT INTO `b_iblock_section_property` VALUES (6,0,15,'N');
INSERT INTO `b_iblock_section_property` VALUES (6,0,16,'N');
INSERT INTO `b_iblock_section_property` VALUES (6,0,17,'N');
INSERT INTO `b_iblock_section_property` VALUES (6,0,18,'N');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_section_right`
-- 




DROP TABLE IF EXISTS `b_iblock_section_right`;
CREATE TABLE `b_iblock_section_right` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `RIGHT_ID` int(11) NOT NULL,
  `IS_INHERITED` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RIGHT_ID`,`SECTION_ID`),
  KEY `ix_b_iblock_section_right_1` (`SECTION_ID`,`IBLOCK_ID`),
  KEY `ix_b_iblock_section_right_2` (`IBLOCK_ID`,`RIGHT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_sequence`
-- 




DROP TABLE IF EXISTS `b_iblock_sequence`;
CREATE TABLE `b_iblock_sequence` (
  `IBLOCK_ID` int(18) NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SEQ_VALUE` int(11) DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_site`
-- 




DROP TABLE IF EXISTS `b_iblock_site`;
CREATE TABLE `b_iblock_site` (
  `IBLOCK_ID` int(18) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_site`
-- 


INSERT INTO `b_iblock_site` VALUES (1,'s1');
INSERT INTO `b_iblock_site` VALUES (2,'s1');
INSERT INTO `b_iblock_site` VALUES (3,'s1');
INSERT INTO `b_iblock_site` VALUES (4,'s1');
INSERT INTO `b_iblock_site` VALUES (5,'s1');
INSERT INTO `b_iblock_site` VALUES (6,'s1');
INSERT INTO `b_iblock_site` VALUES (7,'s1');
INSERT INTO `b_iblock_site` VALUES (8,'s1');
INSERT INTO `b_iblock_site` VALUES (9,'s1');
INSERT INTO `b_iblock_site` VALUES (11,'s1');
INSERT INTO `b_iblock_site` VALUES (12,'s1');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_type`
-- 




DROP TABLE IF EXISTS `b_iblock_type`;
CREATE TABLE `b_iblock_type` (
  `ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SECTIONS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `EDIT_FILE_BEFORE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDIT_FILE_AFTER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IN_RSS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SORT` int(18) NOT NULL DEFAULT '500',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_type`
-- 


INSERT INTO `b_iblock_type` VALUES ('address','Y',NULL,NULL,'N',100);
INSERT INTO `b_iblock_type` VALUES ('affiliatted','Y',NULL,NULL,'N',100);
INSERT INTO `b_iblock_type` VALUES ('content','Y','','','N',500);
INSERT INTO `b_iblock_type` VALUES ('news','Y',NULL,NULL,'N',100);
INSERT INTO `b_iblock_type` VALUES ('regions','Y',NULL,NULL,'N',100);
INSERT INTO `b_iblock_type` VALUES ('requests','Y','','','N',500);
INSERT INTO `b_iblock_type` VALUES ('reviews','Y',NULL,NULL,'N',100);
INSERT INTO `b_iblock_type` VALUES ('vacancy','Y',NULL,NULL,'N',100);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_iblock_type_lang`
-- 




DROP TABLE IF EXISTS `b_iblock_type_lang`;
CREATE TABLE `b_iblock_type_lang` (
  `IBLOCK_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SECTION_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ELEMENT_NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_iblock_type_lang`
-- 


INSERT INTO `b_iblock_type_lang` VALUES ('regions','en','Регионы','Разделы','Регионы');
INSERT INTO `b_iblock_type_lang` VALUES ('regions','ru','Регионы','Разделы','Регионы');
INSERT INTO `b_iblock_type_lang` VALUES ('address','en','Адреса','Разделы','Адреса');
INSERT INTO `b_iblock_type_lang` VALUES ('address','ru','Адреса','Разделы','Адреса');
INSERT INTO `b_iblock_type_lang` VALUES ('news','en','Новости','Разделы','Новости');
INSERT INTO `b_iblock_type_lang` VALUES ('news','ru','Новости','Разделы','Новости');
INSERT INTO `b_iblock_type_lang` VALUES ('reviews','en','Отзывы','Разделы','Отзывы');
INSERT INTO `b_iblock_type_lang` VALUES ('reviews','ru','Отзывы','Разделы','Отзывы');
INSERT INTO `b_iblock_type_lang` VALUES ('vacancy','en','Вакансии','Разделы','Вакансии');
INSERT INTO `b_iblock_type_lang` VALUES ('vacancy','ru','Вакансии','Разделы','Вакансии');
INSERT INTO `b_iblock_type_lang` VALUES ('affiliatted','en','Аффилированные лица','Разделы','Аффилированные лица');
INSERT INTO `b_iblock_type_lang` VALUES ('affiliatted','ru','Аффилированные лица','Разделы','Аффилированные лица');
INSERT INTO `b_iblock_type_lang` VALUES ('requests','ru','Заявки','','');
INSERT INTO `b_iblock_type_lang` VALUES ('requests','en','Requests','','');
INSERT INTO `b_iblock_type_lang` VALUES ('content','ru','Контент','','');
INSERT INTO `b_iblock_type_lang` VALUES ('content','en','Content','','');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_lang`
-- 




DROP TABLE IF EXISTS `b_lang`;
CREATE TABLE `b_lang` (
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(18) NOT NULL DEFAULT '100',
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DIR` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_DATE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_DATETIME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEEK_START` int(1) DEFAULT '1',
  `CHARSET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `DOC_ROOT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOMAIN_LIMITED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SERVER_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SITE_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CULTURE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_lang`
-- 


INSERT INTO `b_lang` VALUES ('s1',1,'Y','Y','Экспресс деньги','/','DD.MM.YYYY','DD.MM.YYYY HH:MI:SS','#NAME# #LAST_NAME#',1,'UTF-8','ru','','N','','','',NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_lang_domain`
-- 




DROP TABLE IF EXISTS `b_lang_domain`;
CREATE TABLE `b_lang_domain` (
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `DOMAIN` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`LID`,`DOMAIN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_language`
-- 




DROP TABLE IF EXISTS `b_language`;
CREATE TABLE `b_language` (
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_DATE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_DATETIME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEEK_START` int(1) DEFAULT '1',
  `CHARSET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIRECTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `CULTURE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_language`
-- 


INSERT INTO `b_language` VALUES ('en',2,'N','Y','English','MM/DD/YYYY','MM/DD/YYYY H:MI T','#NAME# #LAST_NAME#',1,'UTF-8','Y',NULL);
INSERT INTO `b_language` VALUES ('ru',1,'Y','Y','Russian','DD.MM.YYYY','DD.MM.YYYY HH:MI:SS','#NAME# #LAST_NAME#',1,'UTF-8','Y',NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_medialib_collection`
-- 




DROP TABLE IF EXISTS `b_medialib_collection`;
CREATE TABLE `b_medialib_collection` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `DATE_UPDATE` datetime NOT NULL,
  `OWNER_ID` int(11) DEFAULT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KEYWORDS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ITEMS_COUNT` int(11) DEFAULT NULL,
  `ML_TYPE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_medialib_collection_item`
-- 




DROP TABLE IF EXISTS `b_medialib_collection_item`;
CREATE TABLE `b_medialib_collection_item` (
  `COLLECTION_ID` int(11) NOT NULL,
  `ITEM_ID` int(11) NOT NULL,
  PRIMARY KEY (`ITEM_ID`,`COLLECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_medialib_item`
-- 




DROP TABLE IF EXISTS `b_medialib_item`;
CREATE TABLE `b_medialib_item` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_TYPE` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DATE_CREATE` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `SOURCE_ID` int(11) NOT NULL,
  `KEYWORDS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEARCHABLE_CONTENT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_medialib_type`
-- 




DROP TABLE IF EXISTS `b_medialib_type`;
CREATE TABLE `b_medialib_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `EXT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SYSTEM` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_medialib_type`
-- 


INSERT INTO `b_medialib_type` VALUES (1,'image_name','image','jpg,jpeg,gif,png','Y','image_desc');
INSERT INTO `b_medialib_type` VALUES (2,'video_name','video','flv,mp4,wmv','Y','video_desc');
INSERT INTO `b_medialib_type` VALUES (3,'sound_name','sound','mp3,wma,aac','Y','sound_desc');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_module`
-- 




DROP TABLE IF EXISTS `b_module`;
CREATE TABLE `b_module` (
  `ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_ACTIVE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_module`
-- 


INSERT INTO `b_module` VALUES ('bitrix.sitecorporate','2013-09-26 10:01:08');
INSERT INTO `b_module` VALUES ('bitrixcloud','2013-09-26 10:01:10');
INSERT INTO `b_module` VALUES ('ceolab.calcmfo','2013-09-26 10:36:02');
INSERT INTO `b_module` VALUES ('ceolab.mfo','2013-09-26 10:32:41');
INSERT INTO `b_module` VALUES ('clouds','2013-09-26 10:01:11');
INSERT INTO `b_module` VALUES ('compression','2013-09-26 10:01:13');
INSERT INTO `b_module` VALUES ('fileman','2013-09-26 10:01:14');
INSERT INTO `b_module` VALUES ('iblock','2013-09-26 10:01:23');
INSERT INTO `b_module` VALUES ('main','2013-09-26 10:01:02');
INSERT INTO `b_module` VALUES ('perfmon','2013-09-26 10:01:29');
INSERT INTO `b_module` VALUES ('search','2013-09-26 10:01:33');
INSERT INTO `b_module` VALUES ('seo','2013-09-26 10:01:34');
INSERT INTO `b_module` VALUES ('socialservices','2013-09-26 10:01:36');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_module_group`
-- 




DROP TABLE IF EXISTS `b_module_group`;
CREATE TABLE `b_module_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `G_ACCESS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_GROUP_MODULE` (`MODULE_ID`,`GROUP_ID`,`SITE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_module_group`
-- 


INSERT INTO `b_module_group` VALUES (2,'clouds',3,'W',NULL);
INSERT INTO `b_module_group` VALUES (3,'fileman',3,'W',NULL);
INSERT INTO `b_module_group` VALUES (4,'main',3,'W',NULL);
INSERT INTO `b_module_group` VALUES (6,'seo',3,'W',NULL);
INSERT INTO `b_module_group` VALUES (9,'bitrix.sitecorporate',3,'W',NULL);
INSERT INTO `b_module_group` VALUES (10,'perfmon',3,'W',NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_module_to_module`
-- 




DROP TABLE IF EXISTS `b_module_to_module`;
CREATE TABLE `b_module_to_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SORT` int(18) NOT NULL DEFAULT '100',
  `FROM_MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `TO_MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `TO_PATH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_CLASS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_METHOD` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_METHOD_ARG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_module_to_module` (`FROM_MODULE_ID`,`MESSAGE_ID`,`TO_MODULE_ID`,`TO_CLASS`,`TO_METHOD`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_module_to_module`
-- 


INSERT INTO `b_module_to_module` VALUES (1,'2013-09-26 10:01:02',100,'iblock','OnIBlockPropertyBuildList','main','/modules/main/tools/prop_userid.php','CIBlockPropertyUserID','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (2,'2013-09-26 10:01:02',100,'main','OnUserDelete','main','/modules/main/classes/mysql/favorites.php','CFavorites','OnUserDelete','',1);
INSERT INTO `b_module_to_module` VALUES (3,'2013-09-26 10:01:02',100,'main','OnLanguageDelete','main','/modules/main/classes/mysql/favorites.php','CFavorites','OnLanguageDelete','',1);
INSERT INTO `b_module_to_module` VALUES (4,'2013-09-26 10:01:02',100,'main','OnUserDelete','main','','CUserOptions','OnUserDelete','',1);
INSERT INTO `b_module_to_module` VALUES (5,'2013-09-26 10:01:02',100,'main','OnChangeFile','main','','CMain','OnChangeFileComponent','',1);
INSERT INTO `b_module_to_module` VALUES (6,'2013-09-26 10:01:02',100,'main','OnUserTypeRightsCheck','main','','CUser','UserTypeRightsCheck','',1);
INSERT INTO `b_module_to_module` VALUES (7,'2013-09-26 10:01:02',100,'main','OnUserLogin','main','','UpdateTools','CheckUpdates','',1);
INSERT INTO `b_module_to_module` VALUES (8,'2013-09-26 10:01:02',100,'main','OnModuleUpdate','main','','UpdateTools','SetUpdateResult','',1);
INSERT INTO `b_module_to_module` VALUES (9,'2013-09-26 10:01:02',100,'main','OnUpdateCheck','main','','UpdateTools','SetUpdateError','',1);
INSERT INTO `b_module_to_module` VALUES (10,'2013-09-26 10:01:02',100,'main','OnPanelCreate','main','','CUndo','CheckNotifyMessage','',1);
INSERT INTO `b_module_to_module` VALUES (11,'2013-09-26 10:01:03',100,'main','OnAfterAddRating','main','','CRatingsComponentsMain','OnAfterAddRating','',1);
INSERT INTO `b_module_to_module` VALUES (12,'2013-09-26 10:01:03',100,'main','OnAfterUpdateRating','main','','CRatingsComponentsMain','OnAfterUpdateRating','',1);
INSERT INTO `b_module_to_module` VALUES (13,'2013-09-26 10:01:03',100,'main','OnSetRatingsConfigs','main','','CRatingsComponentsMain','OnSetRatingConfigs','',1);
INSERT INTO `b_module_to_module` VALUES (14,'2013-09-26 10:01:03',100,'main','OnGetRatingsConfigs','main','','CRatingsComponentsMain','OnGetRatingConfigs','',1);
INSERT INTO `b_module_to_module` VALUES (15,'2013-09-26 10:01:03',100,'main','OnGetRatingsObjects','main','','CRatingsComponentsMain','OnGetRatingObject','',1);
INSERT INTO `b_module_to_module` VALUES (16,'2013-09-26 10:01:03',100,'main','OnGetRatingContentOwner','main','','CRatingsComponentsMain','OnGetRatingContentOwner','',1);
INSERT INTO `b_module_to_module` VALUES (17,'2013-09-26 10:01:03',100,'main','OnAfterAddRatingRule','main','','CRatingRulesMain','OnAfterAddRatingRule','',1);
INSERT INTO `b_module_to_module` VALUES (18,'2013-09-26 10:01:03',100,'main','OnAfterUpdateRatingRule','main','','CRatingRulesMain','OnAfterUpdateRatingRule','',1);
INSERT INTO `b_module_to_module` VALUES (19,'2013-09-26 10:01:03',100,'main','OnGetRatingRuleObjects','main','','CRatingRulesMain','OnGetRatingRuleObjects','',1);
INSERT INTO `b_module_to_module` VALUES (20,'2013-09-26 10:01:03',100,'main','OnGetRatingRuleConfigs','main','','CRatingRulesMain','OnGetRatingRuleConfigs','',1);
INSERT INTO `b_module_to_module` VALUES (21,'2013-09-26 10:01:03',100,'main','OnAfterUserAdd','main','','CRatings','OnAfterUserRegister','',1);
INSERT INTO `b_module_to_module` VALUES (22,'2013-09-26 10:01:03',100,'main','OnUserDelete','main','','CRatings','OnUserDelete','',1);
INSERT INTO `b_module_to_module` VALUES (23,'2013-09-26 10:01:03',100,'main','OnUserDelete','main','','CAccess','OnUserDelete','',1);
INSERT INTO `b_module_to_module` VALUES (24,'2013-09-26 10:01:03',100,'main','OnAfterGroupAdd','main','','CGroupAuthProvider','OnAfterGroupAdd','',1);
INSERT INTO `b_module_to_module` VALUES (25,'2013-09-26 10:01:03',100,'main','OnBeforeGroupUpdate','main','','CGroupAuthProvider','OnBeforeGroupUpdate','',1);
INSERT INTO `b_module_to_module` VALUES (26,'2013-09-26 10:01:03',100,'main','OnBeforeGroupDelete','main','','CGroupAuthProvider','OnBeforeGroupDelete','',1);
INSERT INTO `b_module_to_module` VALUES (27,'2013-09-26 10:01:03',100,'main','OnAfterUserUpdate','main','','CGroupAuthProvider','OnAfterUserUpdate','',1);
INSERT INTO `b_module_to_module` VALUES (28,'2013-09-26 10:01:03',100,'main','OnUserLogin','main','','CGroupAuthProvider','OnUserLogin','',1);
INSERT INTO `b_module_to_module` VALUES (29,'2013-09-26 10:01:03',100,'main','OnEventLogGetAuditTypes','main','','CEventMain','GetAuditTypes','',1);
INSERT INTO `b_module_to_module` VALUES (30,'2013-09-26 10:01:03',100,'main','OnEventLogGetAuditHandlers','main','','CEventMain','MakeMainObject','',1);
INSERT INTO `b_module_to_module` VALUES (31,'2013-09-26 10:01:03',100,'perfmon','OnGetTableSchema','main','','CTableSchema','OnGetTableSchema','',1);
INSERT INTO `b_module_to_module` VALUES (32,'2013-09-26 10:01:03',110,'main','OnUserTypeBuildList','main','','CUserTypeString','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (33,'2013-09-26 10:01:04',120,'main','OnUserTypeBuildList','main','','CUserTypeInteger','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (34,'2013-09-26 10:01:04',130,'main','OnUserTypeBuildList','main','','CUserTypeDouble','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (35,'2013-09-26 10:01:04',140,'main','OnUserTypeBuildList','main','','CUserTypeDateTime','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (36,'2013-09-26 10:01:04',150,'main','OnUserTypeBuildList','main','','CUserTypeBoolean','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (37,'2013-09-26 10:01:04',160,'main','OnUserTypeBuildList','main','','CUserTypeFile','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (38,'2013-09-26 10:01:04',170,'main','OnUserTypeBuildList','main','','CUserTypeEnum','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (39,'2013-09-26 10:01:04',180,'main','OnUserTypeBuildList','main','','CUserTypeIBlockSection','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (40,'2013-09-26 10:01:04',190,'main','OnUserTypeBuildList','main','','CUserTypeIBlockElement','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (41,'2013-09-26 10:01:04',200,'main','OnUserTypeBuildList','main','','CUserTypeStringFormatted','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (42,'2013-09-26 10:01:08',100,'main','OnBeforeProlog','bitrix.sitecorporate','','CSiteCorporate','ShowPanel','',1);
INSERT INTO `b_module_to_module` VALUES (43,'2013-09-26 10:01:10',100,'main','OnAdminInformerInsertItems','bitrixcloud','','CBitrixCloudCDN','OnAdminInformerInsertItems','',1);
INSERT INTO `b_module_to_module` VALUES (44,'2013-09-26 10:01:10',100,'main','OnAdminInformerInsertItems','bitrixcloud','','CBitrixCloudBackup','OnAdminInformerInsertItems','',1);
INSERT INTO `b_module_to_module` VALUES (45,'2013-09-26 10:01:11',100,'main','OnEventLogGetAuditTypes','clouds','','CCloudStorage','GetAuditTypes','',1);
INSERT INTO `b_module_to_module` VALUES (46,'2013-09-26 10:01:11',100,'main','OnBeforeProlog','clouds','','CCloudStorage','OnBeforeProlog','',1);
INSERT INTO `b_module_to_module` VALUES (47,'2013-09-26 10:01:11',100,'main','OnAdminListDisplay','clouds','','CCloudStorage','OnAdminListDisplay','',1);
INSERT INTO `b_module_to_module` VALUES (48,'2013-09-26 10:01:11',100,'main','OnBuildGlobalMenu','clouds','','CCloudStorage','OnBuildGlobalMenu','',1);
INSERT INTO `b_module_to_module` VALUES (49,'2013-09-26 10:01:12',100,'main','OnFileSave','clouds','','CCloudStorage','OnFileSave','',1);
INSERT INTO `b_module_to_module` VALUES (50,'2013-09-26 10:01:12',100,'main','OnGetFileSRC','clouds','','CCloudStorage','OnGetFileSRC','',1);
INSERT INTO `b_module_to_module` VALUES (51,'2013-09-26 10:01:12',100,'main','OnFileCopy','clouds','','CCloudStorage','OnFileCopy','',1);
INSERT INTO `b_module_to_module` VALUES (52,'2013-09-26 10:01:12',100,'main','OnFileDelete','clouds','','CCloudStorage','OnFileDelete','',1);
INSERT INTO `b_module_to_module` VALUES (53,'2013-09-26 10:01:12',100,'main','OnMakeFileArray','clouds','','CCloudStorage','OnMakeFileArray','',1);
INSERT INTO `b_module_to_module` VALUES (54,'2013-09-26 10:01:12',100,'main','OnBeforeResizeImage','clouds','','CCloudStorage','OnBeforeResizeImage','',1);
INSERT INTO `b_module_to_module` VALUES (55,'2013-09-26 10:01:12',100,'main','OnAfterResizeImage','clouds','','CCloudStorage','OnAfterResizeImage','',1);
INSERT INTO `b_module_to_module` VALUES (56,'2013-09-26 10:01:12',100,'clouds','OnGetStorageService','clouds','','CCloudStorageService_AmazonS3','GetObject','',1);
INSERT INTO `b_module_to_module` VALUES (57,'2013-09-26 10:01:12',100,'clouds','OnGetStorageService','clouds','','CCloudStorageService_GoogleStorage','GetObject','',1);
INSERT INTO `b_module_to_module` VALUES (58,'2013-09-26 10:01:12',100,'clouds','OnGetStorageService','clouds','','CCloudStorageService_OpenStackStorage','GetObject','',1);
INSERT INTO `b_module_to_module` VALUES (59,'2013-09-26 10:01:12',100,'clouds','OnGetStorageService','clouds','','CCloudStorageService_RackSpaceCloudFiles','GetObject','',1);
INSERT INTO `b_module_to_module` VALUES (60,'2013-09-26 10:01:12',100,'clouds','OnGetStorageService','clouds','','CCloudStorageService_ClodoRU','GetObject','',1);
INSERT INTO `b_module_to_module` VALUES (61,'2013-09-26 10:01:12',100,'clouds','OnGetStorageService','clouds','','CCloudStorageService_Selectel','GetObject','',1);
INSERT INTO `b_module_to_module` VALUES (62,'2013-09-26 10:01:13',1,'main','OnPageStart','compression','','CCompress','OnPageStart','',1);
INSERT INTO `b_module_to_module` VALUES (63,'2013-09-26 10:01:13',10000,'main','OnAfterEpilog','compression','','CCompress','OnAfterEpilog','',1);
INSERT INTO `b_module_to_module` VALUES (64,'2013-09-26 10:01:15',100,'main','OnGroupDelete','fileman','','CFileman','OnGroupDelete','',1);
INSERT INTO `b_module_to_module` VALUES (65,'2013-09-26 10:01:15',100,'main','OnPanelCreate','fileman','','CFileman','OnPanelCreate','',1);
INSERT INTO `b_module_to_module` VALUES (66,'2013-09-26 10:01:15',100,'main','OnModuleUpdate','fileman','','CFileman','OnModuleUpdate','',1);
INSERT INTO `b_module_to_module` VALUES (67,'2013-09-26 10:01:15',100,'main','OnModuleInstalled','fileman','','CFileman','ClearComponentsListCache','',1);
INSERT INTO `b_module_to_module` VALUES (68,'2013-09-26 10:01:15',100,'iblock','OnIBlockPropertyBuildList','fileman','','CIBlockPropertyMapGoogle','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (69,'2013-09-26 10:01:15',100,'iblock','OnIBlockPropertyBuildList','fileman','','CIBlockPropertyMapYandex','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (70,'2013-09-26 10:01:15',100,'iblock','OnIBlockPropertyBuildList','fileman','','CIBlockPropertyVideo','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (71,'2013-09-26 10:01:16',100,'main','OnUserTypeBuildList','fileman','','CUserTypeVideo','GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (72,'2013-09-26 10:01:16',100,'main','OnEventLogGetAuditTypes','fileman','','CEventFileman','GetAuditTypes','',1);
INSERT INTO `b_module_to_module` VALUES (73,'2013-09-26 10:01:16',100,'main','OnEventLogGetAuditHandlers','fileman','','CEventFileman','MakeFilemanObject','',1);
INSERT INTO `b_module_to_module` VALUES (74,'2013-09-26 10:01:23',100,'main','OnGroupDelete','iblock','','CIBlock','OnGroupDelete','',1);
INSERT INTO `b_module_to_module` VALUES (75,'2013-09-26 10:01:24',100,'main','OnBeforeLangDelete','iblock','','CIBlock','OnBeforeLangDelete','',1);
INSERT INTO `b_module_to_module` VALUES (76,'2013-09-26 10:01:24',100,'main','OnLangDelete','iblock','','CIBlock','OnLangDelete','',1);
INSERT INTO `b_module_to_module` VALUES (77,'2013-09-26 10:01:24',100,'main','OnUserTypeRightsCheck','iblock','','CIBlockSection','UserTypeRightsCheck','',1);
INSERT INTO `b_module_to_module` VALUES (78,'2013-09-26 10:01:24',100,'search','OnReindex','iblock','','CIBlock','OnSearchReindex','',1);
INSERT INTO `b_module_to_module` VALUES (79,'2013-09-26 10:01:24',100,'search','OnSearchGetURL','iblock','','CIBlock','OnSearchGetURL','',1);
INSERT INTO `b_module_to_module` VALUES (80,'2013-09-26 10:01:24',100,'main','OnEventLogGetAuditTypes','iblock','','CIBlock','GetAuditTypes','',1);
INSERT INTO `b_module_to_module` VALUES (81,'2013-09-26 10:01:24',100,'main','OnEventLogGetAuditHandlers','iblock','','CEventIBlock','MakeIBlockObject','',1);
INSERT INTO `b_module_to_module` VALUES (82,'2013-09-26 10:01:24',200,'main','OnGetRatingContentOwner','iblock','','CRatingsComponentsIBlock','OnGetRatingContentOwner','',1);
INSERT INTO `b_module_to_module` VALUES (83,'2013-09-26 10:01:24',100,'main','OnTaskOperationsChanged','iblock','','CIBlockRightsStorage','OnTaskOperationsChanged','',1);
INSERT INTO `b_module_to_module` VALUES (84,'2013-09-26 10:01:24',100,'main','OnGroupDelete','iblock','','CIBlockRightsStorage','OnGroupDelete','',1);
INSERT INTO `b_module_to_module` VALUES (85,'2013-09-26 10:01:24',100,'main','OnUserDelete','iblock','','CIBlockRightsStorage','OnUserDelete','',1);
INSERT INTO `b_module_to_module` VALUES (86,'2013-09-26 10:01:24',100,'perfmon','OnGetTableSchema','iblock','','iblock','OnGetTableSchema','',1);
INSERT INTO `b_module_to_module` VALUES (87,'2013-09-26 10:01:24',10,'iblock','OnIBlockPropertyBuildList','iblock','','CIBlockProperty','_DateTime_GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (88,'2013-09-26 10:01:24',20,'iblock','OnIBlockPropertyBuildList','iblock','','CIBlockProperty','_XmlID_GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (89,'2013-09-26 10:01:24',30,'iblock','OnIBlockPropertyBuildList','iblock','','CIBlockProperty','_FileMan_GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (90,'2013-09-26 10:01:24',40,'iblock','OnIBlockPropertyBuildList','iblock','','CIBlockProperty','_HTML_GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (91,'2013-09-26 10:01:24',50,'iblock','OnIBlockPropertyBuildList','iblock','','CIBlockProperty','_ElementList_GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (92,'2013-09-26 10:01:24',60,'iblock','OnIBlockPropertyBuildList','iblock','','CIBlockProperty','_Sequence_GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (93,'2013-09-26 10:01:24',70,'iblock','OnIBlockPropertyBuildList','iblock','','CIBlockProperty','_ElementAutoComplete_GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (94,'2013-09-26 10:01:24',80,'iblock','OnIBlockPropertyBuildList','iblock','','CIBlockProperty','_SKU_GetUserTypeDescription','',1);
INSERT INTO `b_module_to_module` VALUES (95,'2013-09-26 10:01:33',100,'main','OnChangePermissions','search','','CSearch','OnChangeFilePermissions','',1);
INSERT INTO `b_module_to_module` VALUES (96,'2013-09-26 10:01:33',100,'main','OnChangeFile','search','','CSearch','OnChangeFile','',1);
INSERT INTO `b_module_to_module` VALUES (97,'2013-09-26 10:01:33',100,'main','OnGroupDelete','search','','CSearch','OnGroupDelete','',1);
INSERT INTO `b_module_to_module` VALUES (98,'2013-09-26 10:01:33',100,'main','OnLangDelete','search','','CSearch','OnLangDelete','',1);
INSERT INTO `b_module_to_module` VALUES (99,'2013-09-26 10:01:33',100,'main','OnAfterUserUpdate','search','','CSearchUser','OnAfterUserUpdate','',1);
INSERT INTO `b_module_to_module` VALUES (100,'2013-09-26 10:01:33',100,'main','OnUserDelete','search','','CSearchUser','DeleteByUserID','',1);
INSERT INTO `b_module_to_module` VALUES (101,'2013-09-26 10:01:33',100,'cluster','OnGetTableList','search','','search','OnGetTableList','',1);
INSERT INTO `b_module_to_module` VALUES (102,'2013-09-26 10:01:33',100,'perfmon','OnGetTableSchema','search','','search','OnGetTableSchema','',1);
INSERT INTO `b_module_to_module` VALUES (103,'2013-09-26 10:01:33',90,'main','OnEpilog','search','','CSearchStatistic','OnEpilog','',1);
INSERT INTO `b_module_to_module` VALUES (104,'2013-09-26 10:01:35',100,'main','OnPanelCreate','seo','','CSeoEventHandlers','SeoOnPanelCreate','',1);
INSERT INTO `b_module_to_module` VALUES (105,'2013-09-26 10:01:36',100,'main','OnUserDelete','socialservices','','CSocServAuthDB','OnUserDelete','',1);
INSERT INTO `b_module_to_module` VALUES (106,'2013-09-26 10:01:36',100,'timeman','OnAfterTMReportDailyAdd','socialservices','','CSocServAuthDB','OnAfterTMReportDailyAdd','',1);
INSERT INTO `b_module_to_module` VALUES (107,'2013-09-26 10:01:36',100,'timeman','OnAfterTMDayStart','socialservices','','CSocServAuthDB','OnAfterTMDayStart','',1);
INSERT INTO `b_module_to_module` VALUES (108,'2013-09-26 10:01:36',100,'timeman','OnTimeManShow','socialservices','','CSocServEventHandlers','OnTimeManShow','',1);
INSERT INTO `b_module_to_module` VALUES (109,'2013-09-26 10:01:50',100,'main','OnBeforeProlog','main','/modules/main/install/wizard_sol/panel_button.php','CWizardSolPanel','ShowPanel','',1);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_operation`
-- 




DROP TABLE IF EXISTS `b_operation`;
CREATE TABLE `b_operation` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BINDING` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'module',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_operation`
-- 


INSERT INTO `b_operation` VALUES (1,'edit_php','main',NULL,'module');
INSERT INTO `b_operation` VALUES (2,'view_own_profile','main',NULL,'module');
INSERT INTO `b_operation` VALUES (3,'edit_own_profile','main',NULL,'module');
INSERT INTO `b_operation` VALUES (4,'view_all_users','main',NULL,'module');
INSERT INTO `b_operation` VALUES (5,'view_groups','main',NULL,'module');
INSERT INTO `b_operation` VALUES (6,'view_tasks','main',NULL,'module');
INSERT INTO `b_operation` VALUES (7,'view_other_settings','main',NULL,'module');
INSERT INTO `b_operation` VALUES (8,'view_subordinate_users','main',NULL,'module');
INSERT INTO `b_operation` VALUES (9,'edit_subordinate_users','main',NULL,'module');
INSERT INTO `b_operation` VALUES (10,'edit_all_users','main',NULL,'module');
INSERT INTO `b_operation` VALUES (11,'edit_groups','main',NULL,'module');
INSERT INTO `b_operation` VALUES (12,'edit_tasks','main',NULL,'module');
INSERT INTO `b_operation` VALUES (13,'edit_other_settings','main',NULL,'module');
INSERT INTO `b_operation` VALUES (14,'cache_control','main',NULL,'module');
INSERT INTO `b_operation` VALUES (15,'lpa_template_edit','main',NULL,'module');
INSERT INTO `b_operation` VALUES (16,'view_event_log','main',NULL,'module');
INSERT INTO `b_operation` VALUES (17,'edit_ratings','main',NULL,'module');
INSERT INTO `b_operation` VALUES (18,'manage_short_uri','main',NULL,'module');
INSERT INTO `b_operation` VALUES (19,'fm_view_permission','main',NULL,'file');
INSERT INTO `b_operation` VALUES (20,'fm_view_file','main',NULL,'file');
INSERT INTO `b_operation` VALUES (21,'fm_view_listing','main',NULL,'file');
INSERT INTO `b_operation` VALUES (22,'fm_edit_existent_folder','main',NULL,'file');
INSERT INTO `b_operation` VALUES (23,'fm_create_new_file','main',NULL,'file');
INSERT INTO `b_operation` VALUES (24,'fm_edit_existent_file','main',NULL,'file');
INSERT INTO `b_operation` VALUES (25,'fm_create_new_folder','main',NULL,'file');
INSERT INTO `b_operation` VALUES (26,'fm_delete_file','main',NULL,'file');
INSERT INTO `b_operation` VALUES (27,'fm_delete_folder','main',NULL,'file');
INSERT INTO `b_operation` VALUES (28,'fm_edit_in_workflow','main',NULL,'file');
INSERT INTO `b_operation` VALUES (29,'fm_rename_file','main',NULL,'file');
INSERT INTO `b_operation` VALUES (30,'fm_rename_folder','main',NULL,'file');
INSERT INTO `b_operation` VALUES (31,'fm_upload_file','main',NULL,'file');
INSERT INTO `b_operation` VALUES (32,'fm_add_to_menu','main',NULL,'file');
INSERT INTO `b_operation` VALUES (33,'fm_download_file','main',NULL,'file');
INSERT INTO `b_operation` VALUES (34,'fm_lpa','main',NULL,'file');
INSERT INTO `b_operation` VALUES (35,'fm_edit_permission','main',NULL,'file');
INSERT INTO `b_operation` VALUES (36,'clouds_browse','clouds',NULL,'module');
INSERT INTO `b_operation` VALUES (37,'clouds_upload','clouds',NULL,'module');
INSERT INTO `b_operation` VALUES (38,'clouds_config','clouds',NULL,'module');
INSERT INTO `b_operation` VALUES (39,'fileman_view_all_settings','fileman','','module');
INSERT INTO `b_operation` VALUES (40,'fileman_edit_menu_types','fileman','','module');
INSERT INTO `b_operation` VALUES (41,'fileman_add_element_to_menu','fileman','','module');
INSERT INTO `b_operation` VALUES (42,'fileman_edit_menu_elements','fileman','','module');
INSERT INTO `b_operation` VALUES (43,'fileman_edit_existent_files','fileman','','module');
INSERT INTO `b_operation` VALUES (44,'fileman_edit_existent_folders','fileman','','module');
INSERT INTO `b_operation` VALUES (45,'fileman_admin_files','fileman','','module');
INSERT INTO `b_operation` VALUES (46,'fileman_admin_folders','fileman','','module');
INSERT INTO `b_operation` VALUES (47,'fileman_view_permissions','fileman','','module');
INSERT INTO `b_operation` VALUES (48,'fileman_edit_all_settings','fileman','','module');
INSERT INTO `b_operation` VALUES (49,'fileman_upload_files','fileman','','module');
INSERT INTO `b_operation` VALUES (50,'fileman_view_file_structure','fileman','','module');
INSERT INTO `b_operation` VALUES (51,'fileman_install_control','fileman','','module');
INSERT INTO `b_operation` VALUES (52,'medialib_view_collection','fileman','','medialib');
INSERT INTO `b_operation` VALUES (53,'medialib_new_collection','fileman','','medialib');
INSERT INTO `b_operation` VALUES (54,'medialib_edit_collection','fileman','','medialib');
INSERT INTO `b_operation` VALUES (55,'medialib_del_collection','fileman','','medialib');
INSERT INTO `b_operation` VALUES (56,'medialib_access','fileman','','medialib');
INSERT INTO `b_operation` VALUES (57,'medialib_new_item','fileman','','medialib');
INSERT INTO `b_operation` VALUES (58,'medialib_edit_item','fileman','','medialib');
INSERT INTO `b_operation` VALUES (59,'medialib_del_item','fileman','','medialib');
INSERT INTO `b_operation` VALUES (60,'sticker_view','fileman','','stickers');
INSERT INTO `b_operation` VALUES (61,'sticker_edit','fileman','','stickers');
INSERT INTO `b_operation` VALUES (62,'sticker_new','fileman','','stickers');
INSERT INTO `b_operation` VALUES (63,'sticker_del','fileman','','stickers');
INSERT INTO `b_operation` VALUES (64,'iblock_admin_display','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (65,'iblock_edit','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (66,'iblock_delete','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (67,'iblock_rights_edit','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (68,'iblock_export','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (69,'section_read','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (70,'section_edit','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (71,'section_delete','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (72,'section_element_bind','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (73,'section_section_bind','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (74,'section_rights_edit','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (75,'element_read','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (76,'element_edit','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (77,'element_edit_any_wf_status','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (78,'element_edit_price','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (79,'element_delete','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (80,'element_bizproc_start','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (81,'element_rights_edit','iblock',NULL,'iblock');
INSERT INTO `b_operation` VALUES (82,'seo_settings','seo','','module');
INSERT INTO `b_operation` VALUES (83,'seo_tools','seo','','module');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_option`
-- 




DROP TABLE IF EXISTS `b_option`;
CREATE TABLE `b_option` (
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `ix_option` (`MODULE_ID`,`NAME`,`SITE_ID`),
  KEY `ix_option_name` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_option`
-- 


INSERT INTO `b_option` VALUES ('main','rating_authority_rating','2','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_assign_rating_group_add','1','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_assign_rating_group_delete','1','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_assign_rating_group','3','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_assign_authority_group_add','2','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_assign_authority_group_delete','2','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_assign_authority_group','4','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_community_size','1','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_community_authority','30','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_vote_weight','10','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_normalization_type','auto','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_normalization','10','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_count_vote','10','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_authority_weight_formula','Y','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_community_last_visit','90','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_text_like_y','Мне нравится','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_text_like_n','Больше не нравится','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_text_like_d','Это нравится','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_assign_type','auto','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_vote_type','like','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_self_vote','Y','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_vote_show','Y','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_vote_template','like','',NULL);
INSERT INTO `b_option` VALUES ('main','rating_start_authority','3','',NULL);
INSERT INTO `b_option` VALUES ('main','auth_comp2','Y','Использовать Компоненты 2.0 для авторизации и регистрации:',NULL);
INSERT INTO `b_option` VALUES ('main','PARAM_MAX_SITES','2','',NULL);
INSERT INTO `b_option` VALUES ('main','PARAM_MAX_USERS','0','',NULL);
INSERT INTO `b_option` VALUES ('main','distributive6','Y','',NULL);
INSERT INTO `b_option` VALUES ('main','~new_license11_sign','Y','',NULL);
INSERT INTO `b_option` VALUES ('main','GROUP_DEFAULT_TASK','1','Task for groups by default',NULL);
INSERT INTO `b_option` VALUES ('main','vendor','1c_bitrix','',NULL);
INSERT INTO `b_option` VALUES ('main','admin_lid','ru','',NULL);
INSERT INTO `b_option` VALUES ('main','update_site','www.bitrixsoft.com','Имя сервера, содержащего обновления:',NULL);
INSERT INTO `b_option` VALUES ('main','update_site_ns','Y','',NULL);
INSERT INTO `b_option` VALUES ('main','admin_passwordh','FVoQeGYUBwYtCUVcBBcBCgsTAQ==',NULL,NULL);
INSERT INTO `b_option` VALUES ('main','server_uniq_id','816e1a60830a324d93160bc90ba3143c','',NULL);
INSERT INTO `b_option` VALUES ('search','version','v2.0','',NULL);
INSERT INTO `b_option` VALUES ('search','dbnode_id','N','',NULL);
INSERT INTO `b_option` VALUES ('search','dbnode_status','ok','',NULL);
INSERT INTO `b_option` VALUES ('main','email_from','qwe@qwe.ru','E-Mail администратора сайта (отправитель по умолчанию):',NULL);
INSERT INTO `b_option` VALUES ('main','~update_autocheck_result','a:4:{s:10:\"check_date\";i:1380205081;s:6:\"result\";b:0;s:5:\"error\";s:424:\"<br>Кодировка используемой вами базы данных MySql \'latin1\', системе обновлений необходима кодировка \'utf8\'. Обратитесь к администратору MySql для выполнения запроса alter database finhelper default character set utf8[LICENSE_NOT_ACTIVE_A] Закончился срок получения обновлений\";s:7:\"modules\";a:0:{}}','',NULL);
INSERT INTO `b_option` VALUES ('fileman','stickers_use_hotkeys','N','',NULL);
INSERT INTO `b_option` VALUES ('main','update_system_check','16.12.2014 18:06:47','',NULL);
INSERT INTO `b_option` VALUES ('main','update_system_update','26.09.2013 18:18:02','',NULL);
INSERT INTO `b_option` VALUES ('ceolab.mfo','~bsm_stop_date','BQQjUmsDUwB+BAZUAiAFIEVTUw==',NULL,NULL);
INSERT INTO `b_option` VALUES ('main','companyName','МФО','',NULL);
INSERT INTO `b_option` VALUES ('main','ownerCompany','ООО «МФО»','',NULL);
INSERT INTO `b_option` VALUES ('main','phone','8 800 700-43-44','',NULL);
INSERT INTO `b_option` VALUES ('main','address','г. Москва','',NULL);
INSERT INTO `b_option` VALUES ('main','inn','0000000000','',NULL);
INSERT INTO `b_option` VALUES ('main','kpp','0000000000','',NULL);
INSERT INTO `b_option` VALUES ('main','ogrn','0000000000','',NULL);
INSERT INTO `b_option` VALUES ('main','okpo','0000000000','',NULL);
INSERT INTO `b_option` VALUES ('main','okato','0000000000','',NULL);
INSERT INTO `b_option` VALUES ('main','bankAccount','0000000000','',NULL);
INSERT INTO `b_option` VALUES ('main','branchNumber','000','',NULL);
INSERT INTO `b_option` VALUES ('main','bik','0000000000','',NULL);
INSERT INTO `b_option` VALUES ('main','cashAccount','0000000000','',NULL);
INSERT INTO `b_option` VALUES ('main','fromSumm','1000','',NULL);
INSERT INTO `b_option` VALUES ('main','toSumm','16000','',NULL);
INSERT INTO `b_option` VALUES ('main','fromTerm','1','',NULL);
INSERT INTO `b_option` VALUES ('main','toTerm','16','',NULL);
INSERT INTO `b_option` VALUES ('main','fromAge','21','',NULL);
INSERT INTO `b_option` VALUES ('main','toAge','65','',NULL);
INSERT INTO `b_option` VALUES ('main','percent','2','',NULL);
INSERT INTO `b_option` VALUES ('main','certificate','','',NULL);
INSERT INTO `b_option` VALUES ('fileman','different_set','Y','',NULL);
INSERT INTO `b_option` VALUES ('fileman','menutypes','a:3:{s:4:\\\"left\\\";s:19:\\\"Левое меню\\\";s:3:\\\"top\\\";s:23:\\\"Верхнее меню\\\";s:6:\\\"bottom\\\";s:21:\\\"Нижнее меню\\\";}','','s1');
INSERT INTO `b_option` VALUES ('main','new_user_registration','N','Позволять ли пользователям регистрироваться самостоятельно?',NULL);
INSERT INTO `b_option` VALUES ('socialnetwork','allow_tooltip','N','',NULL);
INSERT INTO `b_option` VALUES ('main','wizard_firstmfo_s1','Y','',NULL);
INSERT INTO `b_option` VALUES ('main','wizard_solution','itin.buyshop','','s1');
INSERT INTO `b_option` VALUES ('main','~support_finish_date','2013-10-26','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_bucket_id','0','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_encrypt','0','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_use_compression','1','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_max_exec_time','20','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_max_exec_time_sleep','1','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_archive_size_limit','1073741824','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_integrity_check','1','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_max_file_size','0','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_file_public','1','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_file_kernel','1','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_base','1','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_base_skip_stat','0','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_base_skip_search','0','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_base_skip_log','0','',NULL);
INSERT INTO `b_option` VALUES ('main','skip_symlinks','0','',NULL);
INSERT INTO `b_option` VALUES ('main','skip_mask','0','',NULL);
INSERT INTO `b_option` VALUES ('main','site_name','Экспресс деньги','Название сайта:',NULL);
INSERT INTO `b_option` VALUES ('main','server_name','www.denwerzaim.ru','URL сайта (без http://, например www.mysite.com):',NULL);
INSERT INTO `b_option` VALUES ('main','cookie_name','BITRIX_SM','Имя префикса для названия cookies (без точек и пробелов):',NULL);
INSERT INTO `b_option` VALUES ('main','ALLOW_SPREAD_COOKIE','Y','Распространять куки на все домены:',NULL);
INSERT INTO `b_option` VALUES ('main','header_200','N','Посылать в заголовке статус 200 на 404 ошибку:',NULL);
INSERT INTO `b_option` VALUES ('main','error_reporting','85','Режим вывода ошибок (error_reporting):',NULL);
INSERT INTO `b_option` VALUES ('main','templates_visual_editor','N','Использовать визуальный редактор для редактирования шаблонов сайта:',NULL);
INSERT INTO `b_option` VALUES ('main','use_hot_keys','Y','Использовать горячие клавиши:',NULL);
INSERT INTO `b_option` VALUES ('main','all_bcc','','E-Mail адрес или список адресов через запятую на который будут дублироваться все исходящие сообщения:',NULL);
INSERT INTO `b_option` VALUES ('main','send_mid','N','Отправлять в письме идентификаторы почтового события и шаблона:',NULL);
INSERT INTO `b_option` VALUES ('main','fill_to_mail','N','Дублировать E-Mail адрес в заголовок:',NULL);
INSERT INTO `b_option` VALUES ('main','CONVERT_UNIX_NEWLINE_2_WINDOWS','N','Конвертировать символы новой строки<br> Unix формата в Windows формат при отправке email:',NULL);
INSERT INTO `b_option` VALUES ('main','convert_mail_header','Y','Конвертировать 8-битные символы в заголовке письма:',NULL);
INSERT INTO `b_option` VALUES ('main','mail_event_period','14','Сколько дней хранить почтовые события:',NULL);
INSERT INTO `b_option` VALUES ('main','mail_event_bulk','5','Сколько писем отправлять за один хит:',NULL);
INSERT INTO `b_option` VALUES ('main','mail_additional_parameters','','Дополнительный параметр для передачи функции mail:',NULL);
INSERT INTO `b_option` VALUES ('main','disk_space','','Ограничение дискового пространства (Мб):',NULL);
INSERT INTO `b_option` VALUES ('main','upload_dir','upload','Папка по умолчанию для загрузки файлов:',NULL);
INSERT INTO `b_option` VALUES ('main','save_original_file_name','N','Сохранять исходные имена загружаемых файлов:',NULL);
INSERT INTO `b_option` VALUES ('main','translit_original_file_name','N','Производить транслитерацию имени файла:',NULL);
INSERT INTO `b_option` VALUES ('main','convert_original_file_name','Y','Автоматически заменять невалидные символы в именах загружаемых файлов:',NULL);
INSERT INTO `b_option` VALUES ('main','image_resize_quality','95','Качество JPG при масштабировании изображений (в процентах):',NULL);
INSERT INTO `b_option` VALUES ('main','optimize_css_files','N','Объединять CSS файлы:',NULL);
INSERT INTO `b_option` VALUES ('main','optimize_js_files','N','Объединять JS файлы:',NULL);
INSERT INTO `b_option` VALUES ('main','compres_css_js_files','N','Создавать сжатую копию объединенных CSS и JS файлов:',NULL);
INSERT INTO `b_option` VALUES ('main','use_time_zones','N','Разрешить использование часовых поясов:',NULL);
INSERT INTO `b_option` VALUES ('main','auto_time_zone','N','По умолчанию автоматически определять часовой пояс по браузеру:',NULL);
INSERT INTO `b_option` VALUES ('main','map_top_menu_type','top','Тип меню для нулевого уровня карты сайта:',NULL);
INSERT INTO `b_option` VALUES ('main','map_left_menu_type','left','Тип меню для остальных уровней карты сайта:',NULL);
INSERT INTO `b_option` VALUES ('main','update_site_proxy_addr','','Адрес прокси для системы обновлений:',NULL);
INSERT INTO `b_option` VALUES ('main','update_site_proxy_port','','Порт прокси для системы обновлений:',NULL);
INSERT INTO `b_option` VALUES ('main','update_site_proxy_user','admin','Имя пользователя прокси:',NULL);
INSERT INTO `b_option` VALUES ('main','update_site_proxy_pass','Infinity_73','Пароль прокси:',NULL);
INSERT INTO `b_option` VALUES ('main','strong_update_check','Y','Усиленная проверка корректности установки обновлений:',NULL);
INSERT INTO `b_option` VALUES ('main','stable_versions_only','Y','Загружать только стабильные обновления:',NULL);
INSERT INTO `b_option` VALUES ('main','update_autocheck','','Автоматически проверять наличие обновлений:',NULL);
INSERT INTO `b_option` VALUES ('main','update_stop_autocheck','N','Остановить автоматическую проверку при возникновении ошибок:',NULL);
INSERT INTO `b_option` VALUES ('main','update_is_gzip_installed','Y','Не использовать архиватор:',NULL);
INSERT INTO `b_option` VALUES ('main','update_load_timeout','30','Продолжительность шага пошаговой загрузки обновления (сек):',NULL);
INSERT INTO `b_option` VALUES ('main','store_password','Y','Разрешить запоминание авторизации:',NULL);
INSERT INTO `b_option` VALUES ('main','use_secure_password_cookies','N','Использовать защищенное хранение авторизации в cookies:',NULL);
INSERT INTO `b_option` VALUES ('main','auth_multisite','N','Распространять авторизацию на все домены:',NULL);
INSERT INTO `b_option` VALUES ('main','allow_socserv_authorization','Y','Разрешить авторизацию через внешние сервисы:',NULL);
INSERT INTO `b_option` VALUES ('main','use_digest_auth','N','Разрешить авторизацию HTTP Digest:',NULL);
INSERT INTO `b_option` VALUES ('main','custom_register_page','','Страница регистрации (для системного компонента авторизации):',NULL);
INSERT INTO `b_option` VALUES ('main','use_encrypted_auth','N','Передавать пароль в зашифрованном виде:',NULL);
INSERT INTO `b_option` VALUES ('main','captcha_registration','N','Использовать CAPTCHA при регистрации:',NULL);
INSERT INTO `b_option` VALUES ('main','new_user_registration_email_confirmation','N','Запрашивать подтверждение регистрации по E-mail:</label><br><a href=\"/bitrix/admin/message_admin.php?lang=ru&set_filter=Y&find_type_id=NEW_USER_CONFIRM\">Перейти к редактированию почтовых ',NULL);
INSERT INTO `b_option` VALUES ('main','new_user_registration_cleanup_days','7','Сколько дней хранить пользователей с неподтвержденной регистрацией:',NULL);
INSERT INTO `b_option` VALUES ('main','new_user_email_uniq_check','N','Проверять E-mail на уникальность при регистрации:',NULL);
INSERT INTO `b_option` VALUES ('main','session_expand','Y','Продлевать сессию при активности посетителя в окне браузера:',NULL);
INSERT INTO `b_option` VALUES ('main','session_show_message','Y','Показывать пользователям сообщение об окончании сессии:',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_cleanup_days','7','Сколько дней хранить события:',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_logout','N','Записывать выход из системы',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_login_success','N','Записывать успешный вход',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_login_fail','N','Записывать ошибки входа',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_register','N','Записывать регистрацию нового пользователя',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_register_fail','N','Записывать ошибки регистрации',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_password_request','N','Записывать запросы на смену пароля',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_password_change','N','Записывать смену пароля',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_user_edit','N','Записывать редактирование пользователя',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_user_delete','N','Записывать удаление пользователя',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_user_groups','N','Записывать изменение групп пользователя',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_group_policy','N','Записывать изменение политики безопасности группы',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_module_access','N','Записывать изменение доступа к модулю',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_file_access','N','Записывать изменение доступа к файлу',NULL);
INSERT INTO `b_option` VALUES ('main','event_log_task','N','Записывать изменение уровня доступа',NULL);
INSERT INTO `b_option` VALUES ('main','auth_controller_sso','N','Авторизовывать на этом сайте пользователей других сайтов контроллера:',NULL);
INSERT INTO `b_option` VALUES ('main','show_panel_for_users','N;','',NULL);
INSERT INTO `b_option` VALUES ('main','GROUP_DEFAULT_RIGHT','D','Right for groups by default',NULL);
INSERT INTO `b_option` VALUES ('main','last_files_count','19405','',NULL);
INSERT INTO `b_option` VALUES ('main','site_checker_success','N','',NULL);
INSERT INTO `b_option` VALUES ('main','dump_do_clouds','0','',NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_cluster`
-- 




DROP TABLE IF EXISTS `b_perf_cluster`;
CREATE TABLE `b_perf_cluster` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `THREADS` int(11) DEFAULT NULL,
  `HITS` int(11) DEFAULT NULL,
  `ERRORS` int(11) DEFAULT NULL,
  `PAGES_PER_SECOND` float DEFAULT NULL,
  `PAGE_EXEC_TIME` float DEFAULT NULL,
  `PAGE_RESP_TIME` float DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_component`
-- 




DROP TABLE IF EXISTS `b_perf_component`;
CREATE TABLE `b_perf_component` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `HIT_ID` int(18) DEFAULT NULL,
  `NN` int(18) DEFAULT NULL,
  `CACHE_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CACHE_SIZE` int(11) DEFAULT NULL,
  `COMPONENT_TIME` float DEFAULT NULL,
  `QUERIES` int(11) DEFAULT NULL,
  `QUERIES_TIME` float DEFAULT NULL,
  `COMPONENT_NAME` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_PERF_COMPONENT_0` (`HIT_ID`,`NN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_error`
-- 




DROP TABLE IF EXISTS `b_perf_error`;
CREATE TABLE `b_perf_error` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `HIT_ID` int(18) DEFAULT NULL,
  `ERRNO` int(18) DEFAULT NULL,
  `ERRSTR` text COLLATE utf8_unicode_ci,
  `ERRFILE` text COLLATE utf8_unicode_ci,
  `ERRLINE` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_PERF_ERROR_0` (`HIT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_hit`
-- 




DROP TABLE IF EXISTS `b_perf_hit`;
CREATE TABLE `b_perf_hit` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_HIT` datetime DEFAULT NULL,
  `IS_ADMIN` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REQUEST_METHOD` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERVER_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERVER_PORT` int(11) DEFAULT NULL,
  `SCRIPT_NAME` text COLLATE utf8_unicode_ci,
  `REQUEST_URI` text COLLATE utf8_unicode_ci,
  `INCLUDED_FILES` int(11) DEFAULT NULL,
  `MEMORY_PEAK_USAGE` int(11) DEFAULT NULL,
  `CACHE_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CACHE_SIZE` int(11) DEFAULT NULL,
  `QUERIES` int(11) DEFAULT NULL,
  `QUERIES_TIME` float DEFAULT NULL,
  `COMPONENTS` int(11) DEFAULT NULL,
  `COMPONENTS_TIME` float DEFAULT NULL,
  `SQL_LOG` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAGE_TIME` float DEFAULT NULL,
  `PROLOG_TIME` float DEFAULT NULL,
  `PROLOG_BEFORE_TIME` float DEFAULT NULL,
  `AGENTS_TIME` float DEFAULT NULL,
  `PROLOG_AFTER_TIME` float DEFAULT NULL,
  `WORK_AREA_TIME` float DEFAULT NULL,
  `EPILOG_TIME` float DEFAULT NULL,
  `EPILOG_BEFORE_TIME` float DEFAULT NULL,
  `EVENTS_TIME` float DEFAULT NULL,
  `EPILOG_AFTER_TIME` float DEFAULT NULL,
  `MENU_RECALC` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_PERF_HIT_0` (`DATE_HIT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_index_ban`
-- 




DROP TABLE IF EXISTS `b_perf_index_ban`;
CREATE TABLE `b_perf_index_ban` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BAN_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAMES` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_index_complete`
-- 




DROP TABLE IF EXISTS `b_perf_index_complete`;
CREATE TABLE `b_perf_index_complete` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BANNED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAMES` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INDEX_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_perf_index_complete_0` (`TABLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_index_suggest`
-- 




DROP TABLE IF EXISTS `b_perf_index_suggest`;
CREATE TABLE `b_perf_index_suggest` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SQL_MD5` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SQL_COUNT` int(11) DEFAULT NULL,
  `SQL_TIME` float DEFAULT NULL,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_ALIAS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAMES` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SQL_TEXT` text COLLATE utf8_unicode_ci,
  `SQL_EXPLAIN` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_perf_index_suggest_0` (`SQL_MD5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_index_suggest_sql`
-- 




DROP TABLE IF EXISTS `b_perf_index_suggest_sql`;
CREATE TABLE `b_perf_index_suggest_sql` (
  `SUGGEST_ID` int(11) NOT NULL DEFAULT '0',
  `SQL_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SUGGEST_ID`,`SQL_ID`),
  KEY `ix_b_perf_index_suggest_sql_0` (`SQL_ID`,`SUGGEST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_sql`
-- 




DROP TABLE IF EXISTS `b_perf_sql`;
CREATE TABLE `b_perf_sql` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `HIT_ID` int(18) DEFAULT NULL,
  `COMPONENT_ID` int(18) DEFAULT NULL,
  `NN` int(18) DEFAULT NULL,
  `QUERY_TIME` float DEFAULT NULL,
  `MODULE_NAME` text COLLATE utf8_unicode_ci,
  `COMPONENT_NAME` text COLLATE utf8_unicode_ci,
  `SQL_TEXT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_PERF_SQL_0` (`HIT_ID`,`NN`),
  KEY `IX_B_PERF_SQL_1` (`COMPONENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_sql_backtrace`
-- 




DROP TABLE IF EXISTS `b_perf_sql_backtrace`;
CREATE TABLE `b_perf_sql_backtrace` (
  `SQL_ID` int(18) NOT NULL DEFAULT '0',
  `NN` int(18) NOT NULL DEFAULT '0',
  `FILE_NAME` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LINE_NO` int(18) DEFAULT NULL,
  `CLASS_NAME` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FUNCTION_NAME` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`SQL_ID`,`NN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_tab_column_stat`
-- 




DROP TABLE IF EXISTS `b_perf_tab_column_stat`;
CREATE TABLE `b_perf_tab_column_stat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_ROWS` float DEFAULT NULL,
  `COLUMN_ROWS` float DEFAULT NULL,
  `VALUE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_perf_tab_column_stat` (`TABLE_NAME`,`COLUMN_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_tab_stat`
-- 




DROP TABLE IF EXISTS `b_perf_tab_stat`;
CREATE TABLE `b_perf_tab_stat` (
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TABLE_SIZE` float DEFAULT NULL,
  `TABLE_ROWS` float DEFAULT NULL,
  PRIMARY KEY (`TABLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_perf_test`
-- 




DROP TABLE IF EXISTS `b_perf_test`;
CREATE TABLE `b_perf_test` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `REFERENCE_ID` int(18) DEFAULT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_PERF_TEST_0` (`REFERENCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating`
-- 




DROP TABLE IF EXISTS `b_rating`;
CREATE TABLE `b_rating` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CALCULATION_METHOD` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SUM',
  `CREATED` datetime DEFAULT NULL,
  `LAST_MODIFIED` datetime DEFAULT NULL,
  `LAST_CALCULATED` datetime DEFAULT NULL,
  `POSITION` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `AUTHORITY` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `CALCULATED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CONFIGS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_rating`
-- 


INSERT INTO `b_rating` VALUES (1,'N','Рейтинг','USER','SUM','2013-09-26 18:01:00',NULL,NULL,'Y','N','N','a:3:{s:4:\"MAIN\";a:2:{s:4:\"VOTE\";a:1:{s:4:\"USER\";a:2:{s:11:\"COEFFICIENT\";s:1:\"1\";s:5:\"LIMIT\";s:2:\"30\";}}s:6:\"RATING\";a:1:{s:5:\"BONUS\";a:2:{s:6:\"ACTIVE\";s:1:\"Y\";s:11:\"COEFFICIENT\";s:1:\"1\";}}}s:5:\"FORUM\";a:2:{s:4:\"VOTE\";a:2:{s:5:\"TOPIC\";a:3:{s:6:\"ACTIVE\";s:1:\"Y\";s:11:\"COEFFICIENT\";s:3:\"0.5\";s:5:\"LIMIT\";s:2:\"30\";}s:4:\"POST\";a:3:{s:6:\"ACTIVE\";s:1:\"Y\";s:11:\"COEFFICIENT\";s:3:\"0.1\";s:5:\"LIMIT\";s:2:\"30\";}}s:6:\"RATING\";a:1:{s:8:\"ACTIVITY\";a:9:{s:6:\"ACTIVE\";s:1:\"Y\";s:16:\"TODAY_TOPIC_COEF\";s:3:\"0.4\";s:15:\"WEEK_TOPIC_COEF\";s:3:\"0.2\";s:16:\"MONTH_TOPIC_COEF\";s:3:\"0.1\";s:14:\"ALL_TOPIC_COEF\";s:1:\"0\";s:15:\"TODAY_POST_COEF\";s:3:\"0.2\";s:14:\"WEEK_POST_COEF\";s:3:\"0.1\";s:15:\"MONTH_POST_COEF\";s:4:\"0.05\";s:13:\"ALL_POST_COEF\";s:1:\"0\";}}}s:4:\"BLOG\";a:2:{s:4:\"VOTE\";a:2:{s:4:\"POST\";a:3:{s:6:\"ACTIVE\";s:1:\"Y\";s:11:\"COEFFICIENT\";s:3:\"0.5\";s:5:\"LIMIT\";s:2:\"30\";}s:7:\"COMMENT\";a:3:{s:6:\"ACTIVE\";s:1:\"Y\";s:11:\"COEFFICIENT\";s:3:\"0.1\";s:5:\"LIMIT\";s:2:\"30\";}}s:6:\"RATING\";a:1:{s:8:\"ACTIVITY\";a:9:{s:6:\"ACTIVE\";s:1:\"Y\";s:15:\"TODAY_POST_COEF\";s:3:\"0.4\";s:14:\"WEEK_POST_COEF\";s:3:\"0.2\";s:15:\"MONTH_POST_COEF\";s:3:\"0.1\";s:13:\"ALL_POST_COEF\";s:1:\"0\";s:18:\"TODAY_COMMENT_COEF\";s:3:\"0.2\";s:17:\"WEEK_COMMENT_COEF\";s:3:\"0.1\";s:18:\"MONTH_COMMENT_COEF\";s:4:\"0.05\";s:16:\"ALL_COMMENT_COEF\";s:1:\"0\";}}}}');
INSERT INTO `b_rating` VALUES (2,'N','Авторитет','USER','SUM','2013-09-26 18:01:00',NULL,NULL,'Y','Y','N','a:3:{s:4:\"MAIN\";a:2:{s:4:\"VOTE\";a:1:{s:4:\"USER\";a:3:{s:6:\"ACTIVE\";s:1:\"Y\";s:11:\"COEFFICIENT\";s:1:\"1\";s:5:\"LIMIT\";s:1:\"0\";}}s:6:\"RATING\";a:1:{s:5:\"BONUS\";a:2:{s:6:\"ACTIVE\";s:1:\"Y\";s:11:\"COEFFICIENT\";s:1:\"1\";}}}s:5:\"FORUM\";a:2:{s:4:\"VOTE\";a:2:{s:5:\"TOPIC\";a:2:{s:11:\"COEFFICIENT\";s:1:\"1\";s:5:\"LIMIT\";s:2:\"30\";}s:4:\"POST\";a:2:{s:11:\"COEFFICIENT\";s:1:\"1\";s:5:\"LIMIT\";s:2:\"30\";}}s:6:\"RATING\";a:1:{s:8:\"ACTIVITY\";a:8:{s:16:\"TODAY_TOPIC_COEF\";s:2:\"20\";s:15:\"WEEK_TOPIC_COEF\";s:2:\"10\";s:16:\"MONTH_TOPIC_COEF\";s:1:\"5\";s:14:\"ALL_TOPIC_COEF\";s:1:\"0\";s:15:\"TODAY_POST_COEF\";s:3:\"0.4\";s:14:\"WEEK_POST_COEF\";s:3:\"0.2\";s:15:\"MONTH_POST_COEF\";s:3:\"0.1\";s:13:\"ALL_POST_COEF\";s:1:\"0\";}}}s:4:\"BLOG\";a:2:{s:4:\"VOTE\";a:2:{s:4:\"POST\";a:2:{s:11:\"COEFFICIENT\";s:1:\"1\";s:5:\"LIMIT\";s:2:\"30\";}s:7:\"COMMENT\";a:2:{s:11:\"COEFFICIENT\";s:1:\"1\";s:5:\"LIMIT\";s:2:\"30\";}}s:6:\"RATING\";a:1:{s:8:\"ACTIVITY\";a:8:{s:15:\"TODAY_POST_COEF\";s:3:\"0.4\";s:14:\"WEEK_POST_COEF\";s:3:\"0.2\";s:15:\"MONTH_POST_COEF\";s:3:\"0.1\";s:13:\"ALL_POST_COEF\";s:1:\"0\";s:18:\"TODAY_COMMENT_COEF\";s:3:\"0.2\";s:17:\"WEEK_COMMENT_COEF\";s:3:\"0.1\";s:18:\"MONTH_COMMENT_COEF\";s:4:\"0.05\";s:16:\"ALL_COMMENT_COEF\";s:1:\"0\";}}}}');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_component`
-- 




DROP TABLE IF EXISTS `b_rating_component`;
CREATE TABLE `b_rating_component` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ENTITY_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `RATING_TYPE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `COMPLEX_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CALC_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `EXCEPTION_METHOD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_MODIFIED` datetime DEFAULT NULL,
  `LAST_CALCULATED` datetime DEFAULT NULL,
  `NEXT_CALCULATION` datetime DEFAULT NULL,
  `REFRESH_INTERVAL` int(11) NOT NULL,
  `CONFIG` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_RATING_ID_1` (`RATING_ID`,`ACTIVE`,`NEXT_CALCULATION`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_component_results`
-- 




DROP TABLE IF EXISTS `b_rating_component_results`;
CREATE TABLE `b_rating_component_results` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `RATING_TYPE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `COMPLEX_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CURRENT_VALUE` decimal(18,4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ENTITY_TYPE_ID` (`ENTITY_TYPE_ID`),
  KEY `IX_COMPLEX_NAME` (`COMPLEX_NAME`),
  KEY `IX_RATING_ID_2` (`RATING_ID`,`COMPLEX_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_prepare`
-- 




DROP TABLE IF EXISTS `b_rating_prepare`;
CREATE TABLE `b_rating_prepare` (
  `ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_results`
-- 




DROP TABLE IF EXISTS `b_rating_results`;
CREATE TABLE `b_rating_results` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `CURRENT_VALUE` decimal(18,4) DEFAULT NULL,
  `PREVIOUS_VALUE` decimal(18,4) DEFAULT NULL,
  `CURRENT_POSITION` int(11) DEFAULT '0',
  `PREVIOUS_POSITION` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_RATING_3` (`RATING_ID`,`ENTITY_TYPE_ID`,`ENTITY_ID`),
  KEY `IX_RATING_4` (`RATING_ID`,`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_rule`
-- 




DROP TABLE IF EXISTS `b_rating_rule`;
CREATE TABLE `b_rating_rule` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NAME` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_MODULE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONDITION_CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_CONFIG` text COLLATE utf8_unicode_ci NOT NULL,
  `ACTION_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ACTION_CONFIG` text COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ACTIVATE_CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVATE_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEACTIVATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DEACTIVATE_CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEACTIVATE_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED` datetime DEFAULT NULL,
  `LAST_MODIFIED` datetime DEFAULT NULL,
  `LAST_APPLIED` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_rating_rule`
-- 


INSERT INTO `b_rating_rule` VALUES (1,'N','Добавление в группу пользователей, имеющих право голосовать за рейтинг','USER','AUTHORITY',NULL,'CRatingRulesMain','ratingCheck','a:1:{s:9:\"AUTHORITY\";a:2:{s:16:\"RATING_CONDITION\";i:1;s:12:\"RATING_VALUE\";i:1;}}','ADD_TO_GROUP','a:1:{s:12:\"ADD_TO_GROUP\";a:1:{s:8:\"GROUP_ID\";s:1:\"3\";}}','N','CRatingRulesMain','addToGroup','N','CRatingRulesMain ','addToGroup','2013-09-26 18:01:00','2013-09-26 18:01:00',NULL);
INSERT INTO `b_rating_rule` VALUES (2,'N','Удаление из группы пользователей, не имеющих права голосовать за рейтинг','USER','AUTHORITY',NULL,'CRatingRulesMain','ratingCheck','a:1:{s:9:\"AUTHORITY\";a:2:{s:16:\"RATING_CONDITION\";i:2;s:12:\"RATING_VALUE\";i:1;}}','REMOVE_FROM_GROUP','a:1:{s:17:\"REMOVE_FROM_GROUP\";a:1:{s:8:\"GROUP_ID\";s:1:\"3\";}}','N','CRatingRulesMain','removeFromGroup','N','CRatingRulesMain ','removeFromGroup','2013-09-26 18:01:00','2013-09-26 18:01:00',NULL);
INSERT INTO `b_rating_rule` VALUES (3,'N','Добавление в группу пользователей, имеющих право голосовать за авторитет','USER','AUTHORITY',NULL,'CRatingRulesMain','ratingCheck','a:1:{s:9:\"AUTHORITY\";a:2:{s:16:\"RATING_CONDITION\";i:1;s:12:\"RATING_VALUE\";i:2;}}','ADD_TO_GROUP','a:1:{s:12:\"ADD_TO_GROUP\";a:1:{s:8:\"GROUP_ID\";s:1:\"4\";}}','N','CRatingRulesMain','addToGroup','N','CRatingRulesMain ','addToGroup','2013-09-26 18:01:01','2013-09-26 18:01:01',NULL);
INSERT INTO `b_rating_rule` VALUES (4,'N','Удаление из группы пользователей, не имеющих права голосовать за авторитет','USER','AUTHORITY',NULL,'CRatingRulesMain','ratingCheck','a:1:{s:9:\"AUTHORITY\";a:2:{s:16:\"RATING_CONDITION\";i:2;s:12:\"RATING_VALUE\";i:2;}}','REMOVE_FROM_GROUP','a:1:{s:17:\"REMOVE_FROM_GROUP\";a:1:{s:8:\"GROUP_ID\";s:1:\"4\";}}','N','CRatingRulesMain','removeFromGroup','N','CRatingRulesMain ','removeFromGroup','2013-09-26 18:01:01','2013-09-26 18:01:01',NULL);
INSERT INTO `b_rating_rule` VALUES (5,'Y','Автоматическое голосование за авторитет пользователя','USER','VOTE',NULL,'CRatingRulesMain','voteCheck','a:1:{s:4:\"VOTE\";a:6:{s:10:\"VOTE_LIMIT\";i:90;s:11:\"VOTE_RESULT\";i:10;s:16:\"VOTE_FORUM_TOPIC\";d:0.5;s:15:\"VOTE_FORUM_POST\";d:0.10000000000000001;s:14:\"VOTE_BLOG_POST\";d:0.5;s:17:\"VOTE_BLOG_COMMENT\";d:0.10000000000000001;}}','empty','a:0:{}','N','empty','empty','N','empty ','empty','2013-09-26 18:01:01','2013-09-26 18:01:01',NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_rule_vetting`
-- 




DROP TABLE IF EXISTS `b_rating_rule_vetting`;
CREATE TABLE `b_rating_rule_vetting` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RULE_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `ACTIVATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `APPLIED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `RULE_ID` (`RULE_ID`,`ENTITY_TYPE_ID`,`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_user`
-- 




DROP TABLE IF EXISTS `b_rating_user`;
CREATE TABLE `b_rating_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `BONUS` decimal(18,4) DEFAULT '0.0000',
  `VOTE_WEIGHT` decimal(18,4) DEFAULT '0.0000',
  `VOTE_COUNT` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RATING_ID` (`RATING_ID`,`ENTITY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_rating_user`
-- 


INSERT INTO `b_rating_user` VALUES (1,2,1,3.0000,30.0000,13);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_vote`
-- 




DROP TABLE IF EXISTS `b_rating_vote`;
CREATE TABLE `b_rating_vote` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_VOTING_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `OWNER_ID` int(11) NOT NULL,
  `VALUE` decimal(18,4) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED` datetime NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `USER_IP` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_RAT_VOTE_ID` (`RATING_VOTING_ID`,`USER_ID`),
  KEY `IX_RAT_VOTE_ID_2` (`ENTITY_TYPE_ID`,`ENTITY_ID`,`USER_ID`),
  KEY `IX_RAT_VOTE_ID_3` (`OWNER_ID`,`CREATED`),
  KEY `IX_RAT_VOTE_ID_4` (`USER_ID`),
  KEY `IX_RAT_VOTE_ID_5` (`CREATED`,`VALUE`),
  KEY `IX_RAT_VOTE_ID_6` (`ACTIVE`),
  KEY `IX_RAT_VOTE_ID_7` (`RATING_VOTING_ID`,`CREATED`),
  KEY `IX_RAT_VOTE_ID_8` (`ENTITY_TYPE_ID`,`CREATED`),
  KEY `IX_RAT_VOTE_ID_9` (`CREATED`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_vote_group`
-- 




DROP TABLE IF EXISTS `b_rating_vote_group`;
CREATE TABLE `b_rating_vote_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUP_ID` int(11) NOT NULL,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `RATING_ID` (`GROUP_ID`,`TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_rating_vote_group`
-- 


INSERT INTO `b_rating_vote_group` VALUES (5,1,'A');
INSERT INTO `b_rating_vote_group` VALUES (1,1,'R');
INSERT INTO `b_rating_vote_group` VALUES (3,1,'R');
INSERT INTO `b_rating_vote_group` VALUES (2,3,'R');
INSERT INTO `b_rating_vote_group` VALUES (4,3,'R');
INSERT INTO `b_rating_vote_group` VALUES (6,4,'A');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_voting`
-- 




DROP TABLE IF EXISTS `b_rating_voting`;
CREATE TABLE `b_rating_voting` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `OWNER_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED` datetime DEFAULT NULL,
  `LAST_CALCULATED` datetime DEFAULT NULL,
  `TOTAL_VALUE` decimal(18,4) NOT NULL,
  `TOTAL_VOTES` int(11) NOT NULL,
  `TOTAL_POSITIVE_VOTES` int(11) NOT NULL,
  `TOTAL_NEGATIVE_VOTES` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ENTITY_TYPE_ID_2` (`ENTITY_TYPE_ID`,`ENTITY_ID`,`ACTIVE`),
  KEY `IX_ENTITY_TYPE_ID_4` (`TOTAL_VALUE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_voting_prepare`
-- 




DROP TABLE IF EXISTS `b_rating_voting_prepare`;
CREATE TABLE `b_rating_voting_prepare` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_VOTING_ID` int(11) NOT NULL,
  `TOTAL_VALUE` decimal(18,4) NOT NULL,
  `TOTAL_VOTES` int(11) NOT NULL,
  `TOTAL_POSITIVE_VOTES` int(11) NOT NULL,
  `TOTAL_NEGATIVE_VOTES` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_RATING_VOTING_ID` (`RATING_VOTING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_rating_weight`
-- 




DROP TABLE IF EXISTS `b_rating_weight`;
CREATE TABLE `b_rating_weight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_FROM` decimal(18,4) NOT NULL,
  `RATING_TO` decimal(18,4) NOT NULL,
  `WEIGHT` decimal(18,4) DEFAULT '0.0000',
  `COUNT` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_rating_weight`
-- 


INSERT INTO `b_rating_weight` VALUES (1,-1000000.0000,1000000.0000,1.0000,10);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_content`
-- 




DROP TABLE IF EXISTS `b_search_content`;
CREATE TABLE `b_search_content` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_CHANGE` datetime NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CUSTOM_RANK` int(11) NOT NULL DEFAULT '0',
  `USER_ID` int(11) DEFAULT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ENTITY_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` text COLLATE utf8_unicode_ci,
  `TITLE` text COLLATE utf8_unicode_ci,
  `BODY` longtext COLLATE utf8_unicode_ci,
  `TAGS` text COLLATE utf8_unicode_ci,
  `PARAM1` text COLLATE utf8_unicode_ci,
  `PARAM2` text COLLATE utf8_unicode_ci,
  `UPD` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_FROM` datetime DEFAULT NULL,
  `DATE_TO` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_B_SEARCH_CONTENT` (`MODULE_ID`,`ITEM_ID`),
  KEY `IX_B_SEARCH_CONTENT_1` (`MODULE_ID`,`PARAM1`(50),`PARAM2`(50)),
  KEY `IX_B_SEARCH_CONTENT_2` (`ENTITY_ID`(50),`ENTITY_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_search_content`
-- 


INSERT INTO `b_search_content` VALUES (5,'2013-09-26 18:37:08','iblock','S1',0,NULL,NULL,NULL,'=ID=1&EXTERNAL_ID=1&IBLOCK_TYPE_ID=news&IBLOCK_ID=3&IBLOCK_CODE=news&IBLOCK_EXTERNAL_ID=12&CODE=','qqq','',NULL,'news','3',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (8,'2014-10-30 00:35:22','iblock','7',0,NULL,NULL,NULL,'=ID=7&EXTERNAL_ID=429&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=affiliatted&IBLOCK_ID=6&IBLOCK_CODE=affiliated&IBLOCK_EXTERNAL_ID=22&CODE=','Смирнов Алексей Вадимович','','','affiliatted','6',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (9,'2013-10-03 21:19:37','main','s1|/test.php',0,NULL,NULL,NULL,'/test.php','МФО','','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (12,'2014-04-08 13:27:07','main','s1|/jobs/vacancies.php',0,NULL,NULL,NULL,'/jobs/vacancies.php','Вакансии','','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (20,'2015-10-11 16:27:16','main','s1|/index.php',0,NULL,NULL,NULL,'/index.php','Экспресс деньги','Наши партнеры','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (21,'2014-10-29 23:40:06','main','s1|/loan/howtoget.php',0,NULL,NULL,NULL,'/loan/howtoget.php','Готовый сайт для Вашей МФО | Как получить займ','Как получить займ?\rДля получения микрозайма в &laquo;Плюс кредит&raquo; необходимо соблюдение нескольких простых условий: \r- иметь постоянный доход;\r- проживать или работать в городе (области), где Вы хотите взять микрозайм;\r- и соответствовать возрастному критерию &mdash; от 21 до 70 лет \n Все что Вам нужно:\rНайти удобный для Вас офис выдачи микрозаймов, предоставить свой паспорт и заполнить анкету \nВ случае одобрения Вы сразу получите деньги \rОформить заявку, не выходя из дома &mdash; заполните анкету онлайн на сайте. \nВ случае одобрения наши специалисты свяжутся с Вами в течение 2 часов и пригласят на оформление. \nНаши офисы приятно посещать, с нашими сотрудниками приятно общаться!','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (22,'2014-10-30 00:20:28','main','s1|/loan/howtopay.php',0,NULL,NULL,NULL,'/loan/howtopay.php','Готовый сайт для Вашей МФО | Как погасить займ?','Как погасить займ? \rВ любом офисе компании &laquo;Плюс кредит&raquo; \rЕсли подходит срок оплаты, а у Вас нет возможности оплатить самостоятельно, Вы можете попросить друга или родственника, достигшего совершеннолетия, оплатить за Вас: ему необходимо иметь при себе паспорт и знать номер Вашего договора микрозайма.','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (23,'2014-03-24 19:00:11','main','s1|/recomendations/repay.php',0,NULL,NULL,NULL,'/recomendations/repay.php','Как не стать должником','Рекомендации заемщикам \r-\r Мы настоятельно рекомендуем\rНе брать займ на сумму\nпревышающую 50% суммы\nВашего ежемесячного дохода \rНе брать займ, если Вы не уверены,\nчто сможете его погасить без\nосложнений и прочего \rНе откладывать визит в офис\n&laquo;Независимость-МФО&raquo;, если у Вас нет\nвозможности погасить займ в срок,\nуказанный в договоре &mdash; лучше\nосуществить пролонгацию либо\nчастичное погашение \rЕсли вы не успеваете вовремя погасить займ, рекомендуем воспользоваться услугой «Пролонгация» (продление). Вы можете отложить возврат денег на срок до 16 дней, оплатив при этом только начисленные проценты (по 2% за каждый день от даты заключения договора до дня пролонгации). Продлить договор вы можете в любом офисе компании вне зависимости от города выдачи. \r-\r Договор микрозайма может быть продлен на срок от 1 до 16 дней\rв любой день до даты погашения \rв день погашения микрозайма \rне позднее 16 дня от даты погашения микрозайма \r-\r Обратите внимание\rПродлевать договор микрозайма\nможно 10 раз \rПродлевать договор микрозайма\nможно 10 раз\nПродлить договор микрозайма\nможно в любом \rофисе компании\rвне зависимости от города выдачи \rДва раза продлить договор\nмикрозайма в один день невозможно \rОсуществляет продление обязательно\nтолько заемщик в одном из \rофисов «Независимость-МФО»\rПри продлении договора, заемщик\nдолжен оплатить проценты за фактическое\nпользование денежными средствами,\nто есть за срок от даты выдачи до дня\nобращения в офис «Независимость-МФО» для оплаты\nпроцентов \rДля продления договора заемщику\nобязательно иметь при себе паспорт \rПосле частичного погашения продление\nневозможно \rПролонгация в день выдачи микрозама\nневозможна','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (24,'2014-03-21 20:43:49','main','s1|/documents/rules.php',0,NULL,NULL,NULL,'/documents/rules.php','Правила предоставления микрозаймов','Правила предоставления микрозаймов\rООО \r&laquo;Независимость-МФО&raquo;\r \rПравила предоставления микрозаймов \r1. Общие положения и термины\r1.1.\rНастоящие правила предоставления микрозаймов разработаны в соответствии с требованиями Федерального закона от 02.07.2010г. № 151-ФЗ «О Микрофинансовой деятельности и микрофинансовых организациях».\n1.2.\rНастоящие правила определяют порядок и условия предоставления микрозаймов в ООО «Независимость-МФО».\n1.3.\rНастоящие Правила доступны всем лицам для ознакомления и содержат основные условия предоставления микрозаймов. Копия Правил предоставления микрозаймов размещается в месте, доступном для обозрения и ознакомления с ними любого заинтересованного лица - в офисе ООО «Независимость-МФО», на сайте организации по адресу www.nezavisimost18.ru.\n1.4.\rТермины и определения:\n1.4.1. \rМФ\r&ndash; ООО «Независимость-МФО».\n1.4.2. \rПравила\r– настоящие Правила Предоставления Микрозаймов.\n1.4.3. \rМикрозаем\r– заем, предоставляемый займодавцем заемщику на условиях, предусмотренных договором займа, в сумме, не превышающей один миллион рублей.\n1.4.4. \rДоговор микрозайма\r– договор займа, сумма которого не превышает один миллион рублей.\n1.4.5. \rКлиент\r– физическое и/или юридическое лицо, находящееся на обслуживании в \rМФ\r, т.е. заключившее с ним договор займа или лицо обратившееся в \rМФ\rдля получения микрозайма;\n1.4.6. \rЗаявка\r– заявка на предоставление микрозайма.\n1.4.7. \rВыгодоприобретатель\r– лицо, к выгоде которого фактически действует клиент, в том числе на основании агентского договора, договоров поручения, комиссии и доверительного управления, при совершении операций с недвижимым имуществом.\n1.4.8. \rИдентификация\r– совокупность мероприятий по установлению определенных законодательством Российской Федерации сведений о клиентах, их представителях, выгодоприобретателях, по подтверждению достоверности этих сведений с использованием оригиналов документов и (или) надлежащим образом заверенных копий.\n1.4.9. \rПредставитель клиента/Представитель\r– физическое лицо, действующее от имени и в интересах Клиента в соответствии с полномочиями, основанными на доверенности, законе, либо акте уполномоченного на то государственного органа или органа местного самоуправления при обслуживании в \rМФ\r.\n1.4.10. \rПредставитель МФ\r– лицо, уполномоченное представлять интересы \rМФ\rи действовать от его имени.\r2. Порядок и условия предоставления микрозаймов\r2.1.\rПорядок подачи заявки на предоставление микрозайма:\n2.1.1. Для получения займа клиент должен ознакомиться с Правилами предоставления микрозаймов ООО «Независимость-МФО».\n2.1.2. При обращении клиента для получения микрозайма, представитель \rМФ\rвыясняет цель получения микрозайма, разъясняет обязательные условия предоставления микрозайма, знакомит с перечнем документов, необходимых для его получения.\n2.1.3. После ознакомления с настоящими Правилами и при согласии на предлагаемые обязательные условия предоставления займа, заемщик заполняет анкету, установленного образца, для предоставления ему микрозайма.\n2.1.4. Клиент собственноручно заполняет анкету на предоставление ему микрозайма. Если есть поручитель, то он собственноручно заполняет анкету на поручительство. Все графы анкеты, подлежащие обязательному заполнению, заполняются в обязательном порядке, иначе анкета будет считаться недействительной.\n2.1.5. Анкета на предоставление микрозайма считается поданной после подписания ее клиентом и передачи ее менеджеру.\n2.2.\rПорядок рассмотрения анкеты на предоставление микрозайма:\n2.2.1. При подаче заявки Клиент обязан предоставить ниже перечисленные документы и сведения, запрашиваемые менеджером \rМФ\r, необходимые для решения вопроса о предоставлении всех видов займа:\rПаспорт ГР РФ;\rИНН или водительское удостоверение или пенсионное страховое свидетельство (один из трех документов на выбор)\rСведения о доходах.\rПри необходимости в зависимости от вида займа:\rСвидетельство ИП;\rПаспорта всех совместно проживающих совершеннолетних членов семьи;\rСправка с места работы по форме НДФЛ №2;\rМедицинский страховой полис;\rСправка об отсутствии долгов по коммунальным платежам;\rИные документы в соответствии с условиями займа и действующего законодательства в том числе.\r2.2.2. Специалисты\rМФ\rпроводят анализ и возможную необходимую проверку представленных документов клиента (Выгодоприобретателя и Представителя), оценивают его финансовое состояние, на основании чего принимается решение о выдаче займа клиенту.\n2.2.3. По некоторым видам займа обязательным условием является посещение менеджером \rМФ\rбизнеса клиента и/или места его жительства.\n2.2.4. Срок рассмотрения поданной заявки на получение микрозайма зависит от вида микрозайма.\n2.2.5.\rМФ\rвправе принять мотивированное решение об отказе в предоставлении займа клиенту в соответствии со следующим Перечнем:\rПредоставления поддельных и недостоверных документов и сведений;\rВ случае отрицательной кредитной истории (данные из Бюро кредитных историй);\rРанее клиенту был выдан микрозайм и срок его возврата не истек;\rВ случае нарушения клиентом условий договора по ранее выданному ему займу.\rЕсли имеются иные обстоятельства, препятствующие заключению договора.\r2.2.6. В случае принятия решения об отказе, менеджер фиксирует данный факт в журнале регистрации заявок в разделе заключение менеджера с указанием причины.\n2.2.7. Клиент в обязательном порядке информируется о результатах рассмотрения заявки на предоставление займа.\n2.2.8. При положительном решении\rМФ\rзаключает с клиентом договор о предоставлении микрозайма.\n2.3.\rПорядок заключения договора микрозайма\n2.3.1 Клиент ознакамливается под роспись с порядком и условиями предоставления микрозайма, о его правах и обязанностях, связанных с получением микрозайма, а так же должен быть проинформирован до получения им микрозайма, об условиях договора микрозайма, о возможности и порядке изменения его условий по инициативе \rМФ\rи клиента (в части страхования, подсудности), о перечне и размере всех платежей, связанных с получением, обслуживанием и возвратом микрозайма, а также с нарушением условий договора микрозайма.\n2.3.2 Обязательной частью договора микрозайма является график платежей, выдаваемый клиенту, в котором указаны срок и сумма всех выплат по договору займа и процентов по нему.\n2.3.3 Все необходимые документы подписываются представителем\rМФ\rи клиентом, на договорах займа ставятся печати.\n2.3.4 Выдача займа клиенту может осуществляться как наличными денежными средствами, так и перечислением средств на указанный клиентом счет в банке.\r3. Погашение займа\r3.1.\rПогашение займа осуществляется в соответствии с графиком платежей или в ином, установленном сторонами договора порядке.\n3.2.\rПри внесении клиентом денежных средств осуществляется следующий порядок их зачисления:\rВ первую очередь погашаются издержки Займодавца по получению исполнения;\rВо вторую очередь погашается сумма начисленных процентов на сумму займа;\rВ третью очередь основная сумма займа;\rВ четвёртую очередь погашаются начисленные по договору пени;\r3.3.\rВ случае необходимости, по требованию Менеджера, при погашении микрозайма, Клиент обязан представить документы, необходимые для идентификации личности указанные в п. 8.1.\n3.4.\rТребования Займодавца об уплате неустойки, процентов за пользование чужими денежными средствами, иные денежные требования, связанные с применением мер гражданско-правовой ответственности, могут быть добровольно удовлетворены Заемщиком как до, так после удовлетворения требований Займодавца в порядке ст. 319 Гражданского кодекса РФ.\r4. Права и обязанности клиента\r4.1.\rКлиент, подавший заявку на предоставление микрозайма \rобязан:\r4.1.1. Знакомиться с правилами предоставления микрозаймов, утвержденными \rМФ\r.\n4.1.2. Клиент обязан предоставлять документы и сведения, запрашиваемые \rМФ\rв соответствии с п. 1, ч. 1, ст. 9 Федерального закона от 02.07.2010 № 151-ФЗ «О микрофинансовой деятельности и микрофинансовых организациях», и иными Федеральными законами.\n4.2.\rКлиент вправе получать полную и достоверную информацию о порядке и об условиях предоставления микрозаймов, включая информацию обо всех платежах, связанных с получением, обслуживанием и возвратом микрозайма.\n4.3.\rКлиент вправе досрочно полностью или частично возвратить Займодавцу сумму микрозайма, предварительно письменно уведомив о таком намерении Займодавца не менее чем за 10 (десять) календарных дней.\r5. Порядок утверждения и изменения правил\r5.1.\rНастоящие Правила утверждаются директором \rМФ\rи размещаются в офисе \rМФ\rна информационном стенде и на сайте организации по адресу www.nezavisimost18.ru для ознакомления всех заинтересованных лиц.\n5.2.\rИзменения в настоящих Правилах утверждаются директором \rМФ\rи публикуются в офисе \rМФ\rна информационном стенде и на сайте организации по адресу www.nezavisimost18.ru.\r6. Предоставление данных в уполномоченный орган\r6.1.\rЕжеквартально представляются отчеты о микрофинансовой деятельности и персональном составе руководящих органов \rМФ\rв уполномоченный орган.\r7. Ограничения деятельности микрофинансовой организации\rМикрофинансовая организация не вправе:\n1) выдавать \rзаймы\rв \rиностранной\rвалюте;\n2) в \rодностороннем\rпорядке \rизменять\rпроцентные ставки и (или) порядок их определения по договорам микрозайма, комиссионное вознаграждение и сроки действия этих договоров;\n3) применять к заемщику, являющемуся физическим лицом, в том числе к индивидуальному предпринимателю, досрочно полностью или частично возвратившему микрофинансовой организации сумму микрозайма и предварительно письменно уведомившему о таком намерении микрофинансовую организацию не менее чем за десять календарных дней, \rштрафные санкции за досрочный возврат микрозайма\r;\n4) \rвыдавать заемщику микрозаем\r(микрозаймы), \rесли сумма обязательств\rзаемщика перед микрофинансовой организацией по договорам микрозаймов в случае предоставления такого микрозайма (микрозаймов) \rпревысит один миллион рублей\r.\r8. Прочие условия\r8.1.\rЗаемщик дает согласие на хранение и обработку персональных данных в порядке, предусмотренном Федеральным законом № 152-ФЗ от 27.07.2006. Согласно п. 7 ст. 5 указанного выше закона Стороны определили хранения персональных данных до полного исполнения Заемщиком обязательств по договору. ЦМ вправе передавать и раскрывать любую информацию, касающуюся Договора займа или клиента своим аффилированным лицам и агентам, а также третьим лицам (включая любые кредитные, коллекторские бюро) для конфиденциального использования в соответствии с подписанным клиентом документа о Согласии на обработку персональных данных и с положениями Федерального закона от 27.07.2006 г. № 152-ФЗ «О персональных данных».','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (25,'2014-03-21 21:01:07','main','s1|/documents/personal-data.php',0,NULL,NULL,NULL,'/documents/personal-data.php','Политика по персональным данным','Политика по персональным данным\rПолитика ООО &laquo;Независимость-МФО&raquo;\rв отношении обработки персональных данных и\rреализуемых требованиях к защите персональных данных\rОбеспечение безопасности персональных данных является одной из приоритетных задач ООО «МФО» (далее &ndash; Организации). Настоящая политика разработана в целях соблюдения законодательства о персональных данных, Настоящая политика определяет принципы, порядок и условия обработки персональных данных\rработников Организации и иных лиц. персональные данные обрабатываются Организацией с целью обеспечения защиты прав и свобод человека и гражданина, в том числе защиты прав на неприкосновенность частной жизни, личную и семейную.\rОрганизация обрабатывает персональные данные следующих категорий субъектов персональных данных:\rперсональные данные работника Организации - информация, необходимая Организации в связи с трудовыми отношениями и касающиеся конкретного работника.\rперсональные данные аффилированного лица или персональные данные руководителя, участника или сотрудника юридического лица, являющегося аффилированным лицом по отношению к Организации - информация, необходимая Организации для отражения её в отчетных документах о деятельности Организации в соответствии с требованиями федеральных законов и иных нормативных правовых актов.\rперсональные данные контрагента (потенциального контрагента), а также персональные данные руководителя, участника (акционера) или сотрудника юридического лица, являющегося контрагентом Организации - информация, необходимая Организации для заключения договора и исполнения своих обязательств в рамках договорных отношений с контрагентом, а также для выполнения требований законодательства Российской Федерации.\rперсональные данные Заемщика (потенциального Заемщика) - информация, необходимая Организации для заключения договора и выполнения своих обязательств по такому договору, защиты прав и законных интересов Организации (минимизация рисков Организации, связанных с нарушением обязательств по договору займа), осуществления и выполнения возложенных законодательством РФ на Организацию функций и обязанностей (реализация мер по противодействию легализации доходов, полученных преступным путем и др.)\rОрганизация осуществляет обработку персональных данных в следующих случаях:\rосуществления финансовых операций и иной деятельности, предусмотренной Уставом Организации, действующим законодательством РФ, в частности ФЗ: «О микрофинансовой деятельности и микрофинансовых организациях», «О кредитных историях», «О противодействии легализации (отмыванию) доходов, полученных преступным путем, и финансированию терроризма», «О персональных данных».\rзаключения, исполнения и прекращения договоров с физическими, юридическим лицами, индивидуальными предпринимателями и иными лицами, в случаях, предусмотренных действующим законодательством и Уставом Организации;\rобеспечения соблюдения законов и иных нормативных правовых актов, содействия работникам в трудоустройстве, обучении и продвижении по службе, обеспечения личной безопасности работников, контроля количества и качества выполняемой работы и обеспечения сохранности имущества, в том числе исполнения требований налогового законодательства в связи с исчислением и уплатой налога на доходы физических лиц, а также единого социального налога, пенсионного законодательства при формировании и представлении персонифицированных данных о каждом получателе доходов, учитываемых при начислении страховых взносов на обязательное пенсионное страхование и обеспечение, заполнения первичной статистической документации, ведения кадрового делопроизводства в соответствии с Трудовым кодексом РФ, Налоговым кодексом РФ, федеральными законами, в частности: «Об индивидуальном (персонифицированном) учете в системе обязательного пенсионного страхования».\rСроки обработки персональных данных определяются договором с субъектом персональных данных, Приказом Минкультуры РФ от 25.08.2010 № 558 «Об утверждении «Перечня типовых управленческих архивных документов, образующихся в процессе деятельности государственных органов, органов местного самоуправления и организаций, с указанием сроков хранения» и иными нормативными правовыми актами РФ. В Организации создаются и хранятся документы, содержащие сведения о субъектах персональных данных. Требования к документам, в которых содержатся персональные данные, а также к порядку работы с такими документами установлены Постановлением Правительства РФ от 15.09.2008 № 687 «Об утверждении Положения об особенностях обработки персональных данных, осуществляемой без использования средств автоматизации».\rОбработка персональных данных Организацией осуществляется на основе принципов:\rзаконности и справедливости обработки персональных данных;\rдостижения конкретных, заранее определенных и законных целей обработки персональных данных;\rсоответствия целей обработки персональных данных целям, заранее определенным и заявленным при сборе персональных данных;\rсоответствия объема и содержания обрабатываемых персональных данных, целям обработки персональных данных;\rдостоверности персональных данных, их достаточности для целей обработки, недопустимости обработки персональных данных, избыточных по отношению к целям обработки персональных данных;\rнедопустимости объединения баз данных, содержащих персональные данные, обработка которых осуществляется в целях несовместимых между собой;\rосуществление хранения персональных данных в форме, позволяющей определить субъекта персональных данных, не дольше, чем этого требуют цели их обработки;\rОрганизация предпринимает необходимые организационные и технические меры для обеспечения безопасности персональных данных от неправомерного или случайного доступа, уничтожения, изменения, блокирования и других неправомерных действий.\rВ целях координации действий по обеспечению безопасности персональных данных в Организации назначен ответственный за обеспечение безопасности персональных данных.\rНастоящая Политика подлежит изменению, дополнению в случае появления новых законодательных актов и специальных нормативных документов по обработке и защите персональных данных.\rКонтроль исполнения требований настоящей Политики осуществляется ответственным за обеспечение безопасности персональных данных Организации.','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (26,'2015-10-11 17:56:12','iblock','17',0,NULL,NULL,NULL,'=ID=17&EXTERNAL_ID=17&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=regions&IBLOCK_ID=1&IBLOCK_CODE=regions&IBLOCK_EXTERNAL_ID=23&CODE=','Москва','','','regions','1',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (27,'2014-10-30 00:05:09','iblock','18',0,NULL,NULL,NULL,'=ID=18&EXTERNAL_ID=18&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=address&IBLOCK_ID=2&IBLOCK_CODE=offices&IBLOCK_EXTERNAL_ID=19&CODE=','1.2. ОП «ЛЮБЛИНО»','','','address','2',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (28,'2014-03-21 21:23:14','main','s1|/address.php',0,NULL,NULL,NULL,'/address.php','Адреса офисов','','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (31,'2014-03-24 23:59:46','main','s1|/documents/certificate.php',0,NULL,NULL,NULL,'/documents/certificate.php','Свидетельство МФО','Свидетельство МФО','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (33,'2014-07-18 16:40:12','iblock','21',0,NULL,NULL,NULL,'=ID=21&EXTERNAL_ID=21&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=news&IBLOCK_ID=3&IBLOCK_CODE=news&IBLOCK_EXTERNAL_ID=12&CODE=','Тестовая новость','','','news','3',NULL,'2014-07-18 16:40:12',NULL);
INSERT INTO `b_search_content` VALUES (34,'2014-07-18 16:41:13','iblock','22',0,NULL,NULL,NULL,'=ID=22&EXTERNAL_ID=22&IBLOCK_SECTION_ID=1&IBLOCK_TYPE_ID=news&IBLOCK_ID=3&IBLOCK_CODE=news&IBLOCK_EXTERNAL_ID=12&CODE=','ываываыва','ываываыва ыв ыв аыва ыв','','news','3',NULL,'2014-07-18 16:41:13',NULL);
INSERT INTO `b_search_content` VALUES (36,'2014-10-30 00:03:02','iblock','24',0,NULL,NULL,NULL,'=ID=24&EXTERNAL_ID=24&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=address&IBLOCK_ID=2&IBLOCK_CODE=offices&IBLOCK_EXTERNAL_ID=19&CODE=','1.1. ОП «МЕНДЕЛЕЕВСКАЯ»','','','address','2',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (37,'2014-10-30 00:06:25','iblock','25',0,NULL,NULL,NULL,'=ID=25&EXTERNAL_ID=25&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=address&IBLOCK_ID=2&IBLOCK_CODE=offices&IBLOCK_EXTERNAL_ID=19&CODE=','ОП «ЭЛЕКТРОЗАВОДСКАЯ»','','','address','2',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (38,'2014-10-30 00:35:43','iblock','26',0,NULL,NULL,NULL,'=ID=26&EXTERNAL_ID=26&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=affiliatted&IBLOCK_ID=6&IBLOCK_CODE=affiliated&IBLOCK_EXTERNAL_ID=22&CODE=','Сахарова Олеся Дмитриевна','','','affiliatted','6',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (39,'2014-10-30 00:00:00','iblock','27',0,NULL,NULL,NULL,'=ID=27&EXTERNAL_ID=27&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=vacancy&IBLOCK_ID=5&IBLOCK_CODE=vacancies&IBLOCK_EXTERNAL_ID=20&CODE=','Финансовый специалист / Кредитный специалист','Должностные обязанности\rКонсультация 	потенциальных клиентов по условиям 	предоставления и погашения займов;\rПервичная 	оценка платежеспособности клиента;\rОформление 	договора займа с клиентом в офисе 	продаж;\rВыдача/прием 	денежных средств по обязательствам 	договора займа;\rПолное 	сопровождение клиентов от момента 	подачи заявки до погашения займа;\rВедение 	внутреннего документооборота и 	отчетности;\rВыполнение 	плановых показателей и поставленных 	задач.\rКонтроль 	за финансовой дисциплиной: движение 	(пополнение, инкассация) денежных 	средств.\rСверки 	и сбор документов по поставщикам\rМатериальная 	ответственность за денежные средства.\rТребуется\rВозраст: 	от 23 до 43 лет \rОбразование: 	высшее.\rЗнание 	ПК\rКак 	преимущество опыт работы:\r- в области активных продаж;\r- банковской или страховой сфере.\rЖелание 	зарабатывать и развиваться в Финансовой 	сфере.\rМы предлагаем\rМесто 	работы: офисы продаж\rОборудованное 	рабочее место по стандартам компании;\rГрафик 	работы 2/2 (с 9:00-21:00);\rДоход: 	оклад+ премия от 23 000 до 56 000 руб. 	(фиксированный оклад + ежемесячная 	премия);\rСоблюдение 	ТК РФ, &laquo;белая&raquo; заработная плата;\rИспытательный 	срок 3 месяца\rОбучение 	работе, профессиональная поддержка;\rРазвитие 	и карьерный рост;\rРабота 	в дружном и сплоченном коллективе.','','vacancy','5',NULL,'2014-10-30 00:00:00',NULL);
INSERT INTO `b_search_content` VALUES (40,'2014-10-31 20:42:22','main','s1|/jobs/summary.php',0,NULL,NULL,NULL,'/jobs/summary.php','Готовый сайт для Вашей МФО | Отправка резюме','','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (41,'2014-11-10 21:00:25','main','s1|/about.php',0,NULL,NULL,NULL,'/about.php','Готовый сайт для Вашей МФО | О компании','&laquo;\rПЛЮС\rКРЕДИТ\r&raquo; - это микрофинансовая компания предоставляющая широкий спектр востребованных продуктов (займов) физическим лицам, которые по каким-либо причинам не могут получить кредиты/займы в банках или других организациях.\rМы ставим перед собой цели и добиваемся их.  \rМы работаем, чтобы ваши мечты сбывались.\rМы работаем, чтобы предоставлять вам простые и удобные финансовые услуги.   \rМы разработали уникальные программы для вас, и Вы сможете с уверенностью смотреть в завтрашний день.  \rМы вместе с вами работаем на перспективу, теперь вы сами выбираете свое будущее.\rС нами легко решаются любые финансовые задачи!\rОпыт &ndash; наше лучшее оружие:\rСегодня, опираясь на опыт наших западных коллег и проработав более 10 лет на российском финансовом рынке, мы построили НАШУ компанию &laquo;\rПЛЮС\rКРЕДИТ\r&raquo; Теперь мы имеем полное право сказать:  &laquo;Мы готовы помочь вам изменить мир к лучшему!&raquo; \rНаша команда &ndash; наш главный капитал: \r&laquo;\rПЛЮС\rКРЕДИТ\r&raquo; - это команда профессионалов, которые работают на российском финансовом рынке долгое время. Мы объединились вместе, и наша задача расти и развиваться, разделяя достигнутые успехи со своими клиентами, партнерами.\rВ нашей компании работают высококвалифицированные специалисты. \rПрофессионализм &ndash; залог качества обслуживания  наших клиентов.\rЧестность:\rВсе мы сталкиваемся с неожиданными расходами: ремонт автомобиля, образование, медицинские услуги, свадьба и прочее. Это простые проблемы и они требуют простого решения. \rУ нас нет скрытых комиссий.\rУ нас простая система погашения займа.\rУ нас честное решение проблем для честных людей! \rНаша миссия\rулучшение доступа малообеспеченных слоев населения к финансовым услугам;\rповышение уровня жизни людей с невысоким уровнем доходов;\rсоздание доступных рабочих мест;\rсодействие увеличению доли профессиональных игроков на микрофинансовом рынке.','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (42,'2014-11-11 14:38:29','main','s1|/rent.php',0,NULL,NULL,NULL,'/rent.php','Готовый сайт для Вашей МФО | О компании','Информация для:\rСобственников\rАгенств 	недвижимости\rЧастных 	риэлторов\rВ связи с динамичным ростом и развитием Компании рассматриваются помещения для аренды под Офисы продаж по следующим критериям:\r1.\r Удаленностью 	от метро до 500 метров.\r       (для 	Москвы)\r2.\r Жилые 	дома - 1-я линия.\r3.\r ТЦ 	- нахождение павильона, офиса не более 	50 метров от центрального входа или 	отдельный вход.\r4.\r От 	-1 до 7 этажа, в зависимости от места 	нахождения.\r5.\r Площадь 	от 10 &ndash; 45 кв.м (зависит от планировки).\r6.\r Возможность 	размещения рекламы и рекламных указателей 	на центральном фасаде здания.\rИнформацию просим направлять на адрес \rarenda@\rpluskredit\r.ru\rили оставить заявку по телефонам:\r8 (499) 600 5877 \r(для Москвы)\r8 (800) 707 5877 \r(для регионов)','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (43,'2015-10-14 00:54:40','main','s1|/loans-online/index.php',0,NULL,NULL,NULL,'/loans-online/index.php','Займы онлайн','','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (44,'2015-10-10 01:13:04','main','s1|/contacts/index.php',0,NULL,NULL,NULL,'/contacts/index.php','Контакты','Text here....','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (45,'2015-10-10 01:34:35','main','s1|/about/index.php',0,NULL,NULL,NULL,'/about/index.php','О компании','Text here....','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (46,'2015-10-10 02:05:40','main','s1|/requirements/index.php',0,NULL,NULL,NULL,'/requirements/index.php','Требования к заемщикам','','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (47,'2015-10-10 02:13:23','main','s1|/agents/index.php',0,NULL,NULL,NULL,'/agents/index.php','Агентам','','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (48,'2015-10-10 02:14:08','main','s1|/franchise/index.php',0,NULL,NULL,NULL,'/franchise/index.php','Франшиза','','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (49,'2015-10-10 02:26:20','main','s1|/anketa/index.php',0,NULL,NULL,NULL,'/anketa/index.php','Оформление заявки','','','','',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (54,'2015-10-11 14:29:33','iblock','S2',0,NULL,NULL,NULL,'=ID=2&EXTERNAL_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=8&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','Слайдер на главной','Займы на любые \rслучаи жизни!',NULL,'content','8',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (55,'2015-10-11 14:32:17','iblock','42',0,NULL,NULL,NULL,'=ID=42&EXTERNAL_ID=42&IBLOCK_SECTION_ID=2&IBLOCK_TYPE_ID=content&IBLOCK_ID=8&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','Оформите заявку','У Вас это займет не больше 10 минут','','content','8',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (56,'2015-10-11 14:37:23','iblock','43',0,NULL,NULL,NULL,'=ID=43&EXTERNAL_ID=43&IBLOCK_SECTION_ID=2&IBLOCK_TYPE_ID=content&IBLOCK_ID=8&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','Получите ответ','Мы дадим ответ в течение \r20 минут','','content','8',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (57,'2015-10-11 14:34:04','iblock','44',0,NULL,NULL,NULL,'=ID=44&EXTERNAL_ID=44&IBLOCK_SECTION_ID=2&IBLOCK_TYPE_ID=content&IBLOCK_ID=8&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','Мгновенно получите деньги','Любым удобным для вас способом!','','content','8',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (59,'2015-10-11 15:22:18','iblock','46',0,NULL,NULL,NULL,'=ID=46&EXTERNAL_ID=46&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=9&IBLOCK_CODE=partners&IBLOCK_EXTERNAL_ID=&CODE=','Visa','','','content','9',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (60,'2015-10-11 15:22:40','iblock','47',0,NULL,NULL,NULL,'=ID=47&EXTERNAL_ID=47&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=9&IBLOCK_CODE=partners&IBLOCK_EXTERNAL_ID=&CODE=','MasterCard','','','content','9',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (61,'2015-10-11 15:23:09','iblock','48',0,NULL,NULL,NULL,'=ID=48&EXTERNAL_ID=48&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=9&IBLOCK_CODE=partners&IBLOCK_EXTERNAL_ID=&CODE=','Qiwi','','','content','9',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (62,'2015-10-11 15:23:35','iblock','49',0,NULL,NULL,NULL,'=ID=49&EXTERNAL_ID=49&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=9&IBLOCK_CODE=partners&IBLOCK_EXTERNAL_ID=&CODE=','Мир','','','content','9',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (63,'2015-10-11 15:23:56','iblock','50',0,NULL,NULL,NULL,'=ID=50&EXTERNAL_ID=50&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=9&IBLOCK_CODE=partners&IBLOCK_EXTERNAL_ID=&CODE=','NA','','','content','9',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (64,'2015-10-11 15:24:23','iblock','51',0,NULL,NULL,NULL,'=ID=51&EXTERNAL_ID=51&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=9&IBLOCK_CODE=partners&IBLOCK_EXTERNAL_ID=&CODE=','Contact','','','content','9',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (65,'2015-10-11 17:50:01','iblock','52',0,NULL,NULL,NULL,'=ID=52&EXTERNAL_ID=52&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=regions&IBLOCK_ID=1&IBLOCK_CODE=regions&IBLOCK_EXTERNAL_ID=23&CODE=','Казань','','','regions','1',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (70,'2015-10-11 20:24:49','iblock','57',0,NULL,NULL,NULL,'=ID=57&EXTERNAL_ID=57&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=11&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','ЭКСПРЕСС','от 1000 до 5 000 рублей для клиентов количество полученных займов которых составляет от 1 до 3\r\nДалеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Loreum 1','','content','11',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (71,'2015-10-11 20:26:16','iblock','58',0,NULL,NULL,NULL,'=ID=58&EXTERNAL_ID=58&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=11&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','До зарплаты','от 1000 до 10000 рублей для клиентов количество полученных займов которых составляет от 4 до 5\r\n2 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана.','','content','11',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (72,'2015-10-11 23:01:28','iblock','59',0,NULL,NULL,NULL,'=ID=59&EXTERNAL_ID=59&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=11&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','Клубный','от 1000 до 20 000 рублей для постоянных клиентов количество полученных займов которых составляет от 6 и более\r\n3 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана.','','content','11',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (73,'2015-10-11 23:13:52','iblock','60',0,NULL,NULL,NULL,'=ID=60&EXTERNAL_ID=60&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=11&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','Офицерский','от 1 000 до 25 000 на срок до одного года\r\nПриглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас.','','content','11',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (74,'2015-10-11 23:17:46','iblock','61',0,NULL,NULL,NULL,'=ID=61&EXTERNAL_ID=61&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=11&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','Пенсионный','от 1000 до 10 000 на срок до 6 месяцев\r\n5. Приглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас.','','content','11',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (75,'2015-10-11 23:17:56','iblock','62',0,NULL,NULL,NULL,'=ID=62&EXTERNAL_ID=62&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=11&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','Деньги на дом','микрофинансирование от 1000 до 20 000 рублей\r\n6. Приглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас.','','content','11',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (76,'2015-10-12 11:19:24','iblock','63',0,NULL,NULL,NULL,'=ID=63&EXTERNAL_ID=63&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=12&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','Преимущество 1','краткое описание \rпреимущества','','content','12',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (77,'2015-10-12 11:19:35','iblock','64',0,NULL,NULL,NULL,'=ID=64&EXTERNAL_ID=64&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=12&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','Преимущество 1','краткое описание \rпреимущества','','content','12',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (78,'2015-10-12 11:19:50','iblock','65',0,NULL,NULL,NULL,'=ID=65&EXTERNAL_ID=65&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=content&IBLOCK_ID=12&IBLOCK_CODE=&IBLOCK_EXTERNAL_ID=&CODE=','Преимущество 1','краткое описание \rпреимущества','','content','12',NULL,NULL,NULL);
INSERT INTO `b_search_content` VALUES (79,'2015-10-14 01:04:20','main','s1|/loans-online.php',0,NULL,NULL,NULL,'/loans-online.php','Займы онлайн','','','','',NULL,NULL,NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_content_freq`
-- 




DROP TABLE IF EXISTS `b_search_content_freq`;
CREATE TABLE `b_search_content_freq` (
  `STEM` int(11) NOT NULL DEFAULT '0',
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FREQ` float DEFAULT NULL,
  `TF` float DEFAULT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_FREQ` (`STEM`,`LANGUAGE_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_content_param`
-- 




DROP TABLE IF EXISTS `b_search_content_param`;
CREATE TABLE `b_search_content_param` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `PARAM_NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PARAM_VALUE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  KEY `IX_B_SEARCH_CONTENT_PARAM` (`SEARCH_CONTENT_ID`,`PARAM_NAME`),
  KEY `IX_B_SEARCH_CONTENT_PARAM_1` (`PARAM_NAME`,`PARAM_VALUE`(50),`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_content_right`
-- 




DROP TABLE IF EXISTS `b_search_content_right`;
CREATE TABLE `b_search_content_right` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `GROUP_CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_RIGHT` (`SEARCH_CONTENT_ID`,`GROUP_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_search_content_right`
-- 


INSERT INTO `b_search_content_right` VALUES (5,'G2');
INSERT INTO `b_search_content_right` VALUES (8,'G1');
INSERT INTO `b_search_content_right` VALUES (8,'G2');
INSERT INTO `b_search_content_right` VALUES (9,'G2');
INSERT INTO `b_search_content_right` VALUES (12,'G2');
INSERT INTO `b_search_content_right` VALUES (20,'G2');
INSERT INTO `b_search_content_right` VALUES (21,'G2');
INSERT INTO `b_search_content_right` VALUES (22,'G2');
INSERT INTO `b_search_content_right` VALUES (23,'G2');
INSERT INTO `b_search_content_right` VALUES (24,'G2');
INSERT INTO `b_search_content_right` VALUES (25,'G2');
INSERT INTO `b_search_content_right` VALUES (26,'G1');
INSERT INTO `b_search_content_right` VALUES (26,'G2');
INSERT INTO `b_search_content_right` VALUES (27,'G1');
INSERT INTO `b_search_content_right` VALUES (27,'G2');
INSERT INTO `b_search_content_right` VALUES (28,'G2');
INSERT INTO `b_search_content_right` VALUES (31,'G2');
INSERT INTO `b_search_content_right` VALUES (33,'G1');
INSERT INTO `b_search_content_right` VALUES (33,'G2');
INSERT INTO `b_search_content_right` VALUES (34,'G1');
INSERT INTO `b_search_content_right` VALUES (34,'G2');
INSERT INTO `b_search_content_right` VALUES (36,'G1');
INSERT INTO `b_search_content_right` VALUES (36,'G2');
INSERT INTO `b_search_content_right` VALUES (37,'G1');
INSERT INTO `b_search_content_right` VALUES (37,'G2');
INSERT INTO `b_search_content_right` VALUES (38,'G1');
INSERT INTO `b_search_content_right` VALUES (38,'G2');
INSERT INTO `b_search_content_right` VALUES (39,'G1');
INSERT INTO `b_search_content_right` VALUES (39,'G2');
INSERT INTO `b_search_content_right` VALUES (40,'G2');
INSERT INTO `b_search_content_right` VALUES (41,'G2');
INSERT INTO `b_search_content_right` VALUES (42,'G2');
INSERT INTO `b_search_content_right` VALUES (43,'G2');
INSERT INTO `b_search_content_right` VALUES (44,'G2');
INSERT INTO `b_search_content_right` VALUES (45,'G2');
INSERT INTO `b_search_content_right` VALUES (46,'G2');
INSERT INTO `b_search_content_right` VALUES (47,'G2');
INSERT INTO `b_search_content_right` VALUES (48,'G2');
INSERT INTO `b_search_content_right` VALUES (49,'G2');
INSERT INTO `b_search_content_right` VALUES (54,'G1');
INSERT INTO `b_search_content_right` VALUES (54,'G2');
INSERT INTO `b_search_content_right` VALUES (55,'G1');
INSERT INTO `b_search_content_right` VALUES (55,'G2');
INSERT INTO `b_search_content_right` VALUES (56,'G1');
INSERT INTO `b_search_content_right` VALUES (56,'G2');
INSERT INTO `b_search_content_right` VALUES (57,'G1');
INSERT INTO `b_search_content_right` VALUES (57,'G2');
INSERT INTO `b_search_content_right` VALUES (59,'G2');
INSERT INTO `b_search_content_right` VALUES (60,'G2');
INSERT INTO `b_search_content_right` VALUES (61,'G2');
INSERT INTO `b_search_content_right` VALUES (62,'G2');
INSERT INTO `b_search_content_right` VALUES (63,'G2');
INSERT INTO `b_search_content_right` VALUES (64,'G2');
INSERT INTO `b_search_content_right` VALUES (65,'G1');
INSERT INTO `b_search_content_right` VALUES (65,'G2');
INSERT INTO `b_search_content_right` VALUES (70,'G1');
INSERT INTO `b_search_content_right` VALUES (70,'G2');
INSERT INTO `b_search_content_right` VALUES (71,'G1');
INSERT INTO `b_search_content_right` VALUES (71,'G2');
INSERT INTO `b_search_content_right` VALUES (72,'G1');
INSERT INTO `b_search_content_right` VALUES (72,'G2');
INSERT INTO `b_search_content_right` VALUES (73,'G1');
INSERT INTO `b_search_content_right` VALUES (73,'G2');
INSERT INTO `b_search_content_right` VALUES (74,'G1');
INSERT INTO `b_search_content_right` VALUES (74,'G2');
INSERT INTO `b_search_content_right` VALUES (75,'G1');
INSERT INTO `b_search_content_right` VALUES (75,'G2');
INSERT INTO `b_search_content_right` VALUES (76,'G1');
INSERT INTO `b_search_content_right` VALUES (76,'G2');
INSERT INTO `b_search_content_right` VALUES (77,'G1');
INSERT INTO `b_search_content_right` VALUES (77,'G2');
INSERT INTO `b_search_content_right` VALUES (78,'G1');
INSERT INTO `b_search_content_right` VALUES (78,'G2');
INSERT INTO `b_search_content_right` VALUES (79,'G2');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_content_site`
-- 




DROP TABLE IF EXISTS `b_search_content_site`;
CREATE TABLE `b_search_content_site` (
  `SEARCH_CONTENT_ID` int(18) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `URL` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`SEARCH_CONTENT_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_search_content_site`
-- 


INSERT INTO `b_search_content_site` VALUES (5,'s1','');
INSERT INTO `b_search_content_site` VALUES (8,'s1','');
INSERT INTO `b_search_content_site` VALUES (9,'s1','');
INSERT INTO `b_search_content_site` VALUES (12,'s1','');
INSERT INTO `b_search_content_site` VALUES (20,'s1','');
INSERT INTO `b_search_content_site` VALUES (21,'s1','');
INSERT INTO `b_search_content_site` VALUES (22,'s1','');
INSERT INTO `b_search_content_site` VALUES (23,'s1','');
INSERT INTO `b_search_content_site` VALUES (24,'s1','');
INSERT INTO `b_search_content_site` VALUES (25,'s1','');
INSERT INTO `b_search_content_site` VALUES (26,'s1','');
INSERT INTO `b_search_content_site` VALUES (27,'s1','');
INSERT INTO `b_search_content_site` VALUES (28,'s1','');
INSERT INTO `b_search_content_site` VALUES (31,'s1','');
INSERT INTO `b_search_content_site` VALUES (33,'s1','');
INSERT INTO `b_search_content_site` VALUES (34,'s1','');
INSERT INTO `b_search_content_site` VALUES (36,'s1','');
INSERT INTO `b_search_content_site` VALUES (37,'s1','');
INSERT INTO `b_search_content_site` VALUES (38,'s1','');
INSERT INTO `b_search_content_site` VALUES (39,'s1','');
INSERT INTO `b_search_content_site` VALUES (40,'s1','');
INSERT INTO `b_search_content_site` VALUES (41,'s1','');
INSERT INTO `b_search_content_site` VALUES (42,'s1','');
INSERT INTO `b_search_content_site` VALUES (43,'s1','');
INSERT INTO `b_search_content_site` VALUES (44,'s1','');
INSERT INTO `b_search_content_site` VALUES (45,'s1','');
INSERT INTO `b_search_content_site` VALUES (46,'s1','');
INSERT INTO `b_search_content_site` VALUES (47,'s1','');
INSERT INTO `b_search_content_site` VALUES (48,'s1','');
INSERT INTO `b_search_content_site` VALUES (49,'s1','');
INSERT INTO `b_search_content_site` VALUES (54,'s1','');
INSERT INTO `b_search_content_site` VALUES (55,'s1','');
INSERT INTO `b_search_content_site` VALUES (56,'s1','');
INSERT INTO `b_search_content_site` VALUES (57,'s1','');
INSERT INTO `b_search_content_site` VALUES (59,'s1','');
INSERT INTO `b_search_content_site` VALUES (60,'s1','');
INSERT INTO `b_search_content_site` VALUES (61,'s1','');
INSERT INTO `b_search_content_site` VALUES (62,'s1','');
INSERT INTO `b_search_content_site` VALUES (63,'s1','');
INSERT INTO `b_search_content_site` VALUES (64,'s1','');
INSERT INTO `b_search_content_site` VALUES (65,'s1','');
INSERT INTO `b_search_content_site` VALUES (70,'s1','');
INSERT INTO `b_search_content_site` VALUES (71,'s1','');
INSERT INTO `b_search_content_site` VALUES (72,'s1','');
INSERT INTO `b_search_content_site` VALUES (73,'s1','');
INSERT INTO `b_search_content_site` VALUES (74,'s1','');
INSERT INTO `b_search_content_site` VALUES (75,'s1','');
INSERT INTO `b_search_content_site` VALUES (76,'s1','');
INSERT INTO `b_search_content_site` VALUES (77,'s1','');
INSERT INTO `b_search_content_site` VALUES (78,'s1','');
INSERT INTO `b_search_content_site` VALUES (79,'s1','');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_content_stem`
-- 




DROP TABLE IF EXISTS `b_search_content_stem`;
CREATE TABLE `b_search_content_stem` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `STEM` int(11) NOT NULL,
  `TF` float NOT NULL,
  `PS` float NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_STEM` (`STEM`,`LANGUAGE_ID`,`TF`,`PS`,`SEARCH_CONTENT_ID`),
  KEY `IND_B_SEARCH_CONTENT_STEM` (`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci DELAY_KEY_WRITE=1;

-- 
-- Dumping data for table  `b_search_content_stem`
-- 


INSERT INTO `b_search_content_stem` VALUES (5,'ru',7,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (8,'ru',1684,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (8,'ru',1685,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (8,'ru',1686,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (8,'ru',1687,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (9,'ru',89,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (12,'ru',100,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (20,'ru',1875,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (20,'ru',1885,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (20,'ru',1886,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (20,'ru',1887,0.2314,4);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',38,0.161,63);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',48,0.161,109);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',54,0.2552,89);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',60,0.2552,9.5);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',86,0.161,98);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',89,0.161,5);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',97,0.161,1);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',99,0.161,35);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',112,0.3221,110.667);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',113,0.3221,31);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',114,0.161,18);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',115,0.3221,42);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',117,0.161,23);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',118,0.161,24);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',119,0.161,25);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',120,0.161,26);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',121,0.161,27);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',122,0.161,29);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',123,0.161,30);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',124,0.161,31);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',125,0.161,33);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',126,0.161,37);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',127,0.161,38);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',128,0.161,42);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',129,0.161,46);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',130,0.161,47);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',131,0.161,48);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',134,0.161,53);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',135,0.161,57);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',136,0.161,58);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',137,0.161,59);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',138,0.161,61);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',139,0.161,64);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',140,0.161,65);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',141,0.161,67);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',142,0.2552,76.5);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',143,0.2552,77.5);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',144,0.2552,83.5);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',145,0.2552,84.5);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',146,0.161,75);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',147,0.161,77);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',148,0.161,78);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',149,0.161,79);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',150,0.161,81);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',151,0.161,83);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',152,0.161,86);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',153,0.2552,45);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',154,0.161,99);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',155,0.161,103);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',156,0.161,105);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',157,0.161,105);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',158,0.161,107);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',159,0.2552,119.5);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',160,0.161,118);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',161,0.161,121);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',162,0.161,123);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',505,0.161,50);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',540,0.161,22);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',1679,0.161,52);
INSERT INTO `b_search_content_stem` VALUES (21,'ru',1680,0.161,21);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',54,0.195,24);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',60,0.309,12);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',66,0.195,25);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',78,0.195,36);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',89,0.195,5);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',97,0.195,1);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',115,0.195,61);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',117,0.195,51);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',122,0.195,52);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',138,0.309,41.5);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',141,0.195,55);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',153,0.195,2);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',163,0.309,11);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',164,0.195,23);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',166,0.3899,38.3333);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',171,0.195,29);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',172,0.195,30);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',173,0.195,38);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',174,0.195,40);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',175,0.195,41);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',176,0.195,42);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',177,0.195,44);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',178,0.195,45);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',179,0.195,46);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',180,0.195,50);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',181,0.195,57);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',182,0.195,58);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',183,0.195,60);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',540,0.195,27);
INSERT INTO `b_search_content_stem` VALUES (22,'ru',1680,0.195,26);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',23,0.135,217);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',38,0.3134,196.75);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',44,0.135,218);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',54,0.3489,154.8);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',60,0.3134,38.25);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',66,0.2139,150);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',78,0.135,47);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',115,0.4049,163.714);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',122,0.135,240);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',124,0.135,21);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',126,0.2139,154);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',138,0.135,45);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',141,0.135,243);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',163,0.2699,48.6667);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',164,0.2699,146.333);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',165,0.2139,16.5);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',166,0.2699,177.667);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',167,0.135,16);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',172,0.3134,123.25);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',174,0.2699,108.333);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',183,0.4669,154.5);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',219,0.2699,172);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',222,0.135,3);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',223,0.135,4);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',224,0.135,5);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',225,0.3134,163.75);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',226,0.135,9);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',227,0.2139,39);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',228,0.2139,17.5);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',229,0.135,17);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',230,0.135,20);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',231,0.135,28);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',232,0.135,30);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',233,0.135,34);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',234,0.135,36);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',235,0.135,38);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',236,0.135,39);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',237,0.135,52);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',238,0.135,55);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',239,0.135,56);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',240,0.3134,120.5);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',241,0.2139,152);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',242,0.3489,152.2);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',243,0.135,64);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',244,0.135,65);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',245,0.135,69);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',246,0.135,70);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',247,0.3489,192.2);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',248,0.135,80);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',249,0.135,81);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',250,0.135,82);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',251,0.2699,126.667);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',252,0.2139,114);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',253,0.135,92);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',254,0.2699,180);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',255,0.135,97);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',256,0.3489,167.4);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',257,0.3134,156.75);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',258,0.135,101);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',259,0.2699,162);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',260,0.3134,152.5);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',261,0.2139,151);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',262,0.2139,152);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',263,0.135,153);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',264,0.135,161);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',265,0.135,162);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',266,0.2139,166);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',267,0.2139,170);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',268,0.2699,177);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',269,0.135,188);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',270,0.135,194);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',271,0.2699,232.667);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',272,0.135,197);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',273,0.2139,219);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',274,0.135,203);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',275,0.135,205);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',276,0.135,211);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',277,0.135,215);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',278,0.135,216);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',279,0.135,228);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',280,0.135,234);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',281,0.135,244);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',282,0.135,253);
INSERT INTO `b_search_content_stem` VALUES (23,'ru',1648,0.2699,159.667);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',69,0.154,1329);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',193,0.2512,225.6);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',199,0.0972,923);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',267,0.154,1268);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',301,0.154,950);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',303,0.0972,1866);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',304,0.154,965.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',481,0.0972,967);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1033,0.2915,371.571);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1034,0.3972,697.062);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1035,0.37,757.231);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1036,0.0972,18);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1037,0.0972,19);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1038,0.5566,1328.13);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1039,0.0972,21);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1040,0.2256,557.25);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1041,0.154,62);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1042,0.0972,38);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1043,0.5172,1272.21);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1044,0.2915,1208.86);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1045,0.4513,1154.62);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1046,0.0972,42);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1047,0.1944,1423.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1048,0.2256,1646.75);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1049,0.3081,1217.38);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1050,0.2256,1645.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1051,0.0972,58);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1052,0.1944,1444);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1053,0.0972,66);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1054,0.2256,1545.75);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1055,0.154,970.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1056,0.154,971.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1057,0.0972,90);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1058,0.1944,1281);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1059,0.2728,752.333);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1060,0.2512,226.6);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1061,0.0972,118);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1062,0.0972,119);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1063,0.1944,1606);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1064,0.3362,972.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1065,0.2256,755.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1066,0.0972,124);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1067,0.0972,125);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1068,0.0972,134);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1069,0.0972,135);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1070,0.0972,138);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1071,0.0972,140);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1072,0.0972,141);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1073,0.0972,143);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1074,0.0972,147);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1075,0.0972,148);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1076,0.0972,149);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1077,0.0972,150);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1078,0.1944,1402.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1079,0.4672,1143.3);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1080,0.1944,1408.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1081,0.2512,1710.4);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1082,0.4052,1425);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1083,0.1944,1411.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1084,0.1944,1418.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1085,0.0972,191);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1086,0.154,1175.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1087,0.4334,1215.62);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1088,0.0972,239);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1089,0.154,698);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1090,0.0972,242);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1091,0.0972,266);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1092,0.0972,267);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1093,0.0972,268);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1094,0.0972,269);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1095,0.1944,1552.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1096,0.1944,1187);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1097,0.0972,273);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1098,0.0972,274);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1099,0.4269,1144.3);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1100,0.0972,277);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1101,0.2728,1344.17);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1102,0.0972,279);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1103,0.2256,940);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1104,0.1944,944);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1105,0.1944,945);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1106,0.0972,306);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1107,0.4815,1259.57);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1108,0.1944,663);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1109,0.2512,1443.4);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1110,0.154,368.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1111,0.0972,313);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1112,0.3362,1371.1);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1113,0.154,434.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1114,0.3596,1239.75);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1115,0.0972,344);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1116,0.2512,450.2);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1117,0.0972,346);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1118,0.154,452.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1119,0.0972,351);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1120,0.0972,357);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1121,0.0972,363);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1122,0.0972,365);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1123,0.0972,370);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1124,0.2728,747.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1125,0.0972,394);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1126,0.0972,395);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1127,0.2915,939.571);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1128,0.0972,422);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1129,0.1944,1591.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1130,0.0972,425);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1131,0.0972,427);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1132,0.0972,428);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1133,0.154,539.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1134,0.1944,1199.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1135,0.1944,1200.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1136,0.154,734);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1137,0.0972,435);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1138,0.3081,1247.88);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1139,0.154,1302.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1140,0.0972,438);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1141,0.0972,439);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1142,0.0972,441);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1143,0.0972,442);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1144,0.2256,846.25);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1145,0.0972,444);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1146,0.0972,445);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1147,0.0972,447);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1148,0.0972,448);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1149,0.0972,472);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1150,0.0972,473);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1151,0.0972,474);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1152,0.0972,476);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1153,0.0972,477);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1154,0.0972,478);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1155,0.0972,479);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1156,0.0972,480);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1157,0.1944,711);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1158,0.3797,1474.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1159,0.0972,483);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1160,0.1944,1412.67);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1161,0.0972,485);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1162,0.0972,486);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1163,0.0972,488);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1164,0.0972,489);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1165,0.154,1328.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1166,0.0972,493);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1167,0.0972,494);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1168,0.2512,862.8);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1169,0.0972,498);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1170,0.0972,499);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1171,0.0972,500);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1172,0.0972,501);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1173,0.1944,545);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1174,0.2915,1257.43);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1175,0.0972,530);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1176,0.154,563);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1177,0.0972,535);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1178,0.0972,536);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1179,0.0972,540);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1180,0.0972,541);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1181,0.0972,543);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1182,0.0972,544);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1183,0.0972,545);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1184,0.0972,546);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1185,0.0972,547);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1186,0.154,670.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1187,0.0972,550);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1188,0.154,552);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1189,0.0972,554);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1190,0.0972,555);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1191,0.0972,586);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1192,0.0972,587);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1193,0.0972,588);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1194,0.0972,591);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1195,0.2915,1048.86);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1196,0.2512,1082.4);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1197,0.0972,624);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1198,0.2256,978.25);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1199,0.0972,647);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1200,0.154,1013);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1201,0.0972,652);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1202,0.154,694);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1203,0.2512,1084.2);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1204,0.0972,683);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1205,0.0972,688);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1206,0.0972,690);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1207,0.0972,691);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1208,0.0972,694);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1209,0.154,717.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1210,0.0972,699);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1211,0.0972,701);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1212,0.0972,703);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1213,0.0972,730);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1214,0.0972,733);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1215,0.0972,737);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1216,0.0972,739);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1217,0.0972,744);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1218,0.1944,772.667);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1219,0.1944,773.667);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1220,0.0972,747);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1221,0.0972,748);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1222,0.1944,908.333);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1223,0.154,785.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1224,0.154,994);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1225,0.0972,790);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1226,0.0972,791);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1227,0.0972,793);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1228,0.0972,798);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1229,0.154,1143);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1230,0.0972,805);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1231,0.154,840.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1232,0.0972,807);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1233,0.0972,808);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1234,0.0972,809);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1235,0.0972,810);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1236,0.154,1043.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1237,0.3081,1637.62);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1238,0.0972,814);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1239,0.0972,815);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1240,0.0972,816);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1241,0.0972,817);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1242,0.0972,818);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1243,0.0972,842);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1244,0.0972,846);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1245,0.154,977);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1246,0.154,1284.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1247,0.0972,849);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1248,0.154,852);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1249,0.2915,1474.86);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1250,0.0972,853);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1251,0.0972,855);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1252,0.1944,1086.67);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1253,0.0972,898);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1254,0.2256,1536.25);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1255,0.0972,902);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1256,0.0972,903);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1257,0.0972,904);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1258,0.2512,1369.4);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1259,0.154,1365.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1260,0.154,1366.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1261,0.154,991);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1262,0.1944,1350.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1263,0.154,1072.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1264,0.0972,914);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1265,0.1944,1126.67);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1266,0.2728,1445);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1267,0.0972,918);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1268,0.0972,920);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1269,0.0972,921);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1270,0.0972,925);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1271,0.0972,926);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1272,0.0972,928);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1273,0.0972,929);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1274,0.0972,930);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1275,0.154,1050);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1276,0.0972,933);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1277,0.0972,936);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1278,0.0972,937);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1279,0.0972,939);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1280,0.154,1296);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1281,0.0972,948);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1282,0.154,1032);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1283,0.0972,952);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1284,0.0972,953);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1285,0.0972,954);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1286,0.0972,956);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1287,0.0972,957);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1288,0.0972,958);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1289,0.0972,959);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1290,0.0972,960);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1291,0.154,966.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1292,0.154,1021);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1293,0.0972,964);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1294,0.0972,966);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1295,0.0972,969);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1296,0.0972,970);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1297,0.0972,971);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1298,0.2728,1388.67);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1299,0.0972,974);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1300,0.0972,975);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1301,0.0972,977);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1302,0.0972,978);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1303,0.0972,979);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1304,0.154,1172);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1305,0.0972,987);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1306,0.0972,988);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1307,0.0972,1015);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1308,0.0972,1017);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1309,0.0972,1018);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1310,0.0972,1020);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1311,0.0972,1021);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1312,0.0972,1022);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1313,0.0972,1023);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1314,0.0972,1026);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1315,0.0972,1028);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1316,0.0972,1029);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1317,0.0972,1031);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1318,0.0972,1032);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1319,0.0972,1035);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1320,0.0972,1036);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1321,0.154,1090);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1322,0.0972,1039);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1323,0.2512,1265.2);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1324,0.0972,1065);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1325,0.0972,1066);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1326,0.0972,1067);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1327,0.0972,1069);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1328,0.0972,1070);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1329,0.154,1257.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1330,0.0972,1072);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1331,0.0972,1075);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1332,0.0972,1081);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1333,0.0972,1105);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1334,0.0972,1110);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1335,0.0972,1112);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1336,0.2512,1889.6);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1337,0.0972,1141);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1338,0.0972,1142);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1339,0.154,1189.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1340,0.0972,1152);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1341,0.0972,1153);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1342,0.0972,1154);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1343,0.0972,1156);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1344,0.0972,1158);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1345,0.2915,1437);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1346,0.2512,1490.2);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1347,0.0972,1164);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1348,0.0972,1165);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1349,0.0972,1166);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1350,0.0972,1167);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1351,0.0972,1169);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1352,0.0972,1170);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1353,0.0972,1171);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1354,0.0972,1172);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1355,0.0972,1174);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1356,0.0972,1175);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1357,0.0972,1176);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1358,0.154,1315);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1359,0.0972,1180);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1360,0.0972,1182);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1361,0.0972,1185);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1362,0.1944,1332.67);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1363,0.0972,1190);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1364,0.0972,1191);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1365,0.0972,1193);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1366,0.0972,1200);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1367,0.154,1452.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1368,0.0972,1202);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1369,0.0972,1203);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1370,0.0972,1204);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1371,0.0972,1231);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1372,0.0972,1235);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1373,0.0972,1236);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1374,0.0972,1237);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1375,0.0972,1238);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1376,0.0972,1240);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1377,0.0972,1241);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1378,0.0972,1242);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1379,0.0972,1244);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1380,0.0972,1245);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1381,0.0972,1246);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1382,0.0972,1248);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1383,0.0972,1249);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1384,0.0972,1277);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1385,0.0972,1279);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1386,0.0972,1309);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1387,0.0972,1310);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1388,0.0972,1312);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1389,0.0972,1337);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1390,0.0972,1354);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1391,0.0972,1355);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1392,0.0972,1356);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1393,0.0972,1358);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1394,0.0972,1365);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1395,0.0972,1367);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1396,0.1944,1563.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1397,0.1944,1565.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1398,0.1944,1712);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1399,0.1944,1538.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1400,0.0972,1374);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1401,0.154,1545);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1402,0.0972,1377);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1403,0.1944,1805.67);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1404,0.0972,1380);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1405,0.0972,1387);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1406,0.154,1683.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1407,0.0972,1394);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1408,0.0972,1399);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1409,0.0972,1400);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1410,0.0972,1401);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1411,0.0972,1403);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1412,0.0972,1405);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1413,0.1944,1472.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1414,0.154,1664);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1415,0.154,1666);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1416,0.154,1883);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1417,0.0972,1418);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1418,0.0972,1440);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1419,0.0972,1441);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1420,0.0972,1445);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1421,0.0972,1447);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1422,0.0972,1450);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1423,0.0972,1451);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1424,0.0972,1456);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1425,0.1944,1804.67);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1426,0.1944,1591);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1427,0.0972,1463);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1428,0.0972,1485);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1429,0.0972,1486);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1430,0.0972,1491);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1431,0.0972,1493);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1432,0.0972,1494);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1433,0.0972,1513);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1434,0.0972,1516);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1435,0.0972,1517);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1436,0.154,1618);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1437,0.0972,1519);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1438,0.154,1611);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1439,0.154,1612);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1440,0.0972,1524);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1441,0.154,1559);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1442,0.0972,1527);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1443,0.0972,1529);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1444,0.0972,1531);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1445,0.154,1550);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1446,0.154,1576.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1447,0.0972,1563);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1448,0.0972,1567);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1449,0.0972,1568);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1450,0.0972,1569);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1451,0.0972,1590);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1452,0.0972,1592);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1453,0.0972,1595);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1454,0.0972,1598);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1455,0.0972,1600);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1456,0.2256,1614.25);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1457,0.154,1614.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1458,0.0972,1603);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1459,0.2256,1744);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1460,0.0972,1606);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1461,0.154,1964.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1462,0.0972,1608);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1463,0.0972,1609);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1464,0.0972,1611);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1465,0.0972,1613);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1466,0.1944,1917);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1467,0.0972,1619);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1468,0.0972,1621);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1469,0.0972,1625);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1470,0.0972,1628);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1471,0.0972,1631);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1472,0.0972,1648);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1473,0.0972,1649);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1474,0.0972,1651);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1475,0.0972,1655);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1476,0.0972,1659);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1477,0.0972,1660);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1478,0.0972,1661);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1479,0.1944,1930.33);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1480,0.0972,1693);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1481,0.0972,1696);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1482,0.0972,1697);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1483,0.2256,2017.25);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1484,0.0972,1700);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1485,0.0972,1701);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1486,0.0972,1705);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1487,0.0972,1706);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1488,0.0972,1707);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1489,0.0972,1709);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1490,0.0972,1710);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1491,0.0972,1711);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1492,0.0972,1712);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1493,0.0972,1713);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1494,0.0972,1715);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1495,0.0972,1716);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1496,0.154,2020);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1497,0.0972,1722);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1498,0.0972,1723);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1499,0.1944,1958.67);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1500,0.0972,1733);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1501,0.0972,1734);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1502,0.0972,1735);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1503,0.0972,1748);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1504,0.0972,1750);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1505,0.0972,1765);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1506,0.0972,1766);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1507,0.0972,1789);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1508,0.0972,1791);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1509,0.0972,1794);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1510,0.0972,1821);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1511,0.0972,1837);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1512,0.2512,2105);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1513,0.0972,1875);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1514,0.0972,1876);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1515,0.0972,1877);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1516,0.0972,1897);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1517,0.0972,1898);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1518,0.0972,1900);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1519,0.1944,2049.67);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1520,0.154,2131);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1521,0.0972,1911);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1522,0.0972,1913);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1523,0.154,2061);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1524,0.154,2062);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1525,0.154,2064);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1526,0.0972,1944);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1527,0.0972,1945);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1528,0.154,2070);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1529,0.154,2071);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1530,0.0972,1950);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1531,0.154,2074);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1532,0.154,2075);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1533,0.154,2078.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1534,0.154,2079.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1535,0.154,2082);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1536,0.154,2083);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1537,0.154,2084);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1538,0.0972,1975);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1539,0.0972,1978);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1540,0.154,2021);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1541,0.154,2022);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1542,0.0972,1997);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1543,0.154,2030);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1544,0.154,2031);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1545,0.0972,2026);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1546,0.0972,2027);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1547,0.0972,2045);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1548,0.0972,2047);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1549,0.0972,2048);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1550,0.0972,2053);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1551,0.0972,2090);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1552,0.2512,2290.8);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1553,0.154,2106.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1554,0.154,2107.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1555,0.0972,2107);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1556,0.0972,2108);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1557,0.0972,2109);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1558,0.0972,2114);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1559,0.0972,2115);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1560,0.0972,2116);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1561,0.0972,2117);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1562,0.0972,2133);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1563,0.0972,2137);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1564,0.0972,2138);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1565,0.154,2178);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1566,0.0972,2143);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1567,0.0972,2145);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1568,0.0972,2146);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1569,0.0972,2149);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1570,0.0972,2151);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1571,0.0972,2152);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1572,0.0972,2153);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1573,0.154,2193);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1574,0.0972,2162);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1575,0.0972,2163);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1576,0.0972,2165);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1577,0.0972,2166);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1578,0.0972,2170);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1579,0.0972,2173);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1580,0.0972,2174);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1581,0.0972,2175);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1582,0.0972,2180);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1583,0.0972,2181);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1584,0.0972,2186);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1585,0.0972,2194);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1586,0.0972,2198);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1587,0.0972,2199);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1588,0.0972,2207);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1589,0.0972,2208);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1590,0.0972,2210);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1591,0.0972,2211);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1592,0.0972,2216);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1593,0.0972,2217);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1594,0.0972,2218);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1595,0.154,2272);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1596,0.0972,2221);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1597,0.0972,2222);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1598,0.0972,2224);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1599,0.0972,2231);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1600,0.0972,2234);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1601,0.0972,2249);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1602,0.0972,2263);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1603,0.0972,2264);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1604,0.0972,2265);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1605,0.0972,2267);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1606,0.154,2319.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1607,0.2256,2339.75);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1608,0.0972,2274);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1609,0.0972,2275);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1610,0.0972,2276);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1611,0.154,2337.5);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1612,0.154,2329);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1613,0.154,2341);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1614,0.0972,2297);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1615,0.0972,2312);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1616,0.0972,2313);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1617,0.0972,2315);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1618,0.0972,2316);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1619,0.0972,2317);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1620,0.0972,2321);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1621,0.0972,2332);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1622,0.0972,2334);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1623,0.0972,2336);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1624,0.0972,2337);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1625,0.0972,2339);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1626,0.0972,2340);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1627,0.0972,2344);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1628,0.0972,2345);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1629,0.0972,2348);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1630,0.0972,2351);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1631,0.0972,2354);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1632,0.0972,2355);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1633,0.0972,2356);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1634,0.0972,2357);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1635,0.0972,2359);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1636,0.0972,2360);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1637,0.0972,2364);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1638,0.0972,2366);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1639,0.0972,2368);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1640,0.0972,2375);
INSERT INTO `b_search_content_stem` VALUES (24,'ru',1641,0.0972,2392);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',9,0.3052,360.333);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',13,0.1087,776);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',18,0.1723,480.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',32,0.1087,277);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',35,0.1723,382.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',36,0.3446,227.75);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',39,0.1723,610.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',41,0.1723,589);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',44,0.1087,602);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',45,0.1087,448);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',46,0.1087,447);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',49,0.1723,645);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',50,0.3052,521);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',60,0.1087,266);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',67,0.2524,475);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',69,0.3261,442.571);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',75,0.1087,379);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',88,0.1723,142);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',89,0.1087,34);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',93,0.1087,324);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',101,0.1087,772);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',114,0.1723,307.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',117,0.281,286.2);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',118,0.1723,207.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',121,0.1087,58);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',124,0.2524,364.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',144,0.2174,477);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',161,0.1723,170);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',168,0.1087,577);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',172,0.1723,502);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',176,0.1087,733);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',183,0.3052,299.667);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',192,0.2524,178.25);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',193,0.1723,21.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',199,0.3052,300.333);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',225,0.1723,235);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',253,0.1087,435);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',255,0.1087,430);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',258,0.2174,264.667);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',272,0.281,597.8);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',273,0.1723,454.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',274,0.1087,29);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',283,0.2174,140.667);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',286,0.1723,774.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',287,0.2524,412.75);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',288,0.1087,35);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',289,0.1087,44);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',290,0.3897,559.636);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',292,0.281,235.2);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',294,0.1087,691);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',296,0.2174,124.667);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',297,0.1723,263.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',299,0.1723,315.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',300,0.3052,416.833);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',303,0.1087,504);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',305,0.1723,319.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',306,0.2524,326.75);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',307,0.5109,306.16);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',309,0.1723,456.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',310,0.1087,225);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',311,0.1087,226);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',314,0.1087,773);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',316,0.2524,464.25);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',317,0.1087,561);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',320,0.2524,347.75);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',322,0.2524,437);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',328,0.1723,203.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',332,0.1087,517);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',343,0.1087,410);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',352,0.1723,613.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',355,0.1723,309.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',364,0.2524,538);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',365,0.2174,245.333);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',369,0.5934,424.721);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',380,0.1087,469);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',387,0.1723,623);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',388,0.1723,210.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',397,0.1087,728);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',403,0.1723,409.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',404,0.1087,355);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',405,0.1087,255);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',406,0.1087,391);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',440,0.1087,56);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',463,0.1087,698);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',494,0.2174,444);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',499,0.1087,108);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',506,0.281,213.4);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',515,0.1087,93);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',525,0.1087,436);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',528,0.1723,243);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',529,0.1723,244);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',538,0.1087,325);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',546,0.1087,437);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',551,0.1087,567);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',556,0.1723,334.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',587,0.1087,525);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',589,0.5861,417.39);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',591,0.1087,444);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',592,0.1087,546);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',595,0.1087,658);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',596,0.4443,548.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',598,0.1087,260);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',604,0.1087,316);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',621,0.3261,238.571);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',632,0.1087,601);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',643,0.1723,268);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',649,0.1087,304);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',650,0.1087,708);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',655,0.1087,513);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',666,0.1723,748.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',667,0.1087,348);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',677,0.1723,240);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',681,0.2174,420.667);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',691,0.1087,389);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',699,0.1723,375.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',707,0.2174,240.667);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',712,0.1087,642);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',723,0.1087,303);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',736,0.1087,262);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',737,0.1087,526);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',755,0.2174,360);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',758,0.1087,433);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',764,0.1087,349);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',783,0.1087,699);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',785,0.1087,543);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',788,0.1087,258);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',808,0.3261,292);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',822,0.1087,488);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',824,0.2524,460.75);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',840,0.1723,456.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',843,0.1087,768);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',844,0.1087,126);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',846,0.376,471);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',849,0.1087,650);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',852,0.2174,441.667);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',854,0.1723,518.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',855,0.1723,518.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',880,0.1087,644);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',894,0.2174,231.333);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',897,0.1087,731);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',898,0.1087,729);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',912,0.2174,139.667);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',914,0.1087,593);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',920,0.1087,18);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',921,0.1723,401.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',922,0.3052,572.333);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',923,0.1087,31);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',924,0.1087,32);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',925,0.1723,335.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',926,0.1723,224.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',927,0.2174,276.333);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',928,0.1087,82);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',929,0.1087,83);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',930,0.1087,85);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',931,0.1087,92);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',932,0.1087,94);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',933,0.1087,97);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',934,0.1087,109);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',935,0.1723,288.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',936,0.1723,166.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',937,0.1723,167.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',938,0.1087,159);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',939,0.1087,162);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',940,0.1723,270.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',941,0.2524,463.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',942,0.2524,197.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',943,0.1087,194);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',944,0.1087,214);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',945,0.1087,215);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',946,0.1087,255);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',947,0.1087,257);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',948,0.1087,258);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',949,0.1087,270);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',950,0.1087,275);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',951,0.1087,278);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',952,0.1723,498.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',953,0.1723,304);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',954,0.1723,305);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',955,0.2174,348.333);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',956,0.1723,308.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',957,0.1087,288);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',958,0.1723,337);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',959,0.1723,389);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',960,0.1087,329);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',961,0.1087,335);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',962,0.1087,336);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',963,0.1087,375);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',964,0.1087,378);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',965,0.1087,381);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',966,0.1087,383);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',967,0.1087,392);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',968,0.1087,396);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',969,0.1087,397);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',970,0.1723,430);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',971,0.1087,408);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',972,0.2174,429.333);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',973,0.1087,418);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',974,0.1087,419);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',975,0.1087,424);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',976,0.1087,426);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',977,0.1723,446.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',978,0.1087,431);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',979,0.1087,437);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',980,0.1087,445);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',981,0.1087,446);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',982,0.1087,449);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',983,0.1087,450);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',984,0.1087,454);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',985,0.1087,467);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',986,0.1087,489);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',987,0.1087,492);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',988,0.1087,498);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',989,0.1087,505);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',990,0.1723,549.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',991,0.1087,508);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',992,0.1087,509);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',993,0.1087,510);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',994,0.1087,511);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',995,0.1087,515);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',996,0.1087,520);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',997,0.1087,521);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',998,0.1087,541);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',999,0.1087,572);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1000,0.1087,573);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1001,0.1087,574);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1002,0.1087,583);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1003,0.1087,589);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1004,0.1087,590);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1005,0.1087,595);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1006,0.1087,603);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1007,0.1087,615);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1008,0.1087,615);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1009,0.1087,616);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1010,0.1087,619);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1011,0.1087,623);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1012,0.1723,632);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1013,0.1723,633);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1014,0.1087,648);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1015,0.1087,662);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1016,0.1723,672);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1017,0.1087,670);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1018,0.1087,679);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1019,0.1087,680);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1020,0.1087,690);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1021,0.1087,692);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1022,0.1087,700);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1023,0.1087,705);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1024,0.1087,713);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1025,0.1087,715);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1026,0.1087,717);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1027,0.1723,729.5);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1028,0.1087,727);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1029,0.1087,743);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1030,0.1087,752);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1031,0.1087,766);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1032,0.1087,771);
INSERT INTO `b_search_content_stem` VALUES (25,'ru',1648,0.1087,11);
INSERT INTO `b_search_content_stem` VALUES (26,'ru',1888,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (27,'ru',22,0.2314,13);
INSERT INTO `b_search_content_stem` VALUES (27,'ru',1682,0.2314,14);
INSERT INTO `b_search_content_stem` VALUES (28,'ru',54,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (28,'ru',275,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (28,'ru',334,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (31,'ru',89,0.3667,3);
INSERT INTO `b_search_content_stem` VALUES (31,'ru',473,0.3667,2);
INSERT INTO `b_search_content_stem` VALUES (33,'ru',1669,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (33,'ru',1670,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (34,'ru',1671,0.3667,1.5);
INSERT INTO `b_search_content_stem` VALUES (34,'ru',1672,0.4628,4.3333);
INSERT INTO `b_search_content_stem` VALUES (34,'ru',1673,0.2314,5);
INSERT INTO `b_search_content_stem` VALUES (36,'ru',22,0.2314,13);
INSERT INTO `b_search_content_stem` VALUES (36,'ru',1681,0.2314,14);
INSERT INTO `b_search_content_stem` VALUES (37,'ru',22,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (37,'ru',1683,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (38,'ru',1688,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (38,'ru',1689,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (38,'ru',1690,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (38,'ru',1691,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',10,0.1411,99);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',16,0.1411,107);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',18,0.3647,158);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',22,0.1411,110);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',23,0.2822,61.3333);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',31,0.1411,5);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',32,0.1411,6);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',37,0.1411,29);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',38,0.1411,28);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',41,0.1411,62);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',44,0.2822,62.3333);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',45,0.1411,46);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',48,0.1411,20);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',49,0.1411,79);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',50,0.1411,79);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',52,0.2236,23.5);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',53,0.3276,22.5);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',54,0.2236,84.5);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',55,0.2822,95.6667);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',57,0.1411,12);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',59,0.1411,15);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',60,0.3276,29.25);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',61,0.1411,140);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',65,0.1411,130);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',66,0.1411,150);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',68,0.1411,179);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',69,0.1411,180);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',72,0.1411,181);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',73,0.1411,182);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',74,0.1411,183);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',75,0.1411,188);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',76,0.1411,146);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',77,0.2236,144);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',79,0.1411,190);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',80,0.1411,194);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',81,0.1411,195);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',83,0.1411,151);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',86,0.2236,3);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',93,0.1411,3);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',118,0.1411,178);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',121,0.1411,11);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',124,0.1411,159);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',127,0.1411,114);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',134,0.1411,98);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',149,0.1411,42);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',169,0.2236,165.5);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',172,0.1411,185);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',183,0.2236,27.5);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',230,0.1411,176);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',242,0.2236,29);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',286,0.1411,83);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',365,0.1411,51);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',388,0.1411,8);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',395,0.1411,118);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',441,0.1411,41);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',443,0.1411,158);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',454,0.1411,168);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',457,0.1411,100);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',464,0.1411,93);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',487,0.1411,40);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',507,0.2236,127);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',525,0.1411,120);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',593,0.1411,36);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',620,0.1411,47);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',650,0.1411,92);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',696,0.1411,187);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',707,0.1411,33);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',723,0.2822,65.6667);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',880,0.1411,78);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',924,0.1411,56);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',939,0.1411,50);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',980,0.1411,16);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1692,0.1411,7);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1693,0.1411,17);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1694,0.1411,18);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1695,0.1411,37);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1696,0.1411,48);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1697,0.1411,52);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1698,0.1411,53);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1699,0.1411,55);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1700,0.1411,65);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1701,0.1411,66);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1702,0.1411,67);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1703,0.1411,68);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1704,0.1411,76);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1705,0.1411,81);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1706,0.1411,82);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1707,0.2236,129);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1708,0.1411,97);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1709,0.1411,106);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1710,0.1411,109);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1711,0.1411,115);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1712,0.1411,127);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1713,0.1411,128);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1714,0.1411,145);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1715,0.1411,149);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1716,0.1411,157);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1717,0.2236,167.5);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1718,0.2236,169);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1719,0.1411,166);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1720,0.1411,174);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1721,0.1411,184);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1722,0.1411,191);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1723,0.1411,192);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1724,0.1411,198);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1725,0.1411,200);
INSERT INTO `b_search_content_stem` VALUES (39,'ru',1726,0.1411,201);
INSERT INTO `b_search_content_stem` VALUES (40,'ru',89,0.2314,5);
INSERT INTO `b_search_content_stem` VALUES (40,'ru',97,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (40,'ru',153,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (40,'ru',613,0.2314,6);
INSERT INTO `b_search_content_stem` VALUES (40,'ru',1727,0.2314,7);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',10,0.1312,251);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',14,0.1312,90);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',22,0.2079,133.5);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',34,0.1312,234);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',35,0.1312,20);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',36,0.1312,21);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',52,0.1312,236);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',53,0.2079,224.5);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',59,0.1312,19);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',60,0.2624,113);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',65,0.1312,207);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',66,0.3046,98.75);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',76,0.1312,329);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',77,0.1312,330);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',79,0.1312,334);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',86,0.1312,225);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',89,0.1312,5);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',97,0.2079,82);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',99,0.3391,127);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',102,0.1312,134);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',112,0.4358,193.444);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',113,0.1312,28);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',120,0.3046,222);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',124,0.1312,326);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',134,0.1312,145);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',137,0.1312,72);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',138,0.1312,85);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',153,0.1312,2);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',164,0.1312,122);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',176,0.1312,34);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',185,0.1312,32);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',232,0.1312,88);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',234,0.1312,256);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',238,0.2079,150.5);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',242,0.1312,289);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',246,0.2624,215);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',256,0.1312,94);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',267,0.1312,144);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',283,0.1312,160);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',289,0.1312,81);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',290,0.1312,45);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',305,0.2079,174.5);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',307,0.1312,35);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',310,0.2079,168);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',346,0.1312,192);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',374,0.1312,193);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',380,0.1312,288);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',387,0.2079,104);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',397,0.1312,312);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',406,0.1312,233);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',436,0.2079,41);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',483,0.1312,143);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',489,0.2079,284);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',540,0.2624,115);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',560,0.1312,211);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',571,0.1312,248);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',593,0.1312,159);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',597,0.1312,208);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',636,0.1312,112);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',648,0.1312,43);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',650,0.1312,267);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',690,0.2079,151);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',710,0.1312,279);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',723,0.3391,170.2);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',743,0.1312,27);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',916,0.1312,166);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',924,0.2079,164);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',932,0.1312,321);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',955,0.1312,326);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',963,0.1312,331);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1021,0.1312,44);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1680,0.2624,114);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1729,0.1312,15);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1730,0.1312,16);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1731,0.1312,17);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1732,0.1312,18);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1733,0.1312,18);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1734,0.1312,24);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1735,0.1312,25);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1736,0.1312,29);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1737,0.1312,42);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1738,0.1312,47);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1739,0.1312,58);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1740,0.1312,59);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1741,0.1312,82);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1742,0.1312,83);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1743,0.1312,91);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1744,0.1312,93);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1745,0.1312,106);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1746,0.2079,131.5);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1747,0.1312,110);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1748,0.1312,119);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1749,0.1312,120);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1750,0.1312,121);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1751,0.1312,133);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1752,0.1312,135);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1753,0.1312,139);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1754,0.1312,140);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1755,0.1312,142);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1756,0.2624,226);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1757,0.1312,151);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1758,0.1312,158);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1759,0.1312,161);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1760,0.1312,164);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1761,0.1312,167);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1762,0.2079,180);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1763,0.1312,178);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1764,0.1312,179);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1765,0.1312,185);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1766,0.1312,185);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1767,0.1312,200);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1768,0.1312,205);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1769,0.1312,209);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1770,0.1312,210);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1771,0.1312,214);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1772,0.1312,224);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1773,0.1312,231);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1774,0.1312,232);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1775,0.1312,242);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1776,0.1312,245);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1777,0.1312,247);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1778,0.1312,249);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1779,0.1312,250);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1780,0.1312,252);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1781,0.1312,254);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1782,0.1312,264);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1783,0.2624,286.333);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1784,0.1312,278);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1785,0.2079,300);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1786,0.1312,300);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1787,0.2079,312.5);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1788,0.1312,310);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1789,0.1312,311);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1790,0.1312,313);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1791,0.1312,314);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1792,0.1312,314);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1793,0.1312,315);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1794,0.1312,317);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1795,0.1312,319);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1796,0.2079,322.5);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1797,0.1312,324);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1798,0.1312,327);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1799,0.1312,328);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1800,0.1312,332);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1801,0.1312,333);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1802,0.1312,335);
INSERT INTO `b_search_content_stem` VALUES (41,'ru',1803,0.1312,335);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1,0.2486,117.5);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',54,0.2486,53);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',55,0.1569,29);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',64,0.1569,18);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',66,0.2486,14.5);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',77,0.1569,108);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',78,0.1569,146);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',81,0.1569,19);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',89,0.1569,5);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',97,0.1569,1);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',131,0.1569,32);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',149,0.1569,176);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',151,0.1569,59);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',153,0.1569,2);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',192,0.2486,84.5);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',229,0.1569,81);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',262,0.1569,106);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',267,0.1569,123);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',297,0.1569,16);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',328,0.1569,31);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',334,0.1569,165);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',335,0.2486,92.5);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',358,0.1569,147);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',375,0.1569,178);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',418,0.1569,62);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',451,0.1569,132);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',483,0.1569,80);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',515,0.1569,13);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',527,0.1569,12);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',627,0.1569,163);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1645,0.1569,74);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1647,0.1569,104);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1723,0.1569,21);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1804,0.1569,10);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1805,0.1569,10);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1806,0.1569,11);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1807,0.1569,14);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1808,0.1569,14);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1809,0.1569,23);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1810,0.1569,24);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1811,0.1569,26);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1812,0.1569,39);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1813,0.3137,55.6667);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1814,0.1569,43);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1815,0.2486,63);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1816,0.1569,58);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1817,0.1569,61);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1818,0.1569,77);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1819,0.2486,118.5);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1820,0.2486,86.5);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1821,0.1569,87);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1822,0.1569,101);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1823,0.1569,121);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1824,0.1569,124);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1825,0.1569,125);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1826,0.1569,134);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1827,0.1569,148);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1828,0.1569,150);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1829,0.1569,151);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1830,0.1569,154);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1831,0.1569,155);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1832,0.1569,162);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1833,0.1569,166);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1834,0.1569,167);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1835,0.1569,175);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1836,0.1569,180);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1837,0.1569,181);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1838,0.2486,185);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1839,0.1569,186);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1840,0.1569,187);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1841,0.1569,190);
INSERT INTO `b_search_content_stem` VALUES (42,'ru',1842,0.1569,190);
INSERT INTO `b_search_content_stem` VALUES (43,'ru',60,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (43,'ru',152,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (44,'ru',1846,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (44,'ru',1847,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (44,'ru',1848,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (45,'ru',1052,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (45,'ru',1846,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (45,'ru',1847,0.2314,4);
INSERT INTO `b_search_content_stem` VALUES (45,'ru',1849,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (46,'ru',1129,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (46,'ru',1480,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (46,'ru',1850,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (47,'ru',1851,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (48,'ru',1852,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (49,'ru',1198,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (49,'ru',1853,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (54,'ru',1079,0.3667,3.5);
INSERT INTO `b_search_content_stem` VALUES (54,'ru',1631,0.2314,6);
INSERT INTO `b_search_content_stem` VALUES (54,'ru',1843,0.2314,4);
INSERT INTO `b_search_content_stem` VALUES (54,'ru',1855,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (54,'ru',1856,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (54,'ru',1857,0.2314,7);
INSERT INTO `b_search_content_stem` VALUES (54,'ru',1858,0.2314,8);
INSERT INTO `b_search_content_stem` VALUES (55,'ru',267,0.2314,9);
INSERT INTO `b_search_content_stem` VALUES (55,'ru',1101,0.2314,7);
INSERT INTO `b_search_content_stem` VALUES (55,'ru',1506,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (55,'ru',1859,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (55,'ru',1860,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (55,'ru',1861,0.2314,4);
INSERT INTO `b_search_content_stem` VALUES (55,'ru',1862,0.2314,5);
INSERT INTO `b_search_content_stem` VALUES (55,'ru',1863,0.2314,6);
INSERT INTO `b_search_content_stem` VALUES (55,'ru',1864,0.2314,8);
INSERT INTO `b_search_content_stem` VALUES (55,'ru',1865,0.2314,10);
INSERT INTO `b_search_content_stem` VALUES (56,'ru',504,0.2314,8);
INSERT INTO `b_search_content_stem` VALUES (56,'ru',1043,0.2314,6);
INSERT INTO `b_search_content_stem` VALUES (56,'ru',1865,0.2314,9);
INSERT INTO `b_search_content_stem` VALUES (56,'ru',1866,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (56,'ru',1867,0.3667,3.5);
INSERT INTO `b_search_content_stem` VALUES (56,'ru',1868,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (56,'ru',1869,0.2314,4);
INSERT INTO `b_search_content_stem` VALUES (56,'ru',1870,0.2314,7);
INSERT INTO `b_search_content_stem` VALUES (57,'ru',1064,0.2314,6);
INSERT INTO `b_search_content_stem` VALUES (57,'ru',1873,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (57,'ru',1874,0.2314,2);
INSERT INTO `b_search_content_stem` VALUES (57,'ru',1875,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (57,'ru',1876,0.2314,4);
INSERT INTO `b_search_content_stem` VALUES (57,'ru',1877,0.2314,5);
INSERT INTO `b_search_content_stem` VALUES (57,'ru',1878,0.2314,7);
INSERT INTO `b_search_content_stem` VALUES (57,'ru',1879,0.2314,8);
INSERT INTO `b_search_content_stem` VALUES (59,'ru',1880,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (60,'ru',1881,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (61,'ru',216,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (62,'ru',1882,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (63,'ru',1883,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (64,'ru',1884,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (65,'ru',1889,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',169,0.1854,6);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',508,0.1854,1);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',509,0.1854,3);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1038,0.1854,26);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1043,0.2939,32);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1049,0.3709,18);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1064,0.1854,8);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1079,0.1854,44);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1105,0.1854,7);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1266,0.1854,38);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1403,0.2939,10.5);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1483,0.1854,20);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1900,0.1854,9);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1901,0.1854,10);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1902,0.1854,11);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1903,0.1854,12);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1904,0.1854,13);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1905,0.1854,14);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1906,0.1854,19);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1907,0.1854,21);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1908,0.1854,22);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1909,0.1854,24);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1910,0.1854,25);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1911,0.1854,27);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1912,0.2939,33.5);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1913,0.1854,29);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1914,0.1854,30);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1915,0.1854,36);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1916,0.1854,40);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1917,0.1854,42);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1918,0.1854,43);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1919,0.1854,45);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1920,0.1854,46);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1921,0.1854,47);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1922,0.1854,48);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1923,0.1854,49);
INSERT INTO `b_search_content_stem` VALUES (70,'ru',1924,0.1854,55);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',509,0.1854,4);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1038,0.1854,27);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1043,0.2939,33);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1049,0.3709,18.6667);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1064,0.1854,8);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1079,0.1854,45);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1105,0.1854,7);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1266,0.1854,39);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1403,0.2939,11);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1483,0.1854,21);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1900,0.1854,9);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1901,0.1854,10);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1902,0.1854,11);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1903,0.1854,12);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1904,0.1854,13);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1905,0.1854,14);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1906,0.1854,20);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1907,0.1854,22);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1908,0.1854,23);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1909,0.1854,25);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1910,0.1854,26);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1911,0.1854,28);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1912,0.2939,34.5);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1913,0.1854,30);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1914,0.1854,31);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1915,0.1854,37);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1916,0.1854,41);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1917,0.1854,43);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1918,0.1854,44);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1919,0.1854,46);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1920,0.1854,47);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1921,0.1854,48);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1922,0.1854,49);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1923,0.1854,50);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1925,0.1854,1);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1926,0.1854,2);
INSERT INTO `b_search_content_stem` VALUES (71,'ru',1927,0.1854,6);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',169,0.1832,6);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',504,0.1832,5);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',509,0.1832,3);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1038,0.2903,23);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1043,0.2903,34);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1049,0.3663,19);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1064,0.1832,8);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1079,0.1832,46);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1105,0.1832,7);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1266,0.1832,40);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1403,0.1832,4);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1483,0.1832,22);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1900,0.1832,10);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1901,0.1832,11);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1902,0.1832,12);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1903,0.1832,13);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1904,0.1832,14);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1905,0.1832,15);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1906,0.1832,21);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1907,0.1832,23);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1908,0.1832,24);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1909,0.1832,26);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1910,0.1832,27);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1911,0.1832,29);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1912,0.2903,35.5);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1913,0.1832,31);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1914,0.1832,32);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1915,0.1832,38);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1916,0.1832,42);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1917,0.1832,44);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1918,0.1832,45);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1919,0.1832,47);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1920,0.1832,48);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1921,0.1832,49);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1922,0.1832,50);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1923,0.1832,51);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1928,0.1832,1);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1929,0.1832,9);
INSERT INTO `b_search_content_stem` VALUES (72,'ru',1930,0.1832,19);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',147,0.2038,22);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',169,0.323,5.5);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',508,0.2038,21);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',987,0.2038,6);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1038,0.2038,23);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1049,0.2038,2);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1059,0.2038,29);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1079,0.323,17);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1082,0.2038,15);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1129,0.2038,19);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1358,0.2038,9);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1403,0.323,7.5);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1909,0.2038,17);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1931,0.2038,1);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1932,0.2038,11);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1933,0.2038,12);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1934,0.2038,13);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1935,0.2038,14);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1936,0.2038,16);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1937,0.2038,18);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1938,0.2038,20);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1939,0.2038,24);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1940,0.2038,25);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1941,0.2038,27);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1942,0.2038,28);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1943,0.2038,30);
INSERT INTO `b_search_content_stem` VALUES (73,'ru',1944,0.2038,31);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',147,0.2058,27);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',169,0.2058,6);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',267,0.2058,5);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',508,0.2058,26);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',509,0.2058,3);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1038,0.2058,28);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1049,0.2058,2);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1059,0.2058,34);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1079,0.3263,19);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1082,0.2058,20);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1129,0.2058,24);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1358,0.2058,8);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1403,0.3263,6.5);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1909,0.2058,22);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1934,0.2058,18);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1935,0.2058,19);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1936,0.2058,21);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1937,0.2058,23);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1938,0.2058,25);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1939,0.2058,29);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1940,0.2058,30);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1941,0.2058,32);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1942,0.2058,33);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1943,0.2058,35);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1944,0.2058,36);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1945,0.2058,1);
INSERT INTO `b_search_content_stem` VALUES (74,'ru',1949,0.2058,11);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',147,0.2058,26);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',169,0.2058,9);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',504,0.2058,8);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',508,0.2058,25);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',509,0.2058,6);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1038,0.2058,27);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1049,0.2058,5);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1059,0.2058,33);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1079,0.3263,16);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1082,0.2058,19);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1105,0.2058,10);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1129,0.2058,23);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1403,0.2058,7);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1909,0.2058,21);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1934,0.2058,17);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1935,0.2058,18);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1936,0.2058,20);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1937,0.2058,22);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1938,0.2058,24);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1939,0.2058,28);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1940,0.2058,29);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1941,0.2058,31);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1942,0.2058,32);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1943,0.2058,34);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1944,0.2058,35);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1946,0.2058,1);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1947,0.2058,3);
INSERT INTO `b_search_content_stem` VALUES (75,'ru',1948,0.2058,4);
INSERT INTO `b_search_content_stem` VALUES (76,'ru',1710,0.3667,3);
INSERT INTO `b_search_content_stem` VALUES (76,'ru',1950,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (76,'ru',1951,0.2314,4);
INSERT INTO `b_search_content_stem` VALUES (77,'ru',1710,0.3667,3);
INSERT INTO `b_search_content_stem` VALUES (77,'ru',1950,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (77,'ru',1951,0.2314,4);
INSERT INTO `b_search_content_stem` VALUES (78,'ru',1710,0.3667,3);
INSERT INTO `b_search_content_stem` VALUES (78,'ru',1950,0.2314,3);
INSERT INTO `b_search_content_stem` VALUES (78,'ru',1951,0.2314,4);
INSERT INTO `b_search_content_stem` VALUES (79,'ru',60,0.2314,1);
INSERT INTO `b_search_content_stem` VALUES (79,'ru',152,0.2314,2);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_content_text`
-- 




DROP TABLE IF EXISTS `b_search_content_text`;
CREATE TABLE `b_search_content_text` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `SEARCH_CONTENT_MD5` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `SEARCHABLE_CONTENT` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_search_content_text`
-- 


INSERT INTO `b_search_content_text` VALUES (5,'39ba7a74c6d4d9679c891693eb9306e4','QQQ\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (8,'de83a7951cce52ce646de510d5c733e4','СМИРНОВ АЛЕКСЕЙ ВАДИМОВИЧ\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (9,'c16ec5f657b2aa36835bf6c141409b0b','МФО\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (12,'d6ae18283686e0f65091531174c8b418','ВАКАНСИИ\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (20,'b006a955955dee56d3f99f27e0cd5ff3','Экспресс деньги\r\nНаши партнеры\r\n');
INSERT INTO `b_search_content_text` VALUES (21,'46ef89f616c46129167f30f1b2b3b237','ГОТОВЫЙ САЙТ ДЛЯ ВАШЕЙ МФО | КАК ПОЛУЧИТЬ ЗАЙМ\r\nКАК ПОЛУЧИТЬ ЗАЙМ?\rДЛЯ ПОЛУЧЕНИЯ МИКРОЗАЙМА В ПЛЮС КРЕДИТ НЕОБХОДИМО СОБЛЮДЕНИЕ НЕСКОЛЬКИХ ПРОСТЫХ УСЛОВИЙ: \r- ИМЕТЬ ПОСТОЯННЫЙ ДОХОД;\r- ПРОЖИВАТЬ ИЛИ РАБОТАТЬ В ГОРОДЕ (ОБЛАСТИ), ГДЕ ВЫ ХОТИТЕ ВЗЯТЬ МИКРОЗАЙМ;\r- И СООТВЕТСТВОВАТЬ ВОЗРАСТНОМУ КРИТЕРИЮ  ОТ 21 ДО 70 ЛЕТ \n ВСЕ ЧТО ВАМ НУЖНО:\rНАЙТИ УДОБНЫЙ ДЛЯ ВАС ОФИС ВЫДАЧИ МИКРОЗАЙМОВ, ПРЕДОСТАВИТЬ СВОЙ ПАСПОРТ И ЗАПОЛНИТЬ АНКЕТУ \nВ СЛУЧАЕ ОДОБРЕНИЯ ВЫ СРАЗУ ПОЛУЧИТЕ ДЕНЬГИ \rОФОРМИТЬ ЗАЯВКУ, НЕ ВЫХОДЯ ИЗ ДОМА  ЗАПОЛНИТЕ АНКЕТУ ОНЛАЙН НА САЙТЕ. \nВ СЛУЧАЕ ОДОБРЕНИЯ НАШИ СПЕЦИАЛИСТЫ СВЯЖУТСЯ С ВАМИ В ТЕЧЕНИЕ 2 ЧАСОВ И ПРИГЛАСЯТ НА ОФОРМЛЕНИЕ. \nНАШИ ОФИСЫ ПРИЯТНО ПОСЕЩАТЬ, С НАШИМИ СОТРУДНИКАМИ ПРИЯТНО ОБЩАТЬСЯ!\r\n');
INSERT INTO `b_search_content_text` VALUES (22,'5c407ddddf19031f4483dd0fc3ee6403','ГОТОВЫЙ САЙТ ДЛЯ ВАШЕЙ МФО | КАК ПОГАСИТЬ ЗАЙМ?\r\nКАК ПОГАСИТЬ ЗАЙМ? \rВ ЛЮБОМ ОФИСЕ КОМПАНИИ ПЛЮС КРЕДИТ \rЕСЛИ ПОДХОДИТ СРОК ОПЛАТЫ, А У ВАС НЕТ ВОЗМОЖНОСТИ ОПЛАТИТЬ САМОСТОЯТЕЛЬНО, ВЫ МОЖЕТЕ ПОПРОСИТЬ ДРУГА ИЛИ РОДСТВЕННИКА, ДОСТИГШЕГО СОВЕРШЕННОЛЕТИЯ, ОПЛАТИТЬ ЗА ВАС: ЕМУ НЕОБХОДИМО ИМЕТЬ ПРИ СЕБЕ ПАСПОРТ И ЗНАТЬ НОМЕР ВАШЕГО ДОГОВОРА МИКРОЗАЙМА.\r\n');
INSERT INTO `b_search_content_text` VALUES (23,'24d0daee038945fb71321666eff0b60d','КАК НЕ СТАТЬ ДОЛЖНИКОМ\r\nРЕКОМЕНДАЦИИ ЗАЕМЩИКАМ \r-\r МЫ НАСТОЯТЕЛЬНО РЕКОМЕНДУЕМ\rНЕ БРАТЬ ЗАЙМ НА СУММУ\nПРЕВЫШАЮЩУЮ 50% СУММЫ\nВАШЕГО ЕЖЕМЕСЯЧНОГО ДОХОДА \rНЕ БРАТЬ ЗАЙМ, ЕСЛИ ВЫ НЕ УВЕРЕНЫ,\nЧТО СМОЖЕТЕ ЕГО ПОГАСИТЬ БЕЗ\nОСЛОЖНЕНИЙ И ПРОЧЕГО \rНЕ ОТКЛАДЫВАТЬ ВИЗИТ В ОФИС\nНЕЗАВИСИМОСТЬ-МФО, ЕСЛИ У ВАС НЕТ\nВОЗМОЖНОСТИ ПОГАСИТЬ ЗАЙМ В СРОК,\nУКАЗАННЫЙ В ДОГОВОРЕ  ЛУЧШЕ\nОСУЩЕСТВИТЬ ПРОЛОНГАЦИЮ ЛИБО\nЧАСТИЧНОЕ ПОГАШЕНИЕ \rЕСЛИ ВЫ НЕ УСПЕВАЕТЕ ВОВРЕМЯ ПОГАСИТЬ ЗАЙМ, РЕКОМЕНДУЕМ ВОСПОЛЬЗОВАТЬСЯ УСЛУГОЙ «ПРОЛОНГАЦИЯ» (ПРОДЛЕНИЕ). ВЫ МОЖЕТЕ ОТЛОЖИТЬ ВОЗВРАТ ДЕНЕГ НА СРОК ДО 16 ДНЕЙ, ОПЛАТИВ ПРИ ЭТОМ ТОЛЬКО НАЧИСЛЕННЫЕ ПРОЦЕНТЫ (ПО 2% ЗА КАЖДЫЙ ДЕНЬ ОТ ДАТЫ ЗАКЛЮЧЕНИЯ ДОГОВОРА ДО ДНЯ ПРОЛОНГАЦИИ). ПРОДЛИТЬ ДОГОВОР ВЫ МОЖЕТЕ В ЛЮБОМ ОФИСЕ КОМПАНИИ ВНЕ ЗАВИСИМОСТИ ОТ ГОРОДА ВЫДАЧИ. \r-\r ДОГОВОР МИКРОЗАЙМА МОЖЕТ БЫТЬ ПРОДЛЕН НА СРОК ОТ 1 ДО 16 ДНЕЙ\rВ ЛЮБОЙ ДЕНЬ ДО ДАТЫ ПОГАШЕНИЯ \rВ ДЕНЬ ПОГАШЕНИЯ МИКРОЗАЙМА \rНЕ ПОЗДНЕЕ 16 ДНЯ ОТ ДАТЫ ПОГАШЕНИЯ МИКРОЗАЙМА \r-\r ОБРАТИТЕ ВНИМАНИЕ\rПРОДЛЕВАТЬ ДОГОВОР МИКРОЗАЙМА\nМОЖНО 10 РАЗ \rПРОДЛЕВАТЬ ДОГОВОР МИКРОЗАЙМА\nМОЖНО 10 РАЗ\nПРОДЛИТЬ ДОГОВОР МИКРОЗАЙМА\nМОЖНО В ЛЮБОМ \rОФИСЕ КОМПАНИИ\rВНЕ ЗАВИСИМОСТИ ОТ ГОРОДА ВЫДАЧИ \rДВА РАЗА ПРОДЛИТЬ ДОГОВОР\nМИКРОЗАЙМА В ОДИН ДЕНЬ НЕВОЗМОЖНО \rОСУЩЕСТВЛЯЕТ ПРОДЛЕНИЕ ОБЯЗАТЕЛЬНО\nТОЛЬКО ЗАЕМЩИК В ОДНОМ ИЗ \rОФИСОВ «НЕЗАВИСИМОСТЬ-МФО»\rПРИ ПРОДЛЕНИИ ДОГОВОРА, ЗАЕМЩИК\nДОЛЖЕН ОПЛАТИТЬ ПРОЦЕНТЫ ЗА ФАКТИЧЕСКОЕ\nПОЛЬЗОВАНИЕ ДЕНЕЖНЫМИ СРЕДСТВАМИ,\nТО ЕСТЬ ЗА СРОК ОТ ДАТЫ ВЫДАЧИ ДО ДНЯ\nОБРАЩЕНИЯ В ОФИС «НЕЗАВИСИМОСТЬ-МФО» ДЛЯ ОПЛАТЫ\nПРОЦЕНТОВ \rДЛЯ ПРОДЛЕНИЯ ДОГОВОРА ЗАЕМЩИКУ\nОБЯЗАТЕЛЬНО ИМЕТЬ ПРИ СЕБЕ ПАСПОРТ \rПОСЛЕ ЧАСТИЧНОГО ПОГАШЕНИЯ ПРОДЛЕНИЕ\nНЕВОЗМОЖНО \rПРОЛОНГАЦИЯ В ДЕНЬ ВЫДАЧИ МИКРОЗАМА\nНЕВОЗМОЖНА\r\n');
INSERT INTO `b_search_content_text` VALUES (24,'934ba08f02d416d31795866671961472','Правила предоставления микрозаймов\r\nПравила предоставления микрозаймов\rООО \rНезависимость-МФО\r \rПравила предоставления микрозаймов \r1. Общие положения и термины\r1.1.\rНастоящие правила предоставления микрозаймов разработаны в соответствии с требованиями Федерального закона от 02.07.2010г. № 151-ФЗ «О Микрофинансовой деятельности и микрофинансовых организациях».\n1.2.\rНастоящие правила определяют порядок и условия предоставления микрозаймов в ООО «Независимость-МФО».\n1.3.\rНастоящие Правила доступны всем лицам для ознакомления и содержат основные условия предоставления микрозаймов. Копия Правил предоставления микрозаймов размещается в месте, доступном для обозрения и ознакомления с ними любого заинтересованного лица - в офисе ООО «Независимость-МФО», на сайте организации по адресу WWW.NEZAVISIMOST18.RU.\n1.4.\rТермины и определения:\n1.4.1. \rМФ\r ООО «Независимость-МФО».\n1.4.2. \rПравила\r– настоящие Правила Предоставления Микрозаймов.\n1.4.3. \rМикрозаем\r– заем, предоставляемый займодавцем заемщику на условиях, предусмотренных договором займа, в сумме, не превышающей один миллион рублей.\n1.4.4. \rДоговор микрозайма\r– договор займа, сумма которого не превышает один миллион рублей.\n1.4.5. \rКлиент\r– физическое и/или юридическое лицо, находящееся на обслуживании в \rМФ\r, т.е. заключившее с ним договор займа или лицо обратившееся в \rМФ\rдля получения микрозайма;\n1.4.6. \rЗаявка\r– заявка на предоставление микрозайма.\n1.4.7. \rВыгодоприобретатель\r– лицо, к выгоде которого фактически действует клиент, в том числе на основании агентского договора, договоров поручения, комиссии и доверительного управления, при совершении операций с недвижимым имуществом.\n1.4.8. \rИдентификация\r– совокупность мероприятий по установлению определенных законодательством Российской Федерации сведений о клиентах, их представителях, выгодоприобретателях, по подтверждению достоверности этих сведений с использованием оригиналов документов и (или) надлежащим образом заверенных копий.\n1.4.9. \rПредставитель клиента/Представитель\r– физическое лицо, действующее от имени и в интересах Клиента в соответствии с полномочиями, основанными на доверенности, законе, либо акте уполномоченного на то государственного органа или органа местного самоуправления при обслуживании в \rМФ\r.\n1.4.10. \rПредставитель МФ\r– лицо, уполномоченное представлять интересы \rМФ\rи действовать от его имени.\r2. Порядок и условия предоставления микрозаймов\r2.1.\rПорядок подачи заявки на предоставление микрозайма:\n2.1.1. Для получения займа клиент должен ознакомиться с Правилами предоставления микрозаймов ООО «Независимость-МФО».\n2.1.2. При обращении клиента для получения микрозайма, представитель \rМФ\rвыясняет цель получения микрозайма, разъясняет обязательные условия предоставления микрозайма, знакомит с перечнем документов, необходимых для его получения.\n2.1.3. После ознакомления с настоящими Правилами и при согласии на предлагаемые обязательные условия предоставления займа, заемщик заполняет анкету, установленного образца, для предоставления ему микрозайма.\n2.1.4. Клиент собственноручно заполняет анкету на предоставление ему микрозайма. Если есть поручитель, то он собственноручно заполняет анкету на поручительство. Все графы анкеты, подлежащие обязательному заполнению, заполняются в обязательном порядке, иначе анкета будет считаться недействительной.\n2.1.5. Анкета на предоставление микрозайма считается поданной после подписания ее клиентом и передачи ее менеджеру.\n2.2.\rПорядок рассмотрения анкеты на предоставление микрозайма:\n2.2.1. При подаче заявки Клиент обязан предоставить ниже перечисленные документы и сведения, запрашиваемые менеджером \rМФ\r, необходимые для решения вопроса о предоставлении всех видов займа:\rПаспорт ГР РФ;\rИНН или водительское удостоверение или пенсионное страховое свидетельство (один из трех документов на выбор)\rСведения о доходах.\rПри необходимости в зависимости от вида займа:\rСвидетельство ИП;\rПаспорта всех совместно проживающих совершеннолетних членов семьи;\rСправка с места работы по форме НДФЛ №2;\rМедицинский страховой полис;\rСправка об отсутствии долгов по коммунальным платежам;\rИные документы в соответствии с условиями займа и действующего законодательства в том числе.\r2.2.2. Специалисты\rМФ\rпроводят анализ и возможную необходимую проверку представленных документов клиента (Выгодоприобретателя и Представителя), оценивают его финансовое состояние, на основании чего принимается решение о выдаче займа клиенту.\n2.2.3. По некоторым видам займа обязательным условием является посещение менеджером \rМФ\rбизнеса клиента и/или места его жительства.\n2.2.4. Срок рассмотрения поданной заявки на получение микрозайма зависит от вида микрозайма.\n2.2.5.\rМФ\rвправе принять мотивированное решение об отказе в предоставлении займа клиенту в соответствии со следующим Перечнем:\rПредоставления поддельных и недостоверных документов и сведений;\rВ случае отрицательной кредитной истории (данные из Бюро кредитных историй);\rРанее клиенту был выдан микрозайм и срок его возврата не истек;\rВ случае нарушения клиентом условий договора по ранее выданному ему займу.\rЕсли имеются иные обстоятельства, препятствующие заключению договора.\r2.2.6. В случае принятия решения об отказе, менеджер фиксирует данный факт в журнале регистрации заявок в разделе заключение менеджера с указанием причины.\n2.2.7. Клиент в обязательном порядке информируется о результатах рассмотрения заявки на предоставление займа.\n2.2.8. При положительном решении\rМФ\rзаключает с клиентом договор о предоставлении микрозайма.\n2.3.\rПорядок заключения договора микрозайма\n2.3.1 Клиент ознакамливается под роспись с порядком и условиями предоставления микрозайма, о его правах и обязанностях, связанных с получением микрозайма, а так же должен быть проинформирован до получения им микрозайма, об условиях договора микрозайма, о возможности и порядке изменения его условий по инициативе \rМФ\rи клиента (в части страхования, подсудности), о перечне и размере всех платежей, связанных с получением, обслуживанием и возвратом микрозайма, а также с нарушением условий договора микрозайма.\n2.3.2 Обязательной частью договора микрозайма является график платежей, выдаваемый клиенту, в котором указаны срок и сумма всех выплат по договору займа и процентов по нему.\n2.3.3 Все необходимые документы подписываются представителем\rМФ\rи клиентом, на договорах займа ставятся печати.\n2.3.4 Выдача займа клиенту может осуществляться как наличными денежными средствами, так и перечислением средств на указанный клиентом счет в банке.\r3. Погашение займа\r3.1.\rПогашение займа осуществляется в соответствии с графиком платежей или в ином, установленном сторонами договора порядке.\n3.2.\rПри внесении клиентом денежных средств осуществляется следующий порядок их зачисления:\rВ первую очередь погашаются издержки Займодавца по получению исполнения;\rВо вторую очередь погашается сумма начисленных процентов на сумму займа;\rВ третью очередь основная сумма займа;\rВ четвёртую очередь погашаются начисленные по договору пени;\r3.3.\rВ случае необходимости, по требованию Менеджера, при погашении микрозайма, Клиент обязан представить документы, необходимые для идентификации личности указанные в п. 8.1.\n3.4.\rТребования Займодавца об уплате неустойки, процентов за пользование чужими денежными средствами, иные денежные требования, связанные с применением мер гражданско-правовой ответственности, могут быть добровольно удовлетворены Заемщиком как до, так после удовлетворения требований Займодавца в порядке ст. 319 Гражданского кодекса РФ.\r4. Права и обязанности клиента\r4.1.\rКлиент, подавший заявку на предоставление микрозайма \rобязан:\r4.1.1. Знакомиться с правилами предоставления микрозаймов, утвержденными \rМФ\r.\n4.1.2. Клиент обязан предоставлять документы и сведения, запрашиваемые \rМФ\rв соответствии с п. 1, ч. 1, ст. 9 Федерального закона от 02.07.2010 № 151-ФЗ «О микрофинансовой деятельности и микрофинансовых организациях», и иными Федеральными законами.\n4.2.\rКлиент вправе получать полную и достоверную информацию о порядке и об условиях предоставления микрозаймов, включая информацию обо всех платежах, связанных с получением, обслуживанием и возвратом микрозайма.\n4.3.\rКлиент вправе досрочно полностью или частично возвратить Займодавцу сумму микрозайма, предварительно письменно уведомив о таком намерении Займодавца не менее чем за 10 (десять) календарных дней.\r5. Порядок утверждения и изменения правил\r5.1.\rНастоящие Правила утверждаются директором \rМФ\rи размещаются в офисе \rМФ\rна информационном стенде и на сайте организации по адресу WWW.NEZAVISIMOST18.RU для ознакомления всех заинтересованных лиц.\n5.2.\rИзменения в настоящих Правилах утверждаются директором \rМФ\rи публикуются в офисе \rМФ\rна информационном стенде и на сайте организации по адресу WWW.NEZAVISIMOST18.RU.\r6. Предоставление данных в уполномоченный орган\r6.1.\rЕжеквартально представляются отчеты о микрофинансовой деятельности и персональном составе руководящих органов \rМФ\rв уполномоченный орган.\r7. Ограничения деятельности микрофинансовой организации\rМикрофинансовая организация не вправе:\n1) выдавать \rзаймы\rв \rиностранной\rвалюте;\n2) в \rодностороннем\rпорядке \rизменять\rпроцентные ставки и (или) порядок их определения по договорам микрозайма, комиссионное вознаграждение и сроки действия этих договоров;\n3) применять к заемщику, являющемуся физическим лицом, в том числе к индивидуальному предпринимателю, досрочно полностью или частично возвратившему микрофинансовой организации сумму микрозайма и предварительно письменно уведомившему о таком намерении микрофинансовую организацию не менее чем за десять календарных дней, \rштрафные санкции за досрочный возврат микрозайма\r;\n4) \rвыдавать заемщику микрозаем\r(микрозаймы), \rесли сумма обязательств\rзаемщика перед микрофинансовой организацией по договорам микрозаймов в случае предоставления такого микрозайма (микрозаймов) \rпревысит один миллион рублей\r.\r8. Прочие условия\r8.1.\rЗаемщик дает согласие на хранение и обработку персональных данных в порядке, предусмотренном Федеральным законом № 152-ФЗ от 27.07.2006. Согласно п. 7 ст. 5 указанного выше закона Стороны определили хранения персональных данных до полного исполнения Заемщиком обязательств по договору. ЦМ вправе передавать и раскрывать любую информацию, касающуюся Договора займа или клиента своим аффилированным лицам и агентам, а также третьим лицам (включая любые кредитные, коллекторские бюро) для конфиденциального использования в соответствии с подписанным клиентом документа о Согласии на обработку персональных данных и с положениями Федерального закона от 27.07.2006 г. № 152-ФЗ «О персональных данных».\r\n');
INSERT INTO `b_search_content_text` VALUES (25,'387418a79ad0cce5279e7f3fc3533941','ПОЛИТИКА ПО ПЕРСОНАЛЬНЫМ ДАННЫМ\r\nПОЛИТИКА ПО ПЕРСОНАЛЬНЫМ ДАННЫМ\rПОЛИТИКА ООО НЕЗАВИСИМОСТЬ-МФО\rВ ОТНОШЕНИИ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ И\rРЕАЛИЗУЕМЫХ ТРЕБОВАНИЯХ К ЗАЩИТЕ ПЕРСОНАЛЬНЫХ ДАННЫХ\rОБЕСПЕЧЕНИЕ БЕЗОПАСНОСТИ ПЕРСОНАЛЬНЫХ ДАННЫХ ЯВЛЯЕТСЯ ОДНОЙ ИЗ ПРИОРИТЕТНЫХ ЗАДАЧ ООО «МФО» (ДАЛЕЕ  ОРГАНИЗАЦИИ). НАСТОЯЩАЯ ПОЛИТИКА РАЗРАБОТАНА В ЦЕЛЯХ СОБЛЮДЕНИЯ ЗАКОНОДАТЕЛЬСТВА О ПЕРСОНАЛЬНЫХ ДАННЫХ, НАСТОЯЩАЯ ПОЛИТИКА ОПРЕДЕЛЯЕТ ПРИНЦИПЫ, ПОРЯДОК И УСЛОВИЯ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ\rРАБОТНИКОВ ОРГАНИЗАЦИИ И ИНЫХ ЛИЦ. ПЕРСОНАЛЬНЫЕ ДАННЫЕ ОБРАБАТЫВАЮТСЯ ОРГАНИЗАЦИЕЙ С ЦЕЛЬЮ ОБЕСПЕЧЕНИЯ ЗАЩИТЫ ПРАВ И СВОБОД ЧЕЛОВЕКА И ГРАЖДАНИНА, В ТОМ ЧИСЛЕ ЗАЩИТЫ ПРАВ НА НЕПРИКОСНОВЕННОСТЬ ЧАСТНОЙ ЖИЗНИ, ЛИЧНУЮ И СЕМЕЙНУЮ.\rОРГАНИЗАЦИЯ ОБРАБАТЫВАЕТ ПЕРСОНАЛЬНЫЕ ДАННЫЕ СЛЕДУЮЩИХ КАТЕГОРИЙ СУБЪЕКТОВ ПЕРСОНАЛЬНЫХ ДАННЫХ:\rПЕРСОНАЛЬНЫЕ ДАННЫЕ РАБОТНИКА ОРГАНИЗАЦИИ - ИНФОРМАЦИЯ, НЕОБХОДИМАЯ ОРГАНИЗАЦИИ В СВЯЗИ С ТРУДОВЫМИ ОТНОШЕНИЯМИ И КАСАЮЩИЕСЯ КОНКРЕТНОГО РАБОТНИКА.\rПЕРСОНАЛЬНЫЕ ДАННЫЕ АФФИЛИРОВАННОГО ЛИЦА ИЛИ ПЕРСОНАЛЬНЫЕ ДАННЫЕ РУКОВОДИТЕЛЯ, УЧАСТНИКА ИЛИ СОТРУДНИКА ЮРИДИЧЕСКОГО ЛИЦА, ЯВЛЯЮЩЕГОСЯ АФФИЛИРОВАННЫМ ЛИЦОМ ПО ОТНОШЕНИЮ К ОРГАНИЗАЦИИ - ИНФОРМАЦИЯ, НЕОБХОДИМАЯ ОРГАНИЗАЦИИ ДЛЯ ОТРАЖЕНИЯ ЕЁ В ОТЧЕТНЫХ ДОКУМЕНТАХ О ДЕЯТЕЛЬНОСТИ ОРГАНИЗАЦИИ В СООТВЕТСТВИИ С ТРЕБОВАНИЯМИ ФЕДЕРАЛЬНЫХ ЗАКОНОВ И ИНЫХ НОРМАТИВНЫХ ПРАВОВЫХ АКТОВ.\rПЕРСОНАЛЬНЫЕ ДАННЫЕ КОНТРАГЕНТА (ПОТЕНЦИАЛЬНОГО КОНТРАГЕНТА), А ТАКЖЕ ПЕРСОНАЛЬНЫЕ ДАННЫЕ РУКОВОДИТЕЛЯ, УЧАСТНИКА (АКЦИОНЕРА) ИЛИ СОТРУДНИКА ЮРИДИЧЕСКОГО ЛИЦА, ЯВЛЯЮЩЕГОСЯ КОНТРАГЕНТОМ ОРГАНИЗАЦИИ - ИНФОРМАЦИЯ, НЕОБХОДИМАЯ ОРГАНИЗАЦИИ ДЛЯ ЗАКЛЮЧЕНИЯ ДОГОВОРА И ИСПОЛНЕНИЯ СВОИХ ОБЯЗАТЕЛЬСТВ В РАМКАХ ДОГОВОРНЫХ ОТНОШЕНИЙ С КОНТРАГЕНТОМ, А ТАКЖЕ ДЛЯ ВЫПОЛНЕНИЯ ТРЕБОВАНИЙ ЗАКОНОДАТЕЛЬСТВА РОССИЙСКОЙ ФЕДЕРАЦИИ.\rПЕРСОНАЛЬНЫЕ ДАННЫЕ ЗАЕМЩИКА (ПОТЕНЦИАЛЬНОГО ЗАЕМЩИКА) - ИНФОРМАЦИЯ, НЕОБХОДИМАЯ ОРГАНИЗАЦИИ ДЛЯ ЗАКЛЮЧЕНИЯ ДОГОВОРА И ВЫПОЛНЕНИЯ СВОИХ ОБЯЗАТЕЛЬСТВ ПО ТАКОМУ ДОГОВОРУ, ЗАЩИТЫ ПРАВ И ЗАКОННЫХ ИНТЕРЕСОВ ОРГАНИЗАЦИИ (МИНИМИЗАЦИЯ РИСКОВ ОРГАНИЗАЦИИ, СВЯЗАННЫХ С НАРУШЕНИЕМ ОБЯЗАТЕЛЬСТВ ПО ДОГОВОРУ ЗАЙМА), ОСУЩЕСТВЛЕНИЯ И ВЫПОЛНЕНИЯ ВОЗЛОЖЕННЫХ ЗАКОНОДАТЕЛЬСТВОМ РФ НА ОРГАНИЗАЦИЮ ФУНКЦИЙ И ОБЯЗАННОСТЕЙ (РЕАЛИЗАЦИЯ МЕР ПО ПРОТИВОДЕЙСТВИЮ ЛЕГАЛИЗАЦИИ ДОХОДОВ, ПОЛУЧЕННЫХ ПРЕСТУПНЫМ ПУТЕМ И ДР.)\rОРГАНИЗАЦИЯ ОСУЩЕСТВЛЯЕТ ОБРАБОТКУ ПЕРСОНАЛЬНЫХ ДАННЫХ В СЛЕДУЮЩИХ СЛУЧАЯХ:\rОСУЩЕСТВЛЕНИЯ ФИНАНСОВЫХ ОПЕРАЦИЙ И ИНОЙ ДЕЯТЕЛЬНОСТИ, ПРЕДУСМОТРЕННОЙ УСТАВОМ ОРГАНИЗАЦИИ, ДЕЙСТВУЮЩИМ ЗАКОНОДАТЕЛЬСТВОМ РФ, В ЧАСТНОСТИ ФЗ: «О МИКРОФИНАНСОВОЙ ДЕЯТЕЛЬНОСТИ И МИКРОФИНАНСОВЫХ ОРГАНИЗАЦИЯХ», «О КРЕДИТНЫХ ИСТОРИЯХ», «О ПРОТИВОДЕЙСТВИИ ЛЕГАЛИЗАЦИИ (ОТМЫВАНИЮ) ДОХОДОВ, ПОЛУЧЕННЫХ ПРЕСТУПНЫМ ПУТЕМ, И ФИНАНСИРОВАНИЮ ТЕРРОРИЗМА», «О ПЕРСОНАЛЬНЫХ ДАННЫХ».\rЗАКЛЮЧЕНИЯ, ИСПОЛНЕНИЯ И ПРЕКРАЩЕНИЯ ДОГОВОРОВ С ФИЗИЧЕСКИМИ, ЮРИДИЧЕСКИМ ЛИЦАМИ, ИНДИВИДУАЛЬНЫМИ ПРЕДПРИНИМАТЕЛЯМИ И ИНЫМИ ЛИЦАМИ, В СЛУЧАЯХ, ПРЕДУСМОТРЕННЫХ ДЕЙСТВУЮЩИМ ЗАКОНОДАТЕЛЬСТВОМ И УСТАВОМ ОРГАНИЗАЦИИ;\rОБЕСПЕЧЕНИЯ СОБЛЮДЕНИЯ ЗАКОНОВ И ИНЫХ НОРМАТИВНЫХ ПРАВОВЫХ АКТОВ, СОДЕЙСТВИЯ РАБОТНИКАМ В ТРУДОУСТРОЙСТВЕ, ОБУЧЕНИИ И ПРОДВИЖЕНИИ ПО СЛУЖБЕ, ОБЕСПЕЧЕНИЯ ЛИЧНОЙ БЕЗОПАСНОСТИ РАБОТНИКОВ, КОНТРОЛЯ КОЛИЧЕСТВА И КАЧЕСТВА ВЫПОЛНЯЕМОЙ РАБОТЫ И ОБЕСПЕЧЕНИЯ СОХРАННОСТИ ИМУЩЕСТВА, В ТОМ ЧИСЛЕ ИСПОЛНЕНИЯ ТРЕБОВАНИЙ НАЛОГОВОГО ЗАКОНОДАТЕЛЬСТВА В СВЯЗИ С ИСЧИСЛЕНИЕМ И УПЛАТОЙ НАЛОГА НА ДОХОДЫ ФИЗИЧЕСКИХ ЛИЦ, А ТАКЖЕ ЕДИНОГО СОЦИАЛЬНОГО НАЛОГА, ПЕНСИОННОГО ЗАКОНОДАТЕЛЬСТВА ПРИ ФОРМИРОВАНИИ И ПРЕДСТАВЛЕНИИ ПЕРСОНИФИЦИРОВАННЫХ ДАННЫХ О КАЖДОМ ПОЛУЧАТЕЛЕ ДОХОДОВ, УЧИТЫВАЕМЫХ ПРИ НАЧИСЛЕНИИ СТРАХОВЫХ ВЗНОСОВ НА ОБЯЗАТЕЛЬНОЕ ПЕНСИОННОЕ СТРАХОВАНИЕ И ОБЕСПЕЧЕНИЕ, ЗАПОЛНЕНИЯ ПЕРВИЧНОЙ СТАТИСТИЧЕСКОЙ ДОКУМЕНТАЦИИ, ВЕДЕНИЯ КАДРОВОГО ДЕЛОПРОИЗВОДСТВА В СООТВЕТСТВИИ С ТРУДОВЫМ КОДЕКСОМ РФ, НАЛОГОВЫМ КОДЕКСОМ РФ, ФЕДЕРАЛЬНЫМИ ЗАКОНАМИ, В ЧАСТНОСТИ: «ОБ ИНДИВИДУАЛЬНОМ (ПЕРСОНИФИЦИРОВАННОМ) УЧЕТЕ В СИСТЕМЕ ОБЯЗАТЕЛЬНОГО ПЕНСИОННОГО СТРАХОВАНИЯ».\rСРОКИ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ ОПРЕДЕЛЯЮТСЯ ДОГОВОРОМ С СУБЪЕКТОМ ПЕРСОНАЛЬНЫХ ДАННЫХ, ПРИКАЗОМ МИНКУЛЬТУРЫ РФ ОТ 25.08.2010 № 558 «ОБ УТВЕРЖДЕНИИ «ПЕРЕЧНЯ ТИПОВЫХ УПРАВЛЕНЧЕСКИХ АРХИВНЫХ ДОКУМЕНТОВ, ОБРАЗУЮЩИХСЯ В ПРОЦЕССЕ ДЕЯТЕЛЬНОСТИ ГОСУДАРСТВЕННЫХ ОРГАНОВ, ОРГАНОВ МЕСТНОГО САМОУПРАВЛЕНИЯ И ОРГАНИЗАЦИЙ, С УКАЗАНИЕМ СРОКОВ ХРАНЕНИЯ» И ИНЫМИ НОРМАТИВНЫМИ ПРАВОВЫМИ АКТАМИ РФ. В ОРГАНИЗАЦИИ СОЗДАЮТСЯ И ХРАНЯТСЯ ДОКУМЕНТЫ, СОДЕРЖАЩИЕ СВЕДЕНИЯ О СУБЪЕКТАХ ПЕРСОНАЛЬНЫХ ДАННЫХ. ТРЕБОВАНИЯ К ДОКУМЕНТАМ, В КОТОРЫХ СОДЕРЖАТСЯ ПЕРСОНАЛЬНЫЕ ДАННЫЕ, А ТАКЖЕ К ПОРЯДКУ РАБОТЫ С ТАКИМИ ДОКУМЕНТАМИ УСТАНОВЛЕНЫ ПОСТАНОВЛЕНИЕМ ПРАВИТЕЛЬСТВА РФ ОТ 15.09.2008 № 687 «ОБ УТВЕРЖДЕНИИ ПОЛОЖЕНИЯ ОБ ОСОБЕННОСТЯХ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ, ОСУЩЕСТВЛЯЕМОЙ БЕЗ ИСПОЛЬЗОВАНИЯ СРЕДСТВ АВТОМАТИЗАЦИИ».\rОБРАБОТКА ПЕРСОНАЛЬНЫХ ДАННЫХ ОРГАНИЗАЦИЕЙ ОСУЩЕСТВЛЯЕТСЯ НА ОСНОВЕ ПРИНЦИПОВ:\rЗАКОННОСТИ И СПРАВЕДЛИВОСТИ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ;\rДОСТИЖЕНИЯ КОНКРЕТНЫХ, ЗАРАНЕЕ ОПРЕДЕЛЕННЫХ И ЗАКОННЫХ ЦЕЛЕЙ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ;\rСООТВЕТСТВИЯ ЦЕЛЕЙ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ ЦЕЛЯМ, ЗАРАНЕЕ ОПРЕДЕЛЕННЫМ И ЗАЯВЛЕННЫМ ПРИ СБОРЕ ПЕРСОНАЛЬНЫХ ДАННЫХ;\rСООТВЕТСТВИЯ ОБЪЕМА И СОДЕРЖАНИЯ ОБРАБАТЫВАЕМЫХ ПЕРСОНАЛЬНЫХ ДАННЫХ, ЦЕЛЯМ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ;\rДОСТОВЕРНОСТИ ПЕРСОНАЛЬНЫХ ДАННЫХ, ИХ ДОСТАТОЧНОСТИ ДЛЯ ЦЕЛЕЙ ОБРАБОТКИ, НЕДОПУСТИМОСТИ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ, ИЗБЫТОЧНЫХ ПО ОТНОШЕНИЮ К ЦЕЛЯМ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ;\rНЕДОПУСТИМОСТИ ОБЪЕДИНЕНИЯ БАЗ ДАННЫХ, СОДЕРЖАЩИХ ПЕРСОНАЛЬНЫЕ ДАННЫЕ, ОБРАБОТКА КОТОРЫХ ОСУЩЕСТВЛЯЕТСЯ В ЦЕЛЯХ НЕСОВМЕСТИМЫХ МЕЖДУ СОБОЙ;\rОСУЩЕСТВЛЕНИЕ ХРАНЕНИЯ ПЕРСОНАЛЬНЫХ ДАННЫХ В ФОРМЕ, ПОЗВОЛЯЮЩЕЙ ОПРЕДЕЛИТЬ СУБЪЕКТА ПЕРСОНАЛЬНЫХ ДАННЫХ, НЕ ДОЛЬШЕ, ЧЕМ ЭТОГО ТРЕБУЮТ ЦЕЛИ ИХ ОБРАБОТКИ;\rОРГАНИЗАЦИЯ ПРЕДПРИНИМАЕТ НЕОБХОДИМЫЕ ОРГАНИЗАЦИОННЫЕ И ТЕХНИЧЕСКИЕ МЕРЫ ДЛЯ ОБЕСПЕЧЕНИЯ БЕЗОПАСНОСТИ ПЕРСОНАЛЬНЫХ ДАННЫХ ОТ НЕПРАВОМЕРНОГО ИЛИ СЛУЧАЙНОГО ДОСТУПА, УНИЧТОЖЕНИЯ, ИЗМЕНЕНИЯ, БЛОКИРОВАНИЯ И ДРУГИХ НЕПРАВОМЕРНЫХ ДЕЙСТВИЙ.\rВ ЦЕЛЯХ КООРДИНАЦИИ ДЕЙСТВИЙ ПО ОБЕСПЕЧЕНИЮ БЕЗОПАСНОСТИ ПЕРСОНАЛЬНЫХ ДАННЫХ В ОРГАНИЗАЦИИ НАЗНАЧЕН ОТВЕТСТВЕННЫЙ ЗА ОБЕСПЕЧЕНИЕ БЕЗОПАСНОСТИ ПЕРСОНАЛЬНЫХ ДАННЫХ.\rНАСТОЯЩАЯ ПОЛИТИКА ПОДЛЕЖИТ ИЗМЕНЕНИЮ, ДОПОЛНЕНИЮ В СЛУЧАЕ ПОЯВЛЕНИЯ НОВЫХ ЗАКОНОДАТЕЛЬНЫХ АКТОВ И СПЕЦИАЛЬНЫХ НОРМАТИВНЫХ ДОКУМЕНТОВ ПО ОБРАБОТКЕ И ЗАЩИТЕ ПЕРСОНАЛЬНЫХ ДАННЫХ.\rКОНТРОЛЬ ИСПОЛНЕНИЯ ТРЕБОВАНИЙ НАСТОЯЩЕЙ ПОЛИТИКИ ОСУЩЕСТВЛЯЕТСЯ ОТВЕТСТВЕННЫМ ЗА ОБЕСПЕЧЕНИЕ БЕЗОПАСНОСТИ ПЕРСОНАЛЬНЫХ ДАННЫХ ОРГАНИЗАЦИИ.\r\n');
INSERT INTO `b_search_content_text` VALUES (26,'4b8a820a49b1e2da2fb86aa74a867de8','Москва\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (27,'6256c4c1e8d2ebd8a708d519faa5206d','1.2. ОП «ЛЮБЛИНО»\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (28,'62d08d37f3f86fa011cf66fa5e5b04e2','АДРЕСА ОФИСОВ\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (31,'ec2d1d3296c2bfa2b0e8ddfcb6fe03e3','СВИДЕТЕЛЬСТВО МФО\r\nСВИДЕТЕЛЬСТВО МФО\r\n');
INSERT INTO `b_search_content_text` VALUES (33,'f96343ce0c135ff09ddc58781738c70b','Тестовая новость\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (34,'483e9c4554e1e3b3270522beb10a8daa','ываываыва\r\nываываыва ыв ыв аыва ыв\r\n');
INSERT INTO `b_search_content_text` VALUES (36,'184018507a56bf14daee016fe8e852da','1.1. ОП «МЕНДЕЛЕЕВСКАЯ»\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (37,'cfa8ffe03b67c4ce67330c788888b7b4','ОП «ЭЛЕКТРОЗАВОДСКАЯ»\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (38,'6e74115dfa5ff63ced0358db5fdcb9e5','САХАРОВА ОЛЕСЯ ДМИТРИЕВНА\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (39,'84c39e36a2d379eeaff4732c560fb442','ФИНАНСОВЫЙ СПЕЦИАЛИСТ / КРЕДИТНЫЙ СПЕЦИАЛИСТ\r\nДОЛЖНОСТНЫЕ ОБЯЗАННОСТИ\rКОНСУЛЬТАЦИЯ 	ПОТЕНЦИАЛЬНЫХ КЛИЕНТОВ ПО УСЛОВИЯМ 	ПРЕДОСТАВЛЕНИЯ И ПОГАШЕНИЯ ЗАЙМОВ;\rПЕРВИЧНАЯ 	ОЦЕНКА ПЛАТЕЖЕСПОСОБНОСТИ КЛИЕНТА;\rОФОРМЛЕНИЕ 	ДОГОВОРА ЗАЙМА С КЛИЕНТОМ В ОФИСЕ 	ПРОДАЖ;\rВЫДАЧА/ПРИЕМ 	ДЕНЕЖНЫХ СРЕДСТВ ПО ОБЯЗАТЕЛЬСТВАМ 	ДОГОВОРА ЗАЙМА;\rПОЛНОЕ 	СОПРОВОЖДЕНИЕ КЛИЕНТОВ ОТ МОМЕНТА 	ПОДАЧИ ЗАЯВКИ ДО ПОГАШЕНИЯ ЗАЙМА;\rВЕДЕНИЕ 	ВНУТРЕННЕГО ДОКУМЕНТООБОРОТА И 	ОТЧЕТНОСТИ;\rВЫПОЛНЕНИЕ 	ПЛАНОВЫХ ПОКАЗАТЕЛЕЙ И ПОСТАВЛЕННЫХ 	ЗАДАЧ.\rКОНТРОЛЬ 	ЗА ФИНАНСОВОЙ ДИСЦИПЛИНОЙ: ДВИЖЕНИЕ 	(ПОПОЛНЕНИЕ, ИНКАССАЦИЯ) ДЕНЕЖНЫХ 	СРЕДСТВ.\rСВЕРКИ 	И СБОР ДОКУМЕНТОВ ПО ПОСТАВЩИКАМ\rМАТЕРИАЛЬНАЯ 	ОТВЕТСТВЕННОСТЬ ЗА ДЕНЕЖНЫЕ СРЕДСТВА.\rТРЕБУЕТСЯ\rВОЗРАСТ: 	ОТ 23 ДО 43 ЛЕТ \rОБРАЗОВАНИЕ: 	ВЫСШЕЕ.\rЗНАНИЕ 	ПК\rКАК 	ПРЕИМУЩЕСТВО ОПЫТ РАБОТЫ:\r- В ОБЛАСТИ АКТИВНЫХ ПРОДАЖ;\r- БАНКОВСКОЙ ИЛИ СТРАХОВОЙ СФЕРЕ.\rЖЕЛАНИЕ 	ЗАРАБАТЫВАТЬ И РАЗВИВАТЬСЯ В ФИНАНСОВОЙ 	СФЕРЕ.\rМЫ ПРЕДЛАГАЕМ\rМЕСТО 	РАБОТЫ: ОФИСЫ ПРОДАЖ\rОБОРУДОВАННОЕ 	РАБОЧЕЕ МЕСТО ПО СТАНДАРТАМ КОМПАНИИ;\rГРАФИК 	РАБОТЫ 2/2 (С 9:00-21:00);\rДОХОД: 	ОКЛАД+ ПРЕМИЯ ОТ 23 000 ДО 56 000 РУБ. 	(ФИКСИРОВАННЫЙ ОКЛАД + ЕЖЕМЕСЯЧНАЯ 	ПРЕМИЯ);\rСОБЛЮДЕНИЕ 	ТК РФ, БЕЛАЯ ЗАРАБОТНАЯ ПЛАТА;\rИСПЫТАТЕЛЬНЫЙ 	СРОК 3 МЕСЯЦА\rОБУЧЕНИЕ 	РАБОТЕ, ПРОФЕССИОНАЛЬНАЯ ПОДДЕРЖКА;\rРАЗВИТИЕ 	И КАРЬЕРНЫЙ РОСТ;\rРАБОТА 	В ДРУЖНОМ И СПЛОЧЕННОМ КОЛЛЕКТИВЕ.\r\n');
INSERT INTO `b_search_content_text` VALUES (40,'be923bb7f7930df341c12cdcd8ec3c92','ГОТОВЫЙ САЙТ ДЛЯ ВАШЕЙ МФО | ОТПРАВКА РЕЗЮМЕ\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (41,'ca072d376f7fc944f23059acc8ee0266','ГОТОВЫЙ САЙТ ДЛЯ ВАШЕЙ МФО | О КОМПАНИИ\r\n\rПЛЮС\rКРЕДИТ\r - ЭТО МИКРОФИНАНСОВАЯ КОМПАНИЯ ПРЕДОСТАВЛЯЮЩАЯ ШИРОКИЙ СПЕКТР ВОСТРЕБОВАННЫХ ПРОДУКТОВ (ЗАЙМОВ) ФИЗИЧЕСКИМ ЛИЦАМ, КОТОРЫЕ ПО КАКИМ-ЛИБО ПРИЧИНАМ НЕ МОГУТ ПОЛУЧИТЬ КРЕДИТЫ/ЗАЙМЫ В БАНКАХ ИЛИ ДРУГИХ ОРГАНИЗАЦИЯХ.\rМЫ СТАВИМ ПЕРЕД СОБОЙ ЦЕЛИ И ДОБИВАЕМСЯ ИХ.  \rМЫ РАБОТАЕМ, ЧТОБЫ ВАШИ МЕЧТЫ СБЫВАЛИСЬ.\rМЫ РАБОТАЕМ, ЧТОБЫ ПРЕДОСТАВЛЯТЬ ВАМ ПРОСТЫЕ И УДОБНЫЕ ФИНАНСОВЫЕ УСЛУГИ.   \rМЫ РАЗРАБОТАЛИ УНИКАЛЬНЫЕ ПРОГРАММЫ ДЛЯ ВАС, И ВЫ СМОЖЕТЕ С УВЕРЕННОСТЬЮ СМОТРЕТЬ В ЗАВТРАШНИЙ ДЕНЬ.  \rМЫ ВМЕСТЕ С ВАМИ РАБОТАЕМ НА ПЕРСПЕКТИВУ, ТЕПЕРЬ ВЫ САМИ ВЫБИРАЕТЕ СВОЕ БУДУЩЕЕ.\rС НАМИ ЛЕГКО РЕШАЮТСЯ ЛЮБЫЕ ФИНАНСОВЫЕ ЗАДАЧИ!\rОПЫТ  НАШЕ ЛУЧШЕЕ ОРУЖИЕ:\rСЕГОДНЯ, ОПИРАЯСЬ НА ОПЫТ НАШИХ ЗАПАДНЫХ КОЛЛЕГ И ПРОРАБОТАВ БОЛЕЕ 10 ЛЕТ НА РОССИЙСКОМ ФИНАНСОВОМ РЫНКЕ, МЫ ПОСТРОИЛИ НАШУ КОМПАНИЮ \rПЛЮС\rКРЕДИТ\r ТЕПЕРЬ МЫ ИМЕЕМ ПОЛНОЕ ПРАВО СКАЗАТЬ:  МЫ ГОТОВЫ ПОМОЧЬ ВАМ ИЗМЕНИТЬ МИР К ЛУЧШЕМУ! \rНАША КОМАНДА  НАШ ГЛАВНЫЙ КАПИТАЛ: \r\rПЛЮС\rКРЕДИТ\r - ЭТО КОМАНДА ПРОФЕССИОНАЛОВ, КОТОРЫЕ РАБОТАЮТ НА РОССИЙСКОМ ФИНАНСОВОМ РЫНКЕ ДОЛГОЕ ВРЕМЯ. МЫ ОБЪЕДИНИЛИСЬ ВМЕСТЕ, И НАША ЗАДАЧА РАСТИ И РАЗВИВАТЬСЯ, РАЗДЕЛЯЯ ДОСТИГНУТЫЕ УСПЕХИ СО СВОИМИ КЛИЕНТАМИ, ПАРТНЕРАМИ.\rВ НАШЕЙ КОМПАНИИ РАБОТАЮТ ВЫСОКОКВАЛИФИЦИРОВАННЫЕ СПЕЦИАЛИСТЫ. \rПРОФЕССИОНАЛИЗМ  ЗАЛОГ КАЧЕСТВА ОБСЛУЖИВАНИЯ  НАШИХ КЛИЕНТОВ.\rЧЕСТНОСТЬ:\rВСЕ МЫ СТАЛКИВАЕМСЯ С НЕОЖИДАННЫМИ РАСХОДАМИ: РЕМОНТ АВТОМОБИЛЯ, ОБРАЗОВАНИЕ, МЕДИЦИНСКИЕ УСЛУГИ, СВАДЬБА И ПРОЧЕЕ. ЭТО ПРОСТЫЕ ПРОБЛЕМЫ И ОНИ ТРЕБУЮТ ПРОСТОГО РЕШЕНИЯ. \rУ НАС НЕТ СКРЫТЫХ КОМИССИЙ.\rУ НАС ПРОСТАЯ СИСТЕМА ПОГАШЕНИЯ ЗАЙМА.\rУ НАС ЧЕСТНОЕ РЕШЕНИЕ ПРОБЛЕМ ДЛЯ ЧЕСТНЫХ ЛЮДЕЙ! \rНАША МИССИЯ\rУЛУЧШЕНИЕ ДОСТУПА МАЛООБЕСПЕЧЕННЫХ СЛОЕВ НАСЕЛЕНИЯ К ФИНАНСОВЫМ УСЛУГАМ;\rПОВЫШЕНИЕ УРОВНЯ ЖИЗНИ ЛЮДЕЙ С НЕВЫСОКИМ УРОВНЕМ ДОХОДОВ;\rСОЗДАНИЕ ДОСТУПНЫХ РАБОЧИХ МЕСТ;\rСОДЕЙСТВИЕ УВЕЛИЧЕНИЮ ДОЛИ ПРОФЕССИОНАЛЬНЫХ ИГРОКОВ НА МИКРОФИНАНСОВОМ РЫНКЕ.\r\n');
INSERT INTO `b_search_content_text` VALUES (42,'b9d5267a11dd1c99d82438a62c326969','ГОТОВЫЙ САЙТ ДЛЯ ВАШЕЙ МФО | О КОМПАНИИ\r\nИНФОРМАЦИЯ ДЛЯ:\rСОБСТВЕННИКОВ\rАГЕНСТВ 	НЕДВИЖИМОСТИ\rЧАСТНЫХ 	РИЭЛТОРОВ\rВ СВЯЗИ С ДИНАМИЧНЫМ РОСТОМ И РАЗВИТИЕМ КОМПАНИИ РАССМАТРИВАЮТСЯ ПОМЕЩЕНИЯ ДЛЯ АРЕНДЫ ПОД ОФИСЫ ПРОДАЖ ПО СЛЕДУЮЩИМ КРИТЕРИЯМ:\r1.\r УДАЛЕННОСТЬЮ 	ОТ МЕТРО ДО 500 МЕТРОВ.\r       (ДЛЯ 	МОСКВЫ)\r2.\r ЖИЛЫЕ 	ДОМА - 1-Я ЛИНИЯ.\r3.\r ТЦ 	- НАХОЖДЕНИЕ ПАВИЛЬОНА, ОФИСА НЕ БОЛЕЕ 	50 МЕТРОВ ОТ ЦЕНТРАЛЬНОГО ВХОДА ИЛИ 	ОТДЕЛЬНЫЙ ВХОД.\r4.\r ОТ 	-1 ДО 7 ЭТАЖА, В ЗАВИСИМОСТИ ОТ МЕСТА 	НАХОЖДЕНИЯ.\r5.\r ПЛОЩАДЬ 	ОТ 10  45 КВ.М (ЗАВИСИТ ОТ ПЛАНИРОВКИ).\r6.\r ВОЗМОЖНОСТЬ 	РАЗМЕЩЕНИЯ РЕКЛАМЫ И РЕКЛАМНЫХ УКАЗАТЕЛЕЙ 	НА ЦЕНТРАЛЬНОМ ФАСАДЕ ЗДАНИЯ.\rИНФОРМАЦИЮ ПРОСИМ НАПРАВЛЯТЬ НА АДРЕС \rARENDA@\rPLUSKREDIT\r.RU\rИЛИ ОСТАВИТЬ ЗАЯВКУ ПО ТЕЛЕФОНАМ:\r8 (499) 600 5877 \r(ДЛЯ МОСКВЫ)\r8 (800) 707 5877 \r(ДЛЯ РЕГИОНОВ)\r\n');
INSERT INTO `b_search_content_text` VALUES (43,'412fb2675215b77aa7531a5bca21a358','ЗАЙМЫ ОНЛАЙН\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (44,'f329840bf02012127e9f08da323cf121','Контакты\r\nTEXT HERE....\r\n');
INSERT INTO `b_search_content_text` VALUES (45,'255d092219ed945e857fb0adc18faabd','О компании\r\nTEXT HERE....\r\n');
INSERT INTO `b_search_content_text` VALUES (46,'52b8d587686113ba2245334f4bdfab77','Требования к заемщикам\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (47,'228c309b48865722acd0fbbf7f50577c','Агентам\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (48,'108dbdf64dbc79ab55dcbc4eea8b92b7','Франшиза\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (49,'7bcf63f591d7d3f36360d666d03c21e9','Оформление заявки\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (54,'b69f162d1d7332d7968dd7933ccdcc1f','Слайдер на главной\r\nЗаймы на любые \rслучаи жизни!\r\n');
INSERT INTO `b_search_content_text` VALUES (55,'341d07b79bd94bd5de8fa3c856e6ab7c','Оформите заявку\r\nУ Вас это займет не больше 10 минут\r\n');
INSERT INTO `b_search_content_text` VALUES (56,'81d6eeadd365b1299ead61113b60c03c','Получите ответ\r\nМы дадим ответ в течение \r20 минут\r\n');
INSERT INTO `b_search_content_text` VALUES (57,'bb4b8c7c6b93f4d93f87d668626e0022','Мгновенно получите деньги\r\nЛюбым удобным для вас способом!\r\n');
INSERT INTO `b_search_content_text` VALUES (59,'bd2c1d260b06b9d9f943ce28842a9b70','VISA\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (60,'db2d3c2717c8e10daa7c107f48d2ff6c','MASTERCARD\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (61,'5961f64d30da8044b09c885d1da6b341','QIWI\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (62,'115b8324131374d5fd2dec29dbcd3563','Мир\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (63,'e132a97f2fee6d881ff34f8664d742b8','NA\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (64,'7dbd6377787e5c82c94a5112919c6eeb','CONTACT\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (65,'b2269a90ec726cef334411e533ceed6e','Казань\r\n\r\n');
INSERT INTO `b_search_content_text` VALUES (70,'c22baf5fb9c91141e5876d2beda8730c','ЭКСПРЕСС\r\nот 1000 до 5 000 рублей для клиентов количество полученных займов которых составляет от 1 до 3\r\nДалеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. LOREUM 1\r\n');
INSERT INTO `b_search_content_text` VALUES (71,'dfd8f0a4af8d7c8568c65a4f65ffe982','До зарплаты\r\nот 1000 до 10000 рублей для клиентов количество полученных займов которых составляет от 4 до 5\r\n2 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана.\r\n');
INSERT INTO `b_search_content_text` VALUES (72,'3666f6b17b20a48b8c1a3e6534a831e3','Клубный\r\nот 1000 до 20 000 рублей для постоянных клиентов количество полученных займов которых составляет от 6 и более\r\n3 Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана.\r\n');
INSERT INTO `b_search_content_text` VALUES (73,'ba324c0ede5c42941b869a83c2cd9439','Офицерский\r\nот 1 000 до 25 000 на срок до одного года\r\nПриглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас.\r\n');
INSERT INTO `b_search_content_text` VALUES (74,'d38a3dc3c3b5209c09c64ba475b14b83','Пенсионный\r\nот 1000 до 10 000 на срок до 6 месяцев\r\n5. Приглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас.\r\n');
INSERT INTO `b_search_content_text` VALUES (75,'22def3edadbee1a9d9839ff1fef05bd6','Деньги на дом\r\nмикрофинансирование от 1000 до 20 000 рублей\r\n6. Приглашаем партнеров по всей стране присоединиться к бренду «ЭКСПРЕСС–ДЕНЬГИ» и начать зарабатывать на максимально выгодных условия уже сейчас.\r\n');
INSERT INTO `b_search_content_text` VALUES (76,'bc7fe63f19ca3844729f84d60e4c9a22','ПРЕИМУЩЕСТВО 1\r\nКРАТКОЕ ОПИСАНИЕ \rПРЕИМУЩЕСТВА\r\n');
INSERT INTO `b_search_content_text` VALUES (77,'bc7fe63f19ca3844729f84d60e4c9a22','ПРЕИМУЩЕСТВО 1\r\nКРАТКОЕ ОПИСАНИЕ \rПРЕИМУЩЕСТВА\r\n');
INSERT INTO `b_search_content_text` VALUES (78,'bc7fe63f19ca3844729f84d60e4c9a22','ПРЕИМУЩЕСТВО 1\r\nКРАТКОЕ ОПИСАНИЕ \rПРЕИМУЩЕСТВА\r\n');
INSERT INTO `b_search_content_text` VALUES (79,'412fb2675215b77aa7531a5bca21a358','ЗАЙМЫ ОНЛАЙН\r\n\r\n');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_content_title`
-- 




DROP TABLE IF EXISTS `b_search_content_title`;
CREATE TABLE `b_search_content_title` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `POS` int(11) NOT NULL,
  `WORD` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_TITLE` (`SITE_ID`,`WORD`,`SEARCH_CONTENT_ID`,`POS`),
  KEY `IND_B_SEARCH_CONTENT_TITLE` (`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci DELAY_KEY_WRITE=1;

-- 
-- Dumping data for table  `b_search_content_title`
-- 


INSERT INTO `b_search_content_title` VALUES (5,'s1',0,'QQQ');
INSERT INTO `b_search_content_title` VALUES (8,'s1',8,'АЛЕКСЕЙ');
INSERT INTO `b_search_content_title` VALUES (8,'s1',16,'ВАДИМОВИЧ');
INSERT INTO `b_search_content_title` VALUES (8,'s1',0,'СМИРНОВ');
INSERT INTO `b_search_content_title` VALUES (9,'s1',0,'МФО');
INSERT INTO `b_search_content_title` VALUES (12,'s1',0,'ВАКАНСИИ');
INSERT INTO `b_search_content_title` VALUES (20,'s1',17,'деньги');
INSERT INTO `b_search_content_title` VALUES (20,'s1',0,'Экспресс');
INSERT INTO `b_search_content_title` VALUES (21,'s1',17,'ВАШЕЙ');
INSERT INTO `b_search_content_title` VALUES (21,'s1',0,'ГОТОВЫЙ');
INSERT INTO `b_search_content_title` VALUES (21,'s1',13,'ДЛЯ');
INSERT INTO `b_search_content_title` VALUES (21,'s1',42,'ЗАЙМ');
INSERT INTO `b_search_content_title` VALUES (21,'s1',29,'КАК');
INSERT INTO `b_search_content_title` VALUES (21,'s1',23,'МФО');
INSERT INTO `b_search_content_title` VALUES (21,'s1',33,'ПОЛУЧИТЬ');
INSERT INTO `b_search_content_title` VALUES (21,'s1',8,'САЙТ');
INSERT INTO `b_search_content_title` VALUES (22,'s1',17,'ВАШЕЙ');
INSERT INTO `b_search_content_title` VALUES (22,'s1',0,'ГОТОВЫЙ');
INSERT INTO `b_search_content_title` VALUES (22,'s1',13,'ДЛЯ');
INSERT INTO `b_search_content_title` VALUES (22,'s1',42,'ЗАЙМ');
INSERT INTO `b_search_content_title` VALUES (22,'s1',29,'КАК');
INSERT INTO `b_search_content_title` VALUES (22,'s1',23,'МФО');
INSERT INTO `b_search_content_title` VALUES (22,'s1',33,'ПОГАСИТЬ');
INSERT INTO `b_search_content_title` VALUES (22,'s1',8,'САЙТ');
INSERT INTO `b_search_content_title` VALUES (23,'s1',13,'ДОЛЖНИКОМ');
INSERT INTO `b_search_content_title` VALUES (23,'s1',0,'КАК');
INSERT INTO `b_search_content_title` VALUES (23,'s1',4,'НЕ');
INSERT INTO `b_search_content_title` VALUES (23,'s1',7,'СТАТЬ');
INSERT INTO `b_search_content_title` VALUES (24,'s1',44,'микрозаймов');
INSERT INTO `b_search_content_title` VALUES (24,'s1',0,'Правила');
INSERT INTO `b_search_content_title` VALUES (24,'s1',15,'предоставления');
INSERT INTO `b_search_content_title` VALUES (25,'s1',25,'ДАННЫМ');
INSERT INTO `b_search_content_title` VALUES (25,'s1',12,'ПЕРСОНАЛЬНЫМ');
INSERT INTO `b_search_content_title` VALUES (25,'s1',0,'ПО');
INSERT INTO `b_search_content_title` VALUES (25,'s1',0,'ПОЛИТИКА');
INSERT INTO `b_search_content_title` VALUES (26,'s1',0,'Москва');
INSERT INTO `b_search_content_title` VALUES (27,'s1',0,'1');
INSERT INTO `b_search_content_title` VALUES (27,'s1',2,'2');
INSERT INTO `b_search_content_title` VALUES (27,'s1',9,'ЛЮБЛИНО');
INSERT INTO `b_search_content_title` VALUES (27,'s1',5,'ОП');
INSERT INTO `b_search_content_title` VALUES (28,'s1',0,'АДРЕСА');
INSERT INTO `b_search_content_title` VALUES (28,'s1',7,'ОФИСОВ');
INSERT INTO `b_search_content_title` VALUES (31,'s1',14,'МФО');
INSERT INTO `b_search_content_title` VALUES (31,'s1',0,'СВИДЕТЕЛЬСТВО');
INSERT INTO `b_search_content_title` VALUES (33,'s1',17,'новость');
INSERT INTO `b_search_content_title` VALUES (33,'s1',0,'Тестовая');
INSERT INTO `b_search_content_title` VALUES (34,'s1',0,'ываываыва');
INSERT INTO `b_search_content_title` VALUES (36,'s1',0,'1');
INSERT INTO `b_search_content_title` VALUES (36,'s1',9,'МЕНДЕЛЕЕВСКАЯ');
INSERT INTO `b_search_content_title` VALUES (36,'s1',5,'ОП');
INSERT INTO `b_search_content_title` VALUES (37,'s1',0,'ОП');
INSERT INTO `b_search_content_title` VALUES (37,'s1',4,'ЭЛЕКТРОЗАВОДСКАЯ');
INSERT INTO `b_search_content_title` VALUES (38,'s1',15,'ДМИТРИЕВНА');
INSERT INTO `b_search_content_title` VALUES (38,'s1',9,'ОЛЕСЯ');
INSERT INTO `b_search_content_title` VALUES (38,'s1',0,'САХАРОВА');
INSERT INTO `b_search_content_title` VALUES (39,'s1',24,'КРЕДИТНЫЙ');
INSERT INTO `b_search_content_title` VALUES (39,'s1',11,'СПЕЦИАЛИСТ');
INSERT INTO `b_search_content_title` VALUES (39,'s1',0,'ФИНАНСОВЫЙ');
INSERT INTO `b_search_content_title` VALUES (40,'s1',17,'ВАШЕЙ');
INSERT INTO `b_search_content_title` VALUES (40,'s1',0,'ГОТОВЫЙ');
INSERT INTO `b_search_content_title` VALUES (40,'s1',13,'ДЛЯ');
INSERT INTO `b_search_content_title` VALUES (40,'s1',23,'МФО');
INSERT INTO `b_search_content_title` VALUES (40,'s1',29,'ОТПРАВКА');
INSERT INTO `b_search_content_title` VALUES (40,'s1',38,'РЕЗЮМЕ');
INSERT INTO `b_search_content_title` VALUES (40,'s1',8,'САЙТ');
INSERT INTO `b_search_content_title` VALUES (41,'s1',17,'ВАШЕЙ');
INSERT INTO `b_search_content_title` VALUES (41,'s1',0,'ГОТОВЫЙ');
INSERT INTO `b_search_content_title` VALUES (41,'s1',13,'ДЛЯ');
INSERT INTO `b_search_content_title` VALUES (41,'s1',31,'КОМПАНИИ');
INSERT INTO `b_search_content_title` VALUES (41,'s1',23,'МФО');
INSERT INTO `b_search_content_title` VALUES (41,'s1',1,'О');
INSERT INTO `b_search_content_title` VALUES (41,'s1',8,'САЙТ');
INSERT INTO `b_search_content_title` VALUES (42,'s1',17,'ВАШЕЙ');
INSERT INTO `b_search_content_title` VALUES (42,'s1',0,'ГОТОВЫЙ');
INSERT INTO `b_search_content_title` VALUES (42,'s1',13,'ДЛЯ');
INSERT INTO `b_search_content_title` VALUES (42,'s1',31,'КОМПАНИИ');
INSERT INTO `b_search_content_title` VALUES (42,'s1',23,'МФО');
INSERT INTO `b_search_content_title` VALUES (42,'s1',1,'О');
INSERT INTO `b_search_content_title` VALUES (42,'s1',8,'САЙТ');
INSERT INTO `b_search_content_title` VALUES (43,'s1',0,'ЗАЙМЫ');
INSERT INTO `b_search_content_title` VALUES (43,'s1',6,'ОНЛАЙН');
INSERT INTO `b_search_content_title` VALUES (44,'s1',0,'Контакты');
INSERT INTO `b_search_content_title` VALUES (45,'s1',3,'компании');
INSERT INTO `b_search_content_title` VALUES (45,'s1',0,'О');
INSERT INTO `b_search_content_title` VALUES (46,'s1',24,'заемщикам');
INSERT INTO `b_search_content_title` VALUES (46,'s1',21,'к');
INSERT INTO `b_search_content_title` VALUES (46,'s1',0,'Требования');
INSERT INTO `b_search_content_title` VALUES (47,'s1',0,'Агентам');
INSERT INTO `b_search_content_title` VALUES (48,'s1',0,'Франшиза');
INSERT INTO `b_search_content_title` VALUES (49,'s1',21,'заявки');
INSERT INTO `b_search_content_title` VALUES (49,'s1',0,'Оформление');
INSERT INTO `b_search_content_title` VALUES (54,'s1',20,'главной');
INSERT INTO `b_search_content_title` VALUES (54,'s1',15,'на');
INSERT INTO `b_search_content_title` VALUES (54,'s1',0,'Слайдер');
INSERT INTO `b_search_content_title` VALUES (55,'s1',17,'заявку');
INSERT INTO `b_search_content_title` VALUES (55,'s1',0,'Оформите');
INSERT INTO `b_search_content_title` VALUES (56,'s1',17,'ответ');
INSERT INTO `b_search_content_title` VALUES (56,'s1',0,'Получите');
INSERT INTO `b_search_content_title` VALUES (57,'s1',36,'деньги');
INSERT INTO `b_search_content_title` VALUES (57,'s1',0,'Мгновенно');
INSERT INTO `b_search_content_title` VALUES (57,'s1',19,'получите');
INSERT INTO `b_search_content_title` VALUES (59,'s1',0,'VISA');
INSERT INTO `b_search_content_title` VALUES (60,'s1',0,'MASTERCARD');
INSERT INTO `b_search_content_title` VALUES (61,'s1',0,'QIWI');
INSERT INTO `b_search_content_title` VALUES (62,'s1',0,'Мир');
INSERT INTO `b_search_content_title` VALUES (63,'s1',0,'NA');
INSERT INTO `b_search_content_title` VALUES (64,'s1',0,'CONTACT');
INSERT INTO `b_search_content_title` VALUES (65,'s1',0,'Казань');
INSERT INTO `b_search_content_title` VALUES (70,'s1',0,'ЭКСПРЕСС');
INSERT INTO `b_search_content_title` VALUES (71,'s1',0,'До');
INSERT INTO `b_search_content_title` VALUES (71,'s1',5,'зарплаты');
INSERT INTO `b_search_content_title` VALUES (72,'s1',0,'Клубный');
INSERT INTO `b_search_content_title` VALUES (73,'s1',0,'Офицерский');
INSERT INTO `b_search_content_title` VALUES (74,'s1',0,'Пенсионный');
INSERT INTO `b_search_content_title` VALUES (75,'s1',0,'Деньги');
INSERT INTO `b_search_content_title` VALUES (75,'s1',18,'дом');
INSERT INTO `b_search_content_title` VALUES (75,'s1',13,'на');
INSERT INTO `b_search_content_title` VALUES (76,'s1',13,'1');
INSERT INTO `b_search_content_title` VALUES (76,'s1',0,'ПРЕИМУЩЕСТВО');
INSERT INTO `b_search_content_title` VALUES (77,'s1',13,'1');
INSERT INTO `b_search_content_title` VALUES (77,'s1',0,'ПРЕИМУЩЕСТВО');
INSERT INTO `b_search_content_title` VALUES (78,'s1',13,'1');
INSERT INTO `b_search_content_title` VALUES (78,'s1',0,'ПРЕИМУЩЕСТВО');
INSERT INTO `b_search_content_title` VALUES (79,'s1',0,'ЗАЙМЫ');
INSERT INTO `b_search_content_title` VALUES (79,'s1',6,'ОНЛАЙН');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_custom_rank`
-- 




DROP TABLE IF EXISTS `b_search_custom_rank`;
CREATE TABLE `b_search_custom_rank` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `APPLIED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `RANK` int(11) NOT NULL DEFAULT '0',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PARAM1` text COLLATE utf8_unicode_ci,
  `PARAM2` text COLLATE utf8_unicode_ci,
  `ITEM_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IND_B_SEARCH_CUSTOM_RANK` (`SITE_ID`,`MODULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_phrase`
-- 




DROP TABLE IF EXISTS `b_search_phrase`;
CREATE TABLE `b_search_phrase` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `RESULT_COUNT` int(11) NOT NULL,
  `PAGES` int(11) NOT NULL,
  `SESSION_ID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `PHRASE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAGS` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL_TO` text COLLATE utf8_unicode_ci,
  `URL_TO_404` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL_TO_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STAT_SESS_ID` int(18) DEFAULT NULL,
  `EVENT1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IND_PK_B_SEARCH_PHRASE_SESS_PH` (`SESSION_ID`,`PHRASE`(50)),
  KEY `IND_PK_B_SEARCH_PHRASE_SESS_TG` (`SESSION_ID`,`TAGS`(50)),
  KEY `IND_PK_B_SEARCH_PHRASE_TIME` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_stem`
-- 




DROP TABLE IF EXISTS `b_search_stem`;
CREATE TABLE `b_search_stem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `STEM` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_B_SEARCH_STEM` (`STEM`)
) ENGINE=InnoDB AUTO_INCREMENT=1952 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_search_stem`
-- 


INSERT INTO `b_search_stem` VALUES (1822,'-1');
INSERT INTO `b_search_stem` VALUES (443,'00');
INSERT INTO `b_search_stem` VALUES (1716,'00-21');
INSERT INTO `b_search_stem` VALUES (169,'000');
INSERT INTO `b_search_stem` VALUES (1649,'0000000000');
INSERT INTO `b_search_stem` VALUES (301,'02');
INSERT INTO `b_search_stem` VALUES (213,'044525716');
INSERT INTO `b_search_stem` VALUES (1666,'049401601');
INSERT INTO `b_search_stem` VALUES (534,'06');
INSERT INTO `b_search_stem` VALUES (1050,'07');
INSERT INTO `b_search_stem` VALUES (988,'08');
INSERT INTO `b_search_stem` VALUES (1002,'09');
INSERT INTO `b_search_stem` VALUES (860,'1-А');
INSERT INTO `b_search_stem` VALUES (1817,'1-Я');
INSERT INTO `b_search_stem` VALUES (267,'10');
INSERT INTO `b_search_stem` VALUES (509,'1000');
INSERT INTO `b_search_stem` VALUES (1927,'10000');
INSERT INTO `b_search_stem` VALUES (863,'1047796788819');
INSERT INTO `b_search_stem` VALUES (870,'1047820008895');
INSERT INTO `b_search_stem` VALUES (876,'1057748903618');
INSERT INTO `b_search_stem` VALUES (652,'11');
INSERT INTO `b_search_stem` VALUES (198,'111');
INSERT INTO `b_search_stem` VALUES (1662,'1131831001576');
INSERT INTO `b_search_stem` VALUES (204,'1135012000650');
INSERT INTO `b_search_stem` VALUES (197,'12');
INSERT INTO `b_search_stem` VALUES (873,'125009');
INSERT INTO `b_search_stem` VALUES (859,'127006');
INSERT INTO `b_search_stem` VALUES (867,'129090');
INSERT INTO `b_search_stem` VALUES (668,'13');
INSERT INTO `b_search_stem` VALUES (713,'14');
INSERT INTO `b_search_stem` VALUES (336,'143989');
INSERT INTO `b_search_stem` VALUES (168,'15');
INSERT INTO `b_search_stem` VALUES (304,'151-ФЗ');
INSERT INTO `b_search_stem` VALUES (1611,'152-ФЗ');
INSERT INTO `b_search_stem` VALUES (251,'16');
INSERT INTO `b_search_stem` VALUES (630,'160');
INSERT INTO `b_search_stem` VALUES (616,'18');
INSERT INTO `b_search_stem` VALUES (1661,'183101001');
INSERT INTO `b_search_stem` VALUES (1660,'1831159105');
INSERT INTO `b_search_stem` VALUES (132,'19');
INSERT INTO `b_search_stem` VALUES (1676,'1Й');
INSERT INTO `b_search_stem` VALUES (504,'20');
INSERT INTO `b_search_stem` VALUES (85,'20-00');
INSERT INTO `b_search_stem` VALUES (725,'2000');
INSERT INTO `b_search_stem` VALUES (1613,'2006');
INSERT INTO `b_search_stem` VALUES (1003,'2008');
INSERT INTO `b_search_stem` VALUES (303,'2010');
INSERT INTO `b_search_stem` VALUES (1051,'2010г');
INSERT INTO `b_search_stem` VALUES (505,'21');
INSERT INTO `b_search_stem` VALUES (6,'22');
INSERT INTO `b_search_stem` VALUES (1707,'23');
INSERT INTO `b_search_stem` VALUES (206,'23436438');
INSERT INTO `b_search_stem` VALUES (211,'24');
INSERT INTO `b_search_stem` VALUES (987,'25');
INSERT INTO `b_search_stem` VALUES (1612,'27');
INSERT INTO `b_search_stem` VALUES (339,'28');
INSERT INTO `b_search_stem` VALUES (1677,'2Й');
INSERT INTO `b_search_stem` VALUES (495,'30');
INSERT INTO `b_search_stem` VALUES (513,'3000');
INSERT INTO `b_search_stem` VALUES (214,'30101810100000000716');
INSERT INTO `b_search_stem` VALUES (1667,'30101810400000000601');
INSERT INTO `b_search_stem` VALUES (1500,'319');
INSERT INTO `b_search_stem` VALUES (693,'365');
INSERT INTO `b_search_stem` VALUES (694,'366');
INSERT INTO `b_search_stem` VALUES (1678,'3Й');
INSERT INTO `b_search_stem` VALUES (445,'40');
INSERT INTO `b_search_stem` VALUES (444,'40000');
INSERT INTO `b_search_stem` VALUES (803,'401');
INSERT INTO `b_search_stem` VALUES (209,'40702810400000058665');
INSERT INTO `b_search_stem` VALUES (1663,'40702810468000001660');
INSERT INTO `b_search_stem` VALUES (1659,'41');
INSERT INTO `b_search_stem` VALUES (765,'410');
INSERT INTO `b_search_stem` VALUES (1655,'426033');
INSERT INTO `b_search_stem` VALUES (1708,'43');
INSERT INTO `b_search_stem` VALUES (1824,'45');
INSERT INTO `b_search_stem` VALUES (623,'450');
INSERT INTO `b_search_stem` VALUES (208,'46424000000');
INSERT INTO `b_search_stem` VALUES (1836,'499');
INSERT INTO `b_search_stem` VALUES (229,'50');
INSERT INTO `b_search_stem` VALUES (1814,'500');
INSERT INTO `b_search_stem` VALUES (202,'501201001');
INSERT INTO `b_search_stem` VALUES (200,'5012077303');
INSERT INTO `b_search_stem` VALUES (989,'558');
INSERT INTO `b_search_stem` VALUES (1719,'56');
INSERT INTO `b_search_stem` VALUES (1838,'5877');
INSERT INTO `b_search_stem` VALUES (541,'60');
INSERT INTO `b_search_stem` VALUES (1837,'600');
INSERT INTO `b_search_stem` VALUES (133,'65');
INSERT INTO `b_search_stem` VALUES (1004,'687');
INSERT INTO `b_search_stem` VALUES (1679,'70');
INSERT INTO `b_search_stem` VALUES (1840,'707');
INSERT INTO `b_search_stem` VALUES (864,'7710561081');
INSERT INTO `b_search_stem` VALUES (877,'7710606134');
INSERT INTO `b_search_stem` VALUES (871,'7813199667');
INSERT INTO `b_search_stem` VALUES (84,'8-00');
INSERT INTO `b_search_stem` VALUES (542,'80');
INSERT INTO `b_search_stem` VALUES (1839,'800');
INSERT INTO `b_search_stem` VALUES (686,'807');
INSERT INTO `b_search_stem` VALUES (1664,'8618');
INSERT INTO `b_search_stem` VALUES (455,'95');
INSERT INTO `b_search_stem` VALUES (1833,'ARENDA');
INSERT INTO `b_search_stem` VALUES (360,'BAGIRA');
INSERT INTO `b_search_stem` VALUES (1871,'BR');
INSERT INTO `b_search_stem` VALUES (1884,'CONTACT');
INSERT INTO `b_search_stem` VALUES (1847,'HERE');
INSERT INTO `b_search_stem` VALUES (1924,'LOREUM');
INSERT INTO `b_search_stem` VALUES (1881,'MASTERCARD');
INSERT INTO `b_search_stem` VALUES (361,'MFO');
INSERT INTO `b_search_stem` VALUES (1883,'NA');
INSERT INTO `b_search_stem` VALUES (110,'NEW');
INSERT INTO `b_search_stem` VALUES (1084,'NEZAVISIMOST18');
INSERT INTO `b_search_stem` VALUES (1834,'PLUSKREDIT');
INSERT INTO `b_search_stem` VALUES (216,'QIWI');
INSERT INTO `b_search_stem` VALUES (7,'QQQ');
INSERT INTO `b_search_stem` VALUES (3,'QWE');
INSERT INTO `b_search_stem` VALUES (109,'QWEQW');
INSERT INTO `b_search_stem` VALUES (90,'QWEQWEQW');
INSERT INTO `b_search_stem` VALUES (108,'RRR');
INSERT INTO `b_search_stem` VALUES (1872,'STRONG');
INSERT INTO `b_search_stem` VALUES (1846,'TEXT');
INSERT INTO `b_search_stem` VALUES (543,'TUR');
INSERT INTO `b_search_stem` VALUES (1880,'VISA');
INSERT INTO `b_search_stem` VALUES (87,'WERWER');
INSERT INTO `b_search_stem` VALUES (111,'WWWW');
INSERT INTO `b_search_stem` VALUES (1006,'АВТОМАТИЗАЦ');
INSERT INTO `b_search_stem` VALUES (879,'АВТОМАТИЗИРОВА');
INSERT INTO `b_search_stem` VALUES (1779,'АВТОМОБИЛ');
INSERT INTO `b_search_stem` VALUES (1806,'АГЕНСТВ');
INSERT INTO `b_search_stem` VALUES (519,'АГЕНТ');
INSERT INTO `b_search_stem` VALUES (523,'АГЕНТСК');
INSERT INTO `b_search_stem` VALUES (334,'АДРЕС');
INSERT INTO `b_search_stem` VALUES (349,'АДРЕСОВА');
INSERT INTO `b_search_stem` VALUES (26,'АККУРАТН');
INSERT INTO `b_search_stem` VALUES (316,'АКТ');
INSERT INTO `b_search_stem` VALUES (1711,'АКТИВН');
INSERT INTO `b_search_stem` VALUES (852,'АКТОВ');
INSERT INTO `b_search_stem` VALUES (362,'АКЦЕПТ');
INSERT INTO `b_search_stem` VALUES (629,'АКЦЕПТОВА');
INSERT INTO `b_search_stem` VALUES (943,'АКЦИОНЕР');
INSERT INTO `b_search_stem` VALUES (1686,'АЛЕКС');
INSERT INTO `b_search_stem` VALUES (611,'АНАЛИЗ');
INSERT INTO `b_search_stem` VALUES (637,'АНАЛОГ');
INSERT INTO `b_search_stem` VALUES (143,'АНКЕТ');
INSERT INTO `b_search_stem` VALUES (738,'АННУЛИР');
INSERT INTO `b_search_stem` VALUES (847,'АРБИТРАЖН');
INSERT INTO `b_search_stem` VALUES (1811,'АРЕНД');
INSERT INTO `b_search_stem` VALUES (994,'АРХИВН');
INSERT INTO `b_search_stem` VALUES (640,'АУТЕНТИФИКАЦ');
INSERT INTO `b_search_stem` VALUES (386,'АУТЕНТИФИКАЦИОН');
INSERT INTO `b_search_stem` VALUES (88,'АФФИЛИРОВА');
INSERT INTO `b_search_stem` VALUES (1851,'Агентам');
INSERT INTO `b_search_stem` VALUES (1243,'Анкета');
INSERT INTO `b_search_stem` VALUES (422,'БАБУШК');
INSERT INTO `b_search_stem` VALUES (116,'БАГИР');
INSERT INTO `b_search_stem` VALUES (1019,'БАЗ');
INSERT INTO `b_search_stem` VALUES (185,'БАНК');
INSERT INTO `b_search_stem` VALUES (395,'БАНКОВСК');
INSERT INTO `b_search_stem` VALUES (186,'БЕЗНАЛИЧН');
INSERT INTO `b_search_stem` VALUES (922,'БЕЗОПАСН');
INSERT INTO `b_search_stem` VALUES (72,'БЕЛ');
INSERT INTO `b_search_stem` VALUES (410,'БЕНЕФИЦИАР');
INSERT INTO `b_search_stem` VALUES (212,'БИК');
INSERT INTO `b_search_stem` VALUES (479,'БИЛЕТ');
INSERT INTO `b_search_stem` VALUES (414,'БЛИЗК');
INSERT INTO `b_search_stem` VALUES (897,'БЛОКИРОВАН');
INSERT INTO `b_search_stem` VALUES (483,'БОЛ');
INSERT INTO `b_search_stem` VALUES (751,'БОЛЬШ');
INSERT INTO `b_search_stem` VALUES (228,'БРАТ');
INSERT INTO `b_search_stem` VALUES (526,'БРОКЕРСК');
INSERT INTO `b_search_stem` VALUES (734,'БУДЕТ');
INSERT INTO `b_search_stem` VALUES (327,'БУДУТ');
INSERT INTO `b_search_stem` VALUES (636,'БУДУЩ');
INSERT INTO `b_search_stem` VALUES (503,'БЮДЖЕТН');
INSERT INTO `b_search_stem` VALUES (590,'БЮР');
INSERT INTO `b_search_stem` VALUES (1351,'Бюро');
INSERT INTO `b_search_stem` VALUES (1345,'В');
INSERT INTO `b_search_stem` VALUES (1687,'ВАДИМОВИЧ');
INSERT INTO `b_search_stem` VALUES (100,'ВАКАНС');
INSERT INTO `b_search_stem` VALUES (138,'ВАС');
INSERT INTO `b_search_stem` VALUES (385,'ВВОД');
INSERT INTO `b_search_stem` VALUES (359,'ВЕБ-СА');
INSERT INTO `b_search_stem` VALUES (45,'ВЕДЕН');
INSERT INTO `b_search_stem` VALUES (29,'ВЕЖЛИВ');
INSERT INTO `b_search_stem` VALUES (554,'ВЕРНУТ');
INSERT INTO `b_search_stem` VALUES (462,'ВЕЧЕРН');
INSERT INTO `b_search_stem` VALUES (715,'ВЗИМА');
INSERT INTO `b_search_stem` VALUES (546,'ВЗНОС');
INSERT INTO `b_search_stem` VALUES (979,'ВЗНОСОВ');
INSERT INTO `b_search_stem` VALUES (821,'ВЗЫСКАН');
INSERT INTO `b_search_stem` VALUES (128,'ВЗЯТ');
INSERT INTO `b_search_stem` VALUES (448,'ВИД');
INSERT INTO `b_search_stem` VALUES (236,'ВИЗ');
INSERT INTO `b_search_stem` VALUES (798,'ВИН');
INSERT INTO `b_search_stem` VALUES (344,'ВКЛЮЧ');
INSERT INTO `b_search_stem` VALUES (727,'ВКЛЮЧА');
INSERT INTO `b_search_stem` VALUES (570,'ВКЛЮЧЕН');
INSERT INTO `b_search_stem` VALUES (465,'ВКЛЮЧИТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (665,'ВЛЕЧЕТ');
INSERT INTO `b_search_stem` VALUES (690,'ВМЕСТ');
INSERT INTO `b_search_stem` VALUES (261,'ВНЕ');
INSERT INTO `b_search_stem` VALUES (811,'ВНЕДОГОВОРН');
INSERT INTO `b_search_stem` VALUES (842,'ВНЕСЕН');
INSERT INTO `b_search_stem` VALUES (265,'ВНИМАН');
INSERT INTO `b_search_stem` VALUES (28,'ВНИМАТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (791,'ВНОС');
INSERT INTO `b_search_stem` VALUES (423,'ВНУК');
INSERT INTO `b_search_stem` VALUES (620,'ВНУТРЕН');
INSERT INTO `b_search_stem` VALUES (244,'ВОВРЕМ');
INSERT INTO `b_search_stem` VALUES (834,'ВОДИТЕЛЬСК');
INSERT INTO `b_search_stem` VALUES (249,'ВОЗВРАТ');
INSERT INTO `b_search_stem` VALUES (778,'ВОЗВРАЩА');
INSERT INTO `b_search_stem` VALUES (711,'ВОЗЛАГА');
INSERT INTO `b_search_stem` VALUES (949,'ВОЗЛОЖЕН');
INSERT INTO `b_search_stem` VALUES (618,'ВОЗМОЖ');
INSERT INTO `b_search_stem` VALUES (78,'ВОЗМОЖН');
INSERT INTO `b_search_stem` VALUES (293,'ВОЗНИКА');
INSERT INTO `b_search_stem` VALUES (762,'ВОЗНИКНОВЕН');
INSERT INTO `b_search_stem` VALUES (810,'ВОЗНИКНУТ');
INSERT INTO `b_search_stem` VALUES (464,'ВОЗРАСТ');
INSERT INTO `b_search_stem` VALUES (130,'ВОЗРАСТН');
INSERT INTO `b_search_stem` VALUES (662,'ВОЛЕИЗЪЯВЛЕН');
INSERT INTO `b_search_stem` VALUES (56,'ВОПРОС');
INSERT INTO `b_search_stem` VALUES (245,'ВОСПОЛЬЗОВА');
INSERT INTO `b_search_stem` VALUES (1731,'ВОСТРЕБОВА');
INSERT INTO `b_search_stem` VALUES (786,'ВОСТРЕБОВАН');
INSERT INTO `b_search_stem` VALUES (416,'ВОСХОДЯ');
INSERT INTO `b_search_stem` VALUES (572,'ВПРАВ');
INSERT INTO `b_search_stem` VALUES (374,'ВРЕМ');
INSERT INTO `b_search_stem` VALUES (549,'ВСЕМ');
INSERT INTO `b_search_stem` VALUES (633,'ВСЕХ');
INSERT INTO `b_search_stem` VALUES (801,'ВСЛЕДСТВ');
INSERT INTO `b_search_stem` VALUES (853,'ВСТУП');
INSERT INTO `b_search_stem` VALUES (210,'ВТБ');
INSERT INTO `b_search_stem` VALUES (383,'ВТОР');
INSERT INTO `b_search_stem` VALUES (1820,'ВХОД');
INSERT INTO `b_search_stem` VALUES (1747,'ВЫБИРА');
INSERT INTO `b_search_stem` VALUES (771,'ВЫБОР');
INSERT INTO `b_search_stem` VALUES (447,'ВЫБРА');
INSERT INTO `b_search_stem` VALUES (409,'ВЫГОДОПРИОБРЕТАТЕЛ');
INSERT INTO `b_search_stem` VALUES (787,'ВЫДА');
INSERT INTO `b_search_stem` VALUES (38,'ВЫДАЧ');
INSERT INTO `b_search_stem` VALUES (684,'ВЫПЛАТ');
INSERT INTO `b_search_stem` VALUES (770,'ВЫПЛАЧ');
INSERT INTO `b_search_stem` VALUES (661,'ВЫПОЛН');
INSERT INTO `b_search_stem` VALUES (365,'ВЫПОЛНЕН');
INSERT INTO `b_search_stem` VALUES (967,'ВЫПОЛНЯ');
INSERT INTO `b_search_stem` VALUES (631,'ВЫРАЖА');
INSERT INTO `b_search_stem` VALUES (1772,'ВЫСОКОКВАЛИФИЦИРОВА');
INSERT INTO `b_search_stem` VALUES (823,'ВЫСТУП');
INSERT INTO `b_search_stem` VALUES (457,'ВЫСШ');
INSERT INTO `b_search_stem` VALUES (150,'ВЫХОД');
INSERT INTO `b_search_stem` VALUES (773,'ВЫЧЕТ');
INSERT INTO `b_search_stem` VALUES (435,'ВЫШ');
INSERT INTO `b_search_stem` VALUES (497,'ВЫШЕДШ');
INSERT INTO `b_search_stem` VALUES (1861,'Вас');
INSERT INTO `b_search_stem` VALUES (1915,'Вдали');
INSERT INTO `b_search_stem` VALUES (1462,'Во');
INSERT INTO `b_search_stem` VALUES (1229,'Все');
INSERT INTO `b_search_stem` VALUES (1128,'Выгодоприобретатель');
INSERT INTO `b_search_stem` VALUES (1314,'Выгодоприобретателя');
INSERT INTO `b_search_stem` VALUES (1433,'Выдача');
INSERT INTO `b_search_stem` VALUES (312,'ГК');
INSERT INTO `b_search_stem` VALUES (323,'ГЛАВ');
INSERT INTO `b_search_stem` VALUES (1763,'ГЛАВН');
INSERT INTO `b_search_stem` VALUES (619,'ГОД');
INSERT INTO `b_search_stem` VALUES (1651,'ГОЛОВН');
INSERT INTO `b_search_stem` VALUES (371,'ГОЛОСОВ');
INSERT INTO `b_search_stem` VALUES (126,'ГОРОД');
INSERT INTO `b_search_stem` VALUES (471,'ГОСУДАРСТВ');
INSERT INTO `b_search_stem` VALUES (332,'ГОСУДАРСТВЕН');
INSERT INTO `b_search_stem` VALUES (97,'ГОТОВ');
INSERT INTO `b_search_stem` VALUES (1269,'ГР');
INSERT INTO `b_search_stem` VALUES (496,'ГРАЖДАН');
INSERT INTO `b_search_stem` VALUES (930,'ГРАЖДАНИН');
INSERT INTO `b_search_stem` VALUES (308,'ГРАЖДАНСК');
INSERT INTO `b_search_stem` VALUES (518,'ГРАЖДАНСКО-ПРАВ');
INSERT INTO `b_search_stem` VALUES (517,'ГРАЖДАНСКО-ПРАВОВ');
INSERT INTO `b_search_stem` VALUES (391,'ГРАЖДАНСТВ');
INSERT INTO `b_search_stem` VALUES (96,'ГРАМОТН');
INSERT INTO `b_search_stem` VALUES (83,'ГРАФИК');
INSERT INTO `b_search_stem` VALUES (1899,'Галерея');
INSERT INTO `b_search_stem` VALUES (1643,'Головной');
INSERT INTO `b_search_stem` VALUES (1501,'Гражданского');
INSERT INTO `b_search_stem` VALUES (825,'ДАЕТ');
INSERT INTO `b_search_stem` VALUES (288,'ДАЛ');
INSERT INTO `b_search_stem` VALUES (369,'ДАН');
INSERT INTO `b_search_stem` VALUES (257,'ДАТ');
INSERT INTO `b_search_stem` VALUES (269,'ДВА');
INSERT INTO `b_search_stem` VALUES (573,'ДВАДЦА');
INSERT INTO `b_search_stem` VALUES (1701,'ДВИЖЕН');
INSERT INTO `b_search_stem` VALUES (731,'ДВУХМЕСЯЧН');
INSERT INTO `b_search_stem` VALUES (874,'ДЕГТЯРН');
INSERT INTO `b_search_stem` VALUES (421,'ДЕДУШК');
INSERT INTO `b_search_stem` VALUES (399,'ДЕЕСПОСОБН');
INSERT INTO `b_search_stem` VALUES (364,'ДЕЙСТВ');
INSERT INTO `b_search_stem` VALUES (983,'ДЕЛОПРОИЗВОДСТВ');
INSERT INTO `b_search_stem` VALUES (256,'ДЕН');
INSERT INTO `b_search_stem` VALUES (250,'ДЕНЕГ');
INSERT INTO `b_search_stem` VALUES (23,'ДЕНЕЖН');
INSERT INTO `b_search_stem` VALUES (147,'ДЕНЬГ');
INSERT INTO `b_search_stem` VALUES (745,'ДЕСЯ');
INSERT INTO `b_search_stem` VALUES (740,'ДЕСЯТ');
INSERT INTO `b_search_stem` VALUES (306,'ДЕЯТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (64,'ДИНАМИЧН');
INSERT INTO `b_search_stem` VALUES (1700,'ДИСЦИПЛИН');
INSERT INTO `b_search_stem` VALUES (1895,'ДК');
INSERT INTO `b_search_stem` VALUES (1691,'ДМИТРИЕВН');
INSERT INTO `b_search_stem` VALUES (252,'ДНЕ');
INSERT INTO `b_search_stem` VALUES (480,'ДНЕВН');
INSERT INTO `b_search_stem` VALUES (561,'ДНЕМ');
INSERT INTO `b_search_stem` VALUES (259,'ДНЯ');
INSERT INTO `b_search_stem` VALUES (615,'ДНЯМ');
INSERT INTO `b_search_stem` VALUES (1738,'ДОБИВА');
INSERT INTO `b_search_stem` VALUES (612,'ДОВОД');
INSERT INTO `b_search_stem` VALUES (183,'ДОГОВОР');
INSERT INTO `b_search_stem` VALUES (945,'ДОГОВОРН');
INSERT INTO `b_search_stem` VALUES (764,'ДОГОВОРОВ');
INSERT INTO `b_search_stem` VALUES (50,'ДОКУМЕНТ');
INSERT INTO `b_search_stem` VALUES (46,'ДОКУМЕНТАЦ');
INSERT INTO `b_search_stem` VALUES (49,'ДОКУМЕНТОВ');
INSERT INTO `b_search_stem` VALUES (1696,'ДОКУМЕНТООБОРОТ');
INSERT INTO `b_search_stem` VALUES (1801,'ДОЛ');
INSERT INTO `b_search_stem` VALUES (346,'ДОЛГ');
INSERT INTO `b_search_stem` VALUES (276,'ДОЛЖ');
INSERT INTO `b_search_stem` VALUES (490,'ДОЛЖН');
INSERT INTO `b_search_stem` VALUES (223,'ДОЛЖНИК');
INSERT INTO `b_search_stem` VALUES (31,'ДОЛЖНОСТН');
INSERT INTO `b_search_stem` VALUES (1023,'ДОЛЬШ');
INSERT INTO `b_search_stem` VALUES (151,'ДОМ');
INSERT INTO `b_search_stem` VALUES (843,'ДОПОЛНЕН');
INSERT INTO `b_search_stem` VALUES (467,'ДОПОЛНИТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (580,'ДОПУСКА');
INSERT INTO `b_search_stem` VALUES (741,'ДОСРОЧН');
INSERT INTO `b_search_stem` VALUES (1015,'ДОСТАТОЧН');
INSERT INTO `b_search_stem` VALUES (1769,'ДОСТИГНУТ');
INSERT INTO `b_search_stem` VALUES (178,'ДОСТИГШ');
INSERT INTO `b_search_stem` VALUES (1011,'ДОСТИЖЕН');
INSERT INTO `b_search_stem` VALUES (595,'ДОСТОВЕРН');
INSERT INTO `b_search_stem` VALUES (397,'ДОСТУП');
INSERT INTO `b_search_stem` VALUES (1799,'ДОСТУПН');
INSERT INTO `b_search_stem` VALUES (124,'ДОХОД');
INSERT INTO `b_search_stem` VALUES (955,'ДОХОДОВ');
INSERT INTO `b_search_stem` VALUES (957,'ДР');
INSERT INTO `b_search_stem` VALUES (176,'ДРУГ');
INSERT INTO `b_search_stem` VALUES (1724,'ДРУЖН');
INSERT INTO `b_search_stem` VALUES (1906,'Далеко-далеко');
INSERT INTO `b_search_stem` VALUES (1946,'Деньги');
INSERT INTO `b_search_stem` VALUES (1199,'Для');
INSERT INTO `b_search_stem` VALUES (1925,'До');
INSERT INTO `b_search_stem` VALUES (1106,'Договор');
INSERT INTO `b_search_stem` VALUES (1626,'Договора');
INSERT INTO `b_search_stem` VALUES (973,'ЕДИН');
INSERT INTO `b_search_stem` VALUES (485,'ЕДИНОВРЕМЕН');
INSERT INTO `b_search_stem` VALUES (230,'ЕЖЕМЕСЯЧН');
INSERT INTO `b_search_stem` VALUES (180,'ЕМ');
INSERT INTO `b_search_stem` VALUES (1891,'Евротел');
INSERT INTO `b_search_stem` VALUES (1555,'Ежеквартально');
INSERT INTO `b_search_stem` VALUES (1224,'Если');
INSERT INTO `b_search_stem` VALUES (1712,'ЖЕЛАН');
INSERT INTO `b_search_stem` VALUES (195,'ЖЕЛЕЗНОДОРОЖН');
INSERT INTO `b_search_stem` VALUES (1654,'ЖЕЛЬМЕ-БЕЛЬМ');
INSERT INTO `b_search_stem` VALUES (932,'ЖИЗН');
INSERT INTO `b_search_stem` VALUES (1816,'ЖИЛ');
INSERT INTO `b_search_stem` VALUES (393,'ЖИТЕЛЬСТВ');
INSERT INTO `b_search_stem` VALUES (792,'ЗАБЛАГОВРЕМЕН');
INSERT INTO `b_search_stem` VALUES (459,'ЗАВЕДЕН');
INSERT INTO `b_search_stem` VALUES (674,'ЗАВЕРЕН');
INSERT INTO `b_search_stem` VALUES (451,'ЗАВИС');
INSERT INTO `b_search_stem` VALUES (262,'ЗАВИСИМ');
INSERT INTO `b_search_stem` VALUES (1744,'ЗАВТРАШН');
INSERT INTO `b_search_stem` VALUES (924,'ЗАДАЧ');
INSERT INTO `b_search_stem` VALUES (789,'ЗАДЕРЖК');
INSERT INTO `b_search_stem` VALUES (341,'ЗАДОЛЖЕН');
INSERT INTO `b_search_stem` VALUES (225,'ЗАЕМЩИК');
INSERT INTO `b_search_stem` VALUES (500,'ЗАЕМЩИКОВ');
INSERT INTO `b_search_stem` VALUES (60,'ЗАЙМ');
INSERT INTO `b_search_stem` VALUES (59,'ЗАЙМОВ');
INSERT INTO `b_search_stem` VALUES (351,'ЗАКЛЮЧ');
INSERT INTO `b_search_stem` VALUES (258,'ЗАКЛЮЧЕН');
INSERT INTO `b_search_stem` VALUES (913,'ЗАКЛЮЧИТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (300,'ЗАКОН');
INSERT INTO `b_search_stem` VALUES (940,'ЗАКОНОВ');
INSERT INTO `b_search_stem` VALUES (314,'ЗАКОНОДАТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (808,'ЗАКОНОДАТЕЛЬСТВ');
INSERT INTO `b_search_stem` VALUES (784,'ЗАКРЫТ');
INSERT INTO `b_search_stem` VALUES (1774,'ЗАЛОГ');
INSERT INTO `b_search_stem` VALUES (370,'ЗАП');
INSERT INTO `b_search_stem` VALUES (1753,'ЗАПАДН');
INSERT INTO `b_search_stem` VALUES (331,'ЗАПИС');
INSERT INTO `b_search_stem` VALUES (142,'ЗАПОЛН');
INSERT INTO `b_search_stem` VALUES (591,'ЗАПОЛНЕН');
INSERT INTO `b_search_stem` VALUES (671,'ЗАПРОШЕН');
INSERT INTO `b_search_stem` VALUES (1713,'ЗАРАБАТЫВА');
INSERT INTO `b_search_stem` VALUES (73,'ЗАРАБОТН');
INSERT INTO `b_search_stem` VALUES (1012,'ЗАРАН');
INSERT INTO `b_search_stem` VALUES (392,'ЗАРЕГИСТРИРОВА');
INSERT INTO `b_search_stem` VALUES (512,'ЗАРПЛАТ');
INSERT INTO `b_search_stem` VALUES (902,'ЗАТРАГИВА');
INSERT INTO `b_search_stem` VALUES (766,'ЗАЧЕСТ');
INSERT INTO `b_search_stem` VALUES (703,'ЗАЧИСЛЕН');
INSERT INTO `b_search_stem` VALUES (777,'ЗАЧТ');
INSERT INTO `b_search_stem` VALUES (921,'ЗАЩ');
INSERT INTO `b_search_stem` VALUES (912,'ЗАЩИТ');
INSERT INTO `b_search_stem` VALUES (149,'ЗАЯВК');
INSERT INTO `b_search_stem` VALUES (712,'ЗАЯВЛЕН');
INSERT INTO `b_search_stem` VALUES (376,'ЗВОНК');
INSERT INTO `b_search_stem` VALUES (1831,'ЗДАН');
INSERT INTO `b_search_stem` VALUES (221,'ЗДЕ');
INSERT INTO `b_search_stem` VALUES (1709,'ЗНАН');
INSERT INTO `b_search_stem` VALUES (181,'ЗНАТ');
INSERT INTO `b_search_stem` VALUES (329,'ЗНАЧЕН');
INSERT INTO `b_search_stem` VALUES (1602,'Заемщик');
INSERT INTO `b_search_stem` VALUES (1496,'Заемщиком');
INSERT INTO `b_search_stem` VALUES (1459,'Займодавца');
INSERT INTO `b_search_stem` VALUES (1527,'Займодавцу');
INSERT INTO `b_search_stem` VALUES (1843,'Займы');
INSERT INTO `b_search_stem` VALUES (1125,'Заявка');
INSERT INTO `b_search_stem` VALUES (1507,'Знакомиться');
INSERT INTO `b_search_stem` VALUES (1803,'ИГРОК');
INSERT INTO `b_search_stem` VALUES (1802,'ИГРОКОВ');
INSERT INTO `b_search_stem` VALUES (669,'ИДЕНТИФИКАЦ');
INSERT INTO `b_search_stem` VALUES (838,'ИДЕНТИФИКАЦИОН');
INSERT INTO `b_search_stem` VALUES (1657,'ИЖЕВСК');
INSERT INTO `b_search_stem` VALUES (432,'ИЗБЕЖАН');
INSERT INTO `b_search_stem` VALUES (1017,'ИЗБЫТОЧН');
INSERT INTO `b_search_stem` VALUES (782,'ИЗВЕСТН');
INSERT INTO `b_search_stem` VALUES (779,'ИЗДЕРЖЕК');
INSERT INTO `b_search_stem` VALUES (706,'ИЗДЕРЖК');
INSERT INTO `b_search_stem` VALUES (756,'ИЗЛИШН');
INSERT INTO `b_search_stem` VALUES (742,'ИЗЛОЖЕН');
INSERT INTO `b_search_stem` VALUES (916,'ИЗМЕН');
INSERT INTO `b_search_stem` VALUES (666,'ИЗМЕНЕН');
INSERT INTO `b_search_stem` VALUES (1758,'ИМЕ');
INSERT INTO `b_search_stem` VALUES (396,'ИМЕЕТ');
INSERT INTO `b_search_stem` VALUES (658,'ИМЕЛ');
INSERT INTO `b_search_stem` VALUES (366,'ИМЕН');
INSERT INTO `b_search_stem` VALUES (122,'ИМЕТ');
INSERT INTO `b_search_stem` VALUES (535,'ИМЕЮТ');
INSERT INTO `b_search_stem` VALUES (389,'ИМЕЮЩ');
INSERT INTO `b_search_stem` VALUES (969,'ИМУЩЕСТВ');
INSERT INTO `b_search_stem` VALUES (199,'ИН');
INSERT INTO `b_search_stem` VALUES (403,'ИНДИВИДУАЛЬН');
INSERT INTO `b_search_stem` VALUES (1703,'ИНКАССАЦ');
INSERT INTO `b_search_stem` VALUES (411,'ИНОСТРА');
INSERT INTO `b_search_stem` VALUES (405,'ИНТЕРЕС');
INSERT INTO `b_search_stem` VALUES (95,'ИНТЕРЕСН');
INSERT INTO `b_search_stem` VALUES (946,'ИНТЕРЕСОВ');
INSERT INTO `b_search_stem` VALUES (585,'ИНТЕРНЕТ');
INSERT INTO `b_search_stem` VALUES (192,'ИНФОРМАЦ');
INSERT INTO `b_search_stem` VALUES (760,'ИНФОРМИР');
INSERT INTO `b_search_stem` VALUES (1284,'ИП');
INSERT INTO `b_search_stem` VALUES (656,'ИСКЛЮЧА');
INSERT INTO `b_search_stem` VALUES (644,'ИСКЛЮЧЕН');
INSERT INTO `b_search_stem` VALUES (899,'ИСКЛЮЧИТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (322,'ИСПОЛНЕН');
INSERT INTO `b_search_stem` VALUES (819,'ИСПОЛЬЗОВА');
INSERT INTO `b_search_stem` VALUES (632,'ИСПОЛЬЗОВАН');
INSERT INTO `b_search_stem` VALUES (324,'ИСПОЛЬЗУЕМ');
INSERT INTO `b_search_stem` VALUES (1721,'ИСПЫТАТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (557,'ИСТЕЧЕН');
INSERT INTO `b_search_stem` VALUES (538,'ИСТОР');
INSERT INTO `b_search_stem` VALUES (468,'ИСТОЧНИК');
INSERT INTO `b_search_stem` VALUES (971,'ИСЧИСЛЕН');
INSERT INTO `b_search_stem` VALUES (94,'ИЩ');
INSERT INTO `b_search_stem` VALUES (302,'ИЮЛ');
INSERT INTO `b_search_stem` VALUES (1149,'Идентификация');
INSERT INTO `b_search_stem` VALUES (1642,'Ижевск');
INSERT INTO `b_search_stem` VALUES (1547,'Изменения');
INSERT INTO `b_search_stem` VALUES (1303,'Иные');
INSERT INTO `b_search_stem` VALUES (91,'ЙЦУ');
INSERT INTO `b_search_stem` VALUES (92,'ЙЦУЙЦУЙЦ');
INSERT INTO `b_search_stem` VALUES (678,'КАБИНЕТ');
INSERT INTO `b_search_stem` VALUES (982,'КАДРОВ');
INSERT INTO `b_search_stem` VALUES (255,'КАЖД');
INSERT INTO `b_search_stem` VALUES (1734,'КАКИМ-ЛИБ');
INSERT INTO `b_search_stem` VALUES (868,'КАЛАНЧЕВСК');
INSERT INTO `b_search_stem` VALUES (446,'КАЛЕНДАРН');
INSERT INTO `b_search_stem` VALUES (1764,'КАПИТА');
INSERT INTO `b_search_stem` VALUES (80,'КАРЬЕРН');
INSERT INTO `b_search_stem` VALUES (844,'КАСА');
INSERT INTO `b_search_stem` VALUES (772,'КАСС');
INSERT INTO `b_search_stem` VALUES (8,'КАССИР');
INSERT INTO `b_search_stem` VALUES (19,'КАССОВ');
INSERT INTO `b_search_stem` VALUES (499,'КАТЕГОР');
INSERT INTO `b_search_stem` VALUES (406,'КАЧЕСТВ');
INSERT INTO `b_search_stem` VALUES (1825,'КВ');
INSERT INTO `b_search_stem` VALUES (379,'КВИТАНЦ');
INSERT INTO `b_search_stem` VALUES (1646,'КИТ');
INSERT INTO `b_search_stem` VALUES (53,'КЛИЕНТ');
INSERT INTO `b_search_stem` VALUES (52,'КЛИЕНТОВ');
INSERT INTO `b_search_stem` VALUES (904,'КОД');
INSERT INTO `b_search_stem` VALUES (309,'КОДЕКС');
INSERT INTO `b_search_stem` VALUES (691,'КОЛИЧЕСТВ');
INSERT INTO `b_search_stem` VALUES (1754,'КОЛЛЕГ');
INSERT INTO `b_search_stem` VALUES (1726,'КОЛЛЕКТИВ');
INSERT INTO `b_search_stem` VALUES (908,'КОЛЛЕКТОР');
INSERT INTO `b_search_stem` VALUES (907,'КОЛЛЕКТОРОВ');
INSERT INTO `b_search_stem` VALUES (1762,'КОМАНД');
INSERT INTO `b_search_stem` VALUES (710,'КОМИСС');
INSERT INTO `b_search_stem` VALUES (66,'КОМПАН');
INSERT INTO `b_search_stem` VALUES (610,'КОМПЛЕКСН');
INSERT INTO `b_search_stem` VALUES (889,'КОМПОНОВК');
INSERT INTO `b_search_stem` VALUES (699,'КОНКРЕТН');
INSERT INTO `b_search_stem` VALUES (906,'КОНСУЛЬТАНТ');
INSERT INTO `b_search_stem` VALUES (905,'КОНСУЛЬТАНТОВ');
INSERT INTO `b_search_stem` VALUES (1692,'КОНСУЛЬТАЦ');
INSERT INTO `b_search_stem` VALUES (51,'КОНСУЛЬТИРОВАН');
INSERT INTO `b_search_stem` VALUES (381,'КОНТАКТ');
INSERT INTO `b_search_stem` VALUES (942,'КОНТРАГЕНТ');
INSERT INTO `b_search_stem` VALUES (41,'КОНТРОЛ');
INSERT INTO `b_search_stem` VALUES (654,'КОНФИДЕНЦИАЛЬН');
INSERT INTO `b_search_stem` VALUES (486,'КОНЦ');
INSERT INTO `b_search_stem` VALUES (1029,'КООРДИНАЦ');
INSERT INTO `b_search_stem` VALUES (675,'КОП');
INSERT INTO `b_search_stem` VALUES (886,'КОПИРОВАН');
INSERT INTO `b_search_stem` VALUES (869,'КОРП');
INSERT INTO `b_search_stem` VALUES (340,'КОРПУС');
INSERT INTO `b_search_stem` VALUES (387,'КОТОР');
INSERT INTO `b_search_stem` VALUES (201,'КПП');
INSERT INTO `b_search_stem` VALUES (1950,'КРАТК');
INSERT INTO `b_search_stem` VALUES (58,'КРАТКОСРОЧН');
INSERT INTO `b_search_stem` VALUES (540,'КРЕД');
INSERT INTO `b_search_stem` VALUES (1736,'КРЕДИТ');
INSERT INTO `b_search_stem` VALUES (93,'КРЕДИТН');
INSERT INTO `b_search_stem` VALUES (452,'КРЕДИТОВАН');
INSERT INTO `b_search_stem` VALUES (131,'КРИТЕР');
INSERT INTO `b_search_stem` VALUES (62,'КРУПН');
INSERT INTO `b_search_stem` VALUES (1889,'Казань');
INSERT INTO `b_search_stem` VALUES (1112,'Клиент');
INSERT INTO `b_search_stem` VALUES (1178,'Клиента');
INSERT INTO `b_search_stem` VALUES (1928,'Клубный');
INSERT INTO `b_search_stem` VALUES (1848,'Контакты');
INSERT INTO `b_search_stem` VALUES (1068,'Копия');
INSERT INTO `b_search_stem` VALUES (954,'ЛЕГАЛИЗАЦ');
INSERT INTO `b_search_stem` VALUES (1749,'ЛЕГК');
INSERT INTO `b_search_stem` VALUES (106,'ЛЕНИНСК');
INSERT INTO `b_search_stem` VALUES (134,'ЛЕТ');
INSERT INTO `b_search_stem` VALUES (768,'ЛИЕНТ');
INSERT INTO `b_search_stem` VALUES (418,'ЛИН');
INSERT INTO `b_search_stem` VALUES (36,'ЛИЦ');
INSERT INTO `b_search_stem` VALUES (759,'ЛИЦЕВ');
INSERT INTO `b_search_stem` VALUES (677,'ЛИЧН');
INSERT INTO `b_search_stem` VALUES (833,'ЛИЧНОСТ');
INSERT INTO `b_search_stem` VALUES (582,'ЛИШ');
INSERT INTO `b_search_stem` VALUES (238,'ЛУЧШ');
INSERT INTO `b_search_stem` VALUES (164,'ЛЮБ');
INSERT INTO `b_search_stem` VALUES (1682,'ЛЮБЛИН');
INSERT INTO `b_search_stem` VALUES (1787,'ЛЮД');
INSERT INTO `b_search_stem` VALUES (1876,'Любым');
INSERT INTO `b_search_stem` VALUES (1790,'МАЛООБЕСПЕЧЕН');
INSERT INTO `b_search_stem` VALUES (428,'МАТ');
INSERT INTO `b_search_stem` VALUES (1706,'МАТЕРИАЛЬН');
INSERT INTO `b_search_stem` VALUES (196,'МАЯКОВСК');
INSERT INTO `b_search_stem` VALUES (872,'МБКИ');
INSERT INTO `b_search_stem` VALUES (1780,'МЕДИЦИНСК');
INSERT INTO `b_search_stem` VALUES (294,'МЕЖД');
INSERT INTO `b_search_stem` VALUES (1681,'МЕНДЕЛЕЕВСК');
INSERT INTO `b_search_stem` VALUES (574,'МЕНЬШ');
INSERT INTO `b_search_stem` VALUES (952,'МЕР');
INSERT INTO `b_search_stem` VALUES (77,'МЕСТ');
INSERT INTO `b_search_stem` VALUES (996,'МЕСТН');
INSERT INTO `b_search_stem` VALUES (696,'МЕСЯЦ');
INSERT INTO `b_search_stem` VALUES (714,'МЕСЯЦЕВ');
INSERT INTO `b_search_stem` VALUES (1813,'МЕТР');
INSERT INTO `b_search_stem` VALUES (1815,'МЕТРОВ');
INSERT INTO `b_search_stem` VALUES (1739,'МЕЧТ');
INSERT INTO `b_search_stem` VALUES (115,'МИКРОЗАЙМ');
INSERT INTO `b_search_stem` VALUES (139,'МИКРОЗАЙМОВ');
INSERT INTO `b_search_stem` VALUES (282,'МИКРОЗАМ');
INSERT INTO `b_search_stem` VALUES (544,'МИКРОКРЕДИТОВАН');
INSERT INTO `b_search_stem` VALUES (305,'МИКРОФИНАНСОВ');
INSERT INTO `b_search_stem` VALUES (947,'МИНИМИЗАЦ');
INSERT INTO `b_search_stem` VALUES (986,'МИНКУЛЬТУР');
INSERT INTO `b_search_stem` VALUES (1761,'МИР');
INSERT INTO `b_search_stem` VALUES (563,'МИСКРОЗАЙМ');
INSERT INTO `b_search_stem` VALUES (1788,'МИСС');
INSERT INTO `b_search_stem` VALUES (98,'МНОГ');
INSERT INTO `b_search_stem` VALUES (584,'МОБИЛЬН');
INSERT INTO `b_search_stem` VALUES (743,'МОГУТ');
INSERT INTO `b_search_stem` VALUES (174,'МОЖЕТ');
INSERT INTO `b_search_stem` VALUES (219,'МОЖН');
INSERT INTO `b_search_stem` VALUES (63,'МОЛОД');
INSERT INTO `b_search_stem` VALUES (487,'МОМЕНТ');
INSERT INTO `b_search_stem` VALUES (1,'МОСКВ');
INSERT INTO `b_search_stem` VALUES (194,'МОСКОВСК');
INSERT INTO `b_search_stem` VALUES (605,'МОТИВИРОВА');
INSERT INTO `b_search_stem` VALUES (1087,'МФ');
INSERT INTO `b_search_stem` VALUES (89,'МФО');
INSERT INTO `b_search_stem` VALUES (1890,'Магазин');
INSERT INTO `b_search_stem` VALUES (1873,'Мгновенно');
INSERT INTO `b_search_stem` VALUES (1295,'Медицинский');
INSERT INTO `b_search_stem` VALUES (1473,'Менеджера');
INSERT INTO `b_search_stem` VALUES (1091,'Микрозаем');
INSERT INTO `b_search_stem` VALUES (1090,'Микрозаймов');
INSERT INTO `b_search_stem` VALUES (1563,'Микрофинансовая');
INSERT INTO `b_search_stem` VALUES (1053,'Микрофинансовой');
INSERT INTO `b_search_stem` VALUES (1882,'Мир');
INSERT INTO `b_search_stem` VALUES (1888,'Москва');
INSERT INTO `b_search_stem` VALUES (1868,'Мы');
INSERT INTO `b_search_stem` VALUES (17,'НАВЫК');
INSERT INTO `b_search_stem` VALUES (533,'НАДЕЖН');
INSERT INTO `b_search_stem` VALUES (321,'НАДЛЕЖА');
INSERT INTO `b_search_stem` VALUES (831,'НАЗВАН');
INSERT INTO `b_search_stem` VALUES (1030,'НАЗНАЧ');
INSERT INTO `b_search_stem` VALUES (841,'НАИМЕНОВАН');
INSERT INTO `b_search_stem` VALUES (516,'НАЙМ');
INSERT INTO `b_search_stem` VALUES (136,'НАЙТ');
INSERT INTO `b_search_stem` VALUES (882,'НАКОПЛЕН');
INSERT INTO `b_search_stem` VALUES (797,'НАЛИЧ');
INSERT INTO `b_search_stem` VALUES (24,'НАЛИЧН');
INSERT INTO `b_search_stem` VALUES (972,'НАЛОГ');
INSERT INTO `b_search_stem` VALUES (970,'НАЛОГОВ');
INSERT INTO `b_search_stem` VALUES (839,'НАЛОГОПЛАТЕЛЬЩИК');
INSERT INTO `b_search_stem` VALUES (1748,'НАМ');
INSERT INTO `b_search_stem` VALUES (390,'НАМЕРЕН');
INSERT INTO `b_search_stem` VALUES (625,'НАПРАВ');
INSERT INTO `b_search_stem` VALUES (354,'НАПРАВЛЕН');
INSERT INTO `b_search_stem` VALUES (627,'НАПРАВЛЯ');
INSERT INTO `b_search_stem` VALUES (736,'НАРУШЕН');
INSERT INTO `b_search_stem` VALUES (1783,'НАС');
INSERT INTO `b_search_stem` VALUES (1793,'НАСЕЛЕН');
INSERT INTO `b_search_stem` VALUES (287,'НАСТОЯ');
INSERT INTO `b_search_stem` VALUES (226,'НАСТОЯТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (335,'НАХОЖДЕН');
INSERT INTO `b_search_stem` VALUES (253,'НАЧИСЛЕН');
INSERT INTO `b_search_stem` VALUES (482,'НАЧИСЛЯ');
INSERT INTO `b_search_stem` VALUES (112,'НАШ');
INSERT INTO `b_search_stem` VALUES (481,'НДФЛ');
INSERT INTO `b_search_stem` VALUES (437,'НЕВОЗВРАЩЕН');
INSERT INTO `b_search_stem` VALUES (271,'НЕВОЗМОЖН');
INSERT INTO `b_search_stem` VALUES (1797,'НЕВЫСОК');
INSERT INTO `b_search_stem` VALUES (753,'НЕГ');
INSERT INTO `b_search_stem` VALUES (527,'НЕДВИЖИМ');
INSERT INTO `b_search_stem` VALUES (1016,'НЕДОПУСТИМ');
INSERT INTO `b_search_stem` VALUES (1648,'НЕЗАВИСИМОСТЬ-МФ');
INSERT INTO `b_search_stem` VALUES (660,'НЕЗАМЕДЛИТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (795,'НЕИСПОЛНЕН');
INSERT INTO `b_search_stem` VALUES (717,'НЕМ');
INSERT INTO `b_search_stem` VALUES (796,'НЕНАДЛЕЖА');
INSERT INTO `b_search_stem` VALUES (190,'НЕОБХОД');
INSERT INTO `b_search_stem` VALUES (117,'НЕОБХОДИМ');
INSERT INTO `b_search_stem` VALUES (1777,'НЕОЖИДА');
INSERT INTO `b_search_stem` VALUES (812,'НЕОСНОВАТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (438,'НЕПОГАШЕН');
INSERT INTO `b_search_stem` VALUES (845,'НЕПОЛН');
INSERT INTO `b_search_stem` VALUES (425,'НЕПОЛНОРОДН');
INSERT INTO `b_search_stem` VALUES (647,'НЕПОСРЕДСТВЕН');
INSERT INTO `b_search_stem` VALUES (1027,'НЕПРАВОМЕРН');
INSERT INTO `b_search_stem` VALUES (802,'НЕПРЕОДОЛИМ');
INSERT INTO `b_search_stem` VALUES (931,'НЕПРИКОСНОВЕН');
INSERT INTO `b_search_stem` VALUES (558,'НЕПРОДЛЕН');
INSERT INTO `b_search_stem` VALUES (794,'НЕСЕТ');
INSERT INTO `b_search_stem` VALUES (119,'НЕСКОЛЬК');
INSERT INTO `b_search_stem` VALUES (1020,'НЕСОВМЕСТИМ');
INSERT INTO `b_search_stem` VALUES (347,'НЕУПЛАЧЕН');
INSERT INTO `b_search_stem` VALUES (720,'НЕУСТОЙК');
INSERT INTO `b_search_stem` VALUES (11,'НИЖ');
INSERT INTO `b_search_stem` VALUES (657,'НИМ');
INSERT INTO `b_search_stem` VALUES (417,'НИСХОДЯ');
INSERT INTO `b_search_stem` VALUES (101,'НОВ');
INSERT INTO `b_search_stem` VALUES (1675,'НОВОСТ');
INSERT INTO `b_search_stem` VALUES (182,'НОМЕР');
INSERT INTO `b_search_stem` VALUES (941,'НОРМАТИВН');
INSERT INTO `b_search_stem` VALUES (510,'НУЖД');
INSERT INTO `b_search_stem` VALUES (135,'НУЖН');
INSERT INTO `b_search_stem` VALUES (1040,'Настоящие');
INSERT INTO `b_search_stem` VALUES (1886,'Наши');
INSERT INTO `b_search_stem` VALUES (1060,'Независимость-МФО');
INSERT INTO `b_search_stem` VALUES (1052,'О');
INSERT INTO `b_search_stem` VALUES (896,'ОБЕЗЛИЧИВАН');
INSERT INTO `b_search_stem` VALUES (653,'ОБЕСПЕЧ');
INSERT INTO `b_search_stem` VALUES (846,'ОБЕСПЕЧЕН');
INSERT INTO `b_search_stem` VALUES (127,'ОБЛАСТ');
INSERT INTO `b_search_stem` VALUES (884,'ОБНОВЛЕН');
INSERT INTO `b_search_stem` VALUES (813,'ОБОГАЩЕН');
INSERT INTO `b_search_stem` VALUES (1714,'ОБОРУДОВА');
INSERT INTO `b_search_stem` VALUES (927,'ОБРАБАТЫВА');
INSERT INTO `b_search_stem` VALUES (596,'ОБРАБОТК');
INSERT INTO `b_search_stem` VALUES (655,'ОБРАЗ');
INSERT INTO `b_search_stem` VALUES (10,'ОБРАЗОВАН');
INSERT INTO `b_search_stem` VALUES (264,'ОБРАТ');
INSERT INTO `b_search_stem` VALUES (279,'ОБРАЩЕН');
INSERT INTO `b_search_stem` VALUES (775,'ОБСЛУЖИВА');
INSERT INTO `b_search_stem` VALUES (34,'ОБСЛУЖИВАН');
INSERT INTO `b_search_stem` VALUES (555,'ОБУСЛОВЛЕН');
INSERT INTO `b_search_stem` VALUES (75,'ОБУЧЕН');
INSERT INTO `b_search_stem` VALUES (426,'ОБЩ');
INSERT INTO `b_search_stem` VALUES (162,'ОБЩА');
INSERT INTO `b_search_stem` VALUES (284,'ОБЩЕСТВ');
INSERT INTO `b_search_stem` VALUES (1767,'ОБЪЕДИН');
INSERT INTO `b_search_stem` VALUES (1018,'ОБЪЕДИНЕН');
INSERT INTO `b_search_stem` VALUES (1014,'ОБЪЕМ');
INSERT INTO `b_search_stem` VALUES (553,'ОБЯЗ');
INSERT INTO `b_search_stem` VALUES (701,'ОБЯЗА');
INSERT INTO `b_search_stem` VALUES (32,'ОБЯЗАН');
INSERT INTO `b_search_stem` VALUES (273,'ОБЯЗАТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (707,'ОБЯЗАТЕЛЬСТВ');
INSERT INTO `b_search_stem` VALUES (401,'ОГРАНИЧ');
INSERT INTO `b_search_stem` VALUES (285,'ОГРАНИЧЕН');
INSERT INTO `b_search_stem` VALUES (763,'ОГРАНИЧИВ');
INSERT INTO `b_search_stem` VALUES (203,'ОГРН');
INSERT INTO `b_search_stem` VALUES (270,'ОДИН');
INSERT INTO `b_search_stem` VALUES (274,'ОДН');
INSERT INTO `b_search_stem` VALUES (799,'ОДНАК');
INSERT INTO `b_search_stem` VALUES (730,'ОДНОМЕСЯЧН');
INSERT INTO `b_search_stem` VALUES (645,'ОДНОРАЗОВ');
INSERT INTO `b_search_stem` VALUES (624,'ОДНОСТОРОН');
INSERT INTO `b_search_stem` VALUES (145,'ОДОБРЕН');
INSERT INTO `b_search_stem` VALUES (800,'ОКАЗА');
INSERT INTO `b_search_stem` VALUES (826,'ОКАЗЫВА');
INSERT INTO `b_search_stem` VALUES (207,'ОКАТ');
INSERT INTO `b_search_stem` VALUES (857,'ОКБ');
INSERT INTO `b_search_stem` VALUES (1717,'ОКЛАД');
INSERT INTO `b_search_stem` VALUES (559,'ОКОНЧАН');
INSERT INTO `b_search_stem` VALUES (915,'ОКОНЧАТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (205,'ОКП');
INSERT INTO `b_search_stem` VALUES (1690,'ОЛ');
INSERT INTO `b_search_stem` VALUES (152,'ОНЛАЙН');
INSERT INTO `b_search_stem` VALUES (193,'ОО');
INSERT INTO `b_search_stem` VALUES (22,'ОП');
INSERT INTO `b_search_stem` VALUES (649,'ОПЕРАЦ');
INSERT INTO `b_search_stem` VALUES (1752,'ОПИР');
INSERT INTO `b_search_stem` VALUES (1951,'ОПИСАН');
INSERT INTO `b_search_stem` VALUES (166,'ОПЛАТ');
INSERT INTO `b_search_stem` VALUES (728,'ОПЛАЧИВА');
INSERT INTO `b_search_stem` VALUES (1022,'ОПРЕДЕЛ');
INSERT INTO `b_search_stem` VALUES (1013,'ОПРЕДЕЛЕН');
INSERT INTO `b_search_stem` VALUES (643,'ОПРЕДЕЛЯ');
INSERT INTO `b_search_stem` VALUES (887,'ОПУБЛИКОВАН');
INSERT INTO `b_search_stem` VALUES (818,'ОРГА');
INSERT INTO `b_search_stem` VALUES (855,'ОРГАН');
INSERT INTO `b_search_stem` VALUES (307,'ОРГАНИЗАЦ');
INSERT INTO `b_search_stem` VALUES (1025,'ОРГАНИЗАЦИОН');
INSERT INTO `b_search_stem` VALUES (854,'ОРГАНОВ');
INSERT INTO `b_search_stem` VALUES (673,'ОРИГИНАЛ');
INSERT INTO `b_search_stem` VALUES (672,'ОРИГИНАЛОВ');
INSERT INTO `b_search_stem` VALUES (1751,'ОРУЖ');
INSERT INTO `b_search_stem` VALUES (233,'ОСЛОЖНЕН');
INSERT INTO `b_search_stem` VALUES (1008,'ОСН');
INSERT INTO `b_search_stem` VALUES (1007,'ОСНОВ');
INSERT INTO `b_search_stem` VALUES (634,'ОСНОВА');
INSERT INTO `b_search_stem` VALUES (609,'ОСНОВАН');
INSERT INTO `b_search_stem` VALUES (345,'ОСНОВН');
INSERT INTO `b_search_stem` VALUES (1005,'ОСОБЕН');
INSERT INTO `b_search_stem` VALUES (1835,'ОСТАВ');
INSERT INTO `b_search_stem` VALUES (749,'ОСТАТОК');
INSERT INTO `b_search_stem` VALUES (239,'ОСУЩЕСТВ');
INSERT INTO `b_search_stem` VALUES (681,'ОСУЩЕСТВЛЕН');
INSERT INTO `b_search_stem` VALUES (272,'ОСУЩЕСТВЛЯ');
INSERT INTO `b_search_stem` VALUES (477,'ОТВЕТ');
INSERT INTO `b_search_stem` VALUES (286,'ОТВЕТСТВЕН');
INSERT INTO `b_search_stem` VALUES (536,'ОТВЕЧА');
INSERT INTO `b_search_stem` VALUES (184,'ОТДЕЛЕН');
INSERT INTO `b_search_stem` VALUES (1821,'ОТДЕЛЬН');
INSERT INTO `b_search_stem` VALUES (1674,'ОТЗЫВ');
INSERT INTO `b_search_stem` VALUES (607,'ОТКАЗ');
INSERT INTO `b_search_stem` VALUES (606,'ОТКАЗА');
INSERT INTO `b_search_stem` VALUES (235,'ОТКЛАДЫВА');
INSERT INTO `b_search_stem` VALUES (105,'ОТКРЫТ');
INSERT INTO `b_search_stem` VALUES (1668,'ОТЛИЧН');
INSERT INTO `b_search_stem` VALUES (248,'ОТЛОЖ');
INSERT INTO `b_search_stem` VALUES (960,'ОТМЫВАН');
INSERT INTO `b_search_stem` VALUES (501,'ОТНОС');
INSERT INTO `b_search_stem` VALUES (532,'ОТНОСЯ');
INSERT INTO `b_search_stem` VALUES (292,'ОТНОШЕН');
INSERT INTO `b_search_stem` VALUES (761,'ОТОБРАЖЕН');
INSERT INTO `b_search_stem` VALUES (613,'ОТПРАВК');
INSERT INTO `b_search_stem` VALUES (938,'ОТРАЖЕН');
INSERT INTO `b_search_stem` VALUES (781,'ОТСУТСТВ');
INSERT INTO `b_search_stem` VALUES (427,'ОТЦ');
INSERT INTO `b_search_stem` VALUES (828,'ОТЧЕСТВ');
INSERT INTO `b_search_stem` VALUES (939,'ОТЧЕТН');
INSERT INTO `b_search_stem` VALUES (348,'ОФЕРТ');
INSERT INTO `b_search_stem` VALUES (54,'ОФИС');
INSERT INTO `b_search_stem` VALUES (20,'ОФИСН');
INSERT INTO `b_search_stem` VALUES (275,'ОФИСОВ');
INSERT INTO `b_search_stem` VALUES (71,'ОФИЦИАЛЬН');
INSERT INTO `b_search_stem` VALUES (148,'ОФОРМ');
INSERT INTO `b_search_stem` VALUES (48,'ОФОРМЛЕН');
INSERT INTO `b_search_stem` VALUES (1693,'ОЦЕНК');
INSERT INTO `b_search_stem` VALUES (705,'ОЧЕРЕДН');
INSERT INTO `b_search_stem` VALUES (461,'ОЧН');
INSERT INTO `b_search_stem` VALUES (622,'ОШИБОЧН');
INSERT INTO `b_search_stem` VALUES (1036,'Общие');
INSERT INTO `b_search_stem` VALUES (1418,'Обязательной');
INSERT INTO `b_search_stem` VALUES (1562,'Ограничения');
INSERT INTO `b_search_stem` VALUES (1931,'Офицерский');
INSERT INTO `b_search_stem` VALUES (1859,'Оформите');
INSERT INTO `b_search_stem` VALUES (1853,'Оформление');
INSERT INTO `b_search_stem` VALUES (1818,'ПАВИЛЬОН');
INSERT INTO `b_search_stem` VALUES (642,'ПАРОЛ');
INSERT INTO `b_search_stem` VALUES (1771,'ПАРТНЕР');
INSERT INTO `b_search_stem` VALUES (141,'ПАСПОРТ');
INSERT INTO `b_search_stem` VALUES (470,'ПЕНС');
INSERT INTO `b_search_stem` VALUES (494,'ПЕНСИОН');
INSERT INTO `b_search_stem` VALUES (502,'ПЕНСИОНЕР');
INSERT INTO `b_search_stem` VALUES (875,'ПЕР');
INSERT INTO `b_search_stem` VALUES (367,'ПЕРВ');
INSERT INTO `b_search_stem` VALUES (980,'ПЕРВИЧН');
INSERT INTO `b_search_stem` VALUES (545,'ПЕРВОНАЧАЛЬН');
INSERT INTO `b_search_stem` VALUES (187,'ПЕРЕВОД');
INSERT INTO `b_search_stem` VALUES (648,'ПЕРЕД');
INSERT INTO `b_search_stem` VALUES (814,'ПЕРЕДА');
INSERT INTO `b_search_stem` VALUES (903,'ПЕРЕДАВА');
INSERT INTO `b_search_stem` VALUES (588,'ПЕРЕДАЧ');
INSERT INTO `b_search_stem` VALUES (40,'ПЕРЕСЧЕТ');
INSERT INTO `b_search_stem` VALUES (890,'ПЕРЕСЫЛК');
INSERT INTO `b_search_stem` VALUES (318,'ПЕРЕЧЕН');
INSERT INTO `b_search_stem` VALUES (754,'ПЕРЕЧИСЛ');
INSERT INTO `b_search_stem` VALUES (676,'ПЕРЕЧИСЛЕН');
INSERT INTO `b_search_stem` VALUES (439,'ПЕРЕЧИСЛЯ');
INSERT INTO `b_search_stem` VALUES (991,'ПЕРЕЧН');
INSERT INTO `b_search_stem` VALUES (493,'ПЕРИОД');
INSERT INTO `b_search_stem` VALUES (733,'ПЕРИОДИЧН');
INSERT INTO `b_search_stem` VALUES (589,'ПЕРСОНАЛЬН');
INSERT INTO `b_search_stem` VALUES (977,'ПЕРСОНИФИЦИРОВА');
INSERT INTO `b_search_stem` VALUES (1745,'ПЕРСПЕКТИВ');
INSERT INTO `b_search_stem` VALUES (1653,'ПЕТРОВСК');
INSERT INTO `b_search_stem` VALUES (1658,'ПИОНЕР');
INSERT INTO `b_search_stem` VALUES (746,'ПИСЬМЕН');
INSERT INTO `b_search_stem` VALUES (16,'ПК');
INSERT INTO `b_search_stem` VALUES (721,'ПЛАН');
INSERT INTO `b_search_stem` VALUES (1826,'ПЛАНИРОВК');
INSERT INTO `b_search_stem` VALUES (1697,'ПЛАНОВ');
INSERT INTO `b_search_stem` VALUES (74,'ПЛАТ');
INSERT INTO `b_search_stem` VALUES (567,'ПЛАТЕЖ');
INSERT INTO `b_search_stem` VALUES (1694,'ПЛАТЕЖЕСПОСОБН');
INSERT INTO `b_search_stem` VALUES (378,'ПЛАТЕЖН');
INSERT INTO `b_search_stem` VALUES (1823,'ПЛОЩАД');
INSERT INTO `b_search_stem` VALUES (1680,'ПЛЮС');
INSERT INTO `b_search_stem` VALUES (1728,'ПЛЮСКРЕД');
INSERT INTO `b_search_stem` VALUES (1795,'ПОВЫШЕН');
INSERT INTO `b_search_stem` VALUES (163,'ПОГАС');
INSERT INTO `b_search_stem` VALUES (744,'ПОГАШ');
INSERT INTO `b_search_stem` VALUES (242,'ПОГАШЕН');
INSERT INTO `b_search_stem` VALUES (769,'ПОДА');
INSERT INTO `b_search_stem` VALUES (441,'ПОДАЧ');
INSERT INTO `b_search_stem` VALUES (1722,'ПОДДЕРЖК');
INSERT INTO `b_search_stem` VALUES (315,'ПОДЗАКОН');
INSERT INTO `b_search_stem` VALUES (1031,'ПОДЛЕЖ');
INSERT INTO `b_search_stem` VALUES (342,'ПОДЛЕЖА');
INSERT INTO `b_search_stem` VALUES (566,'ПОДЛЕЖАТ');
INSERT INTO `b_search_stem` VALUES (42,'ПОДЛИН');
INSERT INTO `b_search_stem` VALUES (602,'ПОДП');
INSERT INTO `b_search_stem` VALUES (639,'ПОДПИС');
INSERT INTO `b_search_stem` VALUES (377,'ПОДПИСАН');
INSERT INTO `b_search_stem` VALUES (576,'ПОДПУНКТ');
INSERT INTO `b_search_stem` VALUES (472,'ПОДРАБОТК');
INSERT INTO `b_search_stem` VALUES (218,'ПОДРОБН');
INSERT INTO `b_search_stem` VALUES (475,'ПОДТВЕРЖДА');
INSERT INTO `b_search_stem` VALUES (372,'ПОДТВЕРЖДЕН');
INSERT INTO `b_search_stem` VALUES (171,'ПОДХОД');
INSERT INTO `b_search_stem` VALUES (783,'ПОЗВОЛЯ');
INSERT INTO `b_search_stem` VALUES (263,'ПОЗДН');
INSERT INTO `b_search_stem` VALUES (1698,'ПОКАЗАТЕЛ');
INSERT INTO `b_search_stem` VALUES (829,'ПОЛ');
INSERT INTO `b_search_stem` VALUES (659,'ПОЛАГА');
INSERT INTO `b_search_stem` VALUES (621,'ПОЛИТИК');
INSERT INTO `b_search_stem` VALUES (593,'ПОЛН');
INSERT INTO `b_search_stem` VALUES (424,'ПОЛНОРОДН');
INSERT INTO `b_search_stem` VALUES (70,'ПОЛНОСТ');
INSERT INTO `b_search_stem` VALUES (599,'ПОЛНОТ');
INSERT INTO `b_search_stem` VALUES (914,'ПОЛОЖЕН');
INSERT INTO `b_search_stem` VALUES (537,'ПОЛОЖИТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (113,'ПОЛУЧ');
INSERT INTO `b_search_stem` VALUES (368,'ПОЛУЧА');
INSERT INTO `b_search_stem` VALUES (978,'ПОЛУЧАТЕЛ');
INSERT INTO `b_search_stem` VALUES (114,'ПОЛУЧЕН');
INSERT INTO `b_search_stem` VALUES (709,'ПОЛЬЗ');
INSERT INTO `b_search_stem` VALUES (278,'ПОЛЬЗОВАН');
INSERT INTO `b_search_stem` VALUES (15,'ПОЛЬЗОВАТЕЛ');
INSERT INTO `b_search_stem` VALUES (586,'ПОМЕЧЕН');
INSERT INTO `b_search_stem` VALUES (1810,'ПОМЕЩЕН');
INSERT INTO `b_search_stem` VALUES (1760,'ПОМОЧ');
INSERT INTO `b_search_stem` VALUES (1702,'ПОПОЛНЕН');
INSERT INTO `b_search_stem` VALUES (175,'ПОПРОС');
INSERT INTO `b_search_stem` VALUES (900,'ПОРОЖДА');
INSERT INTO `b_search_stem` VALUES (856,'ПОРУЧЕН');
INSERT INTO `b_search_stem` VALUES (551,'ПОРЯДК');
INSERT INTO `b_search_stem` VALUES (440,'ПОРЯДОК');
INSERT INTO `b_search_stem` VALUES (160,'ПОСЕЩА');
INSERT INTO `b_search_stem` VALUES (281,'ПОСЛ');
INSERT INTO `b_search_stem` VALUES (384,'ПОСЛЕД');
INSERT INTO `b_search_stem` VALUES (702,'ПОСЛЕДН');
INSERT INTO `b_search_stem` VALUES (901,'ПОСЛЕДСТВ');
INSERT INTO `b_search_stem` VALUES (1699,'ПОСТАВЛЕН');
INSERT INTO `b_search_stem` VALUES (1705,'ПОСТАВЩИК');
INSERT INTO `b_search_stem` VALUES (1000,'ПОСТАНОВЛЕН');
INSERT INTO `b_search_stem` VALUES (123,'ПОСТОЯ');
INSERT INTO `b_search_stem` VALUES (1757,'ПОСТРО');
INSERT INTO `b_search_stem` VALUES (704,'ПОСТУП');
INSERT INTO `b_search_stem` VALUES (388,'ПОТЕНЦИАЛЬН');
INSERT INTO `b_search_stem` VALUES (614,'ПОЧТ');
INSERT INTO `b_search_stem` VALUES (747,'ПОЧТОВ');
INSERT INTO `b_search_stem` VALUES (1032,'ПОЯВЛЕН');
INSERT INTO `b_search_stem` VALUES (283,'ПРАВ');
INSERT INTO `b_search_stem` VALUES (326,'ПРАВИЛ');
INSERT INTO `b_search_stem` VALUES (47,'ПРАВИЛЬН');
INSERT INTO `b_search_stem` VALUES (1001,'ПРАВИТЕЛЬСТВ');
INSERT INTO `b_search_stem` VALUES (755,'ПРАВОВ');
INSERT INTO `b_search_stem` VALUES (836,'ПРЕБЫВАН');
INSERT INTO `b_search_stem` VALUES (167,'ПРЕВЫША');
INSERT INTO `b_search_stem` VALUES (617,'ПРЕДВАРИТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (708,'ПРЕДЕЛ');
INSERT INTO `b_search_stem` VALUES (61,'ПРЕДЛАГА');
INSERT INTO `b_search_stem` VALUES (718,'ПРЕДЛОЖ');
INSERT INTO `b_search_stem` VALUES (350,'ПРЕДЛОЖЕН');
INSERT INTO `b_search_stem` VALUES (140,'ПРЕДОСТАВ');
INSERT INTO `b_search_stem` VALUES (569,'ПРЕДОСТАВЛ');
INSERT INTO `b_search_stem` VALUES (57,'ПРЕДОСТАВЛЕН');
INSERT INTO `b_search_stem` VALUES (436,'ПРЕДОСТАВЛЯ');
INSERT INTO `b_search_stem` VALUES (520,'ПРЕДПОЛАГА');
INSERT INTO `b_search_stem` VALUES (1024,'ПРЕДПРИНИМА');
INSERT INTO `b_search_stem` VALUES (404,'ПРЕДПРИНИМАТЕЛ');
INSERT INTO `b_search_stem` VALUES (514,'ПРЕДПРИЯТ');
INSERT INTO `b_search_stem` VALUES (407,'ПРЕДСТАВИТЕЛ');
INSERT INTO `b_search_stem` VALUES (976,'ПРЕДСТАВЛЕН');
INSERT INTO `b_search_stem` VALUES (729,'ПРЕДУСМАТРИВА');
INSERT INTO `b_search_stem` VALUES (575,'ПРЕДУСМОТР');
INSERT INTO `b_search_stem` VALUES (556,'ПРЕДУСМОТРЕН');
INSERT INTO `b_search_stem` VALUES (492,'ПРЕДЫДУЩ');
INSERT INTO `b_search_stem` VALUES (1710,'ПРЕИМУЩЕСТВ');
INSERT INTO `b_search_stem` VALUES (400,'ПРЕКРАЩ');
INSERT INTO `b_search_stem` VALUES (667,'ПРЕКРАЩЕН');
INSERT INTO `b_search_stem` VALUES (1718,'ПРЕМ');
INSERT INTO `b_search_stem` VALUES (956,'ПРЕСТУПН');
INSERT INTO `b_search_stem` VALUES (25,'ПРИВЕТСТВ');
INSERT INTO `b_search_stem` VALUES (158,'ПРИГЛАС');
INSERT INTO `b_search_stem` VALUES (37,'ПРИЕМ');
INSERT INTO `b_search_stem` VALUES (919,'ПРИЗНА');
INSERT INTO `b_search_stem` VALUES (822,'ПРИКАЗ');
INSERT INTO `b_search_stem` VALUES (313,'ПРИМЕНИМ');
INSERT INTO `b_search_stem` VALUES (698,'ПРИМЕНИТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (757,'ПРИМЕНЯ');
INSERT INTO `b_search_stem` VALUES (608,'ПРИНИМА');
INSERT INTO `b_search_stem` VALUES (925,'ПРИНЦИП');
INSERT INTO `b_search_stem` VALUES (1009,'ПРИНЦИПОВ');
INSERT INTO `b_search_stem` VALUES (722,'ПРИНЯ');
INSERT INTO `b_search_stem` VALUES (488,'ПРИНЯТ');
INSERT INTO `b_search_stem` VALUES (923,'ПРИОРИТЕТН');
INSERT INTO `b_search_stem` VALUES (1735,'ПРИЧИН');
INSERT INTO `b_search_stem` VALUES (752,'ПРИЧИТА');
INSERT INTO `b_search_stem` VALUES (159,'ПРИЯТН');
INSERT INTO `b_search_stem` VALUES (1786,'ПРОБЛ');
INSERT INTO `b_search_stem` VALUES (1782,'ПРОБЛЕМ');
INSERT INTO `b_search_stem` VALUES (1742,'ПРОГРАММ');
INSERT INTO `b_search_stem` VALUES (55,'ПРОДАЖ');
INSERT INTO `b_search_stem` VALUES (965,'ПРОДВИЖЕН');
INSERT INTO `b_search_stem` VALUES (260,'ПРОДЛ');
INSERT INTO `b_search_stem` VALUES (266,'ПРОДЛЕВА');
INSERT INTO `b_search_stem` VALUES (247,'ПРОДЛЕН');
INSERT INTO `b_search_stem` VALUES (1733,'ПРОДУКТ');
INSERT INTO `b_search_stem` VALUES (1732,'ПРОДУКТОВ');
INSERT INTO `b_search_stem` VALUES (125,'ПРОЖИВА');
INSERT INTO `b_search_stem` VALUES (837,'ПРОЖИВАН');
INSERT INTO `b_search_stem` VALUES (735,'ПРОИЗВЕСТ');
INSERT INTO `b_search_stem` VALUES (484,'ПРОИЗВОД');
INSERT INTO `b_search_stem` VALUES (240,'ПРОЛОНГАЦ');
INSERT INTO `b_search_stem` VALUES (1755,'ПРОРАБОТА');
INSERT INTO `b_search_stem` VALUES (1832,'ПРОС');
INSERT INTO `b_search_stem` VALUES (895,'ПРОСРОЧЕН');
INSERT INTO `b_search_stem` VALUES (564,'ПРОСРОЧК');
INSERT INTO `b_search_stem` VALUES (120,'ПРОСТ');
INSERT INTO `b_search_stem` VALUES (953,'ПРОТИВОДЕЙСТВ');
INSERT INTO `b_search_stem` VALUES (1766,'ПРОФЕССИОНАЛ');
INSERT INTO `b_search_stem` VALUES (1773,'ПРОФЕССИОНАЛИЗМ');
INSERT INTO `b_search_stem` VALUES (1765,'ПРОФЕССИОНАЛОВ');
INSERT INTO `b_search_stem` VALUES (79,'ПРОФЕССИОНАЛЬН');
INSERT INTO `b_search_stem` VALUES (460,'ПРОХОДЯ');
INSERT INTO `b_search_stem` VALUES (820,'ПРОЦЕДУР');
INSERT INTO `b_search_stem` VALUES (254,'ПРОЦЕНТ');
INSERT INTO `b_search_stem` VALUES (449,'ПРОЦЕНТН');
INSERT INTO `b_search_stem` VALUES (280,'ПРОЦЕНТОВ');
INSERT INTO `b_search_stem` VALUES (995,'ПРОЦЕСС');
INSERT INTO `b_search_stem` VALUES (234,'ПРОЧ');
INSERT INTO `b_search_stem` VALUES (220,'ПРОЧИТА');
INSERT INTO `b_search_stem` VALUES (415,'ПРЯМ');
INSERT INTO `b_search_stem` VALUES (917,'ПУБЛИКАЦ');
INSERT INTO `b_search_stem` VALUES (412,'ПУБЛИЧН');
INSERT INTO `b_search_stem` VALUES (27,'ПУНКТУАЛЬН');
INSERT INTO `b_search_stem` VALUES (355,'ПУТ');
INSERT INTO `b_search_stem` VALUES (739,'ПЯТ');
INSERT INTO `b_search_stem` VALUES (1268,'Паспорт');
INSERT INTO `b_search_stem` VALUES (1285,'Паспорта');
INSERT INTO `b_search_stem` VALUES (1945,'Пенсионный');
INSERT INTO `b_search_stem` VALUES (1342,'Перечнем');
INSERT INTO `b_search_stem` VALUES (1324,'По');
INSERT INTO `b_search_stem` VALUES (1445,'Погашение');
INSERT INTO `b_search_stem` VALUES (1866,'Получите');
INSERT INTO `b_search_stem` VALUES (1196,'Порядок');
INSERT INTO `b_search_stem` VALUES (1213,'После');
INSERT INTO `b_search_stem` VALUES (1503,'Права');
INSERT INTO `b_search_stem` VALUES (1069,'Правил');
INSERT INTO `b_search_stem` VALUES (1033,'Правила');
INSERT INTO `b_search_stem` VALUES (1202,'Правилами');
INSERT INTO `b_search_stem` VALUES (1549,'Правилах');
INSERT INTO `b_search_stem` VALUES (1551,'Предоставление');
INSERT INTO `b_search_stem` VALUES (1089,'Предоставления');
INSERT INTO `b_search_stem` VALUES (1173,'Представитель');
INSERT INTO `b_search_stem` VALUES (1315,'Представителя');
INSERT INTO `b_search_stem` VALUES (1203,'При');
INSERT INTO `b_search_stem` VALUES (1934,'Приглашаем');
INSERT INTO `b_search_stem` VALUES (1601,'Прочие');
INSERT INTO `b_search_stem` VALUES (18,'РАБОТ');
INSERT INTO `b_search_stem` VALUES (99,'РАБОТА');
INSERT INTO `b_search_stem` VALUES (506,'РАБОТНИК');
INSERT INTO `b_search_stem` VALUES (926,'РАБОТНИКОВ');
INSERT INTO `b_search_stem` VALUES (478,'РАБОТОДАТЕЛ');
INSERT INTO `b_search_stem` VALUES (76,'РАБОЧ');
INSERT INTO `b_search_stem` VALUES (780,'РАВ');
INSERT INTO `b_search_stem` VALUES (692,'РАВН');
INSERT INTO `b_search_stem` VALUES (663,'РАВНОЗНАЧН');
INSERT INTO `b_search_stem` VALUES (103,'РАД');
INSERT INTO `b_search_stem` VALUES (268,'РАЗ');
INSERT INTO `b_search_stem` VALUES (65,'РАЗВИВА');
INSERT INTO `b_search_stem` VALUES (1723,'РАЗВИТ');
INSERT INTO `b_search_stem` VALUES (597,'РАЗДЕЛ');
INSERT INTO `b_search_stem` VALUES (547,'РАЗМЕР');
INSERT INTO `b_search_stem` VALUES (628,'РАЗМЕЩА');
INSERT INTO `b_search_stem` VALUES (358,'РАЗМЕЩЕН');
INSERT INTO `b_search_stem` VALUES (809,'РАЗНОГЛАС');
INSERT INTO `b_search_stem` VALUES (289,'РАЗРАБОТА');
INSERT INTO `b_search_stem` VALUES (804,'РАЗРЕШЕН');
INSERT INTO `b_search_stem` VALUES (107,'РАЙОН');
INSERT INTO `b_search_stem` VALUES (944,'РАМК');
INSERT INTO `b_search_stem` VALUES (577,'РАН');
INSERT INTO `b_search_stem` VALUES (398,'РАСПОРЯЖА');
INSERT INTO `b_search_stem` VALUES (885,'РАСПРОСТРАНЕН');
INSERT INTO `b_search_stem` VALUES (552,'РАСПРОСТРАНЯ');
INSERT INTO `b_search_stem` VALUES (1809,'РАССМАТРИВА');
INSERT INTO `b_search_stem` VALUES (442,'РАССМОТРЕН');
INSERT INTO `b_search_stem` VALUES (697,'РАССЧИТЫВА');
INSERT INTO `b_search_stem` VALUES (1768,'РАСТ');
INSERT INTO `b_search_stem` VALUES (571,'РАСХОД');
INSERT INTO `b_search_stem` VALUES (688,'РАСЧЕТ');
INSERT INTO `b_search_stem` VALUES (188,'РАСЧЕТН');
INSERT INTO `b_search_stem` VALUES (33,'РАСЧЕТНО-КАССОВ');
INSERT INTO `b_search_stem` VALUES (951,'РЕАЛИЗАЦ');
INSERT INTO `b_search_stem` VALUES (920,'РЕАЛИЗУЕМ');
INSERT INTO `b_search_stem` VALUES (420,'РЕБЕНК');
INSERT INTO `b_search_stem` VALUES (1842,'РЕГИОН');
INSERT INTO `b_search_stem` VALUES (531,'РЕГИОНАЛЬН');
INSERT INTO `b_search_stem` VALUES (1841,'РЕГИОНОВ');
INSERT INTO `b_search_stem` VALUES (835,'РЕГИСТРАЦ');
INSERT INTO `b_search_stem` VALUES (330,'РЕГИСТРАЦИОН');
INSERT INTO `b_search_stem` VALUES (807,'РЕГУЛИР');
INSERT INTO `b_search_stem` VALUES (291,'РЕГУЛИРОВАН');
INSERT INTO `b_search_stem` VALUES (888,'РЕДАКТИРОВАН');
INSERT INTO `b_search_stem` VALUES (918,'РЕДАКЦ');
INSERT INTO `b_search_stem` VALUES (333,'РЕЕСТР');
INSERT INTO `b_search_stem` VALUES (850,'РЕЗОЛЮТИВН');
INSERT INTO `b_search_stem` VALUES (750,'РЕЗУЛЬТАТ');
INSERT INTO `b_search_stem` VALUES (1727,'РЕЗЮМ');
INSERT INTO `b_search_stem` VALUES (191,'РЕКВИЗИТ');
INSERT INTO `b_search_stem` VALUES (1827,'РЕКЛАМ');
INSERT INTO `b_search_stem` VALUES (1828,'РЕКЛАМН');
INSERT INTO `b_search_stem` VALUES (790,'РЕКОМЕНД');
INSERT INTO `b_search_stem` VALUES (224,'РЕКОМЕНДАЦ');
INSERT INTO `b_search_stem` VALUES (227,'РЕКОМЕНДУ');
INSERT INTO `b_search_stem` VALUES (1778,'РЕМОНТ');
INSERT INTO `b_search_stem` VALUES (1750,'РЕША');
INSERT INTO `b_search_stem` VALUES (489,'РЕШЕН');
INSERT INTO `b_search_stem` VALUES (788,'РИСК');
INSERT INTO `b_search_stem` VALUES (948,'РИСКОВ');
INSERT INTO `b_search_stem` VALUES (1808,'РИЭЛТОР');
INSERT INTO `b_search_stem` VALUES (1807,'РИЭЛТОРОВ');
INSERT INTO `b_search_stem` VALUES (419,'РОДИТЕЛ');
INSERT INTO `b_search_stem` VALUES (177,'РОДСТВЕННИК');
INSERT INTO `b_search_stem` VALUES (830,'РОЖДЕН');
INSERT INTO `b_search_stem` VALUES (858,'РОСС');
INSERT INTO `b_search_stem` VALUES (310,'РОССИЙСК');
INSERT INTO `b_search_stem` VALUES (81,'РОСТ');
INSERT INTO `b_search_stem` VALUES (454,'РУБ');
INSERT INTO `b_search_stem` VALUES (170,'РУБЛ');
INSERT INTO `b_search_stem` VALUES (936,'РУКОВОДИТЕЛ');
INSERT INTO `b_search_stem` VALUES (69,'РФ');
INSERT INTO `b_search_stem` VALUES (1756,'РЫНК');
INSERT INTO `b_search_stem` VALUES (1354,'Ранее');
INSERT INTO `b_search_stem` VALUES (1155,'Российской');
INSERT INTO `b_search_stem` VALUES (1897,'Рынок');
INSERT INTO `b_search_stem` VALUES (153,'САЙТ');
INSERT INTO `b_search_stem` VALUES (173,'САМОСТОЯТЕЛЬН');
INSERT INTO `b_search_stem` VALUES (997,'САМОУПРАВЛЕН');
INSERT INTO `b_search_stem` VALUES (2,'САНКТ-ПЕТЕРБУРГ');
INSERT INTO `b_search_stem` VALUES (1689,'САХАР');
INSERT INTO `b_search_stem` VALUES (1688,'САХАРОВ');
INSERT INTO `b_search_stem` VALUES (1665,'СБЕРБАНК');
INSERT INTO `b_search_stem` VALUES (880,'СБОР');
INSERT INTO `b_search_stem` VALUES (1740,'СБЫВА');
INSERT INTO `b_search_stem` VALUES (1781,'СВАДЬБ');
INSERT INTO `b_search_stem` VALUES (592,'СВЕДЕН');
INSERT INTO `b_search_stem` VALUES (670,'СВЕР');
INSERT INTO `b_search_stem` VALUES (1704,'СВЕРК');
INSERT INTO `b_search_stem` VALUES (473,'СВИДЕТЕЛЬСТВ');
INSERT INTO `b_search_stem` VALUES (928,'СВОБОД');
INSERT INTO `b_search_stem` VALUES (154,'СВЯЖУТ');
INSERT INTO `b_search_stem` VALUES (297,'СВЯЗ');
INSERT INTO `b_search_stem` VALUES (598,'СВЯЗА');
INSERT INTO `b_search_stem` VALUES (664,'СДЕЛОК');
INSERT INTO `b_search_stem` VALUES (102,'СЕГОДН');
INSERT INTO `b_search_stem` VALUES (581,'СЕМ');
INSERT INTO `b_search_stem` VALUES (933,'СЕМЕЙН');
INSERT INTO `b_search_stem` VALUES (866,'СЕРВИСИЗ');
INSERT INTO `b_search_stem` VALUES (429,'СЕСТР');
INSERT INTO `b_search_stem` VALUES (524,'СЕТ');
INSERT INTO `b_search_stem` VALUES (530,'СЕТЕВ');
INSERT INTO `b_search_stem` VALUES (685,'СИЛ');
INSERT INTO `b_search_stem` VALUES (683,'СИСИТ');
INSERT INTO `b_search_stem` VALUES (682,'СИСТ');
INSERT INTO `b_search_stem` VALUES (380,'СИСТЕМ');
INSERT INTO `b_search_stem` VALUES (881,'СИСТЕМАТИЗАЦ');
INSERT INTO `b_search_stem` VALUES (1759,'СКАЗА');
INSERT INTO `b_search_stem` VALUES (1784,'СКРЫТ');
INSERT INTO `b_search_stem` VALUES (328,'СЛЕД');
INSERT INTO `b_search_stem` VALUES (1792,'СЛО');
INSERT INTO `b_search_stem` VALUES (1791,'СЛОЕВ');
INSERT INTO `b_search_stem` VALUES (966,'СЛУЖБ');
INSERT INTO `b_search_stem` VALUES (144,'СЛУЧА');
INSERT INTO `b_search_stem` VALUES (1028,'СЛУЧАЙН');
INSERT INTO `b_search_stem` VALUES (82,'СМЕН');
INSERT INTO `b_search_stem` VALUES (878,'СМЕША');
INSERT INTO `b_search_stem` VALUES (1685,'СМИРН');
INSERT INTO `b_search_stem` VALUES (1684,'СМИРНОВ');
INSERT INTO `b_search_stem` VALUES (232,'СМОЖЕТ');
INSERT INTO `b_search_stem` VALUES (1743,'СМОТРЕТ');
INSERT INTO `b_search_stem` VALUES (583,'СМС-СООБЩЕН');
INSERT INTO `b_search_stem` VALUES (560,'СО');
INSERT INTO `b_search_stem` VALUES (1021,'СОБ');
INSERT INTO `b_search_stem` VALUES (118,'СОБЛЮДЕН');
INSERT INTO `b_search_stem` VALUES (1805,'СОБСТВЕННИК');
INSERT INTO `b_search_stem` VALUES (1804,'СОБСТВЕННИКОВ');
INSERT INTO `b_search_stem` VALUES (638,'СОБСТВЕННОРУЧН');
INSERT INTO `b_search_stem` VALUES (719,'СОВЕРШ');
INSERT INTO `b_search_stem` VALUES (363,'СОВЕРШЕН');
INSERT INTO `b_search_stem` VALUES (179,'СОВЕРШЕННОЛЕТ');
INSERT INTO `b_search_stem` VALUES (373,'СОГЛАС');
INSERT INTO `b_search_stem` VALUES (651,'СОГЛАСН');
INSERT INTO `b_search_stem` VALUES (793,'СОГЛАСОВА');
INSERT INTO `b_search_stem` VALUES (816,'СОГЛАША');
INSERT INTO `b_search_stem` VALUES (635,'СОГЛАШЕН');
INSERT INTO `b_search_stem` VALUES (963,'СОДЕЙСТВ');
INSERT INTO `b_search_stem` VALUES (716,'СОДЕРЖ');
INSERT INTO `b_search_stem` VALUES (352,'СОДЕРЖА');
INSERT INTO `b_search_stem` VALUES (849,'СОДЕРЖАН');
INSERT INTO `b_search_stem` VALUES (317,'СОДЕРЖАТ');
INSERT INTO `b_search_stem` VALUES (998,'СОЗДА');
INSERT INTO `b_search_stem` VALUES (1798,'СОЗДАН');
INSERT INTO `b_search_stem` VALUES (433,'СОМНЕН');
INSERT INTO `b_search_stem` VALUES (104,'СООБЩ');
INSERT INTO `b_search_stem` VALUES (646,'СООБЩА');
INSERT INTO `b_search_stem` VALUES (357,'СООБЩЕН');
INSERT INTO `b_search_stem` VALUES (67,'СООТВЕТСТВ');
INSERT INTO `b_search_stem` VALUES (695,'СООТВЕТСТВЕН');
INSERT INTO `b_search_stem` VALUES (129,'СООТВЕТСТВОВА');
INSERT INTO `b_search_stem` VALUES (1695,'СОПРОВОЖДЕН');
INSERT INTO `b_search_stem` VALUES (298,'СОСТАВЛ');
INSERT INTO `b_search_stem` VALUES (724,'СОСТАВЛЯ');
INSERT INTO `b_search_stem` VALUES (161,'СОТРУДНИК');
INSERT INTO `b_search_stem` VALUES (968,'СОХРАН');
INSERT INTO `b_search_stem` VALUES (974,'СОЦИАЛЬН');
INSERT INTO `b_search_stem` VALUES (601,'СОЧТЕТ');
INSERT INTO `b_search_stem` VALUES (1730,'СПЕКТР');
INSERT INTO `b_search_stem` VALUES (86,'СПЕЦИАЛИСТ');
INSERT INTO `b_search_stem` VALUES (13,'СПЕЦИАЛЬН');
INSERT INTO `b_search_stem` VALUES (680,'СПИСАН');
INSERT INTO `b_search_stem` VALUES (1725,'СПЛОЧЕН');
INSERT INTO `b_search_stem` VALUES (806,'СПОР');
INSERT INTO `b_search_stem` VALUES (805,'СПОРОВ');
INSERT INTO `b_search_stem` VALUES (891,'СПОСОБ');
INSERT INTO `b_search_stem` VALUES (1010,'СПРАВЕДЛИВ');
INSERT INTO `b_search_stem` VALUES (474,'СПРАВК');
INSERT INTO `b_search_stem` VALUES (146,'СРАЗ');
INSERT INTO `b_search_stem` VALUES (12,'СРЕДН');
INSERT INTO `b_search_stem` VALUES (44,'СРЕДСТВ');
INSERT INTO `b_search_stem` VALUES (172,'СРОК');
INSERT INTO `b_search_stem` VALUES (737,'СРОКОВ');
INSERT INTO `b_search_stem` VALUES (382,'ССЫЛК');
INSERT INTO `b_search_stem` VALUES (603,'СТ');
INSERT INTO `b_search_stem` VALUES (1737,'СТАВ');
INSERT INTO `b_search_stem` VALUES (450,'СТАВК');
INSERT INTO `b_search_stem` VALUES (1776,'СТАЛКИВА');
INSERT INTO `b_search_stem` VALUES (1715,'СТАНДАРТ');
INSERT INTO `b_search_stem` VALUES (222,'СТАТ');
INSERT INTO `b_search_stem` VALUES (981,'СТАТИСТИЧЕСК');
INSERT INTO `b_search_stem` VALUES (402,'СТАТУС');
INSERT INTO `b_search_stem` VALUES (466,'СТИПЕНД');
INSERT INTO `b_search_stem` VALUES (548,'СТОИМОСТ');
INSERT INTO `b_search_stem` VALUES (319,'СТОРОН');
INSERT INTO `b_search_stem` VALUES (862,'СТР');
INSERT INTO `b_search_stem` VALUES (525,'СТРАХОВ');
INSERT INTO `b_search_stem` VALUES (840,'СТРАХОВАН');
INSERT INTO `b_search_stem` VALUES (30,'СТРЕССОУСТОЙЧИВ');
INSERT INTO `b_search_stem` VALUES (456,'СТУДЕНТ');
INSERT INTO `b_search_stem` VALUES (453,'СТУДЕНЧЕСК');
INSERT INTO `b_search_stem` VALUES (469,'СУБСИД');
INSERT INTO `b_search_stem` VALUES (824,'СУБЪЕКТ');
INSERT INTO `b_search_stem` VALUES (934,'СУБЪЕКТОВ');
INSERT INTO `b_search_stem` VALUES (815,'СУД');
INSERT INTO `b_search_stem` VALUES (817,'СУДЕБН');
INSERT INTO `b_search_stem` VALUES (165,'СУММ');
INSERT INTO `b_search_stem` VALUES (413,'СУПРУГ');
INSERT INTO `b_search_stem` VALUES (511,'СУТОК');
INSERT INTO `b_search_stem` VALUES (353,'СУЩЕСТВЕН');
INSERT INTO `b_search_stem` VALUES (507,'СФЕР');
INSERT INTO `b_search_stem` VALUES (189,'СЧЕТ');
INSERT INTO `b_search_stem` VALUES (687,'СЧИТА');
INSERT INTO `b_search_stem` VALUES (1278,'Сведения');
INSERT INTO `b_search_stem` VALUES (1283,'Свидетельство');
INSERT INTO `b_search_stem` VALUES (1920,'Семантика');
INSERT INTO `b_search_stem` VALUES (1854,'Слайд');
INSERT INTO `b_search_stem` VALUES (1855,'Слайдер');
INSERT INTO `b_search_stem` VALUES (1639,'Согласии');
INSERT INTO `b_search_stem` VALUES (1614,'Согласно');
INSERT INTO `b_search_stem` VALUES (1307,'Специалисты');
INSERT INTO `b_search_stem` VALUES (1291,'Справка');
INSERT INTO `b_search_stem` VALUES (1333,'Срок');
INSERT INTO `b_search_stem` VALUES (1617,'Стороны');
INSERT INTO `b_search_stem` VALUES (320,'ТАКЖ');
INSERT INTO `b_search_stem` VALUES (5,'ТВЕРСК');
INSERT INTO `b_search_stem` VALUES (861,'ТВЕРСКАЯ-ЯМСК');
INSERT INTO `b_search_stem` VALUES (295,'ТЕКСТ');
INSERT INTO `b_search_stem` VALUES (375,'ТЕЛЕФОН');
INSERT INTO `b_search_stem` VALUES (565,'ТЕМ');
INSERT INTO `b_search_stem` VALUES (1746,'ТЕПЕР');
INSERT INTO `b_search_stem` VALUES (325,'ТЕРМИН');
INSERT INTO `b_search_stem` VALUES (217,'ТЕРМИНАЛ');
INSERT INTO `b_search_stem` VALUES (394,'ТЕРРИТОР');
INSERT INTO `b_search_stem` VALUES (962,'ТЕРРОРИЗМ');
INSERT INTO `b_search_stem` VALUES (21,'ТЕХНИК');
INSERT INTO `b_search_stem` VALUES (1026,'ТЕХНИЧЕСК');
INSERT INTO `b_search_stem` VALUES (155,'ТЕЧЕН');
INSERT INTO `b_search_stem` VALUES (992,'ТИПОВ');
INSERT INTO `b_search_stem` VALUES (68,'ТК');
INSERT INTO `b_search_stem` VALUES (522,'ТОВАР');
INSERT INTO `b_search_stem` VALUES (521,'ТОВАРОВ');
INSERT INTO `b_search_stem` VALUES (528,'ТОМ');
INSERT INTO `b_search_stem` VALUES (594,'ТОЧН');
INSERT INTO `b_search_stem` VALUES (600,'ТОЧНОСТ');
INSERT INTO `b_search_stem` VALUES (892,'ТРАНСГРАНИЧН');
INSERT INTO `b_search_stem` VALUES (650,'ТРЕБ');
INSERT INTO `b_search_stem` VALUES (9,'ТРЕБОВАН');
INSERT INTO `b_search_stem` VALUES (408,'ТРЕТ');
INSERT INTO `b_search_stem` VALUES (848,'ТРЕТЕЙСК');
INSERT INTO `b_search_stem` VALUES (679,'ТРЕХ');
INSERT INTO `b_search_stem` VALUES (732,'ТРЕХМЕСЯЧН');
INSERT INTO `b_search_stem` VALUES (726,'ТРИ');
INSERT INTO `b_search_stem` VALUES (578,'ТРИДЦАТ');
INSERT INTO `b_search_stem` VALUES (1652,'ТРК');
INSERT INTO `b_search_stem` VALUES (984,'ТРУД');
INSERT INTO `b_search_stem` VALUES (935,'ТРУДОВ');
INSERT INTO `b_search_stem` VALUES (964,'ТРУДОУСТРОЙСТВ');
INSERT INTO `b_search_stem` VALUES (1898,'ТРЦ');
INSERT INTO `b_search_stem` VALUES (539,'ТУР');
INSERT INTO `b_search_stem` VALUES (1645,'ТЦ');
INSERT INTO `b_search_stem` VALUES (1085,'Термины');
INSERT INTO `b_search_stem` VALUES (1669,'Тестовая');
INSERT INTO `b_search_stem` VALUES (1480,'Требования');
INSERT INTO `b_search_stem` VALUES (1860,'У');
INSERT INTO `b_search_stem` VALUES (910,'УБЫТК');
INSERT INTO `b_search_stem` VALUES (909,'УБЫТКОВ');
INSERT INTO `b_search_stem` VALUES (767,'УВЕДОМ');
INSERT INTO `b_search_stem` VALUES (626,'УВЕДОМЛЕН');
INSERT INTO `b_search_stem` VALUES (748,'УВЕДОМЛЯ');
INSERT INTO `b_search_stem` VALUES (1800,'УВЕЛИЧЕН');
INSERT INTO `b_search_stem` VALUES (231,'УВЕР');
INSERT INTO `b_search_stem` VALUES (14,'УВЕРЕН');
INSERT INTO `b_search_stem` VALUES (1812,'УДАЛЕН');
INSERT INTO `b_search_stem` VALUES (774,'УДЕРЖИВА');
INSERT INTO `b_search_stem` VALUES (137,'УДОБН');
INSERT INTO `b_search_stem` VALUES (434,'УДОВЛЕТВОРЯ');
INSERT INTO `b_search_stem` VALUES (498,'УДОСТОВЕРЕН');
INSERT INTO `b_search_stem` VALUES (832,'УДОСТОВЕРЯ');
INSERT INTO `b_search_stem` VALUES (237,'УКАЗА');
INSERT INTO `b_search_stem` VALUES (587,'УКАЗАН');
INSERT INTO `b_search_stem` VALUES (1829,'УКАЗАТЕЛ');
INSERT INTO `b_search_stem` VALUES (700,'УКАЗЫВА');
INSERT INTO `b_search_stem` VALUES (4,'УЛ');
INSERT INTO `b_search_stem` VALUES (337,'УЛИЦ');
INSERT INTO `b_search_stem` VALUES (1789,'УЛУЧШЕН');
INSERT INTO `b_search_stem` VALUES (1741,'УНИКАЛЬН');
INSERT INTO `b_search_stem` VALUES (898,'УНИЧТОЖЕН');
INSERT INTO `b_search_stem` VALUES (343,'УПЛАТ');
INSERT INTO `b_search_stem` VALUES (491,'УПЛАЧ');
INSERT INTO `b_search_stem` VALUES (689,'УПЛАЧИВА');
INSERT INTO `b_search_stem` VALUES (993,'УПРАВЛЕНЧЕСК');
INSERT INTO `b_search_stem` VALUES (1656,'УР');
INSERT INTO `b_search_stem` VALUES (1796,'УРОВН');
INSERT INTO `b_search_stem` VALUES (121,'УСЛОВ');
INSERT INTO `b_search_stem` VALUES (246,'УСЛУГ');
INSERT INTO `b_search_stem` VALUES (243,'УСПЕВА');
INSERT INTO `b_search_stem` VALUES (1770,'УСПЕХ');
INSERT INTO `b_search_stem` VALUES (958,'УСТАВ');
INSERT INTO `b_search_stem` VALUES (550,'УСТАНАВЛИВА');
INSERT INTO `b_search_stem` VALUES (999,'УСТАНОВЛ');
INSERT INTO `b_search_stem` VALUES (568,'УСТАНОВЛЕН');
INSERT INTO `b_search_stem` VALUES (430,'УСЫНОВИТЕЛ');
INSERT INTO `b_search_stem` VALUES (431,'УСЫНОВЛЕН');
INSERT INTO `b_search_stem` VALUES (990,'УТВЕРЖДЕН');
INSERT INTO `b_search_stem` VALUES (883,'УТОЧНЕН');
INSERT INTO `b_search_stem` VALUES (937,'УЧАСТНИК');
INSERT INTO `b_search_stem` VALUES (458,'УЧЕБН');
INSERT INTO `b_search_stem` VALUES (985,'УЧЕТ');
INSERT INTO `b_search_stem` VALUES (641,'УЧЕТН');
INSERT INTO `b_search_stem` VALUES (758,'УЧИТЫВА');
INSERT INTO `b_search_stem` VALUES (893,'УЧРЕЖДЕН');
INSERT INTO `b_search_stem` VALUES (476,'ФАКТ');
INSERT INTO `b_search_stem` VALUES (277,'ФАКТИЧЕСК');
INSERT INTO `b_search_stem` VALUES (827,'ФАМИЛ');
INSERT INTO `b_search_stem` VALUES (1830,'ФАСАД');
INSERT INTO `b_search_stem` VALUES (299,'ФЕДЕРАЛЬН');
INSERT INTO `b_search_stem` VALUES (311,'ФЕДЕРАЦ');
INSERT INTO `b_search_stem` VALUES (604,'ФЗ');
INSERT INTO `b_search_stem` VALUES (35,'ФИЗИЧЕСК');
INSERT INTO `b_search_stem` VALUES (1720,'ФИКСИРОВА');
INSERT INTO `b_search_stem` VALUES (1650,'ФИЛИАЛ');
INSERT INTO `b_search_stem` VALUES (1794,'ФИНАНС');
INSERT INTO `b_search_stem` VALUES (961,'ФИНАНСИРОВАН');
INSERT INTO `b_search_stem` VALUES (723,'ФИНАНСОВ');
INSERT INTO `b_search_stem` VALUES (463,'ФОРМ');
INSERT INTO `b_search_stem` VALUES (975,'ФОРМИРОВАН');
INSERT INTO `b_search_stem` VALUES (950,'ФУНКЦ');
INSERT INTO `b_search_stem` VALUES (1047,'Федерального');
INSERT INTO `b_search_stem` VALUES (1609,'Федеральным');
INSERT INTO `b_search_stem` VALUES (1514,'Федеральными');
INSERT INTO `b_search_stem` VALUES (1156,'Федерации');
INSERT INTO `b_search_stem` VALUES (1852,'Франшиза');
INSERT INTO `b_search_stem` VALUES (785,'ХРАН');
INSERT INTO `b_search_stem` VALUES (39,'ХРАНЕН');
INSERT INTO `b_search_stem` VALUES (290,'ЦЕЛ');
INSERT INTO `b_search_stem` VALUES (43,'ЦЕЛОСТН');
INSERT INTO `b_search_stem` VALUES (1819,'ЦЕНТРАЛЬН');
INSERT INTO `b_search_stem` VALUES (1621,'ЦМ');
INSERT INTO `b_search_stem` VALUES (157,'ЧАС');
INSERT INTO `b_search_stem` VALUES (156,'ЧАСОВ');
INSERT INTO `b_search_stem` VALUES (851,'ЧАСТ');
INSERT INTO `b_search_stem` VALUES (241,'ЧАСТИЧН');
INSERT INTO `b_search_stem` VALUES (515,'ЧАСТН');
INSERT INTO `b_search_stem` VALUES (959,'ЧАСТНОСТ');
INSERT INTO `b_search_stem` VALUES (929,'ЧЕЛОВЕК');
INSERT INTO `b_search_stem` VALUES (215,'ЧЕРЕЗ');
INSERT INTO `b_search_stem` VALUES (1785,'ЧЕСТН');
INSERT INTO `b_search_stem` VALUES (1775,'ЧЕСТНОСТ');
INSERT INTO `b_search_stem` VALUES (529,'ЧИСЛ');
INSERT INTO `b_search_stem` VALUES (911,'ЧУЖ');
INSERT INTO `b_search_stem` VALUES (776,'ШЕСТ');
INSERT INTO `b_search_stem` VALUES (579,'ШЕСТИДЕСЯТ');
INSERT INTO `b_search_stem` VALUES (1729,'ШИРОК');
INSERT INTO `b_search_stem` VALUES (562,'ШТРАФ');
INSERT INTO `b_search_stem` VALUES (865,'ЭКВИФАКС');
INSERT INTO `b_search_stem` VALUES (508,'ЭКСПРЕСС');
INSERT INTO `b_search_stem` VALUES (1683,'ЭЛЕКТРОЗАВОДСК');
INSERT INTO `b_search_stem` VALUES (356,'ЭЛЕКТРОН');
INSERT INTO `b_search_stem` VALUES (1647,'ЭТАЖ');
INSERT INTO `b_search_stem` VALUES (1885,'Экспресс');
INSERT INTO `b_search_stem` VALUES (1896,'Эльбрус');
INSERT INTO `b_search_stem` VALUES (338,'ЮБИЛЕЙН');
INSERT INTO `b_search_stem` VALUES (894,'ЮРИДИЧЕСК');
INSERT INTO `b_search_stem` VALUES (296,'ЯВЛЯ');
INSERT INTO `b_search_stem` VALUES (1398,'а');
INSERT INTO `b_search_stem` VALUES (1629,'агентам');
INSERT INTO `b_search_stem` VALUES (1137,'агентского');
INSERT INTO `b_search_stem` VALUES (1083,'адресу');
INSERT INTO `b_search_stem` VALUES (1184,'акте');
INSERT INTO `b_search_stem` VALUES (1309,'анализ');
INSERT INTO `b_search_stem` VALUES (1239,'анкета');
INSERT INTO `b_search_stem` VALUES (1219,'анкету');
INSERT INTO `b_search_stem` VALUES (1231,'анкеты');
INSERT INTO `b_search_stem` VALUES (1628,'аффилированным');
INSERT INTO `b_search_stem` VALUES (1673,'аыва');
INSERT INTO `b_search_stem` VALUES (1444,'банке');
INSERT INTO `b_search_stem` VALUES (1919,'берегу');
INSERT INTO `b_search_stem` VALUES (1331,'бизнеса');
INSERT INTO `b_search_stem` VALUES (1930,'более');
INSERT INTO `b_search_stem` VALUES (1864,'больше');
INSERT INTO `b_search_stem` VALUES (1921,'большого');
INSERT INTO `b_search_stem` VALUES (1938,'бренду');
INSERT INTO `b_search_stem` VALUES (1240,'будет');
INSERT INTO `b_search_stem` VALUES (1917,'буквенных');
INSERT INTO `b_search_stem` VALUES (1355,'был');
INSERT INTO `b_search_stem` VALUES (1401,'быть');
INSERT INTO `b_search_stem` VALUES (1634,'бюро');
INSERT INTO `b_search_stem` VALUES (1043,'в');
INSERT INTO `b_search_stem` VALUES (1568,'валюте');
INSERT INTO `b_search_stem` VALUES (1878,'вас');
INSERT INTO `b_search_stem` VALUES (1282,'вида');
INSERT INTO `b_search_stem` VALUES (1326,'видам');
INSERT INTO `b_search_stem` VALUES (1267,'видов');
INSERT INTO `b_search_stem` VALUES (1520,'включая');
INSERT INTO `b_search_stem` VALUES (1451,'внесении');
INSERT INTO `b_search_stem` VALUES (1270,'водительское');
INSERT INTO `b_search_stem` VALUES (1591,'возврат');
INSERT INTO `b_search_stem` VALUES (1359,'возврата');
INSERT INTO `b_search_stem` VALUES (1584,'возвратившему');
INSERT INTO `b_search_stem` VALUES (1526,'возвратить');
INSERT INTO `b_search_stem` VALUES (1415,'возвратом');
INSERT INTO `b_search_stem` VALUES (1405,'возможности');
INSERT INTO `b_search_stem` VALUES (1310,'возможную');
INSERT INTO `b_search_stem` VALUES (1575,'вознаграждение');
INSERT INTO `b_search_stem` VALUES (1264,'вопроса');
INSERT INTO `b_search_stem` VALUES (1894,'воскресенье');
INSERT INTO `b_search_stem` VALUES (1336,'вправе');
INSERT INTO `b_search_stem` VALUES (1936,'всей');
INSERT INTO `b_search_stem` VALUES (1062,'всем');
INSERT INTO `b_search_stem` VALUES (1266,'всех');
INSERT INTO `b_search_stem` VALUES (1463,'вторую');
INSERT INTO `b_search_stem` VALUES (1277,'выбор');
INSERT INTO `b_search_stem` VALUES (1130,'выгоде');
INSERT INTO `b_search_stem` VALUES (1942,'выгодных');
INSERT INTO `b_search_stem` VALUES (1162,'выгодоприобретателях');
INSERT INTO `b_search_stem` VALUES (1421,'выдаваемый');
INSERT INTO `b_search_stem` VALUES (1565,'выдавать');
INSERT INTO `b_search_stem` VALUES (1356,'выдан');
INSERT INTO `b_search_stem` VALUES (1364,'выданному');
INSERT INTO `b_search_stem` VALUES (1322,'выдаче');
INSERT INTO `b_search_stem` VALUES (1424,'выплат');
INSERT INTO `b_search_stem` VALUES (1892,'вых');
INSERT INTO `b_search_stem` VALUES (1616,'выше');
INSERT INTO `b_search_stem` VALUES (1206,'выясняет');
INSERT INTO `b_search_stem` VALUES (1641,'г');
INSERT INTO `b_search_stem` VALUES (1856,'главной');
INSERT INTO `b_search_stem` VALUES (1910,'гласных');
INSERT INTO `b_search_stem` VALUES (1933,'года');
INSERT INTO `b_search_stem` VALUES (1908,'горами');
INSERT INTO `b_search_stem` VALUES (1187,'государственного');
INSERT INTO `b_search_stem` VALUES (1491,'гражданско-правовой');
INSERT INTO `b_search_stem` VALUES (1420,'график');
INSERT INTO `b_search_stem` VALUES (1447,'графиком');
INSERT INTO `b_search_stem` VALUES (1230,'графы');
INSERT INTO `b_search_stem` VALUES (1869,'дадим');
INSERT INTO `b_search_stem` VALUES (1603,'дает');
INSERT INTO `b_search_stem` VALUES (1350,'данные');
INSERT INTO `b_search_stem` VALUES (1374,'данный');
INSERT INTO `b_search_stem` VALUES (1552,'данных');
INSERT INTO `b_search_stem` VALUES (1577,'действия');
INSERT INTO `b_search_stem` VALUES (1194,'действовать');
INSERT INTO `b_search_stem` VALUES (1132,'действует');
INSERT INTO `b_search_stem` VALUES (1305,'действующего');
INSERT INTO `b_search_stem` VALUES (1175,'действующее');
INSERT INTO `b_search_stem` VALUES (1486,'денежные');
INSERT INTO `b_search_stem` VALUES (1438,'денежными');
INSERT INTO `b_search_stem` VALUES (1452,'денежных');
INSERT INTO `b_search_stem` VALUES (1875,'деньги');
INSERT INTO `b_search_stem` VALUES (1535,'десять');
INSERT INTO `b_search_stem` VALUES (1054,'деятельности');
INSERT INTO `b_search_stem` VALUES (1541,'директором');
INSERT INTO `b_search_stem` VALUES (1064,'для');
INSERT INTO `b_search_stem` VALUES (1537,'дней');
INSERT INTO `b_search_stem` VALUES (1403,'до');
INSERT INTO `b_search_stem` VALUES (1494,'добровольно');
INSERT INTO `b_search_stem` VALUES (1181,'доверенности');
INSERT INTO `b_search_stem` VALUES (1142,'доверительного');
INSERT INTO `b_search_stem` VALUES (1108,'договор');
INSERT INTO `b_search_stem` VALUES (1138,'договора');
INSERT INTO `b_search_stem` VALUES (1573,'договорам');
INSERT INTO `b_search_stem` VALUES (1430,'договорах');
INSERT INTO `b_search_stem` VALUES (1139,'договоров');
INSERT INTO `b_search_stem` VALUES (1098,'договором');
INSERT INTO `b_search_stem` VALUES (1425,'договору');
INSERT INTO `b_search_stem` VALUES (1638,'документа');
INSERT INTO `b_search_stem` VALUES (1168,'документов');
INSERT INTO `b_search_stem` VALUES (1258,'документы');
INSERT INTO `b_search_stem` VALUES (1300,'долгов');
INSERT INTO `b_search_stem` VALUES (1200,'должен');
INSERT INTO `b_search_stem` VALUES (1947,'дом');
INSERT INTO `b_search_stem` VALUES (1918,'домах');
INSERT INTO `b_search_stem` VALUES (1523,'досрочно');
INSERT INTO `b_search_stem` VALUES (1590,'досрочный');
INSERT INTO `b_search_stem` VALUES (1164,'достоверности');
INSERT INTO `b_search_stem` VALUES (1518,'достоверную');
INSERT INTO `b_search_stem` VALUES (1072,'доступном');
INSERT INTO `b_search_stem` VALUES (1061,'доступны');
INSERT INTO `b_search_stem` VALUES (1279,'доходах');
INSERT INTO `b_search_stem` VALUES (1120,'е');
INSERT INTO `b_search_stem` VALUES (1195,'его');
INSERT INTO `b_search_stem` VALUES (1248,'ее');
INSERT INTO `b_search_stem` VALUES (1222,'ему');
INSERT INTO `b_search_stem` VALUES (1594,'если');
INSERT INTO `b_search_stem` VALUES (1225,'есть');
INSERT INTO `b_search_stem` VALUES (1400,'же');
INSERT INTO `b_search_stem` VALUES (1912,'живут');
INSERT INTO `b_search_stem` VALUES (1858,'жизни');
INSERT INTO `b_search_stem` VALUES (1332,'жительства');
INSERT INTO `b_search_stem` VALUES (1376,'журнале');
INSERT INTO `b_search_stem` VALUES (1483,'за');
INSERT INTO `b_search_stem` VALUES (1171,'заверенных');
INSERT INTO `b_search_stem` VALUES (1281,'зависимости');
INSERT INTO `b_search_stem` VALUES (1335,'зависит');
INSERT INTO `b_search_stem` VALUES (1092,'заем');
INSERT INTO `b_search_stem` VALUES (1217,'заемщик');
INSERT INTO `b_search_stem` VALUES (1596,'заемщика');
INSERT INTO `b_search_stem` VALUES (1850,'заемщикам');
INSERT INTO `b_search_stem` VALUES (1095,'заемщику');
INSERT INTO `b_search_stem` VALUES (1076,'заинтересованного');
INSERT INTO `b_search_stem` VALUES (1545,'заинтересованных');
INSERT INTO `b_search_stem` VALUES (1099,'займа');
INSERT INTO `b_search_stem` VALUES (1863,'займет');
INSERT INTO `b_search_stem` VALUES (1903,'займов');
INSERT INTO `b_search_stem` VALUES (1094,'займодавцем');
INSERT INTO `b_search_stem` VALUES (1365,'займу');
INSERT INTO `b_search_stem` VALUES (1566,'займы');
INSERT INTO `b_search_stem` VALUES (1388,'заключает');
INSERT INTO `b_search_stem` VALUES (1380,'заключение');
INSERT INTO `b_search_stem` VALUES (1370,'заключению');
INSERT INTO `b_search_stem` VALUES (1389,'заключения');
INSERT INTO `b_search_stem` VALUES (1121,'заключившее');
INSERT INTO `b_search_stem` VALUES (1048,'закона');
INSERT INTO `b_search_stem` VALUES (1515,'законами');
INSERT INTO `b_search_stem` VALUES (1182,'законе');
INSERT INTO `b_search_stem` VALUES (1306,'законодательства');
INSERT INTO `b_search_stem` VALUES (1154,'законодательством');
INSERT INTO `b_search_stem` VALUES (1610,'законом');
INSERT INTO `b_search_stem` VALUES (1234,'заполнению');
INSERT INTO `b_search_stem` VALUES (1218,'заполняет');
INSERT INTO `b_search_stem` VALUES (1235,'заполняются');
INSERT INTO `b_search_stem` VALUES (1260,'запрашиваемые');
INSERT INTO `b_search_stem` VALUES (1940,'зарабатывать');
INSERT INTO `b_search_stem` VALUES (1926,'зарплаты');
INSERT INTO `b_search_stem` VALUES (1454,'зачисления');
INSERT INTO `b_search_stem` VALUES (1126,'заявка');
INSERT INTO `b_search_stem` VALUES (1198,'заявки');
INSERT INTO `b_search_stem` VALUES (1506,'заявку');
INSERT INTO `b_search_stem` VALUES (1378,'заявок');
INSERT INTO `b_search_stem` VALUES (1210,'знакомит');
INSERT INTO `b_search_stem` VALUES (1038,'и');
INSERT INTO `b_search_stem` VALUES (1476,'идентификации');
INSERT INTO `b_search_stem` VALUES (1275,'из');
INSERT INTO `b_search_stem` VALUES (1458,'издержки');
INSERT INTO `b_search_stem` VALUES (1406,'изменения');
INSERT INTO `b_search_stem` VALUES (1570,'изменять');
INSERT INTO `b_search_stem` VALUES (1114,'или');
INSERT INTO `b_search_stem` VALUES (1404,'им');
INSERT INTO `b_search_stem` VALUES (1176,'имени');
INSERT INTO `b_search_stem` VALUES (1366,'имеются');
INSERT INTO `b_search_stem` VALUES (1148,'имуществом');
INSERT INTO `b_search_stem` VALUES (1238,'иначе');
INSERT INTO `b_search_stem` VALUES (1582,'индивидуальному');
INSERT INTO `b_search_stem` VALUES (1407,'инициативе');
INSERT INTO `b_search_stem` VALUES (1448,'ином');
INSERT INTO `b_search_stem` VALUES (1567,'иностранной');
INSERT INTO `b_search_stem` VALUES (1177,'интересах');
INSERT INTO `b_search_stem` VALUES (1193,'интересы');
INSERT INTO `b_search_stem` VALUES (1543,'информационном');
INSERT INTO `b_search_stem` VALUES (1519,'информацию');
INSERT INTO `b_search_stem` VALUES (1384,'информируется');
INSERT INTO `b_search_stem` VALUES (1367,'иные');
INSERT INTO `b_search_stem` VALUES (1513,'иными');
INSERT INTO `b_search_stem` VALUES (1461,'исполнения');
INSERT INTO `b_search_stem` VALUES (1166,'использованием');
INSERT INTO `b_search_stem` VALUES (1636,'использования');
INSERT INTO `b_search_stem` VALUES (1360,'истек');
INSERT INTO `b_search_stem` VALUES (1349,'истории');
INSERT INTO `b_search_stem` VALUES (1353,'историй');
INSERT INTO `b_search_stem` VALUES (1160,'их');
INSERT INTO `b_search_stem` VALUES (1129,'к');
INSERT INTO `b_search_stem` VALUES (1436,'как');
INSERT INTO `b_search_stem` VALUES (1536,'календарных');
INSERT INTO `b_search_stem` VALUES (1625,'касающуюся');
INSERT INTO `b_search_stem` VALUES (1133,'клиент');
INSERT INTO `b_search_stem` VALUES (1174,'клиента');
INSERT INTO `b_search_stem` VALUES (1159,'клиентах');
INSERT INTO `b_search_stem` VALUES (1900,'клиентов');
INSERT INTO `b_search_stem` VALUES (1249,'клиентом');
INSERT INTO `b_search_stem` VALUES (1323,'клиенту');
INSERT INTO `b_search_stem` VALUES (1502,'кодекса');
INSERT INTO `b_search_stem` VALUES (1901,'количество');
INSERT INTO `b_search_stem` VALUES (1633,'коллекторские');
INSERT INTO `b_search_stem` VALUES (1141,'комиссии');
INSERT INTO `b_search_stem` VALUES (1574,'комиссионное');
INSERT INTO `b_search_stem` VALUES (1301,'коммунальным');
INSERT INTO `b_search_stem` VALUES (1849,'компании');
INSERT INTO `b_search_stem` VALUES (1635,'конфиденциального');
INSERT INTO `b_search_stem` VALUES (1172,'копий');
INSERT INTO `b_search_stem` VALUES (1110,'которого');
INSERT INTO `b_search_stem` VALUES (1422,'котором');
INSERT INTO `b_search_stem` VALUES (1904,'которых');
INSERT INTO `b_search_stem` VALUES (1348,'кредитной');
INSERT INTO `b_search_stem` VALUES (1632,'кредитные');
INSERT INTO `b_search_stem` VALUES (1352,'кредитных');
INSERT INTO `b_search_stem` VALUES (1183,'либо');
INSERT INTO `b_search_stem` VALUES (1546,'лиц');
INSERT INTO `b_search_stem` VALUES (1077,'лица');
INSERT INTO `b_search_stem` VALUES (1063,'лицам');
INSERT INTO `b_search_stem` VALUES (1116,'лицо');
INSERT INTO `b_search_stem` VALUES (1581,'лицом');
INSERT INTO `b_search_stem` VALUES (1477,'личности');
INSERT INTO `b_search_stem` VALUES (1075,'любого');
INSERT INTO `b_search_stem` VALUES (1624,'любую');
INSERT INTO `b_search_stem` VALUES (1631,'любые');
INSERT INTO `b_search_stem` VALUES (1941,'максимально');
INSERT INTO `b_search_stem` VALUES (1372,'менеджер');
INSERT INTO `b_search_stem` VALUES (1381,'менеджера');
INSERT INTO `b_search_stem` VALUES (1261,'менеджером');
INSERT INTO `b_search_stem` VALUES (1251,'менеджеру');
INSERT INTO `b_search_stem` VALUES (1533,'менее');
INSERT INTO `b_search_stem` VALUES (1490,'мер');
INSERT INTO `b_search_stem` VALUES (1151,'мероприятий');
INSERT INTO `b_search_stem` VALUES (1292,'места');
INSERT INTO `b_search_stem` VALUES (1071,'месте');
INSERT INTO `b_search_stem` VALUES (1189,'местного');
INSERT INTO `b_search_stem` VALUES (1949,'месяцев');
INSERT INTO `b_search_stem` VALUES (1592,'микрозаем');
INSERT INTO `b_search_stem` VALUES (1357,'микрозайм');
INSERT INTO `b_search_stem` VALUES (1107,'микрозайма');
INSERT INTO `b_search_stem` VALUES (1035,'микрозаймов');
INSERT INTO `b_search_stem` VALUES (1593,'микрозаймы');
INSERT INTO `b_search_stem` VALUES (1948,'микрофинансирование');
INSERT INTO `b_search_stem` VALUES (1512,'микрофинансовой');
INSERT INTO `b_search_stem` VALUES (1586,'микрофинансовую');
INSERT INTO `b_search_stem` VALUES (1055,'микрофинансовых');
INSERT INTO `b_search_stem` VALUES (1104,'миллион');
INSERT INTO `b_search_stem` VALUES (1865,'минут');
INSERT INTO `b_search_stem` VALUES (1493,'могут');
INSERT INTO `b_search_stem` VALUES (1434,'может');
INSERT INTO `b_search_stem` VALUES (1338,'мотивированное');
INSERT INTO `b_search_stem` VALUES (1079,'на');
INSERT INTO `b_search_stem` VALUES (1169,'надлежащим');
INSERT INTO `b_search_stem` VALUES (1437,'наличными');
INSERT INTO `b_search_stem` VALUES (1532,'намерении');
INSERT INTO `b_search_stem` VALUES (1417,'нарушением');
INSERT INTO `b_search_stem` VALUES (1361,'нарушения');
INSERT INTO `b_search_stem` VALUES (1088,'настоящие');
INSERT INTO `b_search_stem` VALUES (1214,'настоящими');
INSERT INTO `b_search_stem` VALUES (1548,'настоящих');
INSERT INTO `b_search_stem` VALUES (1117,'находящееся');
INSERT INTO `b_search_stem` VALUES (1939,'начать');
INSERT INTO `b_search_stem` VALUES (1470,'начисленные');
INSERT INTO `b_search_stem` VALUES (1465,'начисленных');
INSERT INTO `b_search_stem` VALUES (1101,'не');
INSERT INTO `b_search_stem` VALUES (1147,'недвижимым');
INSERT INTO `b_search_stem` VALUES (1242,'недействительной');
INSERT INTO `b_search_stem` VALUES (1344,'недостоверных');
INSERT INTO `b_search_stem` VALUES (1325,'некоторым');
INSERT INTO `b_search_stem` VALUES (1427,'нему');
INSERT INTO `b_search_stem` VALUES (1280,'необходимости');
INSERT INTO `b_search_stem` VALUES (1311,'необходимую');
INSERT INTO `b_search_stem` VALUES (1262,'необходимые');
INSERT INTO `b_search_stem` VALUES (1212,'необходимых');
INSERT INTO `b_search_stem` VALUES (1482,'неустойки');
INSERT INTO `b_search_stem` VALUES (1256,'ниже');
INSERT INTO `b_search_stem` VALUES (1122,'ним');
INSERT INTO `b_search_stem` VALUES (1074,'ними');
INSERT INTO `b_search_stem` VALUES (1670,'новость');
INSERT INTO `b_search_stem` VALUES (1158,'о');
INSERT INTO `b_search_stem` VALUES (1298,'об');
INSERT INTO `b_search_stem` VALUES (1521,'обо');
INSERT INTO `b_search_stem` VALUES (1073,'обозрения');
INSERT INTO `b_search_stem` VALUES (1606,'обработку');
INSERT INTO `b_search_stem` VALUES (1170,'образом');
INSERT INTO `b_search_stem` VALUES (1221,'образца');
INSERT INTO `b_search_stem` VALUES (1123,'обратившееся');
INSERT INTO `b_search_stem` VALUES (1204,'обращении');
INSERT INTO `b_search_stem` VALUES (1414,'обслуживанием');
INSERT INTO `b_search_stem` VALUES (1118,'обслуживании');
INSERT INTO `b_search_stem` VALUES (1368,'обстоятельства');
INSERT INTO `b_search_stem` VALUES (1254,'обязан');
INSERT INTO `b_search_stem` VALUES (1504,'обязанности');
INSERT INTO `b_search_stem` VALUES (1395,'обязанностях');
INSERT INTO `b_search_stem` VALUES (1236,'обязательном');
INSERT INTO `b_search_stem` VALUES (1233,'обязательному');
INSERT INTO `b_search_stem` VALUES (1209,'обязательные');
INSERT INTO `b_search_stem` VALUES (1327,'обязательным');
INSERT INTO `b_search_stem` VALUES (1595,'обязательств');
INSERT INTO `b_search_stem` VALUES (1103,'один');
INSERT INTO `b_search_stem` VALUES (1932,'одного');
INSERT INTO `b_search_stem` VALUES (1569,'одностороннем');
INSERT INTO `b_search_stem` VALUES (1390,'ознакамливается');
INSERT INTO `b_search_stem` VALUES (1201,'ознакомиться');
INSERT INTO `b_search_stem` VALUES (1065,'ознакомления');
INSERT INTO `b_search_stem` VALUES (1923,'океана');
INSERT INTO `b_search_stem` VALUES (1227,'он');
INSERT INTO `b_search_stem` VALUES (1916,'они');
INSERT INTO `b_search_stem` VALUES (1845,'онлайн');
INSERT INTO `b_search_stem` VALUES (1146,'операций');
INSERT INTO `b_search_stem` VALUES (1086,'определения');
INSERT INTO `b_search_stem` VALUES (1153,'определенных');
INSERT INTO `b_search_stem` VALUES (1618,'определили');
INSERT INTO `b_search_stem` VALUES (1057,'определяют');
INSERT INTO `b_search_stem` VALUES (1554,'орган');
INSERT INTO `b_search_stem` VALUES (1188,'органа');
INSERT INTO `b_search_stem` VALUES (1598,'организацией');
INSERT INTO `b_search_stem` VALUES (1081,'организации');
INSERT INTO `b_search_stem` VALUES (1587,'организацию');
INSERT INTO `b_search_stem` VALUES (1564,'организация');
INSERT INTO `b_search_stem` VALUES (1056,'организациях');
INSERT INTO `b_search_stem` VALUES (1561,'органов');
INSERT INTO `b_search_stem` VALUES (1167,'оригиналов');
INSERT INTO `b_search_stem` VALUES (1136,'основании');
INSERT INTO `b_search_stem` VALUES (1180,'основанными');
INSERT INTO `b_search_stem` VALUES (1468,'основная');
INSERT INTO `b_search_stem` VALUES (1067,'основные');
INSERT INTO `b_search_stem` VALUES (1446,'осуществляется');
INSERT INTO `b_search_stem` VALUES (1435,'осуществляться');
INSERT INTO `b_search_stem` VALUES (1049,'от');
INSERT INTO `b_search_stem` VALUES (1867,'ответ');
INSERT INTO `b_search_stem` VALUES (1492,'ответственности');
INSERT INTO `b_search_stem` VALUES (1339,'отказе');
INSERT INTO `b_search_stem` VALUES (1347,'отрицательной');
INSERT INTO `b_search_stem` VALUES (1299,'отсутствии');
INSERT INTO `b_search_stem` VALUES (1557,'отчеты');
INSERT INTO `b_search_stem` VALUES (1644,'офис');
INSERT INTO `b_search_stem` VALUES (1078,'офисе');
INSERT INTO `b_search_stem` VALUES (1844,'оффлайн');
INSERT INTO `b_search_stem` VALUES (1316,'оценивают');
INSERT INTO `b_search_stem` VALUES (1456,'очередь');
INSERT INTO `b_search_stem` VALUES (1479,'п');
INSERT INTO `b_search_stem` VALUES (1935,'партнеров');
INSERT INTO `b_search_stem` VALUES (1887,'партнеры');
INSERT INTO `b_search_stem` VALUES (1471,'пени');
INSERT INTO `b_search_stem` VALUES (1272,'пенсионное');
INSERT INTO `b_search_stem` VALUES (1455,'первую');
INSERT INTO `b_search_stem` VALUES (1597,'перед');
INSERT INTO `b_search_stem` VALUES (1622,'передавать');
INSERT INTO `b_search_stem` VALUES (1250,'передачи');
INSERT INTO `b_search_stem` VALUES (1440,'перечислением');
INSERT INTO `b_search_stem` VALUES (1257,'перечисленные');
INSERT INTO `b_search_stem` VALUES (1411,'перечне');
INSERT INTO `b_search_stem` VALUES (1211,'перечнем');
INSERT INTO `b_search_stem` VALUES (1558,'персональном');
INSERT INTO `b_search_stem` VALUES (1607,'персональных');
INSERT INTO `b_search_stem` VALUES (1432,'печати');
INSERT INTO `b_search_stem` VALUES (1529,'письменно');
INSERT INTO `b_search_stem` VALUES (1302,'платежам');
INSERT INTO `b_search_stem` VALUES (1522,'платежах');
INSERT INTO `b_search_stem` VALUES (1413,'платежей');
INSERT INTO `b_search_stem` VALUES (1082,'по');
INSERT INTO `b_search_stem` VALUES (1464,'погашается');
INSERT INTO `b_search_stem` VALUES (1457,'погашаются');
INSERT INTO `b_search_stem` VALUES (1474,'погашении');
INSERT INTO `b_search_stem` VALUES (1391,'под');
INSERT INTO `b_search_stem` VALUES (1505,'подавший');
INSERT INTO `b_search_stem` VALUES (1245,'поданной');
INSERT INTO `b_search_stem` VALUES (1253,'подаче');
INSERT INTO `b_search_stem` VALUES (1197,'подачи');
INSERT INTO `b_search_stem` VALUES (1343,'поддельных');
INSERT INTO `b_search_stem` VALUES (1232,'подлежащие');
INSERT INTO `b_search_stem` VALUES (1247,'подписания');
INSERT INTO `b_search_stem` VALUES (1637,'подписанным');
INSERT INTO `b_search_stem` VALUES (1428,'подписываются');
INSERT INTO `b_search_stem` VALUES (1410,'подсудности');
INSERT INTO `b_search_stem` VALUES (1163,'подтверждению');
INSERT INTO `b_search_stem` VALUES (1297,'полис');
INSERT INTO `b_search_stem` VALUES (1620,'полного');
INSERT INTO `b_search_stem` VALUES (1179,'полномочиями');
INSERT INTO `b_search_stem` VALUES (1524,'полностью');
INSERT INTO `b_search_stem` VALUES (1517,'полную');
INSERT INTO `b_search_stem` VALUES (1037,'положения');
INSERT INTO `b_search_stem` VALUES (1640,'положениями');
INSERT INTO `b_search_stem` VALUES (1386,'положительном');
INSERT INTO `b_search_stem` VALUES (1516,'получать');
INSERT INTO `b_search_stem` VALUES (1334,'получение');
INSERT INTO `b_search_stem` VALUES (1397,'получением');
INSERT INTO `b_search_stem` VALUES (1460,'получению');
INSERT INTO `b_search_stem` VALUES (1124,'получения');
INSERT INTO `b_search_stem` VALUES (1902,'полученных');
INSERT INTO `b_search_stem` VALUES (1874,'получите');
INSERT INTO `b_search_stem` VALUES (1484,'пользование');
INSERT INTO `b_search_stem` VALUES (1140,'поручения');
INSERT INTO `b_search_stem` VALUES (1226,'поручитель');
INSERT INTO `b_search_stem` VALUES (1228,'поручительство');
INSERT INTO `b_search_stem` VALUES (1237,'порядке');
INSERT INTO `b_search_stem` VALUES (1393,'порядком');
INSERT INTO `b_search_stem` VALUES (1058,'порядок');
INSERT INTO `b_search_stem` VALUES (1330,'посещение');
INSERT INTO `b_search_stem` VALUES (1246,'после');
INSERT INTO `b_search_stem` VALUES (1929,'постоянных');
INSERT INTO `b_search_stem` VALUES (1394,'правах');
INSERT INTO `b_search_stem` VALUES (1539,'правил');
INSERT INTO `b_search_stem` VALUES (1041,'правила');
INSERT INTO `b_search_stem` VALUES (1508,'правилами');
INSERT INTO `b_search_stem` VALUES (1600,'превысит');
INSERT INTO `b_search_stem` VALUES (1111,'превышает');
INSERT INTO `b_search_stem` VALUES (1102,'превышающей');
INSERT INTO `b_search_stem` VALUES (1528,'предварительно');
INSERT INTO `b_search_stem` VALUES (1216,'предлагаемые');
INSERT INTO `b_search_stem` VALUES (1255,'предоставить');
INSERT INTO `b_search_stem` VALUES (1127,'предоставление');
INSERT INTO `b_search_stem` VALUES (1265,'предоставлении');
INSERT INTO `b_search_stem` VALUES (1034,'предоставления');
INSERT INTO `b_search_stem` VALUES (1093,'предоставляемый');
INSERT INTO `b_search_stem` VALUES (1510,'предоставлять');
INSERT INTO `b_search_stem` VALUES (1583,'предпринимателю');
INSERT INTO `b_search_stem` VALUES (1429,'представителем');
INSERT INTO `b_search_stem` VALUES (1205,'представитель');
INSERT INTO `b_search_stem` VALUES (1161,'представителях');
INSERT INTO `b_search_stem` VALUES (1475,'представить');
INSERT INTO `b_search_stem` VALUES (1313,'представленных');
INSERT INTO `b_search_stem` VALUES (1192,'представлять');
INSERT INTO `b_search_stem` VALUES (1556,'представляются');
INSERT INTO `b_search_stem` VALUES (1608,'предусмотренном');
INSERT INTO `b_search_stem` VALUES (1097,'предусмотренных');
INSERT INTO `b_search_stem` VALUES (1369,'препятствующие');
INSERT INTO `b_search_stem` VALUES (1144,'при');
INSERT INTO `b_search_stem` VALUES (1489,'применением');
INSERT INTO `b_search_stem` VALUES (1578,'применять');
INSERT INTO `b_search_stem` VALUES (1320,'принимается');
INSERT INTO `b_search_stem` VALUES (1371,'принятия');
INSERT INTO `b_search_stem` VALUES (1337,'принять');
INSERT INTO `b_search_stem` VALUES (1937,'присоединиться');
INSERT INTO `b_search_stem` VALUES (1383,'причины');
INSERT INTO `b_search_stem` VALUES (1312,'проверку');
INSERT INTO `b_search_stem` VALUES (1308,'проводят');
INSERT INTO `b_search_stem` VALUES (1287,'проживающих');
INSERT INTO `b_search_stem` VALUES (1402,'проинформирован');
INSERT INTO `b_search_stem` VALUES (1571,'процентные');
INSERT INTO `b_search_stem` VALUES (1426,'процентов');
INSERT INTO `b_search_stem` VALUES (1550,'публикуются');
INSERT INTO `b_search_stem` VALUES (1293,'работы');
INSERT INTO `b_search_stem` VALUES (1379,'разделе');
INSERT INTO `b_search_stem` VALUES (1412,'размере');
INSERT INTO `b_search_stem` VALUES (1070,'размещается');
INSERT INTO `b_search_stem` VALUES (1542,'размещаются');
INSERT INTO `b_search_stem` VALUES (1042,'разработаны');
INSERT INTO `b_search_stem` VALUES (1208,'разъясняет');
INSERT INTO `b_search_stem` VALUES (1363,'ранее');
INSERT INTO `b_search_stem` VALUES (1623,'раскрывать');
INSERT INTO `b_search_stem` VALUES (1252,'рассмотрения');
INSERT INTO `b_search_stem` VALUES (1377,'регистрации');
INSERT INTO `b_search_stem` VALUES (1385,'результатах');
INSERT INTO `b_search_stem` VALUES (1321,'решение');
INSERT INTO `b_search_stem` VALUES (1387,'решении');
INSERT INTO `b_search_stem` VALUES (1263,'решения');
INSERT INTO `b_search_stem` VALUES (1392,'роспись');
INSERT INTO `b_search_stem` VALUES (1105,'рублей');
INSERT INTO `b_search_stem` VALUES (1560,'руководящих');
INSERT INTO `b_search_stem` VALUES (1913,'рыбные');
INSERT INTO `b_search_stem` VALUES (1045,'с');
INSERT INTO `b_search_stem` VALUES (1080,'сайте');
INSERT INTO `b_search_stem` VALUES (1190,'самоуправления');
INSERT INTO `b_search_stem` VALUES (1589,'санкции');
INSERT INTO `b_search_stem` VALUES (1157,'сведений');
INSERT INTO `b_search_stem` VALUES (1259,'сведения');
INSERT INTO `b_search_stem` VALUES (1274,'свидетельство');
INSERT INTO `b_search_stem` VALUES (1627,'своим');
INSERT INTO `b_search_stem` VALUES (1488,'связанные');
INSERT INTO `b_search_stem` VALUES (1396,'связанных');
INSERT INTO `b_search_stem` VALUES (1944,'сейчас');
INSERT INTO `b_search_stem` VALUES (1290,'семьи');
INSERT INTO `b_search_stem` VALUES (1453,'следующий');
INSERT INTO `b_search_stem` VALUES (1341,'следующим');
INSERT INTO `b_search_stem` VALUES (1907,'словесными');
INSERT INTO `b_search_stem` VALUES (1346,'случае');
INSERT INTO `b_search_stem` VALUES (1857,'случаи');
INSERT INTO `b_search_stem` VALUES (1340,'со');
INSERT INTO `b_search_stem` VALUES (1223,'собственноручно');
INSERT INTO `b_search_stem` VALUES (1145,'совершении');
INSERT INTO `b_search_stem` VALUES (1288,'совершеннолетних');
INSERT INTO `b_search_stem` VALUES (1286,'совместно');
INSERT INTO `b_search_stem` VALUES (1150,'совокупность');
INSERT INTO `b_search_stem` VALUES (1604,'согласие');
INSERT INTO `b_search_stem` VALUES (1215,'согласии');
INSERT INTO `b_search_stem` VALUES (1911,'согласных');
INSERT INTO `b_search_stem` VALUES (1066,'содержат');
INSERT INTO `b_search_stem` VALUES (1044,'соответствии');
INSERT INTO `b_search_stem` VALUES (1559,'составе');
INSERT INTO `b_search_stem` VALUES (1905,'составляет');
INSERT INTO `b_search_stem` VALUES (1318,'состояние');
INSERT INTO `b_search_stem` VALUES (1879,'способом');
INSERT INTO `b_search_stem` VALUES (1441,'средств');
INSERT INTO `b_search_stem` VALUES (1439,'средствами');
INSERT INTO `b_search_stem` VALUES (1358,'срок');
INSERT INTO `b_search_stem` VALUES (1576,'сроки');
INSERT INTO `b_search_stem` VALUES (1499,'ст');
INSERT INTO `b_search_stem` VALUES (1572,'ставки');
INSERT INTO `b_search_stem` VALUES (1431,'ставятся');
INSERT INTO `b_search_stem` VALUES (1544,'стенде');
INSERT INTO `b_search_stem` VALUES (1450,'сторонами');
INSERT INTO `b_search_stem` VALUES (1909,'стране');
INSERT INTO `b_search_stem` VALUES (1409,'страхования');
INSERT INTO `b_search_stem` VALUES (1273,'страховое');
INSERT INTO `b_search_stem` VALUES (1296,'страховой');
INSERT INTO `b_search_stem` VALUES (1893,'суббота');
INSERT INTO `b_search_stem` VALUES (1109,'сумма');
INSERT INTO `b_search_stem` VALUES (1100,'сумме');
INSERT INTO `b_search_stem` VALUES (1466,'сумму');
INSERT INTO `b_search_stem` VALUES (1443,'счет');
INSERT INTO `b_search_stem` VALUES (1244,'считается');
INSERT INTO `b_search_stem` VALUES (1241,'считаться');
INSERT INTO `b_search_stem` VALUES (1119,'т');
INSERT INTO `b_search_stem` VALUES (1399,'так');
INSERT INTO `b_search_stem` VALUES (1416,'также');
INSERT INTO `b_search_stem` VALUES (1599,'такого');
INSERT INTO `b_search_stem` VALUES (1531,'таком');
INSERT INTO `b_search_stem` VALUES (1914,'тексты');
INSERT INTO `b_search_stem` VALUES (1039,'термины');
INSERT INTO `b_search_stem` VALUES (1870,'течение');
INSERT INTO `b_search_stem` VALUES (1186,'то');
INSERT INTO `b_search_stem` VALUES (1134,'том');
INSERT INTO `b_search_stem` VALUES (1498,'требований');
INSERT INTO `b_search_stem` VALUES (1472,'требованию');
INSERT INTO `b_search_stem` VALUES (1487,'требования');
INSERT INTO `b_search_stem` VALUES (1046,'требованиями');
INSERT INTO `b_search_stem` VALUES (1630,'третьим');
INSERT INTO `b_search_stem` VALUES (1467,'третью');
INSERT INTO `b_search_stem` VALUES (1276,'трех');
INSERT INTO `b_search_stem` VALUES (1530,'уведомив');
INSERT INTO `b_search_stem` VALUES (1585,'уведомившему');
INSERT INTO `b_search_stem` VALUES (1877,'удобным');
INSERT INTO `b_search_stem` VALUES (1497,'удовлетворения');
INSERT INTO `b_search_stem` VALUES (1495,'удовлетворены');
INSERT INTO `b_search_stem` VALUES (1271,'удостоверение');
INSERT INTO `b_search_stem` VALUES (1943,'уже');
INSERT INTO `b_search_stem` VALUES (1382,'указанием');
INSERT INTO `b_search_stem` VALUES (1615,'указанного');
INSERT INTO `b_search_stem` VALUES (1478,'указанные');
INSERT INTO `b_search_stem` VALUES (1442,'указанный');
INSERT INTO `b_search_stem` VALUES (1423,'указаны');
INSERT INTO `b_search_stem` VALUES (1481,'уплате');
INSERT INTO `b_search_stem` VALUES (1185,'уполномоченного');
INSERT INTO `b_search_stem` VALUES (1191,'уполномоченное');
INSERT INTO `b_search_stem` VALUES (1553,'уполномоченный');
INSERT INTO `b_search_stem` VALUES (1143,'управления');
INSERT INTO `b_search_stem` VALUES (1328,'условием');
INSERT INTO `b_search_stem` VALUES (1362,'условий');
INSERT INTO `b_search_stem` VALUES (1059,'условия');
INSERT INTO `b_search_stem` VALUES (1304,'условиями');
INSERT INTO `b_search_stem` VALUES (1096,'условиях');
INSERT INTO `b_search_stem` VALUES (1152,'установлению');
INSERT INTO `b_search_stem` VALUES (1220,'установленного');
INSERT INTO `b_search_stem` VALUES (1449,'установленном');
INSERT INTO `b_search_stem` VALUES (1540,'утверждаются');
INSERT INTO `b_search_stem` VALUES (1538,'утверждения');
INSERT INTO `b_search_stem` VALUES (1509,'утвержденными');
INSERT INTO `b_search_stem` VALUES (1375,'факт');
INSERT INTO `b_search_stem` VALUES (1131,'фактически');
INSERT INTO `b_search_stem` VALUES (1580,'физическим');
INSERT INTO `b_search_stem` VALUES (1113,'физическое');
INSERT INTO `b_search_stem` VALUES (1373,'фиксирует');
INSERT INTO `b_search_stem` VALUES (1317,'финансовое');
INSERT INTO `b_search_stem` VALUES (1294,'форме');
INSERT INTO `b_search_stem` VALUES (1605,'хранение');
INSERT INTO `b_search_stem` VALUES (1619,'хранения');
INSERT INTO `b_search_stem` VALUES (1207,'цель');
INSERT INTO `b_search_stem` VALUES (1511,'ч');
INSERT INTO `b_search_stem` VALUES (1408,'части');
INSERT INTO `b_search_stem` VALUES (1525,'частично');
INSERT INTO `b_search_stem` VALUES (1419,'частью');
INSERT INTO `b_search_stem` VALUES (1319,'чего');
INSERT INTO `b_search_stem` VALUES (1534,'чем');
INSERT INTO `b_search_stem` VALUES (1469,'четвёртую');
INSERT INTO `b_search_stem` VALUES (1135,'числе');
INSERT INTO `b_search_stem` VALUES (1289,'членов');
INSERT INTO `b_search_stem` VALUES (1485,'чужими');
INSERT INTO `b_search_stem` VALUES (1588,'штрафные');
INSERT INTO `b_search_stem` VALUES (1672,'ыв');
INSERT INTO `b_search_stem` VALUES (1671,'ываываыва');
INSERT INTO `b_search_stem` VALUES (1165,'этих');
INSERT INTO `b_search_stem` VALUES (1862,'это');
INSERT INTO `b_search_stem` VALUES (1115,'юридическое');
INSERT INTO `b_search_stem` VALUES (1329,'является');
INSERT INTO `b_search_stem` VALUES (1579,'являющемуся');
INSERT INTO `b_search_stem` VALUES (1922,'языкового');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_suggest`
-- 




DROP TABLE IF EXISTS `b_search_suggest`;
CREATE TABLE `b_search_suggest` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `FILTER_MD5` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `PHRASE` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `RATE` float NOT NULL,
  `TIMESTAMP_X` datetime NOT NULL,
  `RESULT_COUNT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IND_B_SEARCH_SUGGEST` (`FILTER_MD5`,`PHRASE`(50),`RATE`),
  KEY `IND_B_SEARCH_SUGGEST_PHRASE` (`PHRASE`(50),`RATE`),
  KEY `IND_B_SEARCH_SUGGEST_TIME` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_tags`
-- 




DROP TABLE IF EXISTS `b_search_tags`;
CREATE TABLE `b_search_tags` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`SEARCH_CONTENT_ID`,`SITE_ID`,`NAME`),
  KEY `IX_B_SEARCH_TAGS_0` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci DELAY_KEY_WRITE=1;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_search_user_right`
-- 




DROP TABLE IF EXISTS `b_search_user_right`;
CREATE TABLE `b_search_user_right` (
  `USER_ID` int(11) NOT NULL,
  `GROUP_CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_USER_RIGHT` (`USER_ID`,`GROUP_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_seo_keywords`
-- 




DROP TABLE IF EXISTS `b_seo_keywords`;
CREATE TABLE `b_seo_keywords` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KEYWORDS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_seo_keywords_url` (`URL`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_short_uri`
-- 




DROP TABLE IF EXISTS `b_short_uri`;
CREATE TABLE `b_short_uri` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `URI` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `URI_CRC` int(18) NOT NULL,
  `SHORT_URI` varbinary(250) NOT NULL,
  `SHORT_URI_CRC` int(18) NOT NULL,
  `STATUS` int(18) NOT NULL DEFAULT '301',
  `MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LAST_USED` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `NUMBER_USED` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ux_b_short_uri_1` (`SHORT_URI_CRC`),
  KEY `ux_b_short_uri_2` (`URI_CRC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_site_template`
-- 




DROP TABLE IF EXISTS `b_site_template`;
CREATE TABLE `b_site_template` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `TEMPLATE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_B_SITE_TEMPLATE` (`SITE_ID`,`CONDITION`,`TEMPLATE`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_site_template`
-- 


INSERT INTO `b_site_template` VALUES (6,'s1','',1,'express');
INSERT INTO `b_site_template` VALUES (7,'s1','$_GET[\'last\']==\'Y\'',2,'mfo2');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_socialservices_message`
-- 




DROP TABLE IF EXISTS `b_socialservices_message`;
CREATE TABLE `b_socialservices_message` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `SOCSERV_USER_ID` int(11) NOT NULL,
  `PROVIDER` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INSERT_DATE` datetime DEFAULT NULL,
  `SUCCES_SENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_socialservices_user`
-- 




DROP TABLE IF EXISTS `b_socialservices_user`;
CREATE TABLE `b_socialservices_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PHOTO` int(11) DEFAULT NULL,
  `EXTERNAL_AUTH_ID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `XML_ID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CAN_DELETE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `PERSONAL_WWW` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERMISSIONS` varchar(555) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OATOKEN` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OASECRET` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REFRESH_TOKEN` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEND_ACTIVITY` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `SITE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_SOCIALSERVICES_USER` (`XML_ID`,`EXTERNAL_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_sticker`
-- 




DROP TABLE IF EXISTS `b_sticker`;
CREATE TABLE `b_sticker` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAGE_URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PAGE_TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `MODIFIED_BY` int(18) NOT NULL,
  `CREATED_BY` int(18) NOT NULL,
  `PERSONAL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CONTENT` text COLLATE utf8_unicode_ci,
  `POS_TOP` int(11) DEFAULT NULL,
  `POS_LEFT` int(11) DEFAULT NULL,
  `WIDTH` int(11) DEFAULT NULL,
  `HEIGHT` int(11) DEFAULT NULL,
  `COLOR` int(11) DEFAULT NULL,
  `COLLAPSED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `COMPLETED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CLOSED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DELETED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `MARKER_TOP` int(11) DEFAULT NULL,
  `MARKER_LEFT` int(11) DEFAULT NULL,
  `MARKER_WIDTH` int(11) DEFAULT NULL,
  `MARKER_HEIGHT` int(11) DEFAULT NULL,
  `MARKER_ADJUST` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_sticker_group_task`
-- 




DROP TABLE IF EXISTS `b_sticker_group_task`;
CREATE TABLE `b_sticker_group_task` (
  `GROUP_ID` int(11) NOT NULL,
  `TASK_ID` int(11) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_task`
-- 




DROP TABLE IF EXISTS `b_task`;
CREATE TABLE `b_task` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LETTER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SYS` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BINDING` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'module',
  PRIMARY KEY (`ID`),
  KEY `ix_task` (`MODULE_ID`,`BINDING`,`LETTER`,`SYS`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_task`
-- 


INSERT INTO `b_task` VALUES (1,'main_denied','D','main','Y',NULL,'module');
INSERT INTO `b_task` VALUES (2,'main_change_profile','P','main','Y',NULL,'module');
INSERT INTO `b_task` VALUES (3,'main_view_all_settings','R','main','Y',NULL,'module');
INSERT INTO `b_task` VALUES (4,'main_view_all_settings_change_profile','T','main','Y',NULL,'module');
INSERT INTO `b_task` VALUES (5,'main_edit_subordinate_users','V','main','Y',NULL,'module');
INSERT INTO `b_task` VALUES (6,'main_full_access','W','main','Y',NULL,'module');
INSERT INTO `b_task` VALUES (7,'fm_folder_access_denied','D','main','Y',NULL,'file');
INSERT INTO `b_task` VALUES (8,'fm_folder_access_read','R','main','Y',NULL,'file');
INSERT INTO `b_task` VALUES (9,'fm_folder_access_write','W','main','Y',NULL,'file');
INSERT INTO `b_task` VALUES (10,'fm_folder_access_full','X','main','Y',NULL,'file');
INSERT INTO `b_task` VALUES (11,'fm_folder_access_workflow','U','main','Y',NULL,'file');
INSERT INTO `b_task` VALUES (12,'clouds_denied','D','clouds','Y',NULL,'module');
INSERT INTO `b_task` VALUES (13,'clouds_browse','F','clouds','Y',NULL,'module');
INSERT INTO `b_task` VALUES (14,'clouds_upload','U','clouds','Y',NULL,'module');
INSERT INTO `b_task` VALUES (15,'clouds_full_access','W','clouds','Y',NULL,'module');
INSERT INTO `b_task` VALUES (16,'fileman_denied','D','fileman','Y','','module');
INSERT INTO `b_task` VALUES (17,'fileman_allowed_folders','F','fileman','Y','','module');
INSERT INTO `b_task` VALUES (18,'fileman_full_access','W','fileman','Y','','module');
INSERT INTO `b_task` VALUES (19,'medialib_denied','D','fileman','Y','','medialib');
INSERT INTO `b_task` VALUES (20,'medialib_view','F','fileman','Y','','medialib');
INSERT INTO `b_task` VALUES (21,'medialib_only_new','R','fileman','Y','','medialib');
INSERT INTO `b_task` VALUES (22,'medialib_edit_items','V','fileman','Y','','medialib');
INSERT INTO `b_task` VALUES (23,'medialib_editor','W','fileman','Y','','medialib');
INSERT INTO `b_task` VALUES (24,'medialib_full','X','fileman','Y','','medialib');
INSERT INTO `b_task` VALUES (25,'stickers_denied','D','fileman','Y','','stickers');
INSERT INTO `b_task` VALUES (26,'stickers_read','R','fileman','Y','','stickers');
INSERT INTO `b_task` VALUES (27,'stickers_edit','W','fileman','Y','','stickers');
INSERT INTO `b_task` VALUES (28,'iblock_deny','D','iblock','Y',NULL,'iblock');
INSERT INTO `b_task` VALUES (29,'iblock_read','R','iblock','Y',NULL,'iblock');
INSERT INTO `b_task` VALUES (30,'iblock_element_add','E','iblock','Y',NULL,'iblock');
INSERT INTO `b_task` VALUES (31,'iblock_limited_edit','U','iblock','Y',NULL,'iblock');
INSERT INTO `b_task` VALUES (32,'iblock_full_edit','W','iblock','Y',NULL,'iblock');
INSERT INTO `b_task` VALUES (33,'iblock_full','X','iblock','Y',NULL,'iblock');
INSERT INTO `b_task` VALUES (34,'seo_denied','D','seo','Y','','module');
INSERT INTO `b_task` VALUES (35,'seo_edit','F','seo','Y','','module');
INSERT INTO `b_task` VALUES (36,'seo_full_access','W','seo','Y','','module');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_task_operation`
-- 




DROP TABLE IF EXISTS `b_task_operation`;
CREATE TABLE `b_task_operation` (
  `TASK_ID` int(18) NOT NULL,
  `OPERATION_ID` int(18) NOT NULL,
  PRIMARY KEY (`TASK_ID`,`OPERATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_task_operation`
-- 


INSERT INTO `b_task_operation` VALUES (2,2);
INSERT INTO `b_task_operation` VALUES (2,3);
INSERT INTO `b_task_operation` VALUES (3,2);
INSERT INTO `b_task_operation` VALUES (3,4);
INSERT INTO `b_task_operation` VALUES (3,5);
INSERT INTO `b_task_operation` VALUES (3,6);
INSERT INTO `b_task_operation` VALUES (3,7);
INSERT INTO `b_task_operation` VALUES (4,2);
INSERT INTO `b_task_operation` VALUES (4,3);
INSERT INTO `b_task_operation` VALUES (4,4);
INSERT INTO `b_task_operation` VALUES (4,5);
INSERT INTO `b_task_operation` VALUES (4,6);
INSERT INTO `b_task_operation` VALUES (4,7);
INSERT INTO `b_task_operation` VALUES (5,2);
INSERT INTO `b_task_operation` VALUES (5,3);
INSERT INTO `b_task_operation` VALUES (5,5);
INSERT INTO `b_task_operation` VALUES (5,6);
INSERT INTO `b_task_operation` VALUES (5,7);
INSERT INTO `b_task_operation` VALUES (5,8);
INSERT INTO `b_task_operation` VALUES (5,9);
INSERT INTO `b_task_operation` VALUES (6,2);
INSERT INTO `b_task_operation` VALUES (6,3);
INSERT INTO `b_task_operation` VALUES (6,4);
INSERT INTO `b_task_operation` VALUES (6,5);
INSERT INTO `b_task_operation` VALUES (6,6);
INSERT INTO `b_task_operation` VALUES (6,7);
INSERT INTO `b_task_operation` VALUES (6,10);
INSERT INTO `b_task_operation` VALUES (6,11);
INSERT INTO `b_task_operation` VALUES (6,12);
INSERT INTO `b_task_operation` VALUES (6,13);
INSERT INTO `b_task_operation` VALUES (6,14);
INSERT INTO `b_task_operation` VALUES (6,15);
INSERT INTO `b_task_operation` VALUES (6,16);
INSERT INTO `b_task_operation` VALUES (6,17);
INSERT INTO `b_task_operation` VALUES (6,18);
INSERT INTO `b_task_operation` VALUES (8,19);
INSERT INTO `b_task_operation` VALUES (8,20);
INSERT INTO `b_task_operation` VALUES (8,21);
INSERT INTO `b_task_operation` VALUES (9,19);
INSERT INTO `b_task_operation` VALUES (9,20);
INSERT INTO `b_task_operation` VALUES (9,21);
INSERT INTO `b_task_operation` VALUES (9,22);
INSERT INTO `b_task_operation` VALUES (9,23);
INSERT INTO `b_task_operation` VALUES (9,24);
INSERT INTO `b_task_operation` VALUES (9,25);
INSERT INTO `b_task_operation` VALUES (9,26);
INSERT INTO `b_task_operation` VALUES (9,27);
INSERT INTO `b_task_operation` VALUES (9,28);
INSERT INTO `b_task_operation` VALUES (9,29);
INSERT INTO `b_task_operation` VALUES (9,30);
INSERT INTO `b_task_operation` VALUES (9,31);
INSERT INTO `b_task_operation` VALUES (9,32);
INSERT INTO `b_task_operation` VALUES (9,33);
INSERT INTO `b_task_operation` VALUES (9,34);
INSERT INTO `b_task_operation` VALUES (10,19);
INSERT INTO `b_task_operation` VALUES (10,20);
INSERT INTO `b_task_operation` VALUES (10,21);
INSERT INTO `b_task_operation` VALUES (10,22);
INSERT INTO `b_task_operation` VALUES (10,23);
INSERT INTO `b_task_operation` VALUES (10,24);
INSERT INTO `b_task_operation` VALUES (10,25);
INSERT INTO `b_task_operation` VALUES (10,26);
INSERT INTO `b_task_operation` VALUES (10,27);
INSERT INTO `b_task_operation` VALUES (10,28);
INSERT INTO `b_task_operation` VALUES (10,29);
INSERT INTO `b_task_operation` VALUES (10,30);
INSERT INTO `b_task_operation` VALUES (10,31);
INSERT INTO `b_task_operation` VALUES (10,32);
INSERT INTO `b_task_operation` VALUES (10,33);
INSERT INTO `b_task_operation` VALUES (10,34);
INSERT INTO `b_task_operation` VALUES (10,35);
INSERT INTO `b_task_operation` VALUES (11,19);
INSERT INTO `b_task_operation` VALUES (11,20);
INSERT INTO `b_task_operation` VALUES (11,21);
INSERT INTO `b_task_operation` VALUES (11,24);
INSERT INTO `b_task_operation` VALUES (11,28);
INSERT INTO `b_task_operation` VALUES (13,36);
INSERT INTO `b_task_operation` VALUES (14,36);
INSERT INTO `b_task_operation` VALUES (14,37);
INSERT INTO `b_task_operation` VALUES (15,36);
INSERT INTO `b_task_operation` VALUES (15,37);
INSERT INTO `b_task_operation` VALUES (15,38);
INSERT INTO `b_task_operation` VALUES (17,41);
INSERT INTO `b_task_operation` VALUES (17,42);
INSERT INTO `b_task_operation` VALUES (17,43);
INSERT INTO `b_task_operation` VALUES (17,44);
INSERT INTO `b_task_operation` VALUES (17,45);
INSERT INTO `b_task_operation` VALUES (17,46);
INSERT INTO `b_task_operation` VALUES (17,47);
INSERT INTO `b_task_operation` VALUES (17,49);
INSERT INTO `b_task_operation` VALUES (17,50);
INSERT INTO `b_task_operation` VALUES (18,39);
INSERT INTO `b_task_operation` VALUES (18,40);
INSERT INTO `b_task_operation` VALUES (18,41);
INSERT INTO `b_task_operation` VALUES (18,42);
INSERT INTO `b_task_operation` VALUES (18,43);
INSERT INTO `b_task_operation` VALUES (18,44);
INSERT INTO `b_task_operation` VALUES (18,45);
INSERT INTO `b_task_operation` VALUES (18,46);
INSERT INTO `b_task_operation` VALUES (18,47);
INSERT INTO `b_task_operation` VALUES (18,48);
INSERT INTO `b_task_operation` VALUES (18,49);
INSERT INTO `b_task_operation` VALUES (18,50);
INSERT INTO `b_task_operation` VALUES (18,51);
INSERT INTO `b_task_operation` VALUES (20,52);
INSERT INTO `b_task_operation` VALUES (21,52);
INSERT INTO `b_task_operation` VALUES (21,53);
INSERT INTO `b_task_operation` VALUES (21,57);
INSERT INTO `b_task_operation` VALUES (22,52);
INSERT INTO `b_task_operation` VALUES (22,57);
INSERT INTO `b_task_operation` VALUES (22,58);
INSERT INTO `b_task_operation` VALUES (22,59);
INSERT INTO `b_task_operation` VALUES (23,52);
INSERT INTO `b_task_operation` VALUES (23,53);
INSERT INTO `b_task_operation` VALUES (23,54);
INSERT INTO `b_task_operation` VALUES (23,55);
INSERT INTO `b_task_operation` VALUES (23,57);
INSERT INTO `b_task_operation` VALUES (23,58);
INSERT INTO `b_task_operation` VALUES (23,59);
INSERT INTO `b_task_operation` VALUES (24,52);
INSERT INTO `b_task_operation` VALUES (24,53);
INSERT INTO `b_task_operation` VALUES (24,54);
INSERT INTO `b_task_operation` VALUES (24,55);
INSERT INTO `b_task_operation` VALUES (24,56);
INSERT INTO `b_task_operation` VALUES (24,57);
INSERT INTO `b_task_operation` VALUES (24,58);
INSERT INTO `b_task_operation` VALUES (24,59);
INSERT INTO `b_task_operation` VALUES (26,60);
INSERT INTO `b_task_operation` VALUES (27,60);
INSERT INTO `b_task_operation` VALUES (27,61);
INSERT INTO `b_task_operation` VALUES (27,62);
INSERT INTO `b_task_operation` VALUES (27,63);
INSERT INTO `b_task_operation` VALUES (29,69);
INSERT INTO `b_task_operation` VALUES (29,75);
INSERT INTO `b_task_operation` VALUES (30,72);
INSERT INTO `b_task_operation` VALUES (31,64);
INSERT INTO `b_task_operation` VALUES (31,69);
INSERT INTO `b_task_operation` VALUES (31,72);
INSERT INTO `b_task_operation` VALUES (31,75);
INSERT INTO `b_task_operation` VALUES (31,76);
INSERT INTO `b_task_operation` VALUES (31,78);
INSERT INTO `b_task_operation` VALUES (31,79);
INSERT INTO `b_task_operation` VALUES (31,80);
INSERT INTO `b_task_operation` VALUES (32,64);
INSERT INTO `b_task_operation` VALUES (32,69);
INSERT INTO `b_task_operation` VALUES (32,70);
INSERT INTO `b_task_operation` VALUES (32,71);
INSERT INTO `b_task_operation` VALUES (32,72);
INSERT INTO `b_task_operation` VALUES (32,73);
INSERT INTO `b_task_operation` VALUES (32,75);
INSERT INTO `b_task_operation` VALUES (32,76);
INSERT INTO `b_task_operation` VALUES (32,77);
INSERT INTO `b_task_operation` VALUES (32,78);
INSERT INTO `b_task_operation` VALUES (32,79);
INSERT INTO `b_task_operation` VALUES (32,80);
INSERT INTO `b_task_operation` VALUES (33,64);
INSERT INTO `b_task_operation` VALUES (33,65);
INSERT INTO `b_task_operation` VALUES (33,66);
INSERT INTO `b_task_operation` VALUES (33,67);
INSERT INTO `b_task_operation` VALUES (33,68);
INSERT INTO `b_task_operation` VALUES (33,69);
INSERT INTO `b_task_operation` VALUES (33,70);
INSERT INTO `b_task_operation` VALUES (33,71);
INSERT INTO `b_task_operation` VALUES (33,72);
INSERT INTO `b_task_operation` VALUES (33,73);
INSERT INTO `b_task_operation` VALUES (33,74);
INSERT INTO `b_task_operation` VALUES (33,75);
INSERT INTO `b_task_operation` VALUES (33,76);
INSERT INTO `b_task_operation` VALUES (33,77);
INSERT INTO `b_task_operation` VALUES (33,78);
INSERT INTO `b_task_operation` VALUES (33,79);
INSERT INTO `b_task_operation` VALUES (33,80);
INSERT INTO `b_task_operation` VALUES (33,81);
INSERT INTO `b_task_operation` VALUES (35,83);
INSERT INTO `b_task_operation` VALUES (36,82);
INSERT INTO `b_task_operation` VALUES (36,83);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_undo`
-- 




DROP TABLE IF EXISTS `b_undo`;
CREATE TABLE `b_undo` (
  `ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNDO_TYPE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNDO_HANDLER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONTENT` mediumtext COLLATE utf8_unicode_ci,
  `USER_ID` int(11) DEFAULT NULL,
  `TIMESTAMP_X` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user`
-- 




DROP TABLE IF EXISTS `b_user`;
CREATE TABLE `b_user` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LOGIN` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CHECKWORD` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_LOGIN` datetime DEFAULT NULL,
  `DATE_REGISTER` datetime NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PROFESSION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_WWW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_ICQ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_GENDER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_BIRTHDATE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PHOTO` int(18) DEFAULT NULL,
  `PERSONAL_PHONE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_FAX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_MOBILE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PAGER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_STREET` text COLLATE utf8_unicode_ci,
  `PERSONAL_MAILBOX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_CITY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_STATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_ZIP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_COUNTRY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_NOTES` text COLLATE utf8_unicode_ci,
  `WORK_COMPANY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_DEPARTMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_POSITION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_WWW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_PHONE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_FAX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_PAGER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_STREET` text COLLATE utf8_unicode_ci,
  `WORK_MAILBOX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_CITY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_STATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_ZIP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_COUNTRY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_PROFILE` text COLLATE utf8_unicode_ci,
  `WORK_LOGO` int(18) DEFAULT NULL,
  `WORK_NOTES` text COLLATE utf8_unicode_ci,
  `ADMIN_NOTES` text COLLATE utf8_unicode_ci,
  `STORED_HASH` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_BIRTHDAY` date DEFAULT NULL,
  `EXTERNAL_AUTH_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CHECKWORD_TIME` datetime DEFAULT NULL,
  `SECOND_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONFIRM_CODE` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOGIN_ATTEMPTS` int(18) DEFAULT NULL,
  `LAST_ACTIVITY_DATE` datetime DEFAULT NULL,
  `AUTO_TIME_ZONE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIME_ZONE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIME_ZONE_OFFSET` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_login` (`LOGIN`,`EXTERNAL_AUTH_ID`),
  KEY `ix_b_user_email` (`EMAIL`),
  KEY `ix_b_user_activity_date` (`LAST_ACTIVITY_DATE`),
  KEY `IX_B_USER_XML_ID` (`XML_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_user`
-- 


INSERT INTO `b_user` VALUES (1,'2015-10-17 20:45:43','admin','b/O93z@Ebef7a33d7993837f21362fbc57cdae47','N1JbZA0La5e027d0b16e202b95152fcd4a591da0','Y','qwe','qwe','qwe@qwe.ru','2015-10-19 16:47:26','2013-09-26 18:01:50','s1','','','','',NULL,NULL,'','','','','','','','','','0','','','','','','','','','','','','','','0','',NULL,'','',NULL,NULL,NULL,NULL,'2015-10-14 14:44:53','',NULL,0,NULL,'',NULL,NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user_access`
-- 




DROP TABLE IF EXISTS `b_user_access`;
CREATE TABLE `b_user_access` (
  `USER_ID` int(11) DEFAULT NULL,
  `PROVIDER_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACCESS_CODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `ix_ua_user_provider` (`USER_ID`,`PROVIDER_ID`),
  KEY `ix_ua_user_access` (`USER_ID`,`ACCESS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_user_access`
-- 


INSERT INTO `b_user_access` VALUES (0,'group','G2');
INSERT INTO `b_user_access` VALUES (1,'user','U1');
INSERT INTO `b_user_access` VALUES (1,'group','G1');
INSERT INTO `b_user_access` VALUES (1,'group','G3');
INSERT INTO `b_user_access` VALUES (1,'group','G4');
INSERT INTO `b_user_access` VALUES (1,'group','G2');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user_access_check`
-- 




DROP TABLE IF EXISTS `b_user_access_check`;
CREATE TABLE `b_user_access_check` (
  `USER_ID` int(11) DEFAULT NULL,
  `PROVIDER_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `ix_uac_user_provider` (`USER_ID`,`PROVIDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_user_access_check`
-- 


INSERT INTO `b_user_access_check` VALUES (1,'group');
INSERT INTO `b_user_access_check` VALUES (1,'user');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user_counter`
-- 




DROP TABLE IF EXISTS `b_user_counter`;
CREATE TABLE `b_user_counter` (
  `USER_ID` int(18) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '**',
  `CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CNT` int(18) NOT NULL DEFAULT '0',
  `LAST_DATE` datetime DEFAULT NULL,
  `TAG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`USER_ID`,`SITE_ID`,`CODE`),
  KEY `ix_buc_tag` (`TAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user_digest`
-- 




DROP TABLE IF EXISTS `b_user_digest`;
CREATE TABLE `b_user_digest` (
  `USER_ID` int(11) NOT NULL,
  `DIGEST_HA1` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user_field`
-- 




DROP TABLE IF EXISTS `b_user_field`;
CREATE TABLE `b_user_field` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENTITY_ID` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD_NAME` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) DEFAULT NULL,
  `MULTIPLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `MANDATORY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SHOW_FILTER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SHOW_IN_LIST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `EDIT_IN_LIST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `IS_SEARCHABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SETTINGS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_user_type_entity` (`ENTITY_ID`,`FIELD_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_user_field`
-- 


INSERT INTO `b_user_field` VALUES (1,'IBLOCK_8_SECTION','UF_IMAGE','file','',100,'Y','N','N','Y','Y','N','a:6:{s:4:\"SIZE\";i:20;s:10:\"LIST_WIDTH\";i:200;s:11:\"LIST_HEIGHT\";i:200;s:13:\"MAX_SHOW_SIZE\";i:0;s:16:\"MAX_ALLOWED_SIZE\";i:0;s:10:\"EXTENSIONS\";a:0:{}}');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user_field_enum`
-- 




DROP TABLE IF EXISTS `b_user_field_enum`;
CREATE TABLE `b_user_field_enum` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_FIELD_ID` int(11) DEFAULT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_user_field_enum` (`USER_FIELD_ID`,`XML_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user_field_lang`
-- 




DROP TABLE IF EXISTS `b_user_field_lang`;
CREATE TABLE `b_user_field_lang` (
  `USER_FIELD_ID` int(11) NOT NULL DEFAULT '0',
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `EDIT_FORM_LABEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIST_COLUMN_LABEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIST_FILTER_LABEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HELP_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`USER_FIELD_ID`,`LANGUAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_user_field_lang`
-- 


INSERT INTO `b_user_field_lang` VALUES (1,'en','Images','','','','');
INSERT INTO `b_user_field_lang` VALUES (1,'ru','Изображения','','','','');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user_group`
-- 




DROP TABLE IF EXISTS `b_user_group`;
CREATE TABLE `b_user_group` (
  `USER_ID` int(18) NOT NULL,
  `GROUP_ID` int(18) NOT NULL,
  `DATE_ACTIVE_FROM` datetime DEFAULT NULL,
  `DATE_ACTIVE_TO` datetime DEFAULT NULL,
  UNIQUE KEY `ix_user_group` (`USER_ID`,`GROUP_ID`),
  KEY `ix_user_group_group` (`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_user_group`
-- 


INSERT INTO `b_user_group` VALUES (1,1,NULL,NULL);
INSERT INTO `b_user_group` VALUES (1,3,NULL,NULL);
INSERT INTO `b_user_group` VALUES (1,4,NULL,NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user_hit_auth`
-- 




DROP TABLE IF EXISTS `b_user_hit_auth`;
CREATE TABLE `b_user_hit_auth` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `HASH` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMESTAMP_X` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_USER_HIT_AUTH_1` (`HASH`),
  KEY `IX_USER_HIT_AUTH_2` (`USER_ID`),
  KEY `IX_USER_HIT_AUTH_3` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user_option`
-- 




DROP TABLE IF EXISTS `b_user_option`;
CREATE TABLE `b_user_option` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) DEFAULT NULL,
  `CATEGORY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `COMMON` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `ix_user_option_param` (`CATEGORY`,`NAME`),
  KEY `ix_user_option_user` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table  `b_user_option`
-- 


INSERT INTO `b_user_option` VALUES (1,NULL,'intranet','~gadgets_admin_index','a:1:{i:0;a:1:{s:7:\"GADGETS\";a:6:{s:20:\"ADMIN_INFO@333333333\";a:3:{s:6:\"COLUMN\";i:0;s:3:\"ROW\";i:0;s:4:\"HIDE\";s:1:\"N\";}s:24:\"ADMIN_SECURITY@555555555\";a:3:{s:6:\"COLUMN\";i:0;s:3:\"ROW\";i:1;s:4:\"HIDE\";s:1:\"N\";}s:23:\"ADMIN_PERFMON@666666666\";a:3:{s:6:\"COLUMN\";i:0;s:3:\"ROW\";i:2;s:4:\"HIDE\";s:1:\"N\";}s:19:\"HTML_AREA@444444444\";a:5:{s:6:\"COLUMN\";i:1;s:3:\"ROW\";i:0;s:4:\"HIDE\";s:1:\"N\";s:8:\"USERDATA\";a:1:{s:7:\"content\";s:797:\"<table class=\"bx-gadgets-info-site-table\" cellspacing=\"0\"><tr>	<td class=\"bx-gadget-gray\">Создатель сайта:</td>	<td>Группа компаний &laquo;1С-Битрикс&raquo;.</td>	<td class=\"bx-gadgets-info-site-logo\" rowspan=\"5\"><img src=\"/bitrix/components/bitrix/desktop/templates/admin/images/site_logo.png\"></td></tr><tr>	<td class=\"bx-gadget-gray\">Адрес сайта:</td>	<td><a href=\"http://www.1c-bitrix.ru\">www.1c-bitrix.ru</a></td></tr><tr>	<td class=\"bx-gadget-gray\">Сайт сдан:</td>	<td>12 декабря 2010 г.</td></tr><tr>	<td class=\"bx-gadget-gray\">Ответственное лицо:</td>	<td>Иван Иванов</td></tr><tr>	<td class=\"bx-gadget-gray\">E-mail:</td>	<td><a href=\"mailto:info@1c-bitrix.ru\">info@1c-bitrix.ru</a></td></tr></table>\";}s:8:\"SETTINGS\";a:1:{s:9:\"TITLE_STD\";s:34:\"Информация о сайте\";}}s:19:\"RSSREADER@777777777\";a:4:{s:6:\"COLUMN\";i:1;s:3:\"ROW\";i:2;s:4:\"HIDE\";s:1:\"N\";s:8:\"SETTINGS\";a:3:{s:9:\"TITLE_STD\";s:33:\"Новости 1С-Битрикс\";s:3:\"CNT\";i:5;s:7:\"RSS_URL\";s:45:\"https://www.1c-bitrix.ru/about/life/news/rss/\";}}s:25:\"ADMIN_CHECKLIST@777888999\";a:3:{s:6:\"COLUMN\";i:1;s:3:\"ROW\";i:1;s:4:\"HIDE\";s:1:\"N\";}}}}','Y');
INSERT INTO `b_user_option` VALUES (2,1,'admin_panel','settings','a:2:{s:4:\"edit\";s:3:\"off\";s:9:\"collapsed\";s:2:\"on\";}','N');
INSERT INTO `b_user_option` VALUES (3,1,'hot_keys','user_defined','b:1;','N');
INSERT INTO `b_user_option` VALUES (4,1,'favorite','favorite_menu','a:1:{s:5:\"stick\";s:1:\"N\";}','N');
INSERT INTO `b_user_option` VALUES (5,1,'admin_menu','pos','a:3:{s:8:\"sections\";s:562:\"menu_system,menu_iblock,menu_iblock_/reviews/4,iblock_admin,menu_bitrixcloud,menu_iblock_/regions/1,menu_iblock_/news/3,menu_iblock_/reviews,menu_templates,menu_fileman_file_s1_,menu_iblock_/address/2,menu_iblock_/affiliatted,menu_iblock_/affiliatted/6,menu_iblock_/vacancy/5,menu_iblock_/requests,menu_iblock_/requests/7,menu_rating,menu_users,menu_fileman_site_s1_,menu_perfmon,menu_util,menu_site,menu_module_settings,backup,menu_iblock_/content,menu_iblock_/regions,menu_search,menu_fileman,menu_iblock_/regions/10,menu_iblock_/address,menu_iblock_/content/8\";s:3:\"ver\";s:2:\"on\";s:5:\"width\";s:3:\"300\";}','N');
INSERT INTO `b_user_option` VALUES (6,1,'fileman','last_pathes','s:229:\"a:10:{i:0;s:7:\"/agents\";i:1;s:7:\"/anketa\";i:2;s:13:\"/loans-online\";i:3;s:30:\"/bitrix/components/ceolab/calc\";i:4;s:10:\"/franchise\";i:5;s:13:\"/requirements\";i:6;s:6:\"/about\";i:7;s:9:\"/contacts\";i:8;s:7:\"/images\";i:9;s:6:\"/.idea\";}\";','N');
INSERT INTO `b_user_option` VALUES (7,1,'fileman','type_selector_PROP_7__n0__VALUE__TEXT_','s:6:\"editor\";','N');
INSERT INTO `b_user_option` VALUES (8,1,'fileman','type_selector_DETAIL_TEXT5','s:6:\"editor\";','N');
INSERT INTO `b_user_option` VALUES (9,1,'fileman','code_editor','a:1:{s:5:\"theme\";s:5:\"light\";}','N');
INSERT INTO `b_user_option` VALUES (10,1,'form','form_element_8','a:1:{s:4:\"tabs\";s:211:\"edit1--#--Слайд--,--ACTIVE--#--Активность--,--NAME--#--*Название--,--PREVIEW_PICTURE--#--Картинка для анонса--,--PREVIEW_TEXT--#--Описание для анонса--;--\";}','N');
INSERT INTO `b_user_option` VALUES (11,1,'fileman','type_selector_DESCRIPTION','s:4:\"html\";','N');
INSERT INTO `b_user_option` VALUES (12,1,'form','form_section_8','a:1:{s:4:\"tabs\";s:645:\"edit1--#--Слайдер--,--ID--#--ID--,--DATE_CREATE--#--Создана--,--TIMESTAMP_X--#--Изменена--,--ACTIVE--#--Раздел активен--,--IBLOCK_SECTION_ID--#--Родительский раздел--,--NAME--#--*Название--,--UF_IMAGE--#--Изображения--,--DESCRIPTION--#--Описание--;--edit2--#--Дополнительно--,--SORT--#--Сортировка--,--CODE--#--Символьный код--,--DETAIL_PICTURE--#--Детальная картинка--;--user_fields_tab--#--Доп. свойства--,--USER_FIELDS_ADD--#--Добавить пользовательское свойство--;--\";}','N');
INSERT INTO `b_user_option` VALUES (13,NULL,'form','form_section_8','a:1:{s:4:\"tabs\";s:645:\"edit1--#--Слайдер--,--ID--#--ID--,--DATE_CREATE--#--Создана--,--TIMESTAMP_X--#--Изменена--,--ACTIVE--#--Раздел активен--,--IBLOCK_SECTION_ID--#--Родительский раздел--,--NAME--#--*Название--,--UF_IMAGE--#--Изображения--,--DESCRIPTION--#--Описание--;--edit2--#--Дополнительно--,--SORT--#--Сортировка--,--CODE--#--Символьный код--,--DETAIL_PICTURE--#--Детальная картинка--;--user_fields_tab--#--Доп. свойства--,--USER_FIELDS_ADD--#--Добавить пользовательское свойство--;--\";}','Y');
INSERT INTO `b_user_option` VALUES (15,NULL,'form','form_element_8','a:1:{s:4:\"tabs\";s:211:\"edit1--#--Слайд--,--ACTIVE--#--Активность--,--NAME--#--*Название--,--PREVIEW_PICTURE--#--Картинка для анонса--,--PREVIEW_TEXT--#--Описание для анонса--;--\";}','Y');
INSERT INTO `b_user_option` VALUES (16,1,'fileman','type_selector_PREVIEW_TEXT8','s:4:\"html\";','N');
INSERT INTO `b_user_option` VALUES (17,1,'form','form_element_9','a:1:{s:4:\"tabs\";s:614:\"edit1--#--Партнер--,--ACTIVE--#--Активность--,--ACTIVE_FROM--#--Начало активности--,--ACTIVE_TO--#--Окончание активности--,--NAME--#--*Название--,--PREVIEW_PICTURE--#--Картинка для анонса--;--edit6--#--Подробно--,--DETAIL_PICTURE--#--Детальная картинка--,--DETAIL_TEXT--#--Детальное описание--;--edit2--#--Разделы--,--SECTIONS--#--Разделы--;--edit3--#--Дополнительно--,--SORT--#--Сортировка--,--CODE--#--Символьный код--,--TAGS--#--Теги--;--\";}','N');
INSERT INTO `b_user_option` VALUES (18,1,'form','form_element_11','a:1:{s:4:\"tabs\";s:1074:\"edit1--#--Займ--,--ACTIVE--#--Активность--,--ACTIVE_FROM--#--Начало активности--,--ACTIVE_TO--#--Окончание активности--,--NAME--#--*Название--,--IBLOCK_ELEMENT_PROP_VALUE--#----Значения свойств--,--PROPERTY_27--#--*Минимальная сумма (руб)--,--PROPERTY_28--#--*Максимальная сумма (руб)--,--PROPERTY_30--#--*Минимальный срок (дней)--,--PROPERTY_31--#--*Максимальный срок (дней)--,--PROPERTY_29--#--*Процентная ставка--;--edit5--#--Анонс--,--PREVIEW_PICTURE--#--Картинка для анонса--,--PREVIEW_TEXT--#--Описание для анонса--;--edit6--#--Подробно--,--DETAIL_PICTURE--#--Детальная картинка--,--DETAIL_TEXT--#--Детальное описание--;--edit2--#--Разделы--,--SECTIONS--#--Разделы--;--edit3--#--Дополнительно--,--SORT--#--Сортировка--,--CODE--#--Символьный код--,--TAGS--#--Теги--;--\";}','N');
INSERT INTO `b_user_option` VALUES (19,1,'fileman','type_selector_PREVIEW_TEXT12','s:4:\"html\";','N');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_user_stored_auth`
-- 




DROP TABLE IF EXISTS `b_user_stored_auth`;
CREATE TABLE `b_user_stored_auth` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `DATE_REG` datetime NOT NULL,
  `LAST_AUTH` datetime NOT NULL,
  `STORED_HASH` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `TEMP_HASH` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IP_ADDR` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ux_user_hash` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------
-- 
-- Table structure for table `b_utm_iblock_8_section`
-- 




DROP TABLE IF EXISTS `b_utm_iblock_8_section`;
CREATE TABLE `b_utm_iblock_8_section` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUE_ID` int(11) NOT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `VALUE` text,
  `VALUE_INT` int(11) DEFAULT NULL,
  `VALUE_DOUBLE` float DEFAULT NULL,
  `VALUE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_utm_IBLOCK_8_SECTION_1` (`FIELD_ID`),
  KEY `ix_utm_IBLOCK_8_SECTION_2` (`VALUE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table  `b_utm_iblock_8_section`
-- 


INSERT INTO `b_utm_iblock_8_section` VALUES (1,2,1,NULL,5,NULL,NULL);
INSERT INTO `b_utm_iblock_8_section` VALUES (2,2,1,NULL,6,NULL,NULL);
INSERT INTO `b_utm_iblock_8_section` VALUES (3,2,1,NULL,7,NULL,NULL);
INSERT INTO `b_utm_iblock_8_section` VALUES (4,2,1,NULL,8,NULL,NULL);
INSERT INTO `b_utm_iblock_8_section` VALUES (5,2,1,NULL,9,NULL,NULL);


-- --------------------------------------------------------
-- 
-- Table structure for table `b_uts_iblock_8_section`
-- 




DROP TABLE IF EXISTS `b_uts_iblock_8_section`;
CREATE TABLE `b_uts_iblock_8_section` (
  `VALUE_ID` int(11) NOT NULL,
  `UF_IMAGE` text,
  PRIMARY KEY (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table  `b_uts_iblock_8_section`
-- 


INSERT INTO `b_uts_iblock_8_section` VALUES (2,'a:5:{i:0;i:5;i:1;i:6;i:2;i:7;i:3;i:8;i:4;i:9;}');


-- --------------------------------------------------------
-- 
-- Table structure for table `b_xml_tree`
-- 




DROP TABLE IF EXISTS `b_xml_tree`;
CREATE TABLE `b_xml_tree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PARENT_ID` int(11) DEFAULT NULL,
  `LEFT_MARGIN` int(11) DEFAULT NULL,
  `RIGHT_MARGIN` int(11) DEFAULT NULL,
  `DEPTH_LEVEL` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `ATTRIBUTES` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_xml_tree_parent` (`PARENT_ID`),
  KEY `ix_b_xml_tree_left` (`LEFT_MARGIN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

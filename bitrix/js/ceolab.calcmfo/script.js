/**
 * Created with JetBrains WebStorm.
 * User: evgenius
 * Date: 10/13/13
 * Time: 4:09 PM
 * To change this template use File | Settings | File Templates.
 */
//@TODO Вынести в общий файл
validators = {
    number : {
        pattern: "[0-9]+",
        name: "Номер",
        message: "Неверный номер"
    },
    date : {
        pattern: "[0-9]{2}\.[0-9]{2}\.[0-9]{4}",
        name: "Дата",
        message: "Неверный формат даты"
    }
};

function FormFactory()
{

    $(window).scroll(
        function(event)
        {
            if($(this).scrollTop() >= $(".formSettings").offset().top)
            {
                $(".controlPanel").css("width", $(".formSettings").width());
                $(".controlPanel").css("left", $(".formSettings").offset().left);
                $(".controlPanel").css("position", "fixed");
            }
            else
            {
                $(".controlPanel").css("left", 0);
                $(".controlPanel").css("position", "absolute");
            }

            var bottom = $(".formSettings").offset().top + $(".formSettings").height();
            if(bottom > $(this).height()+$(this).scrollTop())
            {
                $(".propertiesPanel").css("width", $(".formSettings").width()-20);
                $(".propertiesPanel").css("left", $(".formSettings").offset().left);
                $(".propertiesPanel").css("position", "fixed");
            }
            else
            {
                $(".propertiesPanel").css("left", 0);
                $(".propertiesPanel").css("position", "absolute");
            }

        }
    )

    $("#formSettings > input[name=\"submit\"]").bind(
        "click",
        this,
        function(eval)
        {
            var result = eval.data.send();
            var input = $("<input type=\"hidden\" name=\"data\"/>").appendTo("#formSettings");
            input.val(JSON.stringify(result));
        }
    )

    this.send = function()
    {
        that = this;
        var result = {};
        result['forms'] = [];
        result['iblock'] = {
            type : iblocksBar.getIblockType(),
            iblock: iblocksBar.getIblock()
        };
        $("#formItems .item").each(
            function()
            {
                var form = {};
                form['name'] = $(this).find(".formName").text();

                if($(this).attr("form_id"))
                    form['id'] = $(this).attr("form_id");

                form['fields'] = [];

                $(this).find(".field").each(
                    function()
                    {
                        var item = {};
                        item['title'] = $(this).find("[name=\"text\"]").text();
                        item['attr'] = {};
                        var s = $(this).attr("systype");
                        item['type'] = s;

                        if($(this).attr("field_id"))
                            item['id'] = $(this).attr("field_id");

                        var field = $(this).find("[name=\"field\"] > *").eq(0);


                        for(var i in fieldTypes[s].props)
                        {
                            if(fieldTypes[s].props[i].type == "keyvalue")
                            {
                                item['attr']['values'] = [];
                                var k = 0;
                                while(true)
                                {
                                    var option = field.find("option").eq(k);
                                    if(!option.length)
                                        break;
                                    item['attr']['values'].push({name: option.text(), value: option.val()});
                                    k++;
                                }
                            }
                            else
                            {
                                var attr = field.attr(fieldTypes[s].props[i].attr);
                                if(attr != undefined)
                                    item['attr'][fieldTypes[s].props[i].attr] = attr;
                            }
                        }

                        form['fields'].push(item);
                    }
                )

                result['forms'].push(form);
            }
        )
        return result;
    }

    this.add = function(title, id)
    {
        var form = $("<div class=\"item\">" +
            "<div class=\"head\">" +
            "Наименование:" +
            "</div>" +
            "<div class=\"body\">" +
            "<div class=\"bgr\"><div class=\"text\">Кликните что-бы редактировать</div></div>" +
            "</div>" +
            "</div>").appendTo("#formItems");

        if(id)
            form.attr("form_id", id);

        if(!title)
            title = "First form";

        var rBtns = $("<div class=\"rightButtons\"></div>").appendTo(form.find(".head"));
        var upBtn = $("<a href=\"javascript:void(0)\"><img src=\"/bitrix/images/ceolab.calcmfo/up.gif\"/></a>").appendTo(rBtns);
        var downBtn = $("<a href=\"javascript:void(0)\"><img src=\"/bitrix/images/ceolab.calcmfo/down.gif\"/></a>").appendTo(rBtns);
        var deleteBtn = $("<a href=\"javascript:void(0)\"><img src=\"/bitrix/images/ceolab.calcmfo/delete.gif\"/></a>").appendTo(rBtns);
        var name = $("<span class=\"formName\">"+title+"</span>").appendTo(form.find(".head"));

        name.bind(
            "click",
            this,
            function(eval)
            {
                var val = $(this).text();
                $(this).empty();
                var input = $("<input type=\"text\"/>").appendTo(this);
                input.val(val);
                input.focus();
                input.select();
                input.blur(
                    function()
                    {
                        var val = $(this).val();
                        if(!val)
                        {
                            alert("Поле не должно быть пустым");
                            $(this).focus();
                            $(this).select();
                            return;
                        }
                        $(this).parent("span.formName").text(val);
                        $(this).remove();
                    }
                )
            }
        )

        upBtn.bind(
            "click",
            this,
            function(eval)
            {
                var element = $(this).parent(".rightButtons").parent(".head").parent(".item");
                eval.data.up(element);
            }
        );

        downBtn.bind(
            "click",
            this,
            function(eval)
            {
                var element = $(this).parent(".rightButtons").parent(".head").parent(".item");
                eval.data.down(element);
            }
        );

        deleteBtn.bind(
            "click",
            this,
            function(eval)
            {
                var element = $(this).parent(".rightButtons").parent(".head").parent(".item");
                eval.data.remove(element);
            }
        );

        form.click(
            function()
            {
                formFactory.setActive(this);
            }
        );

        return form;
    }

    this.remove = function(element)
    {
        element.remove();
    }

    this.up = function(element)
    {
        var prev = element.prev(".item");
        if(prev)
        {
            element.insertBefore(prev);
        }
    }

    this.down = function(element)
    {
        var next = element.next(".item");
        if(next)
        {
            element.insertAfter(next);
        }
    }

    this.setActive = function(element)
    {
        if($(element).find(".bgr").is(":hidden"))
            return;

        $("#formItems").find(".item .bgr:hidden").each(
            function()
            {
                $(this).show();
                $(this).parent(".body").find(".field").removeClass("activeField");
            }
        );
        $(element).find(".bgr").hide();

        props.hide();
    }

    this.getActive = function()
    {
        return $("#formItems .item .bgr:hidden").parent(".body").parent(".item");
    }
}


function Form()
{
    this.addField = function(type, title, data, id)
    {
        var activeForm = formFactory.getActive();
        if(activeForm && fieldTypes[type])
        {
            var element = $("" +
                "<div class=\"field\" sysType=\""+type+"\">" +
                "<span name=\"field\"></span>"+
                "<div class=\"control\">" +
                "<a href=\"javascript:void(0)\" onclick=\"form.fieldUp(this)\"><img src=\"/bitrix/images/ceolab.calcmfo/up.gif\"/></a>" +
                "<a href=\"javascript:void(0)\" onclick=\"form.fieldDown(this)\"><img src=\"/bitrix/images/ceolab.calcmfo/down.gif\"/></a>" +
                "<a href=\"javascript:void(0)\" onclick=\"form.removeField(this)\"><img src=\"/bitrix/images/ceolab.calcmfo/delete.gif\"/></a>" +
                "</div>"+
                "</div>").appendTo(activeForm.find(".body"));

            var field = $(fieldTypes[type].html).appendTo(element.find("span[name=\"field\"]"));
            field.attr("disabled", "disabled");

            if(id)
                element.attr("field_id", id);

            for(var i in data)
            {
                if(i != "values")
                    field.attr(i, data[i]);
                else
                    for(var a in data[i])
                        $("<option value=\""+data[i][a].value+"\">"+data[i][a].name+"</option>").appendTo(field);
            }

            if(!title)
                title = fieldTypes[type].defaultName;
            var name = $("<span name=\"text\">"+title+"</span> ").prependTo(element);

            name.click(
                function()
                {
                    var val = $(this).text();
                    $(this).empty();
                    var input = $("<input type=\"text\"/>").appendTo(this);
                    input.val(val);
                    input.focus();
                    input.select();

                    input.blur(
                        function()
                        {
                            var val = $(this).val();
                            if(!val)
                                val = "defined";

                            $(this).parent("span[name=\"text\"]").text(val);
                            $(this).remove();
                        }
                    )
                }
            )

            element.click(
                function()
                {
                    form.setActive(this);
                }
            )
        }


    }

    this.removeField = function(element)
    {
        element = $(element).parent(".control").parent(".field");
        if(element.is(".activeField"))
            props.hide();
        element.remove();

    }

    this.fieldUp = function(element)
    {
        element = $(element).parent(".control").parent(".field");
        var prev = element.prev()
        element.insertBefore(prev);
    }

    this.fieldDown = function(element)
    {
        element = $(element).parent(".control").parent(".field");
        var next = element.next()
        element.insertAfter(next);
    }

    this.setActive = function(element)
    {
        $(element).parent(".body").find(".field").removeClass("activeField");
        $(element).addClass("activeField");
        props.init(element);
    }

    this.getActiveField = function()
    {
        var activeForm = form.getActive();

    }

    this.clearActive = function()
    {

    }
}

function IblockBar()
{
    this.iblocks;
    this.selectIblockType = $("#selectIblockType");
    this.selectIblock = $("#selectIblock");

    this.selectIblockType.bind(
        "change",
        this,
        function(event)
        {
            event.data.setIblocksType();
        }
    );

    this.selectIblock.bind(
        "change",
        this,
        function(event)
        {
            event.data.setIblocks();
        }
    )

    this.setIblocks = function()
    {
        var val = this.selectIblock.val();
        if(!val)
            return;
        var currentIblockType = this.selectIblockType.val();
        if(currentIblockType)
        {
            var values = [["", ""]];

            for(i in this.iblocks[currentIblockType].iblocks[val].PROPS)
            {
                values.push([this.iblocks[currentIblockType].iblocks[val].PROPS[i], i]);
            }


            for(var a in fieldTypes)
            {
                if(fieldTypes[a].props['iblockprop'])
                {
                    fieldTypes[a].props.iblockprop.values = values;
                }
            }

            if(props.isActive)
                props.init();
        }
    }

    this.setCurrentIblock = function(id)
    {
        this.selectIblock.find("option[value=\""+id+"\"]").prop("selected", "selected");
        this.selectIblock.change();
    }

    this.setCurrentIblockType = function(id)
    {
        this.selectIblockType.find("option[value=\""+id+"\"]").prop("selected", "selected");
        this.selectIblockType.change();
    }

    this.setIblocksType = function()
    {
        var val = this.selectIblockType.val();
        if(val)
        {
            this.selectIblock.empty();
            this.selectIblock.append("<option></option>");
            for(var i in this.iblocks[val].iblocks)
            {
                this.selectIblock.append("<option value=\""+i+"\">"+this.iblocks[val].iblocks[i].NAME+"</option>");
            }
            this.setIblocks();
        }

    }

    this.getIblockType = function()
    {
        return this.selectIblockType.val();
    }

    this.getIblock = function()
    {
        return this.selectIblock.val();
    }
}

function PropertiesBar()
{
    this.baseBlock = "#propsFields";
    this.isActive = false;

    $(".propertiesPanel .hideBtn").click(
        function()
        {
            var div = $(this).parent("div");

            var offset = div.offset();
            var width = div.width()+$(this).width()+10;
            var right = $(window).width()-offset.left-width;

            if(right >= 0)
            {
                div.animate(
                    {
                        right: -width
                    },
                    500,
                    function()
                    {
                        $(this).find(".hideBtn").addClass("hideBtnIsHidden");
                        $(this).css("width", (width-20)+"px");
                    }
                )
            }
            else
            {
                div.animate(
                    {
                        right: 0
                    },
                    500,
                    function()
                    {
                        $(this).find(".hideBtn").removeClass("hideBtnIsHidden");
                        $(this).css("width", "inherit");
                    }
                )
            }
        }
    )

    this.init = function(element)
    {
        $(this.baseBlock).parent("div").parent(".propertiesPanel").css("display", "inline-block");
        $(this.baseBlock).empty();

        if(element)
            this.element = element;
        if(!this.element)
            return;

        var type = $(this.element).attr("sysType");
        var field = $(this.element).find("span[name=\"field\"] > *").eq(0);

        for(i in fieldTypes[type].props)
        {
            var t = fieldTypes[type].props[i]
            this.addField(t, field);
        }

        this.isActive = true;
    }

    this.hide = function()
    {
        this.isActive = false;
        $(this.baseBlock).parent("div").parent(".propertiesPanel").css("display", "none");
    }

    this.addField = function(prop, field)
    {
        var field = new propFields[prop.type](this.baseBlock, prop, field);
        field.Render();
    }
}





propFields = {
    "input" : function (baseBlock, prop, field)
    {
        this.field = field;
        this.baseBlock = baseBlock;
        this.prop = prop;
        this.html = "<input type=\"text\"/>";
    },
    "date" : function (baseBlock, prop, field)
    {
        this.field = field;
        this.baseBlock = baseBlock;
        this.prop = prop;
        this.html = "<input type=\"text\" calendar=\"calendar\"/>";

        this.setEvent = function(element)
        {
            element.bind(
                "change",
                this,
                function(event)
                {
                    event.data.setValue($(this).val(), $(this).attr("setVal"));
                }
            )
        }
    },
    select : function (baseBlock, prop, field)
    {
        this.field = field;
        this.baseBlock = baseBlock;
        this.prop = prop;
        this.html = "<select></select>";

        this.Render = function()
        {
            var element = propFields.input.superclass.Render.apply(this);
            for(i in this.prop.values)
            {
                $("<option value=\""+this.prop.values[i][1]+"\">"+this.prop.values[i][0]+"</option>").appendTo(element);
            }

            element.val(this.field.attr(this.prop.attr));
        }

        this.setEvent = function(element)
        {
            element.bind(
                "change",
                this,
                function(event)
                {
                    event.data.setValue($(this).val(), $(this).attr("setVal"));
                }
            )
        }
    },

    checkbox: function (baseBlock, prop, field)
    {
        this.field = field;
        this.baseBlock = baseBlock;
        this.prop = prop;
        this.html = "<input type=\"checkbox\" value=\"true\"/>";

        this.Render = function()
        {
            var element = propFields.input.superclass.Render.apply(this);
            if(this.field.attr(this.prop.attr) == "true")
                element.attr("checked", "checked");
        }

        this.setEvent = function(element)
        {
            element.bind(
                "change",
                this,
                function(event)
                {
                    event.data.setValue($(this).is(":checked"), $(this).attr("setVal"));
                }
            );
        }
    },

    keyvalue: function(baseBlock, prop, field)
    {
        this.field = field;
        this.baseBlock = baseBlock;
        this.prop = prop;
        this.html = "<div class=\"keyvalue_field\">" +
            "<div class=\"valuesWrap\"><table class=\"values\"><tr name=\"top\"><th>Значение</th><th>Имя</th><th>&nbsp;</th></tr></table></div>" +
            "<div><input type=\"button\" value=\"Добавить\"/></div>"+
            "</div>";

        this.Render = function()
        {
            this.element = propFields.input.superclass.Render.apply(this);
            that = this;
            this.field.find("option").each(
                function()
                {
                    var value = $(this).attr("value");
                    var name = $(this).text();
                    that.addValue(name, value);
                }
            )

        }

        this.addValue = function(name, value)
        {
            var update = false;
            if(!name && !value)
            {
                name = "name";
                value = "value";
                update = true;
            }


            var el = $("<tr name=\"item\"></tr>").appendTo(this.element.find(".values"));
            var value = $("<td class=\"value\">"+value+"</td>").appendTo(el);
            var name = $("<td class=\"name\">"+name+"</td>").appendTo(el);
            var deleteBtn = $("<td><a href=\"javascript:void(0)\"><img src=\"/bitrix/images/ceolab.calcmfo/delete.gif\"/></a></td>").appendTo(el);
            var upBtn = $("<a href=\"javascript:void(0)\"><img src=\"/bitrix/images/ceolab.calcmfo/up.gif\"/></a>").appendTo(el);
            var downBtn = $("<a href=\"javascript:void(0)\"><img src=\"/bitrix/images/ceolab.calcmfo/down.gif\"/></a>").appendTo(el);

            that = this;
            name.click(this.editEvent);
            value.click(this.editEvent);

            upBtn.click(
                function()
                {
                    var element = $(this).parent("tr");
                    if(!element.prev("tr").is("[name=\"top\"]"))
                        element.insertBefore(element.prev("tr"));
                }
            )

            downBtn.click(
                function()
                {
                    var element = $(this).parent("tr");
                    element.insertAfter(element.next("tr"));
                }
            )

            deleteBtn.click(
                function()
                {
                    $(this).parent("tr").remove();
                }
            )
            if(update)
                this.updateField()
        }

        this.up = function(element)
        {

        }

        this.down = function(element)
        {

        }

        this.updateField = function(element)
        {
            that = this;
            this.field.empty();
            $(this.element).find(".values").find("tr[name=\"item\"]").each(
                function()
                {
                    var name = $(this).find(".name").text();
                    var value = $(this).find(".value").text();
                    $("<option value=\""+value+"\">"+name+"</option>").appendTo(that.field)
                }
            )
        }

        this.editEvent = function()
        {
            var val = $(this).text();
            $(this).empty();

            var input = $("<input type=\"text\"/>").appendTo($(this));

            input.val(val);
            input.focus();
            input.select();
            $(this).unbind("click");

            input.blur(
                function()
                {
                    var val = $(this).val();
                    var div = $(this).parent("td");
                    div.empty();
                    div.text(val);
                    div.click(that.editEvent);
                    $(this).unbind("blur");
                    that.updateField();
                }
            )
        }


        this.setEvent = function(element)
        {
            that = this;
            element.find("input[type=\"button\"]").bind(
                "click",
                this,
                function(event)
                {
                    event.data.addValue(this);
                }
            );
        }
    }
}

function validatorsToValues()
{
    var values = [["", ""]];
    for(i in validators)
    {
        values.push([validators[i].name, i]);
    }
    return values;
}

propValidators = {
    uniqueValue:
    {
        errorMessage : "Значение должно быть уникально",
        check : function(element, th)
        {
            var val = element.val();

            var fields = $("#formItems .item .body .field span > input[name=\""+val+"\"]");
            for(var i = 0; i<fields.length; i++)
                if(fields.get(i) != th.field.get(0))
                {
                    alert(this.errorMessage)
                    return false;
                }

            return true;
        }
    }
}

function propValidators_proto()
{

}

propValidators_proto.prototype.errorAlert = function(message)
{
    alert(message);
}

fieldTypes = {
    inputText : {
        html : "<input type=\"text\"/>",
        defaultName: "Текстовое поле",
        props: {
            name : {
                type: "input",
                attr: "name",
                title: "Имя переменной",
                validator: propValidators.uniqueValue
            },
            validator: {
                type: "select",
                attr: "validator",
                title: "Валидатор",
                values: validatorsToValues()
            },
            attachTo: {type: "input", attr: "attachTo", "title": "Привязать к (имя переменной поля)"},
            require: {type: "checkbox", attr: "require", title: "Обязательное поле"},
            iblockprop:  {
                type: "select",
                attr: "iblockprop",
                title: "Свойство инфоблока"
            },
            mask : {
                type: "input",
                attr: "mask",
                title: "Маска ввода"
            }
        }
    },
    select : {
        html : "<select></select>",
        defaultName: "Селект",
        props: {
            name : {
                type: "input",
                attr: "name",
                title: "Имя переменной",
                validator: propValidators.uniqueValue
            },
            attachTo: {type: "input", attr: "attachTo", "title": "Привязать к (имя переменной поля)"},
            require: {type: "checkbox", attr: "require", title: "Обязательное поле"},
            values: {type: "keyvalue", attr: "test", title: "Значения"},
            iblockprop:  {
                type: "select",
                attr: "iblockprop",
                title: "Свойство инфоблока"
            }
        }
    },
    checkbox : {
        html: "<input type=\"checkbox\"/>",
        defaultName: "Чекбокс",
        props: {
            name : {type: "input", attr: "name", title: "Имя переменной"},
            require: {type: "checkbox", attr: "require", title: "Обязательное поле"},
            attachTo: {type: "input", attr: "attachTo", "title": "Привязать к (имя переменной поля)"},
            iblockprop:  {
                type: "select",
                attr: "iblockprop",
                title: "Свойство инфоблока"
            }
        }
    },
    file : {
        html: "<input type=\"file\"/>",
        defaultName: "Файл",
        props : {
            name : {type: "input", attr: "name", title: "Имя переменной", validator: propValidators.uniqueValue},
            max_size: {type: "input", attr: "max_size", title: "Макс. размер"},
            filetypes: {type: "input", attr: "filetypes", title: "Разрешение файлов"},
            require: {type: "checkbox", attr: "require", title: "Обязательное поле"},
            attachTo: {type: "input", attr: "attachTo", "title": "Привязать к (имя переменной поля)"}
        }
    },

    date : {
        html: "<input type=\"\text\" calendar=\"calendar\" placeholder=\"дд.мм.гггг\"/>",
        defaultName: "Дата",
        props: {
            name : {type: "input", attr: "name", title: "Имя переменной", validator:propValidators.uniqueValue},
            require: {type: "checkbox", attr: "require", title: "Обязательное поле"},
            defaultDate: {type: "date", attr: "defaultDate", title: "Месяц, год по умолчанию"},
            attachTo: {type: "input", attr: "attachTo", "title": "Привязать к (имя переменной поля)"},
            iblockprop:  {
                type: "select",
                attr: "iblockprop",
                title: "Свойство инфоблока"
            }
        }
    }
}


function extend(Child, Parent) {
    var F = function() { }
    F.prototype = Parent.prototype
    Child.prototype = new F()
    Child.prototype.constructor = Child
    Child.superclass = Parent.prototype
}

/**
 * Created with JetBrains WebStorm.
 * User: evgenius
 * Date: 11/16/13
 * Time: 1:59 AM
 * To change this template use File | Settings | File Templates.
 */

Date.prototype.daysInMonth = function()
{
    return 33 - new Date(this.getFullYear(), this.getMonth(), 33).getDate();
};

Date.prototype.firstDayInMonth = function()
{
    return new Date(this.getFullYear(), this.getMonth(), 1).getDay();
};
//@todo Refactoring. f variable must be as property
Date.prototype.toLocaleFormat = function(format)
{
    var f = {y : this.getFullYear(), m : this.getMonth() + 1,d : this.getDate(),H : this.getHours(),M : this.getMinutes(),S : this.getSeconds()}
    for(var k in f)
        format = format.replace('%' + k, f[k] < 10 ? "0" + f[k] : f[k]);
    return format;
};
//@todo Refactoring. f variable must be as property
Date.prototype.fromLocaleFormat = function(string, format)
{
    if(!string || !format)
        return;

    var f = {
        y : function(v, date)
        {
            return date.setFullYear(v);
        },
        m : function(v, date)
        {
            return date.setMonth(v-1);
        },

        d : function(v, date)
        {
            return date.setDate(v);
        }
    };
    string = string.split(".");
    format = format.split(".");

    for(var k in string)
    {
        var key = format[k].replace("%", "");
        if(!parseInt(string[k]))
            return false;
        f[key](parseInt(string[k]), this);
    }

    return true;
}

Date.prototype.setCurrentDate = function()
{
    var d = new Date();
    this.setTime(d.getTime());
}




function Calendar(selector)
{
    this.date = new Date();
    this.date.setDate(1);
    this.cType = "months";
    this.type = "months";
    this.resultDate = new Date();
    this.selector = selector;
    this.input = false;
    this.format = "%d.%m.%y";
    var f = {
        d:"setfullYear",
        m:"setMonth()"
    }


    this.init = function()
    {
        this.base = $("<div class=\"calendar\"></div>").appendTo("body");
        var yearMonth = $("<div align=\"center\" class=\"year_month\"></div>").appendTo(this.base);
        var leftArrow = $("<div class=\"arrow_prev\"><img src=\"img/arrow_prev.png\"/></div>").appendTo(yearMonth);
        var months = $("<span name=\"month\"></span>").appendTo(yearMonth);
        var year = $("<span name=\"year\"></span>").appendTo(yearMonth);
        var rightArrow = $("<div class=\"arrow_next\"><img src=\"img/arrow_next.png\"/></div>").appendTo(yearMonth);

        $(window).bind(
            "click",
            this,
            function(event)
            {
                if(!$(event.target).is(event.data.selector) && !$(event.target).parents(".calendar").length)
                    event.data.hide();
            }
        )

        leftArrow.bind("click", this,
            function(event)
            {
                event.data.prev();
            }
        );

        rightArrow.bind("click",this,
            function(event)
            {
                event.data.next();
            }
        );

        months.bind(
            "click",
            this,
            function(event)
            {
                event.data.setType("months");
            }
        );

        year.bind(
            "click",
            this,
            function(event)
            {
                event.data.setType("years");
            }
        );

        this.calendar = $("<div id=\"calendar\"></div>").appendTo(this.base);

        $("html").bind("click", this,
             function(event)
             {
                 if($(event.target).is(event.data.selector))
                 {
                     var offset = $(event.target).offset();


                     if(!event.data.resultDate.fromLocaleFormat($(event.target).val(), event.data.format))
                     {
                         if(!event.data.date.fromLocaleFormat($(event.target).attr("startVal"), event.data.format))
                         {
                             event.data.date.setCurrentDate();
                         }
                     }
                     else
                     {
                         event.data.date.fromLocaleFormat($(event.target).val(), event.data.format)
                     }


                     event.data.show();
                     event.data.Rend();
                     var corr = $(event.data.base).width()+offset.left-$(window).width();
                     //@TODO TODO!!!
                     if(corr < 0)
                        corr = 0;
                     else
                        corr += 50;

                     event.data.setPosition(offset.left-corr, offset.top);
                     event.data.input = $(event.target);
                 }
             }
        )

        this.hide();
    }

    this.hide = function()
    {
        this.base.hide();
    }

    this.show = function()
    {
        this.type = this.cType;
        this.base.show();
        this.Rend();
    }

    this.setPosition = function(x, y)
    {
        this.base.css("top", y);
        this.base.css("left", x);
    }

    this.monthName = [
        "Январь",
        "Февраль",
        "Март",
        "Апрель",
        "Май",
        "Июнь",
        "Июль",
        "Август",
        "Сентябрь",
        "Октябрь",
        "Ноябрь",
        "Декабрь"
    ];
    this.setMonth = function(month)
    {
        this.date.setMonth(month-1);
    }

    this.applyValue = function()
    {
        this.input.val(this.resultDate.toLocaleFormat(this.format));
        this.input.change();
        this.hide();
    }


    this.Rend = function(animate)
    {
        var curMonth = this.date.getMonth();
        var curYear = this.date.getFullYear();
        $(".calendar span[name=\"year\"]").text(curYear);
        $(".calendar span[name=\"month\"]").text(this.monthName[curMonth]);

        if(this.type == "days")
        {
            var d = this.date.firstDayInMonth();

            if(!d)
                d = 7;

            var data = [];

            for(i=1; i<=this.date.daysInMonth(); i++)
            {
                var highlight = false;

                if(i == this.resultDate.getDate() &&
                    this.resultDate.getMonth() == this.date.getMonth() &&
                    this.resultDate.getFullYear() == this.date.getFullYear())
                    highlight = true;

                data.push({text: i, val: i, highlight: highlight});
            }

            this.date.setMonth(curMonth);

            this.RendTable(data, d, 7, animate);

        }
        else if(this.type == "months")
        {
            var data = [];

            for(i in this.monthName)
            {
                var highlight = false;
                if(this.date.getMonth() == i && this.resultDate.getFullYear() == this.date.getFullYear())
                    highlight = true;

                data.push({text:this.monthName[i], val:i, highlight: highlight});
            }

            this.RendTable(data, 1, 3, animate);
        }
        else if(this.type == "years")
        {
            var range = 10;
            var data = [];
            for(i=this.date.getFullYear()-range; i<this.date.getFullYear()+range; i++)
            {
                var highlight = false;
                if(this.resultDate.getFullYear() == i)
                    highlight = true;

                data.push({text: i, val: i, highlight:highlight});
            }
            this.RendTable(data, 1, 5, animate);
        }

    }

    this.getDateString = function()
    {
        this.resultDate.localeFormat();
    }

    this.RendTable = function(data, start, columns, animate)
    {
        if(!animate)
            $("#calendar").empty();

        var lastTable = $("#calendar > table");
        var crHide = false;
        if(lastTable.length)
            crHide = true;


        var table = $("<table></table>").appendTo("#calendar");

        if(this.type == "days")
            table.append("<th>Пн</th><th>Вт</th><th>Ср</th><th>Чт</th><th>Пт</th><th>Сб</th><th>Вс</th>");
        var key = 0;
        var rend = false;
        var breakFlag = false;;

        while(!breakFlag)
        {
            var tr = $("<tr></tr>").appendTo(table);
            for(i=1; i<=columns; i++)
            {
                if(i == start)
                    rend = true;

                var td = $("<td></td>").appendTo(tr);
                that = this;

                if(rend)
                {
                    if(!data[key])
                    {
                        breakFlag = true;
                        continue;
                    }
                    td.attr("val", data[key].val);
                    td.addClass("no_empty_cell");
                    if(data[key]['highlight'])
                        td.addClass("highlight");
                    td.click(
                        function()
                        {
                            that.setVal($(this).attr("val"))
                        }
                    );

                    td.text(data[key].text);
                    key++;
                }
            }

            if(breakFlag)
                break;
        }
        if(crHide && animate)
        {
            this.animations[animate](lastTable, table);
        }
    }

    this.setVal = function(val)
    {
        var close = false;
        if(this.type == this.cType)
              close = true;

        if(this.type == "months")
        {
            this.date.setMonth(val);
            this.setType("days");
        }
        else if(this.type == "years")
        {
            this.date.setFullYear(val);
            this.setType("months");

        }
        else if(this.type == "days")
        {
            this.date.setDate(val);
            this.Rend(false);
        }

        if(close)
        {
            this.resultDate = new Date(this.date.toDateString());
            this.applyValue();
            return;
        }

    }

    this.next = function()
    {
        if(this.type == "days")
        {
            var month = this.date.getMonth();
            this.date.setMonth(month+1);
        }
        else if(this.type == "months")
        {
            var year = this.date.getFullYear();
            this.date.setFullYear(year+1);
        }
        else if(this.type == "years")
        {
            var year = this.date.getFullYear();
            this.date.setFullYear(year+10);
        }

        this.Rend("left");
    }
    this.prev = function()
    {
        if(this.type == "days")
        {
            var month = this.date.getMonth();
            this.date.setMonth(month-1);
        }
        else if(this.type == "months")
        {
            var year = this.date.getFullYear();
            this.date.setFullYear(year-1);
        }
        else if(this.type == "years")
        {
            var year = this.date.getFullYear();
            this.date.setFullYear(year-10);
        }

        this.Rend("right");
    }

    this.setType = function(type)
    {
        this.type = type;
        this.Rend("down");
    }

    this.animations = {
        left: function(lastTable, currentTable)
        {

            var width = $("#calendar").width();
            currentTable.addClass("onDrive");
            currentTable.css("left", width);
            currentTable.css("top", 0);

            lastTable.animate(
                {
                    left: -lastTable.height()
                },
                500
            )

            currentTable.animate(
                {
                    left: 0
                },
                500,
                function()
                {
                    $(this).attr("t", "true");
                    $(this).parent("div").find("table:not([t=\"true\"])").remove();
                    $(this).removeAttr("t");
                    $(this).removeClass("onDrive");
                }
            )
        },

        right : function(lastTable, currentTable)
        {
            var width = $("#calendar").width();
            currentTable.addClass("onDrive");
            currentTable.css("left", -width);
            currentTable.css("top", 0);

            lastTable.animate(
                {
                    left: width
                },
                500
            )

            currentTable.animate(
                {
                    left: 0
                },
                500,
                function()
                {
                    $(this).attr("t", "true");
                    $(this).parent("div").find("table:not([t=\"true\"])").remove();
                    $(this).removeAttr("t");
                    $(this).removeClass("onDrive");
                }
            )
        },

        down : function(lastTable, currentTable)
        {
            var height = $("#calendar").height();
            currentTable.addClass("onDrive");
            currentTable.css("left", 0);
            currentTable.css("top", height);


            currentTable.animate(
                {
                    top: 0
                },
                500,
                function()
                {
                    $(this).attr("t", "true");
                    $(this).parent("div").find("table:not([t=\"true\"])").remove();
                    $(this).removeAttr("t");
                    $(this).removeClass("onDrive");
                }
            )
        }
    }

    this.init();
}

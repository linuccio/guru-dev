/**
 * Created with JetBrains WebStorm.
 * User: evgenius
 * Date: 11/15/13
 * Time: 6:35 PM
 * To change this template use File | Settings | File Templates.
 */
function propFieldsBase(baseBlock, prop, field)
{
    this.prop = prop;
}
propFieldsBase.prototype.Render = function()
{
    var element = $(this.getHtml()).appendTo(this.baseBlock);
    element = element.find(".field");
    element = $(this.html).appendTo(element);
    var val = this.field.attr(this.prop.attr);
    element.val(val);
    element.attr("setVal", this.prop.attr);
    this.setEvent(element);
    this.element = element;
    return element;
}

propFieldsBase.prototype.getHtml = function()
{
    var html = "<div class=\"item\"><div class=\"title\">"+this.prop.title+"</div><div class=\"field\"></div> </div>";
    return html;
}

propFieldsBase.prototype.setValue = function(value, attr)
{
    this.field.attr(attr, value);
}

propFieldsBase.prototype.setEvent = function(element)
{
    that = this;
    element.bind(
        "blur",
        this,
        function(event)
        {
            console.debug(event.data.prop.validator);
            if(!event.data.prop['validator'] || event.data.prop.validator.check($(this), event.data))
            {
                event.data.setValue($(this).val(), $(this).attr("setVal"));
            }
            else
            {
                $(this).css("border", "1px solid red");
                $(this).val("");
                $(this).focus();
                $(this).select();
            }
        }
    )
}
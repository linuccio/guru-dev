<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 12/9/13
 * Time: 5:11 PM
 * To change this template use File | Settings | File Templates.
 */
?>



<?if(!isset($arResult['STATUS'])):?>
    <script>
        $("document").ready(
            function()
            {
                var fields = [
                    {
                        selector: $("input[name=reg_city]"),
                        type: $.kladr.type.city
                    },
                    {
                        selector: $("input[name=reg_street]"),
                        type: $.kladr.type.street
                    },
                    {
                        selector: $("input[name=reg_house]"),
                        type: $.kladr.type.building
                    },
                    {
                        selector: $("input[name=reg_region]"),
                        type: $.kladr.type.region
                    }
                ];

                var kladr1 = $();
                for(var i in fields)
                {
                    kladr1.add(fields[i].selector);
                    fields[i].selector.kladr(
                        {
                            type : fields[i].type
                        }
                    )
                }

                kladr1.kladr(
                    {
                        parentInput: $("#live_address"),
                        verify: true
                    }
                );


                $("input[name=live_post_index]").kladrZip("#live_address");


                var fields = [
                    {
                        selector: $("input[name=live_city]"),
                        type: $.kladr.type.city
                    },
                    {
                        selector: $("input[name=live_street]"),
                        type: $.kladr.type.street
                    },
                    {
                        selector: $("input[name=live_house]"),
                        type: $.kladr.type.building
                    },
                    {
                        selector: $("input[name=region]"),
                        type: $.kladr.type.region
                    }
                ];

                var kladr1 = $();
                for(var i in fields)
                {
                    kladr1.add(fields[i].selector);
                    fields[i].selector.kladr(
                        {
                            type : fields[i].type
                        }
                    )
                }

                kladr1.kladr(
                    {
                        parentInput: $("#live_address"),
                        verify: true
                    }
                );


                $("input[name=live_post_index]").kladrZip("#live_address");



                smsConfirm = new function()
                {
                    this.resend = true;
                    this.isChecked = false;
                    this.showForm = function()
                    {
                        var top = ($("#modalWrap").height() / 2) - ($("#modal").height() / 2) - 50;
                        var left = ($("#modalWrap").width() / 2) - ($("#modal").width() / 2);
                        $("#modal").css(
                            {
                                top: top,
                                left: left
                            }
                        );

                        this.phone = $("#mfoAnketForm input[name=mobile_phone]").val();
                        $("#modal span.phone").text(this.phone);

                        $("#modalWrap").show();

                        this.sendCode();
                    }


                    this.checkCode = function()
                    {
                        var code = $("#modal").find("input.phonecode").val();

                        if(code && this.phone)
                        {
                            $.post(
                                "/bitrix/modules/ceolab.lkmfo/controllers/smsconfirm.php",
                                {
                                    phone : this.phone,
                                    code : code
                                },
                                function(data)
                                {
                                    if(data == "ok")
                                    {
                                        smsConfirm.closeForm();
                                        smsConfirm.isChecked = true;
                                        $("#mfoAnketForm").submit();
                                    }
                                    else if(data == "fail")
                                    {
                                        smsConfirm.showError("Неверный код");
                                    }
                                }
                            )
                        }

                    }

                    this.sendCode = function()
                    {
                        if(this.phone && this.resend)
                        {
                            $.post(
                                "/bitrix/modules/ceolab.lkmfo/controllers/smsconfirm.php",
                                {
                                    phone : this.phone
                                },
                                function(data)
                                {
                                    if(data != "ok")
                                    {
                                        //smsConfirm.showError("Отправка не удалась");
                                    }
                                }
                            )
                            this.resend = false;
                            this.timerResendStart();
                        }
                    }

                    this.timerResendStart = function()
                    {
                        $("#resendsms").addClass("disabledLink");


                        sendSmsTime = new Date();
                        sendSmsTime = sendSmsTime.getTime();
                        sendSmsmInterval = setInterval(
                            function()
                            {
                                var time = new Date();
                                time = 60 - parseInt((time.getTime() - sendSmsTime) / 1000);

                                if(time <= 0)
                                {
                                    clearInterval(sendSmsmInterval);
                                    $("#resendsms").removeClass("disabledLink");
                                    $("#resendsms").text("Отправить код повторно");
                                    smsConfirm.resend = true;
                                }
                                else
                                {
                                    $("#resendsms").text("Повторная отправка через ("+time+")");
                                }
                            },
                            1000
                        );
                    }

                    this.closeForm = function()
                    {
                        $("#modalWrap").hide();
                    }

                    this.showError = function(text)
                    {
                        $("#modal .error").text(text);
                        $("#modal .error").show();
                    }

                    this.hideError = function()
                    {
                        $("#modal .error").hide();
                    }



                    $("#modal").find("input.btn").bind(
                        "click",
                        this,
                        function(event)
                        {
                            event.data.checkCode();
                            event.data.hideError();
                        }
                    );

                    $("#resendsms").bind(
                        "click",
                        this,
                        function(event)
                        {
                            event.data.sendCode();
                        }
                    )


                    $("#modal").find("input.phonecode").bind(
                        "keyup",
                        this,
                        function(event)
                        {
                            if($(this).val().length == $(this).attr("maxlength"))
                            {
                                $("#modal").find("input.btn").removeAttr("disabled");
                                event.data.hideError();
                            }
                            else
                            {
                                $("#modal").find("input.btn").attr("disabled", "disabled");
                            }
                        }
                    );
                    $("#modal").find("input.phonecode").keyup();
                }



            }
        );
    </script>
<form class="anket" align="center" method="POST" id="mfoAnketForm" enctype="multipart/form-data">
    <input type="hidden" name="anket" value="<?=$arResult['anketID']?>"/>
    <input type="hidden" name="product" value="<?=$arResult['product']?>"/>
    <input type="hidden" name="summ" value="<?=$arResult['summ']?>"/>
    <input type="hidden" name="term" value="<?=$arResult['term']?>"/>

    <div class="title" align="center">
        Заполните заявку на займ
    </div>
    <div align="left" class="items">
        <div style="overflow: hidden">
            <div align="left" class="item">
                <div class="headitem">
                        <span>
                            Личная информация
                        </span>

                </div>
                <div class="body">
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Фамилия<span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="lastname" isrequired="1">
                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Имя<span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="firstname" isrequired="1">
                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Отчество<span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="middlename" isrequired="1">
                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Пол <span class="requiredstar">*</span></div>
                            <div class="field">
                                <select name="gender" isrequired="1">
                                    <option disabled="disabled" selected="selected"></option>
                                    <option value="Мужской">Мужской</option>
                                    <option value="Женский">Женский</option>
                                </select>

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Дата рождения <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" placeholder="дд.мм.гггг" mask="99.99.9999" name="birthdate" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Место рождения <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="birthplace" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Гражданство <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="nationality" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Семейное положение <span class="requiredstar">*</span></div>
                            <div class="field">
                                <select name="marital_status" isrequired="1">
                                    <option disabled="disabled" selected="selected"></option>
                                    <option value="married">женат/замужем</option>
                                    <option value="Single">холост/не замужем</option>
                                </select>

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Количество детей <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="count_children" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Уровень образования <span class="requiredstar">*</span></div>
                            <div class="field">
                                <select name="education" isrequired="1">
                                    <option disabled="disabled" selected="selected"></option>
                                    <option value="edu_higher">Высшее</option>
                                    <option value="edu_secondary">Среднее</option>
                                    <option value="edu_general">Общее</option>
                                </select>

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                </div>
            </div>
            <div align="left" class="item">
                <div class="headitem">
                        <span>
                            Паспортные данные
                        </span>

                </div>
                <div class="body">
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Серия<span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="pass_seria" mask="99 99" isrequired="1">
                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Номер<span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="pass_num" mask="999999" isrequired="1">
                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Дата выдачи<span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="pass_date" mask="99.99.9999" placeholder="дд.мм.гггг" isrequired="1"/>
                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Кем выдан<span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="pass_issued" isrequired="1"/>
                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Код подразделения<span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="pass_code" mask="999-999" isrequired="1"/>
                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                </div>
            </div>
        </div>

        <div style="overflow: hidden">
            <div align="left" class="item">
                <div class="headitem">
                        <span>
                            Адрес прописки                    </span>

                </div>
                <div class="body" id="reg_address">
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Почтовый индекс <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="reg_post_index" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Регион <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="reg_region" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Район <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="reg_district" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Область <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="reg_locality" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Город <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="reg_city" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Улица <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="reg_street" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Дом <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="reg_house" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Корпус</div>
                            <div class="field">
                                <input type="text" name="reg_part">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Строение</div>
                            <div class="field">
                                <input type="text" name="reg_build">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Квартира <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="reg_room" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                </div>
            </div>
            <div align="left" class="item" id="live_address">
                <div class="headitem">
                        <span>
                            Адрес фактического проживания                    </span>

                </div>
                <div class="body">
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Почтовый индекс <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="live_post_index" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Регион <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="live_region" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Район <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="live_district" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Область <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="live_locality" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Город <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="live_city" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Улица <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="live_street" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Дом <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="live_house" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Корпус</div>
                            <div class="field">
                                <input type="text" name="live_part">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Строение</div>
                            <div class="field">
                                <input type="text" name="live_build">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Квартира <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="live_room" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                </div>
            </div>
        </div>

        <div style="overflow: hidden">
            <div align="left" class="item">
                <div class="headitem">
                        <span>
                            Контактные данные                    </span>

                </div>
                <div class="body">
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Мобильный <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="mobile_phone" mask="+7 (999) 999-99-99" isrequired="1">
                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Домашний <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="phone" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Рабочий <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="workphone" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Электронная почта <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="email" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                </div>
            </div>
            <div align="left" class="item">
                <div class="headitem">
                        <span>
                            Работа                    </span>

                </div>
                <div class="body">
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Наименование юр.лица / ИП <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="orgname" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Должность<span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="job_post" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>

                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Размер месячного дохода (после вычета НДФЛ) <span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="job_income" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>
                    <div class="fieldWrap">
                        <div class="fw">
                            <div class="text">Опыт работы (в годах)<span class="requiredstar">*</span></div>
                            <div class="field">
                                <input type="text" name="experience" isrequired="1">

                            </div>
                        </div>
                        <div name="errorMessage" class="errorMessage"></div>
                    </div>



                </div>
            </div>
        </div>
    </div>

    <div style="margin-top: 20px">

        <input type="checkbox" id="c1" validator="confirm" name="confirm"/>
        <label for="c1"><span></span></label>
        <span style="color: gray; font-size: 24px">Я даю согласие на обработку персональных данных</span>
        <div class="submitWrap">
            <div class="btnBorderWrap">
                <input type="hidden" name="submit_btn" value="1"/>
                <input type="submit" value="Продолжить"/>
            </div>

        </div>
    </div>

    <div class="modalWrap" id="modalWrap">
        <div class="modal" id="modal">
            <div class="content-smsform">
                На номер <span class="phone">+7 (909) 340-01-45</span> отправлена смс с кодом подтверждения
                <div class="error">Неверный код</div>
                Введите код: <input type="text" maxlength="6" x class="phonecode" name="phonecode"/> <input type="button" class="btn" value="Подтвердить"/>
                <br/>
                <a href="javascript:void(0)" id="resendsms" disabled="disabled">Отправить код повторно</a>
            </div>
        </div>
    </div>
</form>
<?else:?>
<h2>Ваша заявка успешно отправлена</h2>
<?endif?>
<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("name"),
    "DESCRIPTION" => 'Calc',
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => GetMessage("id")
    ),
);
?>

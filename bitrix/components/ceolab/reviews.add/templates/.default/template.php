<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>

<?if (count($arResult["ERRORS"])):?>
    <?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
    <?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<div class="reviews">
    <form class="form" align="left" method="POST">
        <?=bitrix_sessid_post()?>
        <?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>

        <div>
            <div class="formblock left">
                Ваша фамилия:<br/>
                <input name="lastname" type="text"/><br/>
                Ваше имя:<br/>
                <input name="firstname" type="text"/>
            </div>
            <div class="formblock right">
                Введите свой отзыв:<br/>
                <textarea name="review"></textarea>
            </div>
        </div>
        <div align="center">
            <input class="submit" type="submit" value="оставить отзыв">
        </div>
    </form>
</div>
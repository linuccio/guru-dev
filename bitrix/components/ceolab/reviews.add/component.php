<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 10/11/13
 * Time: 12:25 PM
 * To change this template use File | Settings | File Templates.
 */

$validators = array(
    "no_empty" => array(
       "pattern" => "!.*?!is",
        "errorMessage" => "not_empty_error"
    ),
    "email" => array(
        "pattern" => "!.*?!is",
        "errorMessage" => "not_empty_error"
    )
);

$fields = array(
    "FIRSTNAME" => "no_empty",
    "LASTNAME" => "no_empty",
    "REVIEW" => "no_empty"
);

$error = false;
foreach($fields as $name => $validator)
{
    if(!preg_match($validator['pattern'], $_POST[$name]))
    {
        $error = true;
        $errors[$name] = $validator['errorMessage'];
    }
    else
    {
        $arFields[$name] = $_POST[$name];
    }
}

if(!$error)
{
    $el = new CIBlockElement();
    $el->Add($arFields);
}


$this->includeComponentTemplate();
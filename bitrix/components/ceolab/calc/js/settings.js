/**
 * Created with JetBrains PhpStorm.
 * User: EvGenius
 * Date: 22.03.13
 * Time: 9:09
 * To change this template use File | Settings | File Templates.
 */

function calcSettings(arParams)
{
    this.arParams = arParams;
    try
    {
        arParams.oCont.removeChild(this.div);
    }
    catch(e){};

    this.div = document.createElement('DIV');
    arParams.oCont.appendChild(this.div);
    var lang = JSON.parse(arParams.data).lang;


    var calcsData=[];

    try
    {
        calcsData = JSON.parse(arParams.oInput.value);

    }
    catch(e)
    {
        calcsData = {
            calcs:[]
        }
    }
    /*
    if(!calcsData.calcs)
        calcsData = {
            'calcs':[]
        };
    */

    if(calcsData.calcs.length < 20)
    {

        var obButton = document.createElement('BUTTON');
        this.div.appendChild(obButton);
        obButton.innerHTML = lang['add'];
        obButton.onclick = BX.delegate(openAddCalcEditor, this);

    }

    var table = document.createElement("table");
    var tbody = document.createElement("tbody");
    table.appendChild(tbody);
    this.div.appendChild(table);

    var tr;

    if(calcsData.calcs.length)
        for(var key in calcsData.calcs)
        {
            tr = document.createElement('tr');

            tbody.appendChild(tr);
            td = document.createElement("td");

            td.innerHTML = calcsData.calcs[key].calc.name;
            td.setAttribute("calcId", key);
            td.onclick = function(event){
                if(event === undefined)
                    var event = window.event;


                var target = event.target || event.srcElement;

                var id = target.getAttribute("calcId");
                openCalcEditor(id);
            };
            td.setAttribute("style", "cursor:pointer;");

            deleteBtn = document.createElement("td");
            deleteBtn.innerHTML = "["+lang['delete']+"]";
            deleteBtn.setAttribute("calcId", key);
            deleteBtn.onclick = function(event){
                if(event === undefined)
                    var event = window.event;


                var target = event.target || event.srcElement;

                var id = target.getAttribute("calcId");
                deleteCalc(id);
            };
            deleteBtn.setAttribute("style", "cursor:pointer;");

            active = document.createElement("td");
            activeCheckBox = document.createElement("input");
            activeCheckBox.setAttribute("type", "checkbox");




            activeCheckBox.setAttribute("calcId", key);
            activeCheckBox.onchange = function(event){
                if(event === undefined)
                    var event = window.event;


                var target = event.target || event.srcElement;

                var id = target.getAttribute("calcId");
                activeCalc(id);
            };
            activeCheckBox.setAttribute("id", "activeCalc"+key);


            positionUp = document.createElement("td");
            positionDown = document.createElement("td");

            active.appendChild(activeCheckBox);

            if(calcsData.calcs[key].calc.active)
            {
                activeCheckBox.checked = true;
            }

            if(key > 0)
            {
                //positionCalc("+key+", 'up')
                //positionUp.setAttribute("style","background: url(/bitrix/components/ceolab/calc.mfo/templates/.default/img/arrowUp.png)");
                positionUp.innerHTML = "["+lang['up']+"]";
                positionUp.setAttribute("style", "cursor:pointer;");
                positionUp.setAttribute("calcId", key);
                positionUp.onclick = function(event){
                    if(event === undefined)
                        var event = window.event;


                    var target = event.target || event.srcElement;

                    var id = target.getAttribute("calcId");
                    positionCalc(id, 'up')
                };
            }
            if(key < calcsData.calcs.length-1)
            {
                positionDown.innerHTML = "["+lang['down']+"]";
                positionDown.setAttribute("style", "cursor:pointer;");
                positionDown.setAttribute("calcId", key);
                positionDown.onclick = function(event){
                    if(event === undefined)
                        var event = window.event;

                    var target = event.target || event.srcElement;

                    var id = target.getAttribute("calcId");

                    positionCalc(id, 'down')
                };
            }

            tr.appendChild(td);
            tr.appendChild(deleteBtn);
            tr.appendChild(positionUp);
            tr.appendChild(positionDown);
            tr.appendChild(active);

        }


    this.arParams = arParams;

}

function openCalcEditor(id)
{
    window.jsPopup_calcEditor = new BX.CDialog({
        'content_url': '/bitrix/components/ceolab/calc/settings/settings.php',
        'content_post' : "CALC_ID="+id,
        'width':1000, 'height':500,
        'resizable':true
    });

    window.jsPopup_calcEditor.Show();

    return false;
}

function positionCalc(id, direct)
{
    id = parseInt(id);
    if(direct == "up")
        newId = id-1;
    else if(direct == "down")
        newId = id+1;
    else
        return;

    data = JSON.parse(this.arParams.oInput.value);

    d = data.calcs[id];

    data.calcs.splice(id,1,data.calcs[newId]);

    data.calcs[newId] = d;

    this.arParams.oInput.value = JSON.stringify(data);
    this.div.innerHTML = "";
    calcSettings(this.arParams);

}

function activeCalc(id)
{
    data = JSON.parse(this.arParams.oInput.value);
    data.calcs[id].calc.active = $("#activeCalc"+id).prop("checked");
    this.arParams.oInput.value = JSON.stringify(data);
}

function deleteCalc(id)
{
    if(confirm("Delete?"))
    {
        data = JSON.parse(this.arParams.oInput.value);
        data.calcs.splice(id, 1);
        this.arParams.oInput.value = JSON.stringify(data);
        this.div.innerHTML = "";
        calcSettings(this.arParams);
    }
}

function openAddCalcEditor()
{
    window.jsPopup_calcEditor = new BX.CDialog({
        'content_url': '/bitrix/components/ceolab/calc/settings/settings.php',
        'width':1000, 'height':500,
        'resizable':true
    });

    window.jsPopup_calcEditor.Show();

    return false;
}


function addCalc()
{
    openCalcEditor();
}

function editCalc()
{
    openCalcEditor("edit");
}

/*
function checkFormCalc()
{
    if($("#name").val().length == 0)
        return;

    result = true;
    $("table[name=calcFieldSet] input:not(#name, #type_percent)").each(function(i, elem)
    {
        if(!parseInt($(elem).val()) || parseInt($(elem).val()) < 0)
        {
            result = false;
            return;
        }
    });

    return result;
}
*/


function htmlspecialchars(html) {
    html = html.replace(/&/g, "&amp;");
    html = html.replace(/</g, "&lt;");
    html = html.replace(/>/g, "&gt;");
    html = html.replace(/"/g, "&quot;");
    return html;
}
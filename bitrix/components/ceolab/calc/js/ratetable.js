var RateTable = function (initData) {
    this.data = initData || {};
}

RateTable.prototype.init = function (initData) {
    this.data = initData || {};
}

RateTable.prototype.getRate = function (amount) {
    var maxK = -Infinity;
    
    for (var k in this.data) {
        if (+k > maxK && +k <= amount) {
            maxK = +k;
        }
    }
   
    return this.data[maxK];
}
 
RateTable.prototype.minAmount = function () {
    var minK = Infinity;

    for (var k in this.data) {
        if (+k < minK) {
            minK = +k;
        }
    }

    return (minK === Infinity? 0 : minK);
}
 
RateTable.prototype.maxAmount = function () {
    var maxK = -Infinity;
    
    for (var k in this.data) {
        if (+k > maxK) {
            maxK = +k;
        }
    }

    return (maxK === -Infinity? 0 : maxK);
}
 
RateTable.prototype.renderTable = function (parentId) {
    var deleteRow = function () {
        $(this).closest('tr').remove();
    }
   
    var tableHtml = '';
   
    for (var k in this.data) {
        tableHtml += '<tr class="row">';
        tableHtml += '<td><input class="amount" value="' + k + '"></td>';
        tableHtml += '<td><input class="percents" value="' + this.data[k] + '"></td>';
        tableHtml += '<td><a href="javascript: void(0)" class="delete">&times;</a></td>';
        tableHtml += '</tr>';
    }
   
   
    $(parentId).append('<table><tr><th>Сумма от</th><th>Ставка %</th><th></th></tr>'
                       + tableHtml
                       + '</table>'
                       + '<a href="javascript: void(0)" class="add">+</a>')
        .on('click', '.delete', deleteRow)
        .on('click', '.add', function () {
            var rowHtml;

            rowHtml += '<tr class="row">';
            rowHtml += '<td><input class="amount" type="text" value=""></td>';
            rowHtml += '<td><input class="percents" type="text" value=""></td>';
            rowHtml += '<td><a href="javascript: void(0)" class="delete">&times;</a></td>';
            rowHtml += '</tr>';
            
            $(parentId).find('table').append(rowHtml);
            
            $(parentId).find('.delete').unbind().click(deleteRow);
        });
}
 
RateTable.prototype.grabTable = function (parentId) {
    d = {};
    
    var rows = $(parentId).find('.row');
   
    $.each(rows, function (index, value) {
        var amount = parseInt($(value).find('.amount').val());
        var percents = parseFloat($(value).find('.percents').val());

        if (amount)     
            d[amount] = percents;
    });

    this.data = d;

    return this.data;
}

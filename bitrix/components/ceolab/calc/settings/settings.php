<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_js.php");

__IncludeLang($_SERVER['DOCUMENT_ROOT'] . '/bitrix/components/ceolab/calc/lang/' . LANGUAGE_ID . '/settings.php');

if (isset($_REQUEST['CALC_ID']))
    $id = $_REQUEST['CALC_ID'];
else
    $id = false;

?><style>
    .formEditor {
        font-size: 13px !important;
        font-family: Arial, serif;
        margin: 0 !important;
        padding: 0 !important;
        position: relative;
        z-index: 1;
    }

    .formEditor input, select {
        border: 1px solid black !important;
        border-radius: 0px !important;
        padding: 0 !important;
    }

    #rate_table {
        width: 100%;
    }

    #rate_table table {
        border: solid 1px #999;
    }

	#rate_table tr::before {
		content: '';
    }

    #rate_table table th {
        background-color: #C0C0C0;
        color: black;
        font-weight: normal;
    }

    #rate_table table td, #rate-table table th {
        border: solid 1px #999;
        padding: 3px 5px;
    }

    #rate_table input {
        width: 100px;
        border: none;
        box-shadow: none;
        border-radius: 0px;
        background-color: #F5F9F9;
    }

    #rate_table a.add {
        background: #5a5;
        display: inline-block;
        text-decoration: none;
        color: white;
        font-size: 18px;
        padding: 3px 5px;
        width: 18px;
        text-align: center;
        margin: 3px;
    }

    #rate_table a.delete {
        background: #e55;
        display: inline-block;
        text-decoration: none;
        color: white;
        font-size: 18px;
        padding: 3px 5px;
        width: 15px;
        text-align: center;
    }

</style>

<script type="text/javascript" src="/bitrix/components/ceolab/calc/js/ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="/bitrix/components/ceolab/calc/js/ratetable.js"></script>

<script type="text/javascript" src="/bitrix/components/ceolab/calc/js/settings.js"
        xmlns="http://www.w3.org/1999/html"></script>


<script>

    var saveValidator = {
        "isInt": function (element) {
            return parseInt(element.val());
        },
        "isNotEmpty": function (element) {
            return element.val();
        },
        "isFloat": function (element) {
            return parseFloat(element.val());
        }
    }

    function saveData(id) {
        try {
            var oldData = JSON.parse(this.arParams.oInput.value);
        } catch (e) {
            var oldData = {
                calcs: [],
                validators: {}
            };
        }


        var validateFields = {
            "name": "isNotEmpty",
            "step": "isInt",
            "step_term": "isInt",
            "min_term": "isInt",
            "max_term": "isInt",
            "currency": "isNotEmpty"

        };

        var isValidate = true;
        for (var i in validateFields) {
            if (!saveValidator[validateFields[i]]($("#" + i))) {
                $("#" + i).attr("style", "border: 1px solid red");
                isValidate = false;
            } else {
                $("#" + i).removeAttr("style");
            }
        }

        if (!isValidate) {
            alert("<?=GetMessage("incorrectValues")?>");
            return;
        }

        var returnData = {
            calc: {
                name: htmlspecialchars($("#name").val()),
                rate_table: (new RateTable).grabTable('#rate_table'),
                step: parseInt($("#step").val()),
                type_percent: $("#type_percent").val(),
                step_term: parseInt($("#step_term").val()),
                min_term: parseInt($("#min_term").val()),
                max_term: parseInt($("#max_term").val()),
                additionInfo: $("#additionInfo").val(),
                currency: $("#currency").val(),
                type_calc: $("#type_calc").val(),
                count_pay: parseInt($("#count_pay").val()),
				slider_image: $("#slider_image").val()
            }
        };

        if (id || id === 0) {
            returnData.calc.active = oldData.calcs[id].calc.active;
            oldData.calcs[id] = returnData;
        } else {
            if (!oldData.calcs) {
                oldData.calcs = [];
            }
            oldData.calcs.push(returnData);
        }

        this.arParams.oInput.value = JSON.stringify(oldData);

        window.jsPopup_calcEditor.Close(this.arParams.oInput.value);

        calcSettings(this.arParams);
    }

    $("#type_calc").change(
        function()
        {
            if($(this).val() == "annuity")
                $("#count_pay").parent("td").parent("tr").show();
            else
                $("#count_pay").parent("td").parent("tr").hide();
        }
    );

    function initForms(id) {
        try {
            var data = JSON.parse(this.arParams.oInput.value);
        } catch (e) {
            return;
        }

        if (id || id === 0) {
            $("#name").val(data.calcs[id].calc.name);
            (new RateTable(data.calcs[id].calc.rate_table)).renderTable('#rate_table');
            $("#step").val(data.calcs[id].calc.step);
            $("#type_percent").val(data.calcs[id].calc.type_percent);
            $("#step_term").val(data.calcs[id].calc.step_term);
            $("#min_term").val(data.calcs[id].calc.min_term);
            $("#max_term").val(data.calcs[id].calc.max_term);
            $("#additionInfo").val(data.calcs[id].calc.additionInfo);
            $("#currency").val(data.calcs[id].calc.currency);
            $("#type_calc").val(data.calcs[id].calc['type_calc']);
            $("#count_pay").val(data.calcs[id].calc['count_pay']);
			if (data.calcs[id].calc['slider_image']) {
				$("#show_slider_image").prop("src", data.calcs[id].calc['slider_image']);
				$("#slider_image").val(data.calcs[id].calc['slider_image']);
			}
        }



        // }
        try {
            data = JSON.parse(this.sendDataParams.oInput.value);
        } catch (e) {
            data = {};
        }

        $("#type_calc").change();
    }



</script>
<div id="windowEditCalc" style='position:relative'>
    <div>
        <table width='100%'>
            <tr>
                <td valign='top' align='left'>
                    <table name="calcFieldSet">
                        <tr>
                            <td>
                                <?= GetMessage("calcName") ?>:
                            </td>
                            <td>
                                <input type="text" id="name"/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <?= GetMessage("percent") ?>
                            </td>
                            <td>
                                <div id="rate_table"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Начисление
                            </td>
                            <td>
                                <select id="type_percent" name="type_percent">
                                    <option value="day"><?= GetMessage("day") ?></option>
                                    <option value="week"><?= GetMessage("week") ?></option>
                                    <option value="month"><?= GetMessage("month") ?></option>
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <?= GetMessage("step") ?>:
                            </td>
                            <td>
                                <input type="text" id="step" name="step"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= GetMessage("minTerm") ?>:
                            </td>
                            <td>
                                <input type="text" id="min_term" name="min_term"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= GetMessage("maxTerm") ?>:
                            </td>
                            <td>
                                <input type="text" id="max_term" name="max_term"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= GetMessage("stepTerm") ?>:
                            </td>
                            <td>
                                <input type="text" id="step_term" name="step_term"/>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <?= GetMessage("currency") ?>
                            </td>
                            <td>
                                <input type="text" id="currency"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= GetMessage("type_calc") ?>
                            </td>
                            <td>
                                <select id="type_calc">
                                    <option value="annuity">
                                        <?= GetMessage("annuity"); ?>
                                    </option>
                                    <option value="discrete">
                                        <?= GetMessage("discrete"); ?>
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= GetMessage("countPay") ?>
                            </td>
                            <td>
                                <input type="text" id="count_pay"/>
                            </td>
                        </tr>
                        <tr>

                            <td>
                                <?= GetMessage("additionInfo") ?>:
                            </td>
                            <td>
                                <textarea id="additionInfo">

                                </textarea>
                            </td>
                        </tr>
						<tr>
							<td>Картинка слайдера (путь на сервере)</td>
							<td>
								<input type="text" id="slider_image">
								<br>
								<img src="" id="show_slider_image" style="max-width: 500px">
							<td>
						</tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <input style='margin-top: 100px;' type="button" onclick="saveData(<?= $id ?>)" value="<?= GetMessage("save") ?>"/>
    <? if ($id !== false): ?>
        <script>
            initForms(<?=$id?>);
        </script>
    <? endif ?>
</div>
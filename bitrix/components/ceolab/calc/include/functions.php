<?php
function paramsDecode($str)
{
    $str = str_replace("'","\'",str_replace('\\"','\\\"', str_replace("\\n", "", htmlspecialchars_decode($str))));

    $data = json_decode($str);

    if(!$data)
    {
        $data = json_decode(utf8_encode($str));

        $data = utfParamDecodeRecursive($data);
    }

    return $data;
}

function utfParamDecodeRecursive($data)
{
    if($data)
        foreach($data as $key => $val)
        {
            if(is_array($val) || is_object($val))
            {
                $data->$key = utfParamDecodeRecursive($val);
            }
            else
            {
                $data->$key = utf8_decode($val);
            }
        }

    return $data;
}
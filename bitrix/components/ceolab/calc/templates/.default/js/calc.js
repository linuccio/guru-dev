/**
 * Created with JetBrains WebStorm.
 * User: evgenius
 * Date: 12/4/13
 * Time: 5:28 PM
 * To change this template use File | Settings | File Templates.
 */
function merge(obj1, obj2)
{
    for(var i in obj2)
    {
        obj1[i] = obj2[i];
    }
    return obj1;
}


function ceolab_calcmfo(render)
{
    this.render = render;

    this.percent = 2.5;

    this.term = {
        min: 1,
        max: 5,
        step: 1,
        value: 30,
        certain: 2,
        type: "soft"
    };

    this.current = {
        summ: 0,
        term: 0,
        summPercent: 0,
        toPay: 0
    }

    this.summ = {
        min: 1000,
        max: 15000,
        step: 1000,
        value: 30,
        certain: 2,
        type: "soft"
    }

    this.rate_table = null;

    this.slider_summ = new vg_slider();

    this.slider_summ.bind(
        "setval",
        this,
        function(val, data)
        {
            val = val.toString().replace(" ", "")*1;

            return val;
        }
    )


    this.slider_summ.lastStep = 0;
    this.slider_summ.bind(
        "slide",
        this,
        function(val, data)
        {
            
            val = parseInt(val);
            data.current.summ = val;
            val = formatPrice(val);
            data.render.setSumm(val);

            return val;
        }
    )

    this.slider_summ.bind(
        "down",
        this,
        function(val, data)
        {
            data.render.setToPay("--");
            data.render.setSummPercent("--");
            data.render.setTotalPay("--");
            data.render.setSumm("--");
            data.render.setTerm("--");
            data.render.setToDate("--");
            data.render.setToResultSumm("--");
            data.render.setToResultDate("--");			
        }
    )

    this.slider_summ.bind(
        "up",
        this,
        function(val, data)
        {
            data.calculate();
        }
    )

    this.slider_term = new vg_slider();

    this.slider_term.lastStep = 0;
    this.slider_term.bind(
        "slide",
        this,
        function(val, data)
        {
            var steps = parseInt((data.slider_term.baseBlock_Width) / data.slider_term.step);
            var currentStep = parseInt((val / data.term.step)-2);



            data.slider_term.lastStep = currentStep;

            val = parseInt(val);
            data.current.term = val;
            data.render.setTerm(val);

            return val;
        }
    );

    this.slider_term.bind(
        "down",
        this,
        function(val, data)
        {
            data.render.setToPay("--");
            data.render.setSummPercent("--");
            data.render.setTotalPay("--");
            data.render.setSumm("--");
            data.render.setTerm("--");
            data.render.setToDate("--");
            data.render.setToResultSumm("--");
            data.render.setToResultDate("--");
        }
    );

    this.slider_term.bind(
        "up",
        this,
        function(val, data)
        {
            data.calculate();
        }
    )

    this.init = function(data)
    {
        if(data)
        {
            this.rate_table = new RateTable(data.rate_table);

            if(data.summ) {
                this.summ = merge(this.summ, data.summ);
                this.summ.max = this.rate_table.maxAmount();
                this.summ.min = this.rate_table.minAmount();
            }

            if(data.term)
                this.term = merge(this.term, data.term);

            this.type_calc = "discrete";


            if(data.type_calc && data.type_calc == "annuity")
            {
                this.type_calc = "annuity";

                if(data.count_pay)
                    this.count_pay = data.count_pay;
                else
                    this.count_pay = 1;
            }


            this.type_percent = data.type;

        }

        this.initSliders();


        this.render.initScaleSumm(this.slider_summ);
        this.render.initScaleTerm(this.slider_term);
    }


    this.initSliders = function()
    {
        this.slider_summ.init(this.render.summFieldSlider,this.summ);
        this.slider_summ.setValue(this.summ.min);
        this.slider_summ.stepFix();
        this.p = (this.summ.max - this.summ.min)/100;
        this.slider_term.init(this.render.termFieldSlider,this.term);
        this.slider_term.setValue(this.term.min);
        this.slider_term.stepFix();
        this.calculate();
    }

    this.rend = function()
    {
        this.render.setToPay(this.current.toPay);
		this.render.setToPayMonth(Math.round(this.current.toPay/this.current.term));
        this.render.setSummPercent(this.current.summPercent);
		
        this.render.setTotalPay(this.current.summPercent);
        this.render.setSumm(this.current.summPercent);
        this.render.setToDate(this.current.toDate);
        this.render.setToResultSumm(this.current.summ);
        this.render.setToResultDate(this.current.term);	
    }

    this.calculate = function(summ, term)
    {
        var percent = this.rate_table.getRate(this.current.summ);

        if (this.summ.max === this.summ.min) {
            this.current.summ = this.summ.max;
        }

        if (this.term.min === this.term.max) {
            this.current.term = this.term.max;
        }


        if(this.type_calc == "annuity")
        {
            countPayPerMonth = 1;
            if(this.count_pay > 1)
            {
                countPayPerMonth = this.count_pay;
                var r = false;
                var countPay = false;

                switch(this.type_percent)
                {
                    case "day":
                        r = percent * 365;
                        countPay = this.current.term / (30 / countPayPerMonth);
                        break;
                    case "month":
                        r = percent * 12;
                        countPay = (this.current.term * 30) / (30 / countPayPerMonth);
                        break;
                    case "week":
                        countPay = (this.current.term * 365) / (30 / countPayPerMonth);
                        r = percent * 48;
                }



                this.current.toPay = Math.round(ExcelFormulas.PMT((r / 24.3333) / 100, countPay, -this.current.summ) * countPay);
                this.current.summPercent = this.current.toPay - this.current.summ;
            }
            else
            {
                percent = percent / 100;
                var k = percent*Math.pow((1+percent),this.current.term)/(Math.pow((1+percent),this.current.term)-1);
                this.current.summPercent = this.current.toPay - this.current.summ;
                this.current.toPay = (k * this.current.summ) * this.current.term;
            }

        }
        else
        {
            this.current.summPercent = ((this.current.summ/100)*percent)*this.current.term;
            this.current.toPay = this.current.summ+this.current.summPercent;
        }

        this.current.toPay = this.current.toPay.toFixed(2);

        this.current.summPercent = this.current.toPay - this.current.summ;
        this.current.summPercent = this.current.summPercent.toFixed(2)*1;
		
		var today = new Date().getTime();
		var judgmentDay = new Date(today + (this.current.term * 3600 * 24 * 1000));
		var currentDate = judgmentDay.getDate() + "." +getCurMonth(judgmentDay.getMonth()) + "." + judgmentDay.getFullYear();		
		this.current.toDate = currentDate;
        //return;
        this.rend();
    }
}

function formatPrice(price)
{
    price = price.toString().split(".");
    var l = price[0].length;
    var a = 3;
    var result = "";
    do
    {
        l-=3;
        if(l<0)
        {
            a+=l;
            l=0;
        }

        result = price[0].substr(l, a)+" "+result;
    }while(l);
    if(price[1])
        result+="."+price[1];
    return result;
}

function getCurMonth(month) {
	numberMonth = parseInt(month);
	if (numberMonth <= 8) {
		return ("0" + (numberMonth + 1));
	}
	else {
		return ("" + (numberMonth + 1));
	}
}

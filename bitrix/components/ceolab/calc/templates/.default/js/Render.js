/**
 * Created by evgenius on 7/26/15.
 */

function Render()
{
    this.baseBlock = $("#ceolab_calcmfo");
    this.switcherBlock = this.baseBlock.find("*[name=\"loan_switcher\"]");
    this.textBlock = this.baseBlock.find("*[name=\"loan_description\"]");

    this.summFieldSlider = this.baseBlock.find("input[name=\"slider_summ\"]");
    this.termFieldSlider = this.baseBlock.find("input[name=\"slider_term\"]");

    this.mainCalc = null;

    this.items = [];

    this.setSumm = function(summ)
    {
        this.baseBlock.find("span[name=\"summ\"]").text(summ);
        this.baseBlock.find("input[name=fieldSumm]").val(summ);
    }

    this.setTerm = function(term)
    {
        this.baseBlock.find("span[name=\"term\"]").text(term);
        this.baseBlock.find("input[name=fieldTerm]").val(term);
    }

    this.setToPay = function(val)
    {
        this.baseBlock.find("span[name=\"toPay\"]").text(val);
    }

    this.setToPayMonth = function(val)
    {
	this.baseBlock.find('span[name="toPayMonth"]').text(val);
    }

    this.setSummPercent = function(val)
    {
        this.baseBlock.find("span[name=\"summPercent\"]").text(val);
    }
	
    this.setTotalPay = function(val)
    {
        this.baseBlock.find("span[name=\"totalPay\"]").text(val);
    }
	
    this.setSumm = function(val)
    {
        this.baseBlock.find("span[name=\"summ\"]").text(val);
    }
	
    this.setTerm = function(val)
    {
        this.baseBlock.find("span[name=\"term\"]").text(val);
    }

    this.setToDate = function(val)
    {
        this.baseBlock.find("span[name=\"toDate\"]").text(val);
    }

    this.setToResultSumm = function(val)
    {
        this.baseBlock.find("span[name=\"toResultSumm\"]").text(val);
    }	
	
    this.setToResultDate = function(val)
    {
        this.baseBlock.find("span[name=\"toResultDate\"]").text(val);
    }		

    this.rendSwitcher = function(items)
    {
        var item;

        for(var i in items)
        {
            item = $("<li></li>").appendTo(this.switcherBlock);
            item.text(items[i].name);
            item.bind(
                "click",
                [this, i],
                function(event)
                {
                    event.data[0].mainCalc.setActive(event.data[1]);
                }
            );
            this.items.push(item);
        }
    }

    this.setActiveSwitch = function(id)
    {
        this.switcherBlock.find("*").removeClass("itemActive");
        this.items[id].addClass("itemActive");
    }

    this.setTextBlock = function(text)
    {
        this.baseBlock.find("div[name=loan_description]").text(text);
    }

    this.setType = function(type)
    {
        if(type == "day")
        {
            this.baseBlock.find("span[name=\"typeLoan\"]").text("дней");
        }
        else if(type == "week")
        {
            this.baseBlock.find("span[name=\"typeLoan\"]").text("недель");
        }
        else
        {
            this.baseBlock.find("span[name=\"typeLoan\"]").text("месяцев");
        }

        this.baseBlock.find("input[name=\"type\"]").val(type);
    }

    this.setProductName = function(name)
    {
        if(!this.baseBlock.find("input[name=\"product\"]").length)
        {
            this.baseBlock.append("<input name=\"product\" type=\"hidden\"/>");
        }

        this.baseBlock.find("input[name=\"product\"]").val(name);
    }

    this.setAnketId = function(id)
    {
        if(!this.baseBlock.find("input[name=\"anket\"]").length)
        {
            this.baseBlock.append("<input name=\"anket\" type=\"hidden\"/>");
        }

        this.baseBlock.find("input[name=\"anket\"]").val(id);
    }

    this.initScaleSumm = function(slider)
    {
        var objectScale = this.baseBlock.find("div[name=\"scale_summ\"]");
        this.baseBlock.find("div[name=\"scale_min_summ\"]").text(formatPrice(slider.data.min));

        this.baseBlock.find("div[name=\"scale_max_summ\"]").text(formatPrice(slider.data.max));
        //this.baseBlock.find("div[name=\"scale_max_summ\"]").css("marginRight", -this.baseBlock.find("div[name=\"scale_max_summ\"]").width()/2);


        this.initScale(slider, objectScale);
    }

    this.initScaleTerm = function(slider)
    {
        var objectScale = this.baseBlock.find("div[name=\"scale_term\"]");
        this.baseBlock.find("div[name=\"scale_min_term\"]").text(formatPrice(slider.data.min));

        this.baseBlock.find("div[name=\"scale_max_term\"]").text(formatPrice(slider.data.max));
        this.baseBlock.find("div[name=\"scale_max_term\"]").css("marginRight", -this.baseBlock.find("div[name=\"scale_max_term\"]").width()/2);


        this.initScale(slider, objectScale);
    }


    this.initScale = function(slider, objectScale)
    {
        var stepLength = slider.step;
        var steps = parseInt((slider.baseBlock_Width) / stepLength);


        var maxSteps = 30;
        if(steps > maxSteps)
        {
            stepLength = stepLength * (steps/maxSteps);
            steps = maxSteps;
        }

        objectScale.empty();
        var stepScale;
        for(var i = 0; i<=steps+1; i++)
        {
            if (stepLength * i <= 280)
                stepScale = $("<div class=\"step\"></div>").appendTo(objectScale).css("left", stepLength * i);
        }
    }
}


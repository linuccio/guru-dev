/**
 * Created with JetBrains WebStorm.
 * User: evgenius
 * Date: 12/7/13
 * Time: 10:50 PM
 * To change this template use File | Settings | File Templates.
 */

function main_calc(products)
{
    this.render = new Render();
    this.render.mainCalc = this;

    this.products = products;
    this.items = [];

    this.calc = new ceolab_calcmfo(this.render);

    this.setActive = function(i)
    {
        this.calc.init(this.products[i]);
        this.render.setActiveSwitch(i, this.products);
        this.render.setTextBlock(this.products[i].text);
        this.render.setProductName(this.products[i].name);
        this.render.setAnketId(this.products[i].anket);
        this.render.setType(this.products[i].type);

    }

    this.render.rendSwitcher(this.products);

}

<script>
    $("document").ready(
        function()
        {
            var data = eval('('+$('#calcJsonData').val()+')');
            var products = [];
            for(var i in data.calcs)
            {
                var calc = data.calcs[i].calc;

                products.push({
                    name: calc.name,
                    text: calc.additionInfo,
                    percent : calc.percent,
                    type : calc.type_percent,
                    rate_table: calc.rate_table,
                    type_calc: calc.type_calc,
                    count_pay: calc['count_pay'] ? calc['count_pay'] : null,
                    summ: {
                        min : calc.min_summ,
                        max : calc.max_summ,
                        step: calc.step
                    },
                    term : {
                        min : calc.min_term,
                        max : calc.max_term,
                        step: calc.step_term
                    },
                    anket:59
                });


            }

            calc = new main_calc(products);
            calc.setActive(0);
        }
    )
</script>
<input id="calcJsonData" type="hidden" value="<?=$arParams['PRODUCTS']?>"/>

<form class="form_calc_mfo" id="ceolab_calcmfo" method="post" action="<?= $APPLICATION->GetCurUri()?>">
	<input type="hidden" name="anket"/>
	<input type="hidden" name="product"/>
	<input type="hidden" name="type">
	<input type="hidden" name="currency">
	<input name="fieldSumm" type="hidden"/>
	<input name="fieldTerm" type="hidden"/>


<div class="container">
	<ul class="etabs clearfix" name="loan_switcher">

	</ul>
</div>	

<div class="tab-content">
	<div role="tabpanel" class="tab-pane fade in active" id="loan-express">
		<div class="container">
			<div class="calc" id="calc-loan-express">
				<div class="calc-left col-md-8">

				
					<div class="slider calc-row clearfix">
						<h5><strong>На какую сумму хотите оформить займ?</strong></h5>
						<div class="scale_numbers">
							<div class="first" name="scale_min_summ">
								
							</div>
							<div class="last" name="scale_max_summ">
								
							</div>
						</div>
						<div class="scale" name="scale_summ">
						</div>
						<input type="slider" name="slider_summ"/>
						<div class="calc-value"><span name="toResultSumm"></span> р.</div>
					</div>

					<div class="slider calc-row clearfix">
						<h5><strong>На сколько дней</strong></h5>
						<div class="scale_numbers">
							<div class="first" name="scale_min_term">
								
							</div>
							<div class="last" name="scale_max_term">
								
							</div>
						</div>
						<div class="scale" name="scale_term">

						</div>
						<input type="slider" name="slider_term"/>
						<div class="calc-value""><span name="toResultDate"></span> дн.</div>
					</div>
				</div>

				<div class="calc-right col-md-4">
					<div class="calc-row">
						<div class="calc-cell">Плата за займ:</div>
						<div class="calc-cell"><b><span name="totalPay"></span> руб</b></div>
					</div>

					<div class="calc-row">
						<div class="calc-cell">Итого к оплате:</div>
						<div class="calc-cell"><b><span name="toPay"></span> руб</b></div>
					</div>

					<div class="calc-row">
						<div class="calc-cell">Погасить включительно до:</div>
						<div class="calc-cell"><b><span name="toDate"></span></b></div>
					</div>

					<div class="calc-row">
						<span><input type="submit" href="/anketa/" value="Получить займ" class="button solid red" /></span>
						<br>
						<span>или <a href="#">связаться с менеджером</a> <br> для уточнения деталей</span>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

</form>

<div class="address" align="left">
    <div style="padding-left: 30px;">
        <span class="city"><?=$arResult["CURRENT_CITY"]['NAME']?></span>
        <?if($arResult['COUNT_CITY'] > 1):?>
            <a href="javascript:onclick()" class="selectCityAddress" onclick="selectRegionShow('address_office_select_region')"><?=GetMessage("select_city")?></a>
        <?endif?>
        <div id="address_office_select_region" class="selectRegion_vacancies_wrapper">
            <div class="selectRegion_vacancies">
                <div align="right">
                    <a href="javascript:onclick()" onclick="selectRegionHide('address_office_select_region')">[x]</a>
                </div>
                <div class="regionList">
                    <div style="display: inline-block;">
                        <?foreach($arResult['CITY'] as $lether => $citys):?>
                        <div class="selectRegion_vacancies_block">
                            <div align="left" style="font-size: 30px;">
                                <?=$lether?>
                            </div>
                            <?foreach($citys as $cityId => $city):?>
                            <a href="?city=<?=$cityId?>"><?=$city['NAME']?></a><br/>
                            <?endforeach?>
                        </div>
                        <?endforeach?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 21.03.13
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
$status = 1;//CModule::IncludeModuleEx('ceolab.mfo');

if($status != MODULE_NOT_FOUND)
{
    $arResult['status'] = $status;
    if($status != MODULE_DEMO_EXPIRED)
    {
        CModule::IncludeModule("iblock");
        $fromSession = false;
        $useDefault = false;
        if($arParams['SET_GET'] && isset($_GET[$arParams['SET_GET']]))
        {
            $cCity = $_GET[$arParams['SET_GET']];
        }
        elseif(isset($arParams['GET_SESSION']) && $arParams['GET_SESSION'] && isset($_SESSION[$arParams['GET_SESSION']]))
        {
            $cCity = $_SESSION[$arParams['GET_SESSION']];
            $fromSession = true;
        }
        elseif(isset($arParams['DEFAULT_CITY']))
        {
            $cCity = $arParams['DEFAULT_CITY'];
            $useDefault = true;
        }

        if($this->StartResultCache(false, $cCity))
        {
            $filter = array("IBLOCK_ID" => $arParams['IBLOCK_ID']);
            $res = CIBlockElement::GetList(array(), $filter);

            while($r = $res->GetNextElement())
            {
                $buttons = CIBlock::GetPanelButtons(
                    $r->fields["IBLOCK_ID"],
                    $r->fields["ID"],
                    0,
                    array("SECTION_BUTTONS"=>false, "SESSID"=>false)
                );

                $city[$r->fields['ID']] = $r->fields;
                $city[$r->fields['ID']]['EDIT_LINK'] = $buttons["edit"]["edit_element"]["ACTION_URL"];
                if($r->fields['ID'] != $arParams['DEFAULT_CITY'])
                    $city[$r->fields['ID']]['DELETE_LINK'] = $buttons["edit"]["delete_element"]["ACTION_URL"];
            }
            if(!isset($city[$cCity]))
            {
                $this->AbortResultCache();
                if($fromSession)
                    unset($_SESSION[$arParams['GET_SESSION']]);
                if(!$useDefault)
                    LocalRedirect("/");
            }

            $arResult["CURRENT_CITY"] = array(
                "ID" => $cCity,
                "NAME" => $city[$cCity]['NAME']
            );

            $arResult["CITY"] = $city;


            $this->includeComponentTemplate();
        }

        if(isset($arParams['SET_SESSION']) && $arParams['SET_SESSION'])
            $_SESSION[$arParams['SET_SESSION']] = $cCity;







        $APPLICATION->AddHeadString("<script src=\"/bitrix/components/ceolab/office.selectcity/js/script.js\"></script>", true);
        $APPLICATION->AddHeadString("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js\"></script>", true);

        if(CModule::IncludeModule("iblock"))
        {
            if(!isset($arParams['NO_MENU']) || $arParams['NO_MENU'] != "Y")
            {
                $arButtons = CIBlock::GetPanelButtons($arParams['IBLOCK_ID']);
                unset($arButtons['configure']['add_section']);


                if($APPLICATION->GetShowIncludeAreas())
                    $this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

                if($arParams["SET_TITLE"])
                {
                    $arTitleOptions = array(
                        'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_iblock"]["ACTION"],
                        'PUBLIC_EDIT_LINK' => "",
                        'COMPONENT_NAME' => $this->GetName(),
                    );
                }
            }
        }
    }
    else
    {
        echo "<font style='color: red'>".GetMessage('demo_expired')."</font>";
    }
}

?>


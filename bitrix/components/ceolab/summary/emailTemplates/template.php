Имя: %FIRSTNAME%<br/>
Фамилия: %LASTNAME%<br/>
Отчество: %MIDDLENAME%<br/>
<br/>
Дата рождения: %BIRTHDAY%<br/>
Город проживания: %CITY%<br/>
<br/>
Cемья: %FAMILY%<br/>
Дети: %CHILDS%<br/>
<br/>
<h2>
    Работа и образование
</h2>

Желаемая должность: %POSITION%<br/>
Желаемый уровень заработной платы: %SALARY%<br/>
Желаемый график работы: %SHEDULE%<br/>
<br/>
Образование: %EDUCATION%<br/>
Учебное заведение: %INSTITUT%<br/>
Год окончания: %ENDYEAR%<br/>
Специальность: %SPECIALITY%<br/>
<br/>
3 последних места работы:<br/>
%PERIOD_1% %NAMEORG_1% %LASTPOSITION_1%<br/>
%PERIOD_2% %NAMEORG_2% %LASTPOSITION_2%<br/>
%PERIOD_3% %NAMEORG_3% %LASTPOSITION_3%<br/>
<br/>
Рекомендации:<br/>
%RECOMENDATIONS%
<h2>
    Контактные данные
</h2>
Телефон: %PHONE%<br/>
Email: %EMAIL%<br/>









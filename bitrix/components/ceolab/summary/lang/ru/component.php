<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 06.06.13
 * Time: 0:01
 * To change this template use File | Settings | File Templates.
 */
$MESS['wrong_type'] = "Неверный тип файла";
$MESS['wrong_size'] = "Превышен размер файла";
$MESS['demo_expired'] = "Тестовый период истек";
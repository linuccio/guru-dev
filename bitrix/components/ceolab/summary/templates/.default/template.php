

<div style="margin-top: 50px;">
    <?if(!isset($arResult['STATUS'])):?>
        <form method="POST" enctype="multipart/form-data">
            <div style="border-bottom: 2px dashed #BABABA; padding-bottom: 30px;">
                <div style="font-size: 18px; margin-bottom: 20px;">
                    <?=GetMessage("upload_summary")?>
                </div>
                <?if(isset($arResult["errorUpload"])):?>
                    <div class="mfo_summary_errormessage" style="display: block"><?=$arResult["errorUpload"]?></div>
                <?endif?>
                <input type="file" name="FILE" <?if(isset($arResult["errorUpload"])):?> style="border: 1px solid red;" <?endif?>/>
                <div style="font-size: 10px; margin-top: 10px;">
                    <?=GetMessage("file_type")?> <?=$arParams['FILE_TYPES']?> <?=GetMessage("size_not_more")?> <?=$arParams['MAX_FILE_SIZE'];?> <?=GetMessage("mb")?>
                </div>
                <input type="submit" name="SUBMIT_FILE" class="mfo_summary_submit" value="<?=GetMessage("submit")?>">
            </div>
        </form>
        <form method="POST">
            <div class="mfo_summary_header">
                <?=GetMessage("personal_data")?>
            </div>
            <div style="display: inline-block;">
                <div class="mfo_summary_fieldsblock_left" align="right">
                    <div class="mfo_summary_fieldline" id="mfo_LASTNAME">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("lastname")?></div> <input type="text" name="LASTNAME"/>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_FIRSTNAME">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("firstname")?></div> <input type="text" name="FIRSTNAME"/>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_MIDDLENAME">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("middlename")?></div> <input type="text" name="MIDDLENAME"/>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_BIRTHDAY">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("birth_day")?></div> <input type="text" name="BIRTHDAY"/>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_CITY">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("city")?></div> <input type="text" name="CITY"/>
                    </div>
                </div>

                <div class="mfo_summary_fieldsblock_right" align="right">
                    <div class="mfo_summary_fieldline" id="mfo_PHONE">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("phone")?></div> <input type="text" name="PHONE"/>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_EMAIL">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("email")?></div> <input type="text" name="EMAIL"/>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_FAMILY">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("family")?></div>
                        <select name="FAMILY">
                            <option>
                                <?=GetMessage("not_married")?>
                            </option>
                            <option>
                                <?=GetMessage("married")?>
                            </option>
                        </select>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_CHILDS">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("childs")?></div>
                        <select name="CHILDS">
                            <option>
                                <?=GetMessage("childs_no")?>
                            </option>
                            <option>
                                <?=GetMessage("childs_yes")?>
                            </option>
                        </select>
                    </div>
                </div>
            </div>



            <div class="mfo_summary_header">
                <?=GetMessage("education_and_work")?>
            </div>
            <div style="display: inline-block;">
                <div class="mfo_summary_fieldsblock_left" align="right" >
                    <div class="mfo_summary_fieldline" id="mfo_POSITION">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("position")?></div> <input type="text" name="POSITION"/>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_SALARY">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("salary")?></div> <input type="text" name="SALARY"/>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_SHEDULE">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("shedule")?></div>
                        <select name="SHEDULE">
                            <option>
                                <?=GetMessage("shedule_2")?>
                            </option>
                            <option>
                                <?=GetMessage("shedule_5")?>
                            </option>
                            <option>
                                <?=GetMessage("shedule_free")?>
                            </option>
                            <option>
                                <?=GetMessage("shedule_partial")?>
                            </option>
                        </select>
                    </div>

                </div>

                <div class="mfo_summary_fieldsblock_right" align="right">
                    <div class="mfo_summary_fieldline" id="mfo_EDUCATION">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("education")?></div>
                        <select name="EDUCATION">
                            <option>
                                <?=GetMessage("education_secondary")?>
                            </option>
                            <option>
                                <?=GetMessage("education_special_secondary")?>
                            </option>
                            <option>
                                <?=GetMessage("education_high")?>
                            </option>
                            <option>
                                <?=GetMessage("education_science")?>
                            </option>
                        </select>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_INSTITUT">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("institut")?></div> <input type="text" name="INSTITUT"/>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_ENDYEAR">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("end_year")?></div>
                        <input type="text" name="ENDYEAR"/>
                    </div>
                    <div class="mfo_summary_fieldline" id="mfo_SPECIALITY">
                        <div class="mfo_summary_errormessage"></div>
                        <div class="mfo_summary_div"><?=GetMessage("speciality")?></div>
                        <input type="text" name="SPECIALITY"/>
                    </div>
                </div>
            </div>
            <div class="mfo_summary_fieldsblock_center" align="center">
                <div class="mfo_summary_div">
                    <?=GetMessage("last_work_place")?>
                </div>
                <table width="700px">
                    <th align="center">
                        <?=GetMessage("work_period")?>
                    </th>
                    <th align="center">
                        <?=GetMessage("org_name")?>
                    </th>
                    <th align="center">
                        <?=GetMessage("work_position")?>
                    </th>
                    <tr>
                        <td align="center">
                            <input type="text" name="PERIOD_1" />
                        </td>
                        <td align="center">
                            <input type="text" name="NAMEORG_1" />
                        </td>
                        <td align="center">
                            <input type="text" name="LASTPOSITION_1"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <input type="text" name="PERIOD_2" />
                        </td>
                        <td align="center">
                            <input type="text" name="NAMEORG_2" />
                        </td>
                        <td align="center">
                            <input type="text" name="LASTPOSITION_2"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <input type="text" name="PERIOD_3" />
                        </td>
                        <td align="center">
                            <input type="text" name="NAMEORG_3" />
                        </td>
                        <td align="center">
                            <input type="text" name="LASTPOSITION_3"/>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="mfo_summary_recomendations" align="center" id="mfo_RECOMENDATIONS">
                <div class="mfo_summary_div">
                    <?=GetMessage("recomendations")?>
                </div>
                <div class="mfo_summary_errormessage"></div>
                <textarea name="RECOMENDATIONS">

                </textarea>
            </div>
            <div class="mfo_summary_confirm_text">
                <?=GetMessage("confirm_text")?>

            </div>
            <input type="submit" name="SUBMIT" class="mfo_summary_submit" value="<?=GetMessage("submit")?>">
        </form>
    <? elseif($arResult['STATUS'] == "OK"): ?>
        <div>
            <?=GetMessage("send_success")?>
        </div>
    <? else: ?>
        <div>
            <?=GetMessage("send_fail")?>
        </div>
    <? endif ?>
</div>
<script>
    <?foreach($arResult['errorFields'] as $field):?>
        $("#mfo_<?=$field?> > [name=<?=$field?>]").attr("style", "border: 1px solid red");
        $("#mfo_<?=$field?> > .mfo_summary_errormessage").attr("style", "display: block");
        $("#mfo_<?=$field?> > .mfo_summary_errormessage").text(<?=GetMessage("essential_field")?>);
    <?endforeach?>

    <?if(isset($arResult['initFields'])):?>
        <?foreach($arResult['initFields'] as $key => $val):?>
            $("[name=<?=$key?>]").val("<?=trim($val)?>");
        <?endforeach?>
    <?endif?>
</script>

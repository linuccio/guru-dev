<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 21.03.13
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
$status = 1;//CModule::IncludeModuleEx('ceolab.mfo');

if($status != MODULE_NOT_FOUND)
{
    if($status != MODULE_DEMO_EXPIRED)
    {
        CModule::IncludeModule("iblock");

        if($arParams['SET_GET'] && isset($_GET[$arParams['SET_GET']]))
            $cCity = $_GET[$arParams['SET_GET']];
        elseif(isset($arParams['GET_SESSION']) && $arParams['GET_SESSION'] && isset($_SESSION[$arParams['GET_SESSION']]))
            $cCity = $_SESSION[$arParams['GET_SESSION']];
        elseif(isset($arParams['DEFAULT_CITY']) && isset($city[$arParams['DEFAULT_CITY']]))
            $cCity = $arParams['DEFAULT_CITY'];

        $filter = array("IBLOCK_ID" => $arParams['IBLOCK_ID']);
        $filter['PROPERTY_CITY'] = $cCity;

        if($this->StartResultCache(false, $filter))
        {
            $res = CIBlockElement::GetList(array(), $filter);
            $keyGroup = "DESTINCT";
            while($r = $res->GetNextElement())
            {
                $p = array();
                $props = $r->GetProperties();
                $fields = $r->GetFields();

                foreach($props as $k => $v)
                {
                    $p[$k] = $v['VALUE'];
                }
                $buttons = CIBlock::GetPanelButtons(
                    $fields["IBLOCK_ID"],
                    $fields["ID"],
                    0,
                    array("SECTION_BUTTONS"=>false, "SESSID"=>false)
                );
                $p['ID'] = $fields['ID'];
                $p['IBLOCK_ID'] = $fields['IBLOCK_ID'];
                $p['EDIT_LINK'] = $buttons["edit"]["edit_element"]["ACTION_URL"];
                $p['DELETE_LINK'] = $buttons["edit"]["delete_element"]["ACTION_URL"];

                $arResult["DATA"][]  = $p;
                $data[$p[$keyGroup]][] = $p;
            }


            $arResult["ITEMS"] = $data;
            $this->includeComponentTemplate();
        }
        $APPLICATION->AddHeadString("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js\"></script>", true);
        $APPLICATION->AddHeadString('<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>', true);

        if(CModule::IncludeModule("iblock"))
        {
            $arButtons = CIBlock::GetPanelButtons($arParams['IBLOCK_ID']);
            unset($arButtons['configure']['add_section']);


            if($APPLICATION->GetShowIncludeAreas())
                $this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

            if($arParams["SET_TITLE"])
            {

                $arTitleOptions = array(
                    'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_iblock"]["ACTION"],
                    'PUBLIC_EDIT_LINK' => "",
                    'COMPONENT_NAME' => $this->GetName(),
                );

            }
        }

    }
    else
    {
        echo "<font style='color: red'>".GetMessage('demo_expired')."</font>";
    }
}

?>


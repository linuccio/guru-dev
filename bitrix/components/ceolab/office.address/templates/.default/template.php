<?
$address = current($arResult["ITEMS"]);
$address = $address[0];
if (!isset($_GET['city'])) {
    header("location:/address.php?city=17");
}
?>

    <script type="text/javascript">
        ymaps.ready(init);
        var myMap;

        function init(){
            myMap = new ymaps.Map ("map", {
                center: [55.76, 37.64],
                zoom: 15
            });

            myMap.controls.add(
                new ymaps.control.ZoomControl()
            );

            <?foreach($arResult['DATA'] as $res):?>
            <? if($res['COORD']):?>
            myMap.geoObjects.add(new ymaps.Placemark([<?=$res['COORD'];?>], { content: '', balloonContent: '' }));
            <? endif ?>
            <? endforeach ?>
            setObject('<?=$address['COORD']?>', '<?=$address['ADDRESS']?>', '<?=$address['PHONE']?>', '<?=$address['EMAIL']?>', '<?=implode("<br/>", $address['SCHEDULE'])?>')        }

        function setObject(coord, address, phone, email, schedule)
        {
            var coord = coord.split(",");

            $("*[name=address]").text(address);
            $("*[name=phone]").text(phone);
            $("*[name=email]").text(email);
            $("*[name=schedule]").html(schedule.replace(",", "<br/>"));

            console.debug(schedule);

            if(coord.length == 2)
            {
                myMap.setCenter(coord);
            }
        }


    </script>
<?if($arResult['ITEMS']):?>
    <table align="center" width="100%" cellspacing="30">
        <tr>
            <?
            $trOpen = false;

            foreach($arResult['ITEMS'] as $destinct => $item):
                $count++;
                if($count == 3)
                {
                    echo "</tr><tr>";
                    $count = 0;
                }

                ?>
                <td valign="top" class="addressLinks">
                    <span class="area"><?=$destinct?></span>
                    <br/>
                    <br/>
                    <?foreach($item as $address):
                        $this->AddEditAction($address['ID'], $address['EDIT_LINK'], CIBlock::GetArrayByID($address["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($address['ID'], $address['DELETE_LINK'], CIBlock::GetArrayByID($address["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <a id="<?=$this->GetEditAreaId($address['ID']);?>" href="javascript:onclick()" onclick="setObject('<?=$address['COORD']?>', '<?=$address['ADDRESS']?>', '<?=$address['PHONE']?>', '<?=$address['EMAIL']?>', '<?=implode("<br/>", $address['SCHEDULE'])?>')"><?=$address['ADDRESS']?></a><br/>
                    <? endforeach ?>
                </td>
            <? endforeach ?>

        </tr>

    </table>


    <div class="map"  align="center">

        <div class="mapBottomBlock">
            <table align="center" style="width: 80%">
                <tr>
                    <td valign="top" align="left">
                        <div id="map" style="width: 600px; height: 335px; margin-left: 10px;"></div>
                    </td>
                    <td colspan="2" width="400px" valign="top" >
                        <div class="addressData">
                            Адрес: <span name="address"></span><br/>
                            Телефон: <span name="phone"></span><br/>
                            Электронная почта: <span name="email"></span>
                            <div class="shedule">
                                <div>
                                    Режим работы
                                </div>
                                <div name="schedule">

                                </div>
                            </div>

                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div name="border">

        </div>
    </div>
<?else:?>
    <p align="center">
        <?=GetMessage("no_data")?>
    </p>
<?endif?>
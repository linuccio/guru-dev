<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "vacancies",
    "DESCRIPTION" => 'vacancies',
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "CeoLab",
        "CHILD" => array(
            "ID" => "vacancies.list",
            "NAME" => "vacancies"
        )
    ),
);
?>

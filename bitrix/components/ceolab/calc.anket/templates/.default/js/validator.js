/**
 * Created with JetBrains PhpStorm.
 * User: evgenius
 * Date: 1/19/14
 * Time: 7:18 PM
 * To change this template use File | Settings | File Templates.
 */

function Validator(formSelector)
{
    this.formObj;
    this.alertError = "Неверно заполнены поля";
    this.init = function(formSelector)
    {
        $(formSelector).bind(
            "submit",
            this,
            function(event)
            {
                var status = event.data.run();
                if(status)
                {
                    if(!$("input[validator=\"confirm\"]").is(":checked"))
                    {
                        event.data.rendError("Для продолжения необходимо ваше согласие на обработку персональных данных");
                        status = false;
                    }
                }

                return status;
            }
        )
    }

    this.run = function()
    {
        var status = true;
        var t = this;
        $("*[validator], *[isrequired]").each(
            function()
            {
                if($(this).attr("isrequired") && !$(this).val())
                {
                    t.rendErrorField(
                        {
                            text: "Это обязательное поле"
                        },
                        $(this)
                    );
                    status = false;
                    return true;
                }

                var validator = $(this).attr("validator");
                if(t.validators[validator])
                {
                    if(!t.validators[validator].func($(this)))
                    {
                        status = false;
                        t.rendErrorField(t.validators[validator], $(this));
                    }
                }
            }
        )

        if(!status)
            this.rendError();
        return status;
    }

    this.check = function()
    {
        var t = this;
        $("*[errors]").each(
            function()
            {
                if($(this).attr("errors") == "required")
                {
                    t.rendErrorField(
                        {
                            text: "Это обязательное поле"
                        },
                        $(this)
                    );
                    return true;
                }

                var validator = $(this).attr("validator");
                if(t.validators[validator])
                {
                    t.rendErrorField(t.validators[validator], $(this));
                }
            }
        );
    }

    this.rendErrorField = function(validator, obj)
    {

    }

    this.rendError = function(text)
    {
        if(!text)
            text = this.alertError;

        alert(text);
    }

    this.init(formSelector);

    this.validators = {
        number : {
            func : function(obj)
            {
                if(/[^0-9]{1,}/.test($(obj).val()))
                {
                    return false;
                }

                return true;
            },
            text : "Неверный номер"
        },
        date : {
            func : function(obj)
            {
                if(!/[0-9]{2}\.[0-9]{2}.[0-9]{4}\./.test($(obj).val()))
                {
                    return false;
                }

                return true;
            },
            text : "Неверный формат даты"
        }
    }

}

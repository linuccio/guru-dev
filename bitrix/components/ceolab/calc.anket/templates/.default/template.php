<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 12/9/13
 * Time: 5:11 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<?if(!isset($arResult['STATUS'])):?>
<form class="anket" align="center" method="POST" id="mfoAnketForm" enctype="multipart/form-data">
    <input type="hidden" name="anket" value="<?=$arResult['anketID']?>"/>
    <input type="hidden" name="product" value="<?=$arResult['product']?>"/>
    <input type="hidden" name="summ" value="<?=$arResult['summ']?>"/>
    <input type="hidden" name="term" value="<?=$arResult['term']?>"/>

    <div class="title" align="center">
        Заполните заявку на займ
    </div>
    <div class="items" align="left">
        <?foreach($arResult['FORMS'] as $form):?>
            <div class="item" align="left">
                <div class="headitem">
                    <span>
                        <?=$form['NAME']?>
                    </span>

                </div>
                <div class="body">
                    <?foreach($form['FIELDS'] as $field):?>
                        <div class="fieldWrap">
                            <div class="fw">
                                <div class="text"><?=$field['title']?> <?if($field['isRequired']):?><span class="requiredstar">*</span><?endif?></div>
                                <div class="field">
                                    <? switch($field['type'])
                                    {
                                        case "input":
                                    ?>
                                            <input type="text" <?=$field['attr']?> />
                                    <?
                                        break;
                                        case "select":
                                    ?>
                                            <select <?=$field['attr']?>  >
                                                <option selected="selected" disabled="disabled"></option>
                                                <?foreach($field['values'] as $value):?>
                                                    <option value="<?=$value['value']?>"><?=$value['name']?></option>
                                                <?endforeach?>
                                            </select>
                                    <?
                                        break;
                                        case "file":
                                    ?>
                                            <input type="file" <?=$field['attr']?> />
                                    <?
                                        break;
                                        case "date":
                                    ?>
                                            <input <?=$field['attr']?> type="text" mask="99.99.9999" placeholder="дд.мм.гггг"  />
                                    <?
                                        break;
                                        case "checkbox":
                                    ?>
                                            <input <?=$field['attr']?> type="checkbox"  />
                                    <?
                                    }
                                    ?>

                                </div>
                            </div>
                            <div class="errorMessage" name="errorMessage"></div>
                        </div>

                    <?endforeach?>
                </div>
            </div>
        <?endforeach?>
    </div>

    <div style="margin-top: 20px">

        <input type="checkbox" id="c1" validator="confirm" name="confirm"/>
        <label for="c1"><span></span></label>
        <span style="color: gray; font-size: 24px">Я даю согласие на обработку персональных данных</span>
        <div class="submitWrap">
            <div class="btnBorderWrap">
                <input name="submit" type="submit" value="Продолжить"/>
            </div>

        </div>
    </div>
</form>
<?else:?>
<h2>Ваша заявка успешно отправлена</h2>
<?endif?>
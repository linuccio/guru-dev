/**
 * Created with JetBrains PhpStorm.
 * User: evgenius
 * Date: 12/9/13
 * Time: 6:09 PM
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(
    function()
    {
        calendar = new Calendar("input[type=\"customdate\"]");
        //calendar.type = "year";
        validator = new Validator("#mfoAnketForm");
        validator.rendErrorField = function(validator, obj)
        {
            var emBlock = $(obj).parent(".field").parent("div").parent(".fieldWrap").find(".errorMessage");
            emBlock.text(validator.text);
            emBlock.show();
            $(obj).addClass("errorField");
            $(obj).focus(
                function()
                {
                    $(this).removeClass("errorField");
                    emBlock.hide();
                }
            )
        }

        validator.check();


        $("input[mask]").each(
            function()
            {
                var mask = $(this).attr("mask");
                $(this).mask(mask);
            }
        )
    }
)







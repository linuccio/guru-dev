<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 12/10/13
 * Time: 10:48 AM
 * To change this template use File | Settings | File Templates.
 */
$attrTypes = array(
    "nameVar" => "name",
    "isRequired" => "isRequired",
    "validator" => "validator",
    "errors"    => "errors",
    "value"     => "value"
);

foreach($arResult['FORMS'] as $kForm => $form)
{
    foreach($form['FIELDS'] as $kfield => $field)
    {
        $attr = "";
        foreach($field as $k => $v)
        {
            if(isset($attrTypes[$k]) && $v)
            {
                $attr .= sprintf("%s=\"%s\" ", $attrTypes[$k], $v);
            }
        }
        $arResult['FORMS'][$kForm]['FIELDS'][$kfield]['attr'] = $attr;
    }
}

$APPLICATION->AddHeadString("<script src=\"/bitrix/components/ceolab/calc.anket/templates/.default/js/mask.js\"></script>");


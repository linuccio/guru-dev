<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 12/9/13
 * Time: 5:11 PM
 * To change this template use File | Settings | File Templates.
 */

$essentialFields = array(
    "anket"
);

foreach($essentialFields as $field)
{
    if(!isset($_POST[$field]) || !$_POST[$field])
    {
        LocalRedirect("/");
        break;
    }
}

$arResult = array(
    "product" => $_POST['product'],
    "summ" => $_POST['fieldSumm'],
    "term" => $_POST['fieldTerm'],
    "anketID" => $_POST['anket']
);


CModule::IncludeModule("ceolab.calcmfo");
$aFactory = new AnketFactory();

$anket = $aFactory->getAnket($_POST['anket']);
if(!$anket)
{
    LocalRedirect("/");
}
$forms = $anket->getForms();

if(isset($_POST['submit']) && $_POST['submit'])
{
    $mailVars = array(
        "product" => $_POST['product'],
        "summ" => $_POST['summ'],
        "term" => $_POST['term']
    );
    foreach($forms as $form)
    {
        $f = array(
            "NAME" => $form->getName()
        );

        $status = true;
        $fields = $form->getFields();

        foreach($fields as $field)
        {
            $nameVar = $field->getNameVar();
            $fArray = $field->toArray();
            if($nameVar)
            {

                $container = $_POST;
                if($field->GetType() == "file")
                {
                    $ex = explode(".", $_FILES[$nameVar]['name']);
                    if(isset($ex[1]) && $ex[1])
                    {
                        do
                        {
                            $name = rand(100000, 9999999);
                            $filepath = $_SERVER["DOCUMENT_ROOT"]."/upload/mfo.anket/".$name.".".$ex[1];
                        }
                        while(file_exists($filepath));



                        if(copy($_FILES[$nameVar]['tmp_name'], $filepath))
                        {
                            $mailVars['attach_files'][] = $filepath;
                        }
                    }
                }
                else
                {
                    if($field->getRequire() && !strlen($container[$nameVar]))
                    {
                        $fArray['errors'] = "required";
                        $status = false;
                    }
                    elseif(!$field->checkValue($container[$nameVar]))
                    {
                        $fArray['errors'] = "validator";
                        $status = false;
                    }
                    $mailVars[$nameVar] = $container[$nameVar];
                }





                if(isset($container[$nameVar]) && strlen($container[$nameVar]))
                    $fArray['value'] = $container[$nameVar];
            }
            $f['FIELDS'][] = $fArray;
        }

        $arResult['FORMS'][] = $f;
    }
    if($status)
    {
        $eventId =  CEvent::Send("SEND_REQUEST", "s1", $mailVars, "N", 8);
        $arResult['STATUS'] = 'SENDED';
    }
}
else
{
    foreach($forms as $form)
    {
        $f = array(
            "NAME" => $form->getName()
        );

        $fields = $form->getFields();
        foreach($fields as $field)
        {
            $f['FIELDS'][] = $field->toArray();
        }

        $arResult['FORMS'][] = $f;
    }


}



$APPLICATION->AddHeadString("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js\"></script>");
$APPLICATION->AddHeadString("<script src=\"/bitrix/components/ceolab/calc.anket/templates/.default/js/calendar.js\"></script>");
$APPLICATION->AddHeadString("<script src=\"/bitrix/components/ceolab/calc.anket/templates/.default/js/validator.js\"></script>");


$this->IncludeComponentTemplate();

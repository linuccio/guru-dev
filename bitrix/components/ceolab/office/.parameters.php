<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if(!CModule::IncludeModule("iblock"))
    return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
    $arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$regionProp = CIBlock::GetProperties($arCurrentValues['IBLOCK_ID'], array(), array("CODE" => "CITY"));
$prop = $regionProp->Fetch();
if(isset($prop['LINK_IBLOCK_ID']))
{
    $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $prop['LINK_IBLOCK_ID']));
    while($r = $res->GetNextElement())
    {
        $arCity[$r->fields['ID']] = $r->fields['NAME'];;
    }
}



$arComponentParameters = array(
    "GROUPS" => array(
    ),
	"PARAMETERS" => array(
        "IBLOCK_TYPE" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_IBLOCK_DESC_LIST_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arTypesEx,
            "DEFAULT" => "news",
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_IBLOCK_DESC_LIST_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "REFRESH" => "Y",
        ),
        "DEFAULT_CITY" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("default_city"),
            "TYPE" => "LIST",
            "VALUES" => $arCity
        ),
        "SET_SESSION" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("set_session"),
            "TYPE" => "STRING"
        ),
        "GET_SESSION" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("get_session"),
            "TYPE" => "STRING"
        ),
        "SET_GET" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("region"),
            "TYPE" => "STRING"
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>3600)

 )
);

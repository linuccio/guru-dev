<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("offices"),
    "DESCRIPTION" => GetMessage("offices_desc"),
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "CeoLab",
        "CHILD" => array(
            "ID" => "office",
            "NAME" => GetMessage("offices")
        )
    ),
);
?>

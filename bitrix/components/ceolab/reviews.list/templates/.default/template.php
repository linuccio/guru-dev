<div class="reviews">
    <div class="items" align="left">
        <?foreach($arResult as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            if($arItem['ACTIVE'] != "Y")
                $APPLICATION->setEditArea($this->getEditAreaId($arItem['ID']), array(
                    array(
                        "URL" => "?active=".$arItem['ID'],
                        "TITLE" => "Активировать отзыв",
                        "ICON" => "bx-context-toolbar-edit-icon"
                    )
                ));            ?>
            <div class="item <?if($arItem['ACTIVE'] != "Y"){echo ' no_active'; }?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="title">
                    <span class="name"><?=$arItem['NAME']?></span>&nbsp;&nbsp;<?=$arItem['ACTIVE_FROM']?>
                </div>
                <div class="text">
                    <?=$arItem['PREVIEW_TEXT']?>
                </div>
            </div>
        <?endforeach?>
        <?if(!count($arResult)):?>
            Отзывов нет
        <?endif?>
    </div>
</div>
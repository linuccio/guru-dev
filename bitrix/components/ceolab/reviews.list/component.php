<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 10/10/13
 * Time: 1:04 AM
 * To change this template use File | Settings | File Templates.
 */

CModule::IncludeModule("iblock");

$filter = array("IBLOCK_ID" => $arParams['IBLOCK_ID']);

$groupAccess = explode(",", $arParams['USER_GROUP_ACCESS']);
$arGroups = CUser::GetUserGroup($USER->GetID());

if(!array_intersect($arGroups, $groupAccess))
{
    $filter['ACTIVE'] = "Y";
}
elseif($_GET['active'])
{
    $el = new CIBlockElement();
    $el->Update((int)$_GET['active'], array("ACTIVE" => "Y"));
}

$res = CIBlockElement::GetList(array("ID" => "DESC"), $filter);
while($r = $res->GetNextElement())
{
    $buttons = CIBlock::GetPanelButtons(
        $r->fields["IBLOCK_ID"],
        $r->fields["ID"],
        0,
        array("SECTION_BUTTONS"=>false, "SESSID"=>false)
    );

    $arResult[] = array_merge(
        $r->GetFields(),
        array(
            "EDIT_LINK" =>  $buttons["edit"]["edit_element"]["ACTION_URL"],
            "DELETE_LINK" => $buttons["edit"]["delete_element"]["ACTION_URL"]
        )
    );
}

$this->includeComponentTemplate();


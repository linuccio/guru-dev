<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/28/13
 * Time: 7:14 AM
 * To change this template use File | Settings | File Templates.
 */

class Field_Model extends Base_Model
{
    protected $table = "b_ceolab_calcmfo_anket_forms_fields";

    function addField($id, $type)
    {
        global $DB;
        $DB->Query(sprintf("INSERT INTO %s (form, type) VALUES(%s, '%s')", $this->table, (int)$id, $type));
        $id = $DB->LastID();
        return $id;
    }

    function getFieldsByForm($id)
    {
        global $DB;
        $res = $DB->Query(sprintf("SELECT * FROM %s WHERE `form`= '%s' ORDER BY `sort` ASC", $this->table, $id));
        while($d = $res->GetNext())
        {
            $data[] = $d;
        }
        return $data;
    }



    function setTitle($id, $title)
    {
        $this->update($id, array("title" => $title));
    }

    function setRequire($id, $val)
    {
        $this->update($id, array("require" => $val));
    }

    function setNameVar($id, $val)
    {
        $this->update($id, array("name" => $val));
    }

    function getAttr($id, $name)
    {
        global $DB;
        $res = $DB->Query(sprintf("SELECT attr FROM %s WHERE id=%s", $this->table, $id));
        $d = $res->Fetch();

        if($d['attr'])
        {
            if($attr = unserialize($d['attr']))
                if(isset($attr[$name]))
                    return $attr[$name];
        }

    }

    protected function getAllAttr($id)
    {
        global $DB;
        $res = $DB->Query(sprintf("SELECT attr FROM %s WHERE id=%s", $this->table, $id));
        $d = $res->Fetch();
        if($attr = unserialize($d['attr']))
        {
            return $attr;
        }
        else
            return array();
    }

    function setAttr($id, $name, $val)
    {

        $attr = $this->getAllAttr($id, $name);
        
        $attr[$name] = $val;
        $attr = serialize($attr);

        $this->update($id, array("attr" => $attr));

    }

    function setSort($id, $val)
    {
        $this->update($id, array("sort" => $val));
    }


}
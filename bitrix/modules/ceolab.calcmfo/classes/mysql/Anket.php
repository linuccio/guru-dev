<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/27/13
 * Time: 10:53 PM
 * To change this template use File | Settings | File Templates.
 */
class Anket_Model extends Base_Model
{
    protected $table = "b_ceolab_calcmfo_anket";

    function newAnket($name)
    {
        global $DB;
        $res = $DB->Query(sprintf("INSERT INTO %s (name) VALUES ('%s')", $this->table, $name));
        $id = $DB->LastID();

        return $id;
    }

    function getAllAnket()
    {
        global $DB;
        $res = $DB->Query(sprintf("SELECT * FROM %s", $this->table));
        while($d = $res->GetNext())
        {
            $data[] = $d;
        }
        return $data;
    }

    function getAnket($id)
    {
        global $DB;
        $res = $DB->Query(sprintf("SELECT * FROM %s WHERE id='%s'", $this->table, $id));
        $d = $res->GetNext();

        return $d;
    }

    function setName($id, $name)
    {
        $this->update($id, array("name" => $name));
    }

    function setIblock($id, $type, $iblock)
    {
        $this->update($id, array("iblockType" => $type, "iblock" => $iblock));
    }

    function getIblock($id)
    {
        global $DB;
        $res = $DB->Query(sprintf("SELECT iblockType, iblock FROM %s WHERE id='%s'", $this->table, $id));
        $d = $res->GetNext();

        $result = array(
            "type" => $d['iblockType'],
            "iblock" => $d['iblock']
        );

        return $result;
    }
}
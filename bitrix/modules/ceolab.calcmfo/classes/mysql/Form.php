<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/28/13
 * Time: 2:04 AM
 * To change this template use File | Settings | File Templates.
 */

class Form_Model extends Base_Model
{
    protected $table = "b_ceolab_calcmfo_anket_forms";
    function newForm($id)
    {
        global $DB;
        $res = $DB->Query(sprintf("INSERT INTO %s (anket) VALUES ('%s')", $this->table, $id));
        $id = $DB->LastID();
        return $id;
    }

    function setName($id, $name)
    {
        $this->update($id, array("name" => $name));
    }

    function getFormsByAnketId($id)
    {
        global $DB;
        $res = $DB->Query(sprintf("SELECT * FROM %s WHERE `anket`='%s' ORDER BY `sort` ASC", $this->table, $id));
        $data = array();
        while($d = $res->GetNext())
        {
            $data[] = $d;
        }
        return $data;
    }

    function setSort($id, $val)
    {
        $this->update($id, array("sort" => $val));
    }

    function setIblock($id, $type, $iblock)
    {
        $this->update($id, array("iblockType" => $type, "iblock" => $iblock));
    }
}
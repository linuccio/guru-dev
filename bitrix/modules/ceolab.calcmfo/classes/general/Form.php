<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/7/13
 * Time: 2:56 PM
 * To change this template use File | Settings | File Templates.
 */

class Form
{
    protected $id;
    protected $fields;
    protected $name;
    protected $model;
    protected $fieldFactory;

    function __construct($data, $model = null)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];


        if($this->model)
            $this->model = $model;
        else
            $this->model = new Form_Model();

    }

    function getFields()
    {
        if(!$this->fields)
        {
            $factory = new FieldFactory();
            $this->fields = $factory->getField($this->id);
        }

        return $this->fields;
    }

    function getName()
    {
        return $this->name;
    }

    function setName($name)
    {
        $this->model->setName($this->id, $name);
    }

    function addField($type)
    {
        $factory = new FieldFactory();
        return $factory->add($this->id, $type);
    }

    function isValidate()
    {

    }

    function setSort($val)
    {
        if($this->sort != $val)
            $this->model->setSort($this-> id, $val);

        $this->sort = $val;
    }

    function getId()
    {
        return $this->id;
    }

    function setIblock($type, $iblock)
    {
        $this->model->setIblock($this->id, $type, $iblock);

        $this->iblock = array(
            "type" => $type,
            "iblock" => $iblock
        );
    }

    function getIblock()
    {

    }


}
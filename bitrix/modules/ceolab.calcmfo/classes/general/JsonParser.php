<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/30/13
 * Time: 4:19 AM
 * To change this template use File | Settings | File Templates.
 */

class JsonParser
{
    protected $fieldTypes = array(
        "inputText" =>array(
            "class" => "FieldFactory_Input",
            "methods" => array(
                "name"      => "setNameVar",
                "require"   => "setRequire"
            )
        ),
        "date" =>array(
            "class" => "FieldFactory_Date",
            "methods" => array(
                "name"      => "setNameVar",
                "require"   => "setRequire",
                "defaultDate" => "setDefaultMonthYear"
            )
        ),
        "checkbox" =>array(
            "class" => "FieldFactory_Checkbox",
            "methods" => array(
                "name"      => "setNameVar",
                "require"   => "setRequire"
            )
        ),
        "file" =>array(
            "class" => "FieldFactory_File",
            "methods" => array(
                "name"      => "setNameVar",
                "require"   => "setRequire"
            )
        ),
        "select" => array(
            "class" => "FieldFactory_Select",
            "methods" => array(
                "name"      => "setNameVar",
                "require"   => "setRequire",
                "values"    => "setValues"
            )
        )

    );

    function getData($json)
    {
        $aFactory = new AnketFactory();
        $anket = $aFactory->addAnket("first");

        $data = json_decode($json);

        foreach($data->forms as $fData)
        {
            $form = $anket->addForm();
            $form->setName($fData->name);

            foreach($fData->fields as $fieldData)
            {
                if(!$fieldData->type || !$fieldTypes[$fieldData->type])
                    continue;

                $objectType = $fieldTypes[$fieldData->type];

                $factory = new $objectType['class']();
                $field = $form->addField($factory);

                $field->setTitle($fieldData->title);

                foreach($fieldData->attr as $key => $val)
                {
                    if(isset($objectType['methods'][$key]) && $method = $objectType['methods'][$key])
                        $field->$method($val);

                }

            }
        }

        return $data;
    }
}
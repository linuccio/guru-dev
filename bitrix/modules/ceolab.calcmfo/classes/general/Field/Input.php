<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/23/13
 * Time: 7:54 PM
 * To change this template use File | Settings | File Templates.
 */

class Input_Field extends Field
{
    protected $type = "input";
    protected $validator;

    function __construct($data, $model = null)
    {
        parent::__construct($data, $model);
    }

    function setMask($val)
    {
        $this->mask = $val;
        $this->setAttr("mask", $val);
    }

    function getMask()
    {
        if(!$this->mask)
            $this->mask = $this->getAttr("mask");

        return $this->mask;
    }

    function toArray()
    {
        $result = array(
            "title" => $this->getTitle(),
            "isRequired" => $this->getRequire(),
            "nameVar" => $this->getNameVar(),
            "type" => $this->getType(),
            "validator" => $this->getValidator(),
            "mask" => $this->getMask()
        );
        return $result;
    }

    function setIblockProp($val)
    {
        $this->iblockProp = $val;
        $this->setAttr("iblockProp", $val);
    }

    function getIblockProp()
    {
        if(!$this->iblockProp)
            $this->iblockProp = $this->getAttr("iblockProp");

        return $this->iblockProp;
    }
}

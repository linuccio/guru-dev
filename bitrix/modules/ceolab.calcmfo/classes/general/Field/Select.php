<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/23/13
 * Time: 7:54 PM
 * To change this template use File | Settings | File Templates.
 */
class Select_Field extends Field
{
    protected $type = "select";
    protected $values;

    function __construct($data, $model = null)
    {
        parent::__construct($data, $model);
    }

    function setValues($array)
    {
        $this->values = $array;
        $this->setAttr("values", $array);
    }

    function getValues($array)
    {
        if(!$this->values)
            $this->values = $this->getAttr("values");

        return $this->values;

    }

    function toArray()
    {
        $result = parent::toArray();
        $result['values'] = $this->getValues();
        foreach($result['values'] as $k => $v)
        {
            $result['values'][$k] = (array)$v;
        }
        return $result;
    }

    function setIblockProp($val)
    {
        $this->iblockProp = $val;
        $this->setAttr("iblockProp", $val);
    }

    function getIblockProp()
    {
        if(!$this->iblockProp)
            $this->iblockProp = $this->getAttr("iblockProp");

        return $this->iblockProp;
    }
}
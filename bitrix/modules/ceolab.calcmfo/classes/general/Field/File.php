<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/23/13
 * Time: 7:54 PM
 * To change this template use File | Settings | File Templates.
 */

class File_Field extends Field
{
    protected $type = "file";
    protected $maxSize;
    protected $ext;

    function __construct($data, $model = null)
    {
        parent::__construct($data, $model);
    }

    function setMaxSize($val)
    {
        $this->maxSize = $val;
        $this->setAttr("maxSize", $val);
    }

    function setExt($val)
    {
        $this->ext = $val;
        $this->ext("ext", $val);
    }

    function getExt()
    {
        if(!$this->ext)
            $this->ext = $this->getAttr("ext");
        return $this->ext;
    }

    function getMaxSize()
    {
        if(!$this->maxSize)
            $this->maxSize = $this->getAttr("maxSize");
        return $this->maxSize;
    }

    function toArray()
    {
        $result = parent::toArray();
        $result['max_size'] = $this->getMaxSize();
        $result['ext']  = $this->getExt();
        return $result;
    }
}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/23/13
 * Time: 7:55 PM
 * To change this template use File | Settings | File Templates.
 */

class Date_Field extends Field
{
    protected $type = "date";
    protected $defaultMonthYear;

    function __construct($data, $model = null)
    {
        parent::__construct($data, $model);
    }

    function setDefaultMonthYear($val)
    {
        $this->defaultMonthYear = $val;
        $this->setAttr("defaultMonthYear", $val);
    }

    function getDefaultMonthYear()
    {
        if(!$this->defaultMonthYear)
            $this->defaultMonthYear = $this->getAttr("defaultMonthYear");

        return $this->defaultMonthYear;
    }

    function toArray()
    {
        $result = array(
            "title" => $this->getTitle(),
            "isRequired" => $this->getRequire(),
            "nameVar" => $this->getNameVar(),
            "type" => $this->getType(),
            "validator" => $this->getValidator(),
            "defaultMonthYear" => $this->getDefaultMonthYear()
        );
        return $result;
    }

    function setIblockProp($val)
    {
        $this->iblockProp = $val;
        $this->setAttr("iblockProp", $val);
    }

    function getIblockProp()
    {
        if(!$this->iblockProp)
            $this->iblockProp = $this->getAttr("iblockProp");

        return $this->iblockProp;
    }

}
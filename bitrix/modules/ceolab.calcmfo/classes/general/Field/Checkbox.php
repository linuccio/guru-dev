<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/23/13
 * Time: 7:54 PM
 * To change this template use File | Settings | File Templates.
 */

class Checkbox_Field extends Field
{
    protected $type = "checkbox";
    function __construct($data, $model = null)
    {
        parent::__construct($data, $model);
    }
}
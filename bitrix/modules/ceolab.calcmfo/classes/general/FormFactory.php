<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/25/13
 * Time: 2:07 AM
 * To change this template use File | Settings | File Templates.
 */

class Form_Factory
{
    protected $model;

    function __construct()
    {
        $this->model = new Form_Model();
    }

    function addForm($anketId)
    {
        $id = $this->model->newForm($anketId);
        $form = new Form(array("id" => $id));
        return $form;
    }

    function getFormById($id)
    {
        $data = $this->model->getById($id);
        return new Form($data);
    }

    function getFormsByAnketId($id)
    {
        $data = $this->model->getFormsByAnketId($id);
        $forms = array();
        foreach($data as $d)
        {
            $forms[] = new Form($d);
        }
        return $forms;
    }

    function removeForm(Form $form)
    {
        $this->model->remove($form->getId());
    }
}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/23/13
 * Time: 6:48 PM
 * To change this template use File | Settings | File Templates.
 */

class AnketFactory
{
    protected $model;

    function __construct()
    {
        $this->model = new Anket_Model();
    }


    function addAnket($name)
    {
        $id = $this->model->newAnket($name);
        $anket = new Anket(array("id" => $id, "name" => $name));
        return $anket;
    }

    function getAllAnkets()
    {
        $data = $this->model->getAllAnket();
        foreach ($data as $d)
        {
            $result[] = new Anket($d);
        }
        return $result;
    }

    function getAnket($id)
    {
        $data = $this->model->getAnket($id);
        if($data)
        {
            return new Anket($data);
        }
    }
}
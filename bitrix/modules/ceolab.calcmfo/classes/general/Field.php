<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/23/13
 * Time: 7:09 PM
 * To change this template use File | Settings | File Templates.
 */

class Field
{
    protected $id;
    protected $isRequired;
    protected $attr;
    protected $formId;
    protected $nameVar;
    protected $title;
    protected $type;
    protected $validator;

    protected $model;

    function __construct($data, Field_Model $model = null)
    {
        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->nameVar = $data['name'];
        $this->isRequired = (bool)$data['require'];

        if($model)
            $this->model = $model;
        else
            $this->model = new Field_Model();
    }

    function setTitle($title)
    {
        if($this->title != $title)
        {
            global $DB;
            $this->title = $title;
            $this->model->setTitle($this->id, $title);        }
    }

    function setNameVar($name)
    {
        if($this->nameVar != $name)
        {
            global $DB;
            $this->nameVar = $name;
            $this->model->setNameVar($this->id, $name);
        }
    }

    function getNameVar()
    {
        return $this->nameVar;
    }

    function getTitle()
    {
        return $this->title;
    }

    function setRequire($val)
    {
        if($val == "false")
            $val = false;

        $val = (bool)$val;
        if($this->isRequired != $val)
        {
            global $DB;
            $this->isRequired = $val;

            if($val)
                $val = 1;
            else
                $val = 0;

            $this->model->setRequire($this->id, $val);
        }
    }

    function getRequire()
    {
        return $this->isRequired;
    }

    protected function setAttr($name, $val)
    {
        $this->model->setAttr($this->id, $name, $val);
    }

    protected function getAttr($name)
    {
        $values = $this->model->getAttr($this->id, $name);
        return $values;
    }

    function setSort($val)
    {
        if($val != $this->sort)
        {
            $this->model->setSort($this->id, $val);
        }
    }

    function getId()
    {
        return $this->id;
    }

    function getType()
    {
        return $this->type;
    }

    function toArray()
    {
        $result = array(
            "title" => $this->getTitle(),
            "isRequired" => $this->getRequire(),
            "nameVar" => $this->getNameVar(),
            "type" => $this->getType(),
            "validator" => $this->getValidator()
        );
        return $result;
    }

    function setValidator($val)
    {
        $this->setAttr("validator", $val);
    }

    function getValidator()
    {
        if(!$this->validator)
            $this->validator = $this->getAttr("validator");

        return $this->validator;
    }

    function checkValue($val)
    {
        if(!$this->getValidator())
            return true;

        $validator = ucfirst(strtolower($this->getValidator()));
        $class = $validator."_Validator";

        if(class_exists($class))
            return $class::check($val);
        else
            return true;

    }

    function getIblockProp()
    {
        return false;
    }
}
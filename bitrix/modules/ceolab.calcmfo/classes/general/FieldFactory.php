<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/25/13
 * Time: 2:42 AM
 * To change this template use File | Settings | File Templates.
 */

class FieldFactory
{
    protected $type;
    protected $model;

    protected $types = array(
        "input"     => "Input_Field",
        "select"    => "Select_Field",
        "checkbox"  => "Checkbox_Field",
        "file"      => "File_Field",
        "date"      => "Date_Field"
    );

    function __construct($model = null)
    {
        if($model)
            $this->model = $model;
        else
            $this->model = new Field_Model();
    }

    function get($id, $type, $data = array())
    {
        $data['id'] = $id;
        if(isset($this->types[$type]))
            return new $this->types[$type]($data);
        else
            throw new Exception("Field ".$type." of type is invalid");
    }

    function getFieldById($id)
    {
        $data = $this->model->getById($id);
        return $this->get($id, $data['type'], $data);
    }

    function getField($formId)
    {
        $data = $this->model->getFieldsByForm($formId);

        foreach($data as $val)
        {
            $fields[] = $this->get($val["id"], $val['type'], $val);
        }
        return $fields;
    }

    function add($formId, $type)
    {
        $id = $this->model->addField($formId, $type);
        $field = $this->get($id, $type);
        return $field;
    }

    function removeField(Field $field)
    {
        $this->model->remove($field->getId());
    }


}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/26/13
 * Time: 5:43 PM
 * To change this template use File | Settings | File Templates.
 */
class FieldFactory_File extends FieldFactory
{
    protected $type = "file";

    function get($id)
    {
        return new File_Field($id);
    }
}
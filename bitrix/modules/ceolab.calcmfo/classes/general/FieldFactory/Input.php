<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/25/13
 * Time: 10:33 PM
 * To change this template use File | Settings | File Templates.
 */

class FieldFactory_Input extends FieldFactory
{
    protected $type = "input";

    function get($id)
    {
        return new Input_Field($id);
    }
}
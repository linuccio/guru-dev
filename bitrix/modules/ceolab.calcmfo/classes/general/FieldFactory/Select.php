<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/25/13
 * Time: 10:33 PM
 * To change this template use File | Settings | File Templates.
 */

class FieldFactory_Select extends FieldFactory
{
    protected $type = "select";

    function get($id)
    {
        return new Select_Field($id);
    }
}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/26/13
 * Time: 12:26 AM
 * To change this template use File | Settings | File Templates.
 */
class FieldFactory_Date extends FieldFactory
{
    protected $type = "date";

    function get($id)
    {
        return new Date_Field($id);
    }
}
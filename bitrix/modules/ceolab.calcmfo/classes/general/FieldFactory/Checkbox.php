<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/26/13
 * Time: 5:34 PM
 * To change this template use File | Settings | File Templates.
 */
class FieldFactory_Checkbox extends FieldFactory
{
    protected $type = "checkbox";

    function get($id)
    {
        return new Checkbox_Field($id);
    }
}
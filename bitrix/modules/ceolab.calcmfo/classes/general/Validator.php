<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 12/10/13
 * Time: 11:59 AM
 * To change this template use File | Settings | File Templates.
 */

class Validator
{
    static protected $pattern;
    static function check($value)
    {
        if(preg_match(static::$pattern, $value))
            return true;
    }
}
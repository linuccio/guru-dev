<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/24/13
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */

class Anket
{
    protected $id;
    protected $forms = false;
    protected $name;
    protected $formFactory;
    protected $model;
    protected $iblock = array();

    function __construct($data, $formFactory = false, Anket_Model $model = null)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
        if($data['forms'])
            $this->forms = $data['forms'];

        if($model)
            $this->model = $model;
        else
            $this->model = new Anket_Model();

        if($formFactory)
            $this->formFactory = $formFactory;
        else
            $this->formFactory = new Form_Factory();
    }

    function setName($name)
    {
        $this->model->setName($this->id, $name);
    }

    function getName()
    {
        return $this->name;
    }

    function getId()
    {
        return $this->id;
    }

    function addForm()
    {
        return $this->formFactory->addForm($this->id);
    }

    function initForms()
    {
        $fFactory = new Form_Factory();
        $this->forms = $this->formFactory->getFormsByAnketId($this->id);
    }

    function getForms()
    {
        if($this->forms === false)
            $this->initForms();

        return $this->forms;
    }

    function setIblock($type, $iblock)
    {
        $this->model->setIblock($this->id, $type, $iblock);
    }

    function getIblock()
    {
        return $this->model->getIblock($this->id);
    }

}
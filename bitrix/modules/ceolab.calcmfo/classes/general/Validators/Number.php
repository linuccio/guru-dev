<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/25/13
 * Time: 11:55 PM
 * To change this template use File | Settings | File Templates.
 */
class Number_Validator extends Validator
{
    static protected $pattern = "![^0-9]{1,}!";
    static function check($value)
    {
        if(!preg_match(static::$pattern, $value))
            return true;
    }
}
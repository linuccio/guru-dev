<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 3/25/14
 * Time: 1:40 AM
 * To change this template use File | Settings | File Templates.
 */
class Date_Validator extends Validator
{
    static protected $pattern = "/[0-9]{2}\.[0-9]{2}.[0-9]{4}\./";
    static function check($value)
    {
        if(!preg_match(static::$pattern, $value))
            return true;
    }
}
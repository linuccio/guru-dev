<?php
/**
 * Created by PhpStorm.
 * User: evgenius
 * Date: 10/31/14
 * Time: 12:15 AM
 */

define("ADMIN_MODULE_NAME", "diforce.backet");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile(__FILE__);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

CModule::IncludeModule("ceolab.calcmfo");

$aFactory = new AnketFactory();
$anket = $aFactory->getAnket(59);
$forms = $anket->getForms();

$htmlStr = "";
foreach($forms as $form)
{
    $fields = $form->getFields();

    $htmlStr .= sprintf("<p>%s<br/>\n", $form->getName());
    foreach($fields as $field)
    {
        if($field->GetType() == "file")
            continue;

        if($field->getNameVar())
        {
            $htmlStr .= sprintf("%s: #%s#<br/>\n", $field->getTitle(), $field->getNameVar());
        }
        else
        {
            $warnings[] = $form->getName().": ".$field->getTitle();
        }

    }
    $htmlStr .= "</p>\n\n";
}

?>

<div style="background-color: white; font-size: 14px; padding: 20px">
    <div style="color: red">
        <?foreach($warnings as $text):?>
            <?=$text?><br/>
        <?endforeach?>
    </div>
    <textarea style="width: 500px; height: 500px">
        <?=$htmlStr?>
    </textarea>
</div>

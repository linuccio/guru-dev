<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/10/13
 * Time: 11:38 AM
 * To change this template use File | Settings | File Templates.
 */
define("ADMIN_MODULE_NAME", "diforce.backet");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile(__FILE__);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
CModule::IncludeModule("ceolab.calcmfo");
$aFactory = new AnketFactory();
$ankets = $aFactory->getAllAnkets();

?>
<div class="adm-list-table-wrap">
    <div class="adm-list-table-top">
        <a href="ceolab_calc_anket.php" class="adm-btn adm-btn-save adm-btn-add">Добавить</a>
    </div>
    <table class="adm-list-table">
        <tr class="adm-list-table-header">
            <th class="adm-list-table-cell adm-list-table-checkbox">
                <div class="adm-list-table-cell-inner">
                    <label class="adm-designed-checkbox-label" for="tbl_iblock_type_check_all"></label>
                </div>
            </th>
            <th class="adm-list-table-cell adm-list-table-popup-block">
                <div class="adm-list-table-cell-inner"></div>
            </th>
            <th class="adm-list-table-cell">
                <div class="adm-list-table-cell-inner">
                    ID
                </div>
            </th>
            <th class="adm-list-table-cell">
                <div class="adm-list-table-cell-inner">
                    Имя
                </div>
            </th>
            <th class="adm-list-table-cell">
                <div class="adm-list-table-cell-inner">
                    Форм
                </div>
            </th>
            <th class="adm-list-table-cell">
                <div class="adm-list-table-cell-inner">
                    Полей
                </div>
            </th>
        </tr>
        <?foreach($ankets as $anket):?>
            <tr class="adm-list-table-row" ondblclick="location='/bitrix/admin/ceolab_calc_anket.php?anket=<?=$anket->getId()?>'">
                <td class="adm-list-table-cell adm-list-table-checkbox adm-list-table-checkbox-hover">
                    <label class="adm-designed-checkbox-label adm-checkbox" for="tbl_iblock_type_vacancy_6wJkd"></label>
                </td>
                <td class="adm-list-table-cell adm-list-table-popup-block">
                    <div class="adm-list-table-popup" title="Действия"></div>
                </td>
                <td class="adm-list-table-cell">
                    <?=$anket->getId();?>
                </td>
                <td class="adm-list-table-cell">
                    <?=$anket->getName();?>
                </td>
                <td class="adm-list-table-cell">
                    <?=count($anket->getForms());?>
                </td>
                <td class="adm-list-table-cell">
                    0
                </td>
            </tr>
        <?endforeach?>
    </table>
    <div class="adm-list-table-footer">
        <span class="adm-selectall-wrap">
            <label class="adm-checkbox adm-designed-checkbox-label" for="action_target"></label> Для всех
        </span>
        <span class="adm-table-item-edit-wrap">
            <a id="" title="Редактировать отмеченные записи" onclick="this.blur();if(tbl_iblock_type.IsActionEnabled('edit')){document.forms['form_tbl_iblock_type'].elements['action_button'].value='edit'; ; BX.submit(document.forms.form_tbl_iblock_type, 'action_button', 'edit');}" hidefocus="true" class="adm-table-btn-edit adm-edit-disable" href="javascript:void(0)"></a>
            <a id="" title="Удалить записи" onclick="this.blur();if(tbl_iblock_type.IsActionEnabled() &amp;&amp; confirm((document.getElementById('action_target') &amp;&amp; document.getElementById('action_target').checked? 'Вы уверены, что хотите удалить ВСЕ записи из списка?':'Вы уверены, что хотите удалить отмеченные записи?'))) {document.forms['form_tbl_iblock_type'].elements['action_button'].value='delete'; ; BX.submit(document.forms.form_tbl_iblock_type, 'action_button', 'delete');}" hidefocus="true" class="adm-table-btn-delete adm-edit-disable" href="javascript:void(0);"></a>
	    </span>
    </div>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>
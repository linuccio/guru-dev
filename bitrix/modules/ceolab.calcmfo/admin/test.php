<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/10/13
 * Time: 11:38 AM
 * To change this template use File | Settings | File Templates.
 */
define("ADMIN_MODULE_NAME", "diforce.backet");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile(__FILE__);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
//$APPLICATION->AddHeadString("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js\"></script>");
$APPLICATION->AddHeadScript('/bitrix/js/ceolab.calcmfo/jquery-2.0.3.min.js');
$APPLICATION->AddHeadScript('/bitrix/js/ceolab.calcmfo/calendar.js');
$APPLICATION->AddHeadScript('/bitrix/js/ceolab.calcmfo/script.js');
$APPLICATION->AddHeadScript('/bitrix/js/ceolab.calcmfo/propFieldsBase.js');


CModule::IncludeModule("ceolab.calcmfo");
$fieldTypes = array(
    "inputText" =>array(
        "class" => "Input_Field",
        "methods" => array(
            "name"          => array("setNameVar", "getNameVar"),
            "require"       => array("setRequire", "getRequire"),
            "validator"     => array("setValidator", "getValidator"),
            "mask"          => array("setMask", "getMask"),
            "iblockprop"    => array("setIblockProp", "getIblockProp")
        )
    ),
    "date" =>array(
        "class" => "Date_Field",
        "methods" => array(
            "name"      => array("setNameVar", "getNameVar"),
            "require"   => array("setRequire", "getRequire"),
            "defaultDate" => array("setDefaultMonthYear", "getDefaultMonthYear"),
            "validator" => array("setValidator", "getValidator"),
            "iblockprop"    => array("setIblockProp", "getIblockProp")
        )
    ),
    "checkbox" =>array(
        "class" => "Checkbox_Field",
        "methods" => array(
            "name"      => array("setNameVar", "getNameVar"),
            "require"   => array("setRequire", "getRequire"),
            "validator" => array("setValidator", "getValidator")
        )
    ),
    "file" =>array(
        "class" => "File_Field",
        "methods" => array(
            "name"      => array("setNameVar", "getNameVar"),
            "require"   => array("setRequire", "getRequire"),
            "validator" => array("setValidator", "getValidator")
        )
    ),
    "select" => array(
        "class" => "Select_Field",
        "methods" => array(
            "name"      => array("setNameVar", "getNameVar"),
            "require"   => array("setRequire", "getRequire"),
            "values"    => array("setValues", "getValues"),
            "validator" => array("setValidator", "getValidator"),
            "iblockprop"    => array("setIblockProp", "getIblockProp")
        )
    )

);

if($_POST['submit'])
{
    $aFactory = new AnketFactory();
    $data = json_decode($_POST['data']);

    if(!isset($_GET['anket']))
    {
        $anket = $aFactory->addAnket("first");

        foreach($data->forms as $sortForm => $fData)
        {
            $form = $anket->addForm();
            $form->setSort($sortForm+1);
            $form->setName($fData->name);
            //$form->setIblock()

            foreach($fData->fields as $sort => $fieldData)
            {
                if(!$fieldData->type || !$fieldTypes[$fieldData->type])
                    continue;

                $objectType = $fieldTypes[$fieldData->type];

                if($fieldData->type == "inputText")
                    $fieldData->type = "input";



                $field = $form->addField($fieldData->type);

                $field->setTitle($fieldData->title);
                $field->setSort($sort+1);

                foreach($fieldData->attr as $key => $val)
                {
                    if(isset($objectType['methods'][$key][0]) && $method = $objectType['methods'][$key][0])
                    {
                        $field->$method($val);
                    }

                }

            }
        }
        $data = json_encode($data);
    }
    else
    {
        $anket = $aFactory->getAnket($_GET['anket']);
        if($anket)
        {
            $formFactory = new Form_Factory();
            $fieldFactory = new FieldFactory();
            $forms = $anket->getForms();
            $anket->setIblock($data->iblock->type, $data->iblock->iblock);
            foreach($data->forms as $sortForm => $fData)
            {
                if(isset($fData->id))
                {
                    foreach($forms as $k => $form)
                    {
                        if($fData->id == $form->GetId())
                        {
                            unset($forms[$k]);
                            break;
                        }
                        $form = false;
                    }

                    if(!$form)
                        continue;
                }
                else
                {
                    $form = $anket->addForm();
                }

                $form->setName($fData->name);
                $form->setSort($sortForm+1);

                $fields = $form->getFields();
                $removeFields = $fields;

                foreach($fData->fields as $sort => $fieldData)
                {
                    if(!$fieldData->type || !$fieldTypes[$fieldData->type])
                        continue;

                    $objectType = $fieldTypes[$fieldData->type];

                    if($fieldData->type == "inputText")
                        $fieldData->type = "input";

                    if(isset($fieldData->id))
                    {
                        foreach($fields as $k => $field)
                        {
                            if($field->getId() == $fieldData->id)
                            {
                                unset($fields[$k]);
                                break;
                            }
                            $field = false;
                        }
                        if(!$field)
                            continue;
                    }
                    else
                    {
                        $field = $form->addField($fieldData->type);
                    }


                    $field->setTitle($fieldData->title);
                    $field->setSort($sort+1);

                    foreach($fieldData->attr as $key => $val)
                    {
                        if(isset($objectType['methods'][$key][0]) && $method = $objectType['methods'][$key][0])
                        {
                            $field->$method($val);
                        }
                    }
                }

                foreach($fields as $field)
                    $fieldFactory->removeField($field);

            }

            foreach($forms as $form)
                $formFactory->removeForm($form);
        }
    }

    LocalRedirect("ceolab_calc_anket_list.php");
}
elseif(isset($_GET['anket']))
{
    $aFactory = new AnketFactory();
    $anket = $aFactory->getAnket($_GET['anket']);
    if($anket)
    {
        $forms = $anket->getForms();
        $data = array();
        $data['iblock'] = $anket->getIblock();
        foreach($forms as $form)
        {
            $fData = array();
            $fData['name'] = $form->getName();
            $fData['id'] = $form->getId();
            foreach($form->getFields() as $field)
            {

                $fieldData = array();
                $class = get_class($field);
                foreach($fieldTypes as $k => $v)
                {
                    if($v['class'] == $class)
                    {
                        $fieldData['type'] = $k;
                        break;
                    }
                }

                $fieldData['title'] = $field->getTitle();
                $fieldData['id'] = $field->getId();

                $objectType = $fieldTypes[$fieldData['type']];

                foreach($objectType['methods'] as $key => $method)
                {
                    $method = $method[1];
                    $fieldData['attr'][$key] = $field->$method();
                }
                $fData['fields'][] = $fieldData;

            }
            $data['name'] = "first";
            $data['forms'][] = $fData;
        }

        $data = json_encode($data);
    }
}

$iblockTypes = CIBlockType::GetList();
while($d = $iblockTypes->Fetch())
{
    if($d = CIBlockType::GetByIDLang($d["ID"], LANG))
    {
        $arIBTypes[$d['IBLOCK_TYPE_ID']]['NAME'] = $d['NAME'];
    }
}

$iblockList = CIBlock::GetList();
while($d = $iblockList->Fetch())
{
    $arIBTypes[$d['IBLOCK_TYPE_ID']]['iblocks'][$d['ID']]['NAME'] = $d['NAME'];
    $res = CIBlock::GetProperties($d['ID']);
    while($r = $res->Fetch())
    {
        $arIBTypes[$d['IBLOCK_TYPE_ID']]['iblocks'][$d['ID']]['PROPS'][$r['CODE']] = $r['NAME'];
    }
}
$iblocksJson = json_encode($arIBTypes);
?>
<script>
    $("document").ready(
        function()
        {
            extend(propFields.input, propFieldsBase);
            extend(propFields.select, propFieldsBase);
            extend(propFields.checkbox, propFieldsBase);
            extend(propFields.keyvalue, propFieldsBase);
            extend(propFields.date, propFieldsBase);

            extend(propValidators.uniqueValue, propValidators_proto);


            formFactory = new FormFactory();
            iblocksBar = new IblockBar();
            form = new Form();
            props = new PropertiesBar();

            calendar = new Calendar("input[calendar=\"calendar\"]");
            calendar.format = "%m.%y";

            iblocksBar.iblocks = JSON.parse('<?=$iblocksJson?>');
            iblocksBar.setIblocksType();



            <?if($data):?>
                var data = '<?=$data?>';
                data = JSON.parse(data);
                for(var i in data.forms)
                {
                    formFactory.setActive(formFactory.add(data.forms[i].name, data.forms[i]['id']));
                    for(var a in data.forms[i].fields)
                    {
                        var field = data.forms[i].fields[a];
                        form.addField(field.type, field.title, field.attr, field['id']);
                    }
                }
                iblocksBar.setCurrentIblockType(data.iblock.type);
                iblocksBar.setCurrentIblock(data.iblock.iblock);
            <?endif?>
        }
    )
</script>

<form method="POST" id="formSettings">
    <div class="formSettings">
        <div class="controlPanel leftPanel">
            <div class="buttons">
                <div class="fieldsPanel">
                    <a href="javascript:void(0)" onclick="form.addField('inputText')">Поле</a>
                    <a href="javascript:void(0)" onclick="form.addField('select')">Селект</a>
                    <a href="javascript:void(0)" onclick="form.addField('checkbox')">Чекбокс</a>
                    <a href="javascript:void(0)" onclick="form.addField('file')">Прикрепление файла</a>
                    <a href="javascript:void(0)" onclick="form.addField('date')">Выбор даты</a>
                </div>
                <div class="formsPanel">
                    <a href="javascript:void(0)" onclick="formFactory.add()"><span clas="plus">+</span> Добавить форму</a>
                </div>
            </div>
        </div>
        <div class="propertiesPanel">
            <div>
                <div class="head">
                    Свойства
                </div>
                <div class="fields" id="propsFields">

                </div>
            </div>
        </div>

        <div class="forms">
            <div class="items" id="formItems">

            </div>
        </div>
    </div>
    <div class="iblock_select">
        Тип инфоблока:
        <select id="selectIblockType">
            <option></option>
            <?foreach($arIBTypes as $k => $arIBType):?>
                <option value="<?=$k?>">
                    <?=$arIBType['NAME']?>
                </option>
            <?endforeach?>
        </select>
        Инфоблок:
        <select id="selectIblock">

        </select>
        <div class="properties">
            
        </div>
    </div>
    <input name="submit" type="submit" onclick="formFactory.send()" value="сохранить"/>
</form>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>
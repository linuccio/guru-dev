<?
IncludeModuleLangFile(__FILE__);
if ($USER->IsAdmin())
{
    $menu = array(
        "parent_menu" => "global_menu_settings",
        "section" => "bitrixcloud",
        "sort" => 1645,
        "text" => "Калькулятор займов",
        "icon" => "bitrixcloud_menu_icon",
        "page_icon" => "bitrixcloud_page_icon",
        "items_id" => "menu_bitrixcloud",
        "items" => array(),
    );
    $menu["items"][] = array(
        "text" => "Анкеты",
        "url" => "ceolab_calc_anket_list.php?lang=".LANGUAGE_ID
    );
    return $menu;
}
else
{
    return false;
}
?>
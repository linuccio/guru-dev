<?php
/**
 * Created by JetBrains PhpStorm.
 * User: evgenius
 * Date: 11/28/13
 * Time: 4:06 PM
 * To change this template use File | Settings | File Templates.
 */

$arr = array(
    "fieldTypes" => array(
        "input"     => "FieldFactory_Input",
        "date"      => "FieldFactory_Date",
        "checkbox"  => "FieldFactory_Checkbox",
        "file"      => "FieldFactory_File",
        "select"    => "FieldFactory_Select"
    )
);
﻿<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "PRODUCTS" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("products"),
            "TYPE" => "CUSTOM",
            "JS_FILE" => "bitrix/components/ceolab/calc.mfo/js/settings.js",
            "JS_EVENT" => "calcSettings",

            "JS_DATA" => '
                {
                    "lang":{
                        "add":"'.GetMessage("add").'",
                        "delete":"'.GetMessage("delete").'",
                        "up":"'.GetMessage("up").'",
                        "down":"'.GetMessage("down").'"
                    }
                }'
        ),
        "CAPTHA" => array(
            "PARENT" => "PARENT",
            "NAME" => GetMessage("captha"),
            "TYPE" => "CHECKBOX",
        )

    ),
);
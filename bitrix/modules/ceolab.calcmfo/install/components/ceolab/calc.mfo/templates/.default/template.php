<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<script xmlns="http://www.w3.org/1999/html">
    var calcs = [];



    $(document).ready(function(){

        var data = JSON.parse('<?=$arParams['data']?>');
        Validators = data.validators;

        captha = new captha();
        captha.getCaptha();

        <?if($arParams['result'] == "ok"):?>
            $("#calcStatusMessage").text("<?=GetMessage("requestSuccess")?>");
        <?elseif($arParams['result'] == "fail"):?>
            $("#calcStatusMessage").text("<?=GetMessage("requestFail")?>");
        <?endif?>

        <?if(isset($arParams['result'])):?>
            $("#calcStatusMessage").slideDown("fast");
            setTimeout("$(\"#calcStatusMessage\").slideUp(\"fast\")", 3000);
        <?endif?>

        for(i in data.calcs)
        {
            if(!data.calcs[i].calc.active)
                continue;

            var calc = new Calculator();
            calc.id = i;
            calc.name = data.calcs[i].calc.name;
            calc.summ.InputId = "summ";
            calc.summ.SliderId = "summ_slider";
            calc.summ.min = data.calcs[i].calc.min_summ;
            calc.summ.max = data.calcs[i].calc.max_summ;
            calc.summ.step = data.calcs[i].calc.step;
            calc.info = data.calcs[i].calc.additionInfo;
            calc.classForm = "calcForm";
            calc.currency = data.calcs[i].calc.currency;

            calc.term.InputId = "term";
            calc.term.SliderId = "term_slider";

            calc.term.max = data.calcs[i].calc.max_term;
            calc.percent = data.calcs[i].calc.percent;
            calc.type = data.calcs[i].calc.type_percent;
            calc.forms = data.calcs[i].forms;

            calc.funcCalc = function(result)
            {
                $("#calcTerm").text(result.termVal);
                $("#calcSumm").text(formatPrice(result.summVal));
                $("#calcPercent").text(formatPrice(parseInt(result.summPercent)));
                $("#calcTotal").text(formatPrice(parseInt(result.total)));
                var p = result.summVal/(result.summ.max/100);
                var v = 60;
                var val = parseInt((v/100)*p);

                $("#calcMoneyBag").attr("width", 100+val);
                $("#calcMoneyBag").attr("height", 120+val);
                $("#calcMoneyBag").attr("style", "position: relative; top:"+(60-val)+"px");
            }

            calc.initFunc = function(t)
            {
                $("#summMarkMin").text(t.summ.min.toString());
                $("#summMarkMax").text(t.summ.max.toString());

                $("#termMarkMin").text("1");
                $("#termMarkMax").text(t.term.max.toString());

                $("[name=calcCurrency]").text(t.currency);

                if(t.type == "day")
                    $("#calcPeriod").text("<?=GetMessage("day")?>");
                else
                    $("#calcPeriod").text("<?=GetMessage("week")?>");
            }

            calc.funcShowForm = function(t)
            {
                if(t.currentForm == 1)
                    $("#calcSubmitBtn").text("<?=GetMessage("submit")?>");
                else
                    $("#calcSubmitBtn").text("<?=GetMessage("next")?>");
            }


            calcs.push(calc);
        }


        if(calcs.length > 0)
            calcs[0].init();
    });




    function toggleCalc(calc, id)
    {
        $("li").removeAttr("class");
        $("#"+id).attr("class", "activeCalc");
        calc.init();
    }

    function formatPrice(price)
    {
        price = price.toString().split(".");
        var l = price[0].length;
        var a = 3;
        var result = "";
        do
        {
            l-=3;
            if(l<0)
            {
                a+=l;
                l=0;
            }

            result = price[0].substr(l, a)+" "+result;
        }while(l);
        if(price[1])
            result+="."+price[1];
        return result;
    }
</script>

<form method="POST" id="form">
    <table height="100" cellspacing="10px">
        <tr>
            <td >
                <div id="calcStatusMessage" style="display:none"></div>
                <div class='calc'>
                    <div id="calcHeader">
                        <div id='calcList'>
                            <table width="100%">
                                <tr>
                                    <td nowrap>
                                        <?=GetMessage("typeLoan")?>:
                                    </td>
                                    <td align='right' nowrap>
                                        <?
                                        if($arParams['status'] != MODULE_DEMO_EXPIRED)
                                        {
                                            $active = true;
                                            $countActive = 0;

                                            foreach($arParams['PRODUCTS'] as $key => $val):
                                                ?>
                                                <li id="calc<?=$key?>" style="cursor:pointer;" class="<?if($active){echo 'activeCalc'; $countActive++;}?>" onclick="toggleCalc(calcs[<?=$key?>], 'calc<?=$key?>')">
                                                    <?=$val->calc->name?>
                                                </li>
                                                <?

                                                $active = false;
                                            endforeach;
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </table>


                        </div>

                        <div class='calcInfo'>
                            <div id="calcInfo"></div>
                        </div>
                    </div>

                    <div class='calcContent' >
                        <?if($arParams['status'] != MODULE_DEMO_EXPIRED):?>
                                <? if($countActive):?>
                                <div id='calcForms' class='calcForms'>
                                    <div id="form1" style="height: 198px;">
                                        <table class='sliderContainer' width="100%" style='border-bottom: 1px solid #ebebeb' cellspacing="1px">
                                            <tr>
                                                <td valign='bottom' style="font-size: 14px;">
                                                        <strong><?=GetMessage("iwant")?>:</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align='left' valign='top'>
                                                    <table width="400px" >
                                                        <tr>
                                                            <td id="summMarkMin" align="left" style="font-size: 10px;">
                                                                1000
                                                            </td>
                                                            <td id="summMarkMax" align="right" style="font-size: 10px;">
                                                                5000
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class='scale'></td>
                                                        </tr>
                                                    </table>
                                                    <div id="summ_slider" class="slider"></div>
                                                </td>
                                                <td align='center' valign='top'>
                                                    <div id="summ_val"><strong><?=GetMessage("summ")?></strong></div>
                                                    <input type="text" size="10" value="3000" id="summ" name="summ"/>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class='sliderContainer'  width="100%" style='border-top: 1px solid #fcfcfc'>
                                            <tr>
                                                <td valign="bottom" style="font-size: 14px;">
                                                    <strong><?=GetMessage("onTerm")?>:</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align='left' valign='top'>
                                                    <table width="400px">
                                                        <tr>
                                                            <td id="termMarkMin" align="left" style="font-size: 10px;">
                                                                1000
                                                            </td>
                                                            <td id="termMarkMax" align="right" style="font-size: 10px;">
                                                                5000
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class='scale'></td>
                                                        </tr>
                                                    </table>
                                                    <div id="term_slider" class="slider"></div>
                                                </td>
                                                <td align='center' valign='top' style="padding-left: 50px;">
                                                    <div><strong><?=GetMessage("term")?></strong></div>
                                                    <input type="text" size="10" value="3000" id="term" name="term"/>
                                                </td>
                                            </tr>
                                        </table>


                                    </div>
                                    <?if($arParams['CAPTHA'] == "Y") :?>
                                        <div id="formCaptha" style="display:none; width:529px; height: 198px">
                                            <p><?=GetMessage("checkBot")?></p>
                                            <img id='calcCapthaImg' src=""><br/>
                                            <?=GetMessage("enterCaptha")?><br/>
                                            <input  name="calcCapthaCode" id="calcCapthaCode" type="hidden"/>
                                            <input type="text" validator="captha" name="calcCapthaWord" id="calcCapthaResult"/>
                                        </div>
                                    <?endif?>
                                </div>

                            <? else: ?>
                                <div align="center" id="noCalcs">
                                    <?=GetMessage("NoProducts"); ?>
                                </div>
                            <? endif ?>
                            <? if($arParams['status'] == MODULE_DEMO): ?>
                                <p align="right" style="font-size: 10px; color: red; margin: 0px; padding: 0px;"><?=GetMessage("demoType")?></p>
                            <? endif ?>
                        <?else:?>
                            <div align="center" id="noCalcs">
                                <?=GetMessage("testPeriodExpired")?>
                            </div>
                        <?endif?>

                    </div>

                    <div id='footerCalc'>

                    </div>

                </div>
            </td>
            <? if($countActive):?>
            <td valign='bottom' align='center' width="250px" nowrap class="calcPanel" nowrap>
                <div style="height: 200px">
                    <img id='calcMoneyBag' style="position:relative; top:100px" src='/bitrix/components/ceolab/calc.mfo/templates/.default/img/moneyBag.png' width="171px" height="190px"/>
                </div>
                <div>
                    <div style='font-size: 16px; font-weight: 600;'>
                        <?=GetMessage("on")?> <span id='calcTerm'></span>&nbsp;<span id="calcPeriod"></span>&nbsp;<?=GetMessage("iget")?>
                    </div>
                    <div style='font-size: 40px; font-weight: 300; white-space: nowrap' nowrap>
                        <span id='calcSumm'></span> <span name="calcCurrency"></span>
                    </div>
                    <table style='color: #969595; font-size: 11px;' width="100%">
                        <tr>
                            <td nowrap>
                                <?=GetMessage("percent")?>:
                            </td>
                            <td align="right" nowrap>
                                <span id='calcPercent'></span> <span name="calcCurrency"></span>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap >
                                <?=GetMessage("totalSum")?>:
                            </td>
                            <td align="right" nowrap>
                                <span id='calcTotal'></span> <span name="calcCurrency"></span>
                            </td>
                        </tr>
                    </table>
                    <p><span class='submitButton' name="nextFormButton"><span id="calcSubmitBtn"><?=GetMessage("submit")?></span></span></p>
                </div>
            </td>
            <? endif ?>
        </tr>
    </table>

</form>


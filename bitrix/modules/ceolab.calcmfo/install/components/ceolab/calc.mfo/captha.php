<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



if($_POST["word"])
{
    if(!$APPLICATION->CaptchaCheckCode($_POST["word"], $_POST["code"]))
    {
          $result['status'] = "fail";
          $result['code'] = getCapthaCode();

    }
    else
    {
            $result['status'] = "ok";
            $_SESSION['capthaSuccess'] = true;
    }
}
else
{
    $result['code'] = getCapthaCode();
}


function getCapthaCode()
{
    $cpt = new CCaptcha();
    $captchaPass = randString(10);
    if(strlen($captchaPass) <= 0)
    {
        COption::SetOptionString("main", "captcha_password", $captchaPass);
    }
    $cpt->SetCodeCrypt($captchaPass);

    return htmlspecialchars($cpt->GetCodeCrypt());
}






echo json_encode($result);


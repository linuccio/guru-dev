<?php
$MESS['calcName'] = "Наименование";
$MESS['minSum'] = "Минимальная сумма";
$MESS['maxSum'] = "Максимальная сумма";
$MESS['step'] = "Шаг";
$MESS['maxTerm'] = "Максимальный срок";
$MESS['percent'] = "Проценты";
$MESS['currency'] = "Валюта";
$MESS['in'] = "в";
$MESS['additionInfo'] = "Доп. информация";
$MESS['day'] = "день";
$MESS['week'] = "неделю";
$MESS['validators'] = "Валидаторы";
$MESS['nameValidator'] = "Имя";
$MESS['valValidator'] = "Выражение";
$MESS['editValidator'] = "Редактировать";
$MESS['deleteValidator'] = "Удалить";
$MESS['addValidator'] = "Добавить";
$MESS['settingsForm'] = "Настройка анкеты";
$MESS['addForm'] = "Добавить форму";
$MESS['addField'] = "Добавить поле";
$MESS['addText'] = "Добавить текст";
$MESS['text'] = "Текст";
$MESS['confirmDelete'] = "Удалить?";
$MESS['addSelect'] = "Добавить select";
$MESS['addCheckbox'] = "Добавить checkbox";
$MESS['settingsSend'] = "Настройка отправки";
$MESS['emailRecipient'] = "Email (через запятую)";
$MESS['emailSubject'] = "Тема письма";
$MESS['emailSender'] = "Email отправителя";
$MESS['template'] = "Шаблон";
$MESS['save'] = "Сохранить";
$MESS['add'] = "Добавить";

$MESS['nameTag'] = "Имя переменной";
$MESS['width'] = "Ширина";
$MESS['height'] = "Высота";
$MESS['validator'] = "Валидатор";
$MESS['values'] = "Значения";

$MESS['position'] = "Позиция";
$MESS['actions'] = "Действия";
$MESS['delete'] = "Удалить";

$MESS['name'] = "Имя";
$MESS['value'] = "Значение";

$MESS['properties'] = "Свойства";
$MESS['propPosition'] = "Позиция";
$MESS['propFixed'] = "Закрепить";
$MESS['propUnfixed'] = "Открепить";

$MESS['additionSettings'] = "Дополнительные настройки";
$MESS['captcha'] = "Защита от ботов (капча)";
$MESS['errorDuplicateTagName'] = "Такое имя переменной уже присвоено другому элементу";

$MESS['wrongSymbols'] = "Недопустимое значение для имени переменной, допускаются только латинские символы и цифры";

$MESS['additionTags'] = "Дополнительные переменные";
$MESS['summTag'] = "Сумма займа";
$MESS['termTag'] = "Срок займа";
$MESS['percentTag'] = "Начисленные проценты";
$MESS['totalTag'] = "Итого к выплате";

$MESS['validatorNotEmpty'] = "Поле не пустое";
$MESS['validatorOnlyNumbers'] = "Только цифры";
$MESS['validatorEmail'] = "Email";
$MESS['formSize'] = "Размер";

$MESS['essential'] = "Обязательный";
$MESS['incorrectValues'] = "Неверные значения";

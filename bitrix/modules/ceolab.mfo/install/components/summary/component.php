<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 21.03.13
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$status = CModule::IncludeModuleEx('ceolab.mfo');

if($status != MODULE_NOT_FOUND)
{
    $arResult['status'] = $status;
    if($status != MODULE_DEMO_EXPIRED)
    {
        $fileTypes = explode(",", $arParams['FILE_TYPES']);

        $status = false;
        if($_POST['SUBMIT_FILE'])
        {
            if($_FILES['FILE']['size'] <= ($arParams['MAX_FILE_SIZE']*1024)*1024)
            {
                $ex = explode(".", $_FILES['FILE']['name']);
                $type = $ex[count($ex)-1];

                $checkType = false;
                foreach($fileTypes as $t)
                {
                    $t = trim($t);
                    if($type == $t)
                    {
                        $checkType = true;
                        break;
                    }
                }

                if(!$checkType)
                {
                    $arResult["errorUpload"] = GetMessage("wrong_type");
                }
            }
            else
            {
                $arResult["errorUpload"] = GetMessage("wrong_size");
            }

            if(!isset($arResult["errorUpload"]))
                $status = true;
        }


        if($_POST['SUBMIT'])
        {
            $fields = $arParams['FIELDS'];
            $arResult['errorFields'] = array();
            foreach($fields as $key)
            {
                if(!isset($_POST[$key]) || !trim($_POST[$key]))
                    $arResult['errorFields'][] = $key;
            }


            if(!$arResult['errorFields'])
            {

                $template = file_get_contents("../bitrix/components/ceolab/summary/emailTemplates/template.php");

                foreach($_POST as $key => $val)
                {
                    $template = str_replace("%".$key."%", htmlspecialchars($val), $template);
                }

                $status = true;
            }
            else
            {
                $arResult['initFields'] = $_POST;
            }

        }

        if($status)
        {
            $bound = "mail-1123";
            $header = "From: ".$arParams['FROM']."\n";
            $header .= "Subject: ".mb_encode_mimeheader($arParams['SUBJECT'])."\n";
            $header .= "Mime-Version: 1.0\n";


            if(isset($_FILES['FILE']))
            {
                $header .= "Content-Type: multipart/mixed; boundary=\"".$bound."\"\n";
                $body = "--".$bound."\n";
                $body .= "Content-Type: application/octet-stream; name=\"file.".$type."\"\n";
                $body .= "Content-Transfer-Encoding:base64\n";
                $body .= "Content-Disposition:attachment\n\n";

                $body .= base64_encode(file_get_contents($_FILES['FILE']['tmp_name']));
                $body .= "--".$bound."\n";
            }
            elseif(isset($template))
            {
                $header .= "Content-Type: text/html; charset = utf-8\n";
                $body = $template;
            }

            if(mail($arParams['EMAILS'], "", $body, $header))
                $arResult['STATUS'] = "OK";
            else
                $arResult['STATUS'] = "FAIL";
        }


        $APPLICATION->AddHeadString("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js\"></script>");
        $this->includeComponentTemplate();
    }
    else
    {
        echo "<font style='color: red'>".GetMessage('demo_expired')."</font>";
    }
}
?>
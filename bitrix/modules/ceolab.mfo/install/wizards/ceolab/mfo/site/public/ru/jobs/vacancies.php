<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вакансии");
?>
<div >
    <?$APPLICATION->IncludeComponent(
        "bitrix:menu",
        "mfo_tabs",
        Array(
            "ROOT_MENU_TYPE" => "page",
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "page",
            "USE_EXT" => "Y",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => Array()
        )
    );?>
    <div style="margin-top: 50px;">
        <?$APPLICATION->IncludeComponent("ceolab:vacancies", ".default", array(
	"IBLOCK_TYPE" => "vacanies",
	"IBLOCK_ID" => "#vacancies#",
	"DEFAULT_CITY" => "430",
	"PAGE_ID" => "id",
	"COUNT_ELEMENTS" => "20",
	"SET_SESSION" => "",
	"GET_SESSION" => "REGION_ID",
	"SET_GET" => "city",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600"
	),
	false
);?>
    </div>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if(!CModule::IncludeModule("iblock"))
    return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));
$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));


$enum = CIBlockProperty::GetPropertyEnum("CITY", Array(), Array("IBLOCK_ID"=>$arCurrentValues['IBLOCK_ID']));
while($c = $enum->GetNext())
{
    $arCity[$c['ID']] = $c['VALUE'];
}

while($arRes = $db_iblock->Fetch())
    $arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arComponentParameters = array(
    "GROUPS" => array(
    ),
	"PARAMETERS" => array(
        "IBLOCK_TYPE" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_IBLOCK_DESC_LIST_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arTypesEx,
            "DEFAULT" => "news",
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_IBLOCK_DESC_LIST_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "REFRESH" => "Y",
        ),
        "DEFAULT_CITY" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("default_city"),
            "TYPE" => "LIST",
            "VALUES" => $arCity
        ),

        "PAGE_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("id_vacancy"),
            "TYPE" => "STRING",
            "DEFAULT" => "vacancy_id"
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>3600)

 )
);

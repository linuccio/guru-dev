<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?> 
<p style="font-size: 42px; margin-bottom: -40px;">
    О компании
</p>
<div class="how_to_get">
    <h1><span style="color: rgb(44, 136, 184);">-</span>&nbsp;Наша миссия</h1>

    <p>Наша Компания относится к сегменту «займы до зарплаты». Такой вид услуги предназначен для покрытия текущих кассовых разрывов (условно говоря, когда зарплата уже потрачена, а аванс еще не начислен). Сегодня в офисах «#company_name#» можно получить от #min_summ# до #max_summ# рублей на срок от #min_term# до #max_term# дней.</p>


</div>

<div class="how_to_get">
    <h1><span style="color: rgb(44, 136, 184);">-</span>&nbsp;Наша цель</h1>

    <p>Обеспечить любого гражданина Российской Федерации, в возрасте от #from_age# до #to_age# лет и имеющего постоянный доход, возможностью получения оперативной финансовой помощи (микрозайма) в компании «#company_name#» на всей территории страны</p>


</div>


<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
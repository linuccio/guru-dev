<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 21.03.13
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$status = CModule::IncludeModuleEx('ceolab.mfo');

if($status != MODULE_NOT_FOUND)
{
    $arResult['status'] = $status;
    if($status != MODULE_DEMO_EXPIRED)
    {

        $arResult['DETAIL'] = false;
        if(isset($_GET[$arParams['PAGE_ID']]))
        {
            $arResult['DETAIL'] = true;
        }
        else
        {
            $regionProp = CIBlock::GetProperties($arParams['IBLOCK_ID'], array(), array("CODE" => "CITY"));
            $prop = $regionProp->Fetch();
            $arParams['IBLOCK_REGION_ID'] = $prop['LINK_IBLOCK_ID'];
        }

        $this->includeComponentTemplate();
    }
    else
    {
        echo "<font style='color: red'>".GetMessage('demo_expired')."</font>";
    }
}
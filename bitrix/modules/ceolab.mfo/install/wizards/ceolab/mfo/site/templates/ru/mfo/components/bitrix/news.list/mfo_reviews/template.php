<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<div class="reviewWrapper">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="reviewItem" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <p>
                <?=$arItem["NAME"]?>
                                <span>
                                    <?=$arItem["DISPLAY_ACTIVE_FROM"]?>
                                </span>
            </p>
            <div>
                <?=$arItem["PREVIEW_TEXT"];?>.
            </div>
            <div name="border"></div>
        </div>
    <?endforeach;?>
    <?if(!$arResult["ITEMS"]):?>
        <p align="center">
            <?=GetMessage("no_reviews")?>
        </p>
    <?endif?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

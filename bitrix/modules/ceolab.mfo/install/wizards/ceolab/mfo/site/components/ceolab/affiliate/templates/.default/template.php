
<table width="100%" class="mfo_affiliation" cellspacing="0px">
	<th>
		&nbsp;
	</th>
	<th>
		<?=GetMessage('name')?>
	</th>
	<th>
		<?=GetMessage('place_live')?>
	</th>
	<th>
		<?=GetMessage('affiliate')?>
	</th>
	<th>
		<?=GetMessage('date')?>
	</th>
	<th>
		<?=GetMessage('share')?>
	</th>
	<?foreach($arResult['ITEMS'] as $item):?>

		<tr>
			<td>
				<?=++$i?>
			</td>
			<td>
				<?
				$this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
				<span id="<?=$this->GetEditAreaId($item['ID']);?>"><?=$item['NAME']?></span>
			</td>
			<td>
				<?=$item['PROPERTY_ADDRESS']?>
			</td>
			<td>
				<?=$item['PROPERTY_AFFILIATION']?>
			</td>
			<td>
				<?=$item['PROPERTY_DATE']?>
			</td>
			<td>
				<?=$item['PROPERTY_SHARE']?>
			</td>
		</tr>
	<?endforeach?>
</table>
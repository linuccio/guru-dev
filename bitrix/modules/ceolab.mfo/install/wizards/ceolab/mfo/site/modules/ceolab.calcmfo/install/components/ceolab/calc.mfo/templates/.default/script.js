
function Calculator()
{
    this.percent = 10;
    this.type = "dayly";
    this.calcId = "calcForm";
    this.formId = "form";
    this.errorClass = "errorClass";
    this.infoId = "calcInfo";
    this.info = "";
    this.resultId = "summResult";
    this.commisionId = "percent";
    this.calcFormsId = "calcForms";
    this.calcFormPrefixId = "form";
    this.totalId = "total";
    this.classForm = false;


    this.validators = {};

    this.funcCalc = function(){};
    this.initFunc = function(){};


    this.forms = {};

    this.summ = {
        min: 1000,
        max: 30000,
        InputId: "inputId",
        SliderId: "id",
        step: 1000
    };

    this.term = {
        min: 1,
        max: 30,
        InputId: "inputId",
        SliderId: "id",
        step: 1
    };

    this.init = function()
    {
        this.currentForm = 1
        that = this;

        this.initSlider(this.summ);
        this.initSlider(this.term);

        $("#"+this.infoId).empty();
        $("#"+this.infoId).text(this.info);



        $("[name = nextFormButton]").unbind();
        $("[name = nextFormButton]").click(function(event)
        {
            that.nextForm();
        })
        $("[name = prevFormButton]").unbind();
        $("[name = prevFormButton]").click(function(event)
        {
            that.prevForm();
        })
        $("[name = submitFormButton]").unbind();
        $("[name = submitFormButton]").click(function(event)
        {
            that.sendForm();
        })

        this.initForms();
        this.showForm(that.currentForm);

        this.initFunc(that);

    }

    this.initForms = function()
    {
        that = this;
        $("#"+this.calcFormsId+" > *").each(
            function(i, elem)
            {
                if($(elem).attr("id") == that.calcFormPrefixId+"1" || $(elem).attr("id") == that.calcFormPrefixId+"Captha")
                    return true;

                $(elem).remove();
            }
        );

        for(var i in this.forms)
        {
            var form = this.forms[i];

            var elementsStr = "";
            for(f in form.fields)
            {
                var element = form.fields[f];
                elementsStr += Elements[element.type](element.data);
            }

            i = parseInt(i);
            var id = i+2;

            var classForm = "";
            if(this.classForm)
                classForm = "class='"+this.classForm+"'";

            $("#calcForms").append(
                "<div  id='"+this.calcFormPrefixId+id+"' "+classForm+" style='display:none; width:"+form.data.width+"px; height: "+form.data.height+"px;'>"+elementsStr+"</div>"
            )
        }
    }

    this.initSlider = function(data)
    {
        $(function() {
            $("#"+data.SliderId).slider({
                range: "min",
                min: data.min,
                max: data.max,
                step: data.step,
                slide:function(event, ui)
                {
                    $("#"+data.InputId).val(ui.value);

                    that.calculate();
                },
                change: function(event, ui)
                {
                    that.calculate();
                }

            })
            that.checkVal(data.InputId, data.SliderId);
        });
        $("#"+data.InputId).unbind();
        $("#"+data.InputId).change(function(event){
            that.checkVal(data.InputId, data.SliderId);

        });
    }

    this.checkVal = function(inputId, sliderId)
    {
        var val = parseInt($("#"+inputId).val());
        if(!val)
            return;
        if($("#"+sliderId).slider("option", "max") < val)
        {
            val = $("#"+sliderId).slider("option", "max");
        }

        if($("#"+sliderId).slider("option", "min") > val)
        {
            val = $("#"+sliderId).slider("option", "min");
        }
        $("#"+inputId).val(val);
        $("#"+sliderId).slider("value", val);
    }

    this.calculate = function ()
    {
        this.summVal = parseInt($("#"+this.summ.InputId).val());
        this.termVal = parseInt($("#"+this.term.InputId).val());
        if(!this.summVal || !this.termVal)
            return;

        this.summPercent = (this.summVal*this.termVal/100)*this.percent;
        this.total = this.summVal+this.summPercent;

        $("#"+this.resultId).text(this.summVal);
        $("#"+this.commisionId).text(parseInt(this.summPercent));
        $("#"+this.totalId).text(parseInt(this.total));

        that = this;
        this.funcCalc(that);
    }

    this.nextForm = function ()
    {
        if(this.validateForm())
        {
            if(!this.showForm(++that.currentForm))
                this.sendForm();
        }

    }

    this.prevForm = function ()
    {
        this.showForm(--that.currentForm);
    }

    this.showForm = function(id, speed)
    {
        if(!speed)
            speed = 300;



        if(!$("#"+this.calcFormPrefixId+id).length)
        {
            var capthaForm = $("#calcForms > #"+this.calcFormPrefixId+"Captha");
            if(!capthaForm.length || capthaForm.is(":visible"))
                return false;

            this.currentForm = "Captha";
            id="Captha";
        }



        $("#"+this.calcFormsId+" > *").slideUp(speed);

        var oldWidth = $("#calcForms > *:visible").width();
        $("#"+this.calcFormPrefixId+id).slideDown(speed);

        var width = $("#"+this.calcFormPrefixId+id).width();

        if(oldWidth && oldWidth != width)
        {
            $("#"+this.calcFormPrefixId+id).width(oldWidth);

             $("#"+this.calcFormPrefixId+id).animate(
                 {
                    width: width
                 },
                 {
                     queue:true,
                     duration:speed
                 }
             );

        }



        this.funcShowForm(this);

        return true;
    }


    this.validateForm = function()
    {

        var result = true;
        that = this;
        $("#form"+ that.currentForm + " [validator]").each(function (i, elem)
        {
            var validator = $(elem).attr("validator");
            if(validator == undefined)
                return true;

            if(validator == "captha")
            {
                captha.checkCaptha(
                    function(){that.sendForm(true)},
                    function(){
                        $(elem).attr("class", that.errorClass);
                        $(elem).val("")
                    });
                result = false;

            }
            else if(validator == "essential")
            {
                if(!$(elem).attr("checked"))
                {
                    $(elem).attr("class", that.errorClass);
                    result = false;
                }
            }
            else if(Validators[validator] != undefined)
            {
                try
                {
                    var re = new RegExp(Validators[validator].value);
                    if(!re.exec($(elem).val()))
                    {
                        $(elem).attr("class", that.errorClass);
                        result = false;
                    }
                    else
                        $(elem).removeAttr("class");
                }catch(e)
                {

                }

            }

        });
        return result;
    }

    this.showMessage = function(text)
    {
        $("#"+this.messageId).text(text);
        $("#"+this.messageId).fadeIn(1000);
        setTimeout("$(\"#"+this.messageId+"\").fadeOut(1000)", 2000);
    }

    this.sendForm = function(checked)
    {
        $("#"+this.formId).append("<input type='hidden' name='productName' value='"+this.name+"'/>");
        $("#"+this.formId).append("<input type='hidden' name='total' id='totalField' value='"+this.total+"'/>");
        $("#"+this.formId).append("<input type='hidden' name='percent' id='percentField' value='"+this.summPercent+"'/>");
        $("#"+this.formId).append("<input type='hidden' name='calcFormSend' value=''/>");
        $("#"+this.formId).append("<input type='hidden' name='calcId' value='"+this.id+"'/>");

        if(checked || this.validateForm())
            $("#"+this.formId).submit();
    }
}

function captha()
{
    this.imgId = "calcCapthaImg";
    this.codeFieldId = "calcCapthaCode";
    this.wordFieldId = "calcCapthaResult";
    this.currentCode = "";

    this.getCaptha = function()
    {
        thatCaptha = this;
        $.post(
            "/bitrix/components/ceolab/calc.mfo/captha.php",
            {
                action: "get_code"
            },
            function(data)
            {
                thatCaptha.currentCode = data.code;
                $("#calcCapthaImg").attr("src", "/bitrix/tools/captcha.php?captcha_code="+data.code);
            },
            "json"
        );
    }

    this.checkCaptha = function(funcSucces, funcError)
    {
        if(this.status == "inprogress")
            return;
        this.status = "inprogress";
        thatCaptha = this;
        $.post(
            "/bitrix/components/ceolab/calc.mfo/captha.php",
            {
                word: $("#"+thatCaptha.wordFieldId).val(),
                code: thatCaptha.currentCode
            },
            function(data)
            {
                if(!data.status || data.status != "ok")
                {
                    thatCaptha.currentCode = data.code;
                    $("#"+thatCaptha.imgId).attr("src", "/bitrix/tools/captcha.php?captcha_code="+data.code);
                    funcError();
                }
                else
                {
                    $("#"+thatCaptha.codeFieldId).val(thatCaptha.currentCode);
                    funcSucces();
                }
                thatCaptha.status = "done"
            },
            "json"
        );
    }
}


Elements = {
    "input" : function(data)
    {
        return "<input validator='"+data.validator+"' name='"+data.name+"' type='text' style='top:"+data.positionY+"px; left:"+data.positionX+"px; position: absolute; width:"+data.width+"px; height:"+data.height+"px;' />";
    },
    "span" : function(data)
    {
        return "<span style='top:"+data.positionY+"px; left:"+data.positionX+"px; position: absolute;'>"+data.text+"</span>";
    },
    "checkbox" : function(data)
    {
        var validator = "";
        if(data.essential)
            validator = "validator='essential'";

        return "<input type='checkbox' value='Y' name='"+data.name+"' style='top:"+data.positionY+"px; left:"+data.positionX+"px; position: absolute;' "+validator+" />";
    },
    "select" : function(data)
    {
        var options = "";
        for(var i in data.values)
        {
            var value = data.values[i].value;
            if(data.values[i].value == "default")
                value = data.values[i].name;

            options += "<option value='"+value+"'>"+data.values[i].name+"</option>";
        }
        return "<select name='"+data.name+"' style='top:"+data.positionY+"px; left:"+data.positionX+"px; position: absolute; width:"+data.width+"px; height:"+data.height+"px'>"+options+"</select>";
    }
}


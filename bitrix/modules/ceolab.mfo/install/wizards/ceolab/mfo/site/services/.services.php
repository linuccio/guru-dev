<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 03.06.13
 * Time: 23:38
 * To change this template use File | Settings | File Templates.
 */
$arServices = array(
    "main" => array(
        "NAME" => GetMessage("main_name"),
        "STAGES" => array(
            "create_site.php",
            "copy_files.php",
            "copy_templates.php",
            "copy_components.php",
            "copy_modules.php"
        )
    ),
    "iblock" => array(
        "NAME" => GetMessage("iblock_name"),
        "STAGES" => array(
            "types.php",
            "import.php"
        )
    )
);
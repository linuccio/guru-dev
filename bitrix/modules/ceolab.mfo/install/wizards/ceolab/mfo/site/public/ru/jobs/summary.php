<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отправка резюме");
?>
<div>
    <?$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "mfo_tabs",
    Array(
        "ROOT_MENU_TYPE" => "page",
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "page",
        "USE_EXT" => "Y",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => Array()
    )
);?>
    <div style="margin-top: 50px;">
        <?$APPLICATION->IncludeComponent("ceolab:summary", ".default", array(
	"FROM" => "test@colab.ru",
	"SUBJECT" => "Резюме",
	"EMAILS" => "prohitman777@yandex.ru",
	"FIELDS" => array(
		0 => "FIRSTNAME",
	),
	"MAX_FILE_SIZE" => "5",
	"FILE_TYPES" => "doc, txt"
	),
	false
);?>
    </div>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
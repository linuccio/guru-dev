<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как погасить займ?");
?>

 <?$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "mfo_tabs",
    Array(
        "ROOT_MENU_TYPE" => "page",
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "page",
        "USE_EXT" => "Y",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => Array()
    )
);?>
<div class="how_to_get">
    <h1><span style="color: rgb(44, 136, 184);">-</span>&nbsp;Как погасить займ? </h1>

    <table width="100%">
      <tbody>
        <tr> <td width="400" valign="top" height="">
            <p style="color: rgb(44, 136, 184); font-size: 20px;"> В любом офисе компании &laquo;#company_name#&raquo; </p>

            <div style="line-height: 25px;"> Если сумма к оплате превышает 15 000 рублей, при себе необходимо иметь паспорт. Если подходит срок оплаты, а у Вас нет возможности оплатить самостоятельно, Вы можете попросить друга или родственника, достигшего совершеннолетия, оплатить за Вас: ему необходимо иметь при себе паспорт и знать номер Вашего договора микрозайма. </div>
           </td> <td valign="top" rowspan="2" style="padding-left: 30px;">
            <p style="color: rgb(44, 136, 184); font-size: 20px;"> В любом отделении любого банка </p>

            <div style="line-height: 25px;"> Безналичным переводом на расчетный счет нашей компании. Необходим паспорт, реквизиты компании и информация о договоре. </div>

            <br />

            <div style="color: silver; font-size: 12px;"> ООО &laquo;#owner_company#&raquo;
              <br />
             #address#
              <br />
             ИНН #inn#
              <br />
             КПП #kpp#
              <br />
             ОГРН #ogrn#
              <br />
             ОКПО #okpo#
              <br />
             ОКАТО #okato#
              <br />
             р/с #bank_account#
              <br />
             в филиале № #branch_number#
              <br />
             БИК #bik#
              <br />
             к/с #cash_account#
              <br />
             </div>
           </td> </tr>

        <tr> <td style="padding-top: 60px;">
            <p style="color: rgb(44, 136, 184); font-size: 20px;"> Через QIWI терминал </p>

            <div style="line-height: 25px;"> Подробнее об оплате через терминалы QIWI можно прочитать здесь. </div>
           </td> </tr>
       </tbody>
     </table>
</div>
 <?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
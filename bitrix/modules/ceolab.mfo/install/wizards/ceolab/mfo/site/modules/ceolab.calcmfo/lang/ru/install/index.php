<?php
$MESS['module_name'] = "Калькулятор МФО";
$MESS['module_description'] = "Калькулятор для рассчета и оформления микрозаймов";
$MESS['module_install'] = "Установка модуля Калькулятор МФО";
$MESS['module_uninstall'] = 'Деинсталляция модуля Калькулятор МФО';
$MESS['partner_name'] = "Ceolab - Лаборатория брендовых решений";
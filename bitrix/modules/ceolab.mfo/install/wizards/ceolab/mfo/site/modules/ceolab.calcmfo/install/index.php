<?
IncludeModuleLangFile(__FILE__);
Class ceolab_calcmfo extends CModule
{
    var $MODULE_ID = "ceolab.calcmfo";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $PARTNER_NAME;
    var $PARTNER_URI;

    function ceolab_calcmfo()
    {
        $arModuleVersion = array();
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");
		$this->PARTNER_URI = "http://ceo-lab.ru";
        $this->PARTNER_NAME = GetMessage('partner_name');

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = GetMessage('module_name');
        $this->MODULE_DESCRIPTION = GetMessage('module_description');
    }

    function DoInstall()
    {
		global $DOCUMENT_ROOT, $APPLICATION;
		if (!IsModuleInstalled($this->MODULE_ID))
		{
			$this->InstallDB();
			CopyDirFiles($DOCUMENT_ROOT."/bitrix/modules/ceolab.calcmfo/install/components", $DOCUMENT_ROOT."/bitrix/components", true, true);
			$APPLICATION->IncludeAdminFile(GetMessage("module_install"), $DOCUMENT_ROOT."/bitrix/modules/ceolab.calcmfo/install/step.php");
		}
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
		$this->UnInstallDB();
        DeleteDirFilesEx("/bitrix/components/ceolab/calc.mfo");
        $APPLICATION->IncludeAdminFile(GetMessage("module_uninstall"), $DOCUMENT_ROOT."/bitrix/modules/ceolab.calcmfo/install/unstep.php");
    }
	
	function InstallDB()
	{
		RegisterModule($this->MODULE_ID);
		return true;
	}
	
	function UnInstallDB()
	{
		UnRegisterModule($this->MODULE_ID);
		return true;
	}
}
?>
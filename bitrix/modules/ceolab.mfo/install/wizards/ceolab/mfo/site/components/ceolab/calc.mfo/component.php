<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 21.03.13
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if (!defined("CALC_INIT") || CALC_INIT!==true)
{
    define("CALC_INIT", true);
    require_once "include/functions.php";

    $status = CModule::IncludeModuleEx('ceolab.calcmfo');
    if($status && $status !== MODULE_NOT_FOUND)
    {
        $arParams['status'] = $status;
        if($status != MODULE_DEMO_EXPIRED)
        {

            $data = paramsDecode($arParams['PRODUCTS']);

            $arParams['data'] = str_replace("'","\'",str_replace('\\"','\\\"', str_replace("\\n", "", htmlspecialchars_decode($arParams['PRODUCTS']))));

            $arParams['PRODUCTS'] = array();

            if($data->calcs)
                foreach($data->calcs as $val)
                {
                    if($val->calc->active)
                    {
                        $val->calc->name = $val->calc->name;
                        $arParams['PRODUCTS'][] = $val;
                    }
                }

            if(isset($_POST['calcFormSend']))
            {
                if(
                    (!$arParams['CAPTHA'] || $arParams['CAPTHA'] == "N") ||
                    ($arParams['CAPTHA'] == "Y" && $_SESSION['capthaSuccess'])
                )
                {
                    unset($_SESSION['capthaSuccess']);

                    $data = $data->calcs[$_POST['calcId']];

                    $sendData = $data->sendData;
                    $sendData->templateText = mb_convert_encoding($sendData->templateText, "UTF-8");
                    foreach($_POST as $key => $val)
                    {
                        $sendData->templateText = str_replace("%".$key."%", mb_convert_encoding($val, "UTF-8"), $sendData->templateText);
                    }

                    $sendData->subject = mb_convert_encoding($sendData->subject, "UTF-8");
                    $headers = sprintf("Content-type:text/html; charset = utf-8\r\nFrom:%s", $sendData->from);
                    if(mail($sendData->emails, $sendData->subject, $sendData->templateText, $headers))
                        $arParams['result'] = "ok";
                    else
                        $arParams['result'] = "fail";
                }

            }

            $APPLICATION->AddHeadString("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js\"></script>");
            $APPLICATION->AddHeadString("<script src=\"http://code.jquery.com/ui/1.10.2/jquery-ui.js\"></script>");
            $APPLICATION->AddHeadString('<script type="text/javascript" src="/bitrix/components/ceolab/calc.mfo/js/json/json2.js"></script>');
        }

        $this->includeComponentTemplate();

    }
}


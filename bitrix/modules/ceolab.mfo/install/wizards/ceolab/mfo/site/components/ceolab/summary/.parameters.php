<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();


$arComponentParameters = array(
    "GROUPS" => array(
    ),
	"PARAMETERS" => array(
        "FROM" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("emailSender"),
            "TYPE" => "STRING"
        ),
        "SUBJECT" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("submit"),
            "TYPE" => "STRING",
            "DEFAULT" => GetMessage("default_subject")
        ),
        "EMAILS" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("emails"),
            "TYPE" => "STRING",
        ),
        "FIELDS" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("essential_fields"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => Array(
                "FIRSTNAME" => GetMessage("firstname"),
                "LASTNAME" => GetMessage("lastname"),
                "MIDDLENAME" => GetMessage("middlename"),
                "BIRTHDAY" => GetMessage("birthday"),
                "CITY" => GetMessage("city"),
                "PHONE" => GetMessage("phone"),
                "EMAIL" => GetMessage("email_field"),
                "FAMILY" => GetMessage("family"),
                "CHILDS" => GetMessage("childs"),
                "POSITION" => GetMessage("position"),
                "SALARY" => GetMessage("salary"),
                "SHEDULE" => GetMessage("shedule"),
                "EDUCATION" => GetMessage("education"),
                "INSTITUT" => GetMessage("institut"),
                "ENDYEAR" => GetMessage("endyear"),
                "SPECIALITY" => GetMessage("speciality"),
                "RECOMENDATIONS" => GetMessage("recomendations")
            )
        ),
        "MAX_FILE_SIZE" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("max_size_file"),
            "TYPE" => "STRING",
            "DEFAULT" => "5"
        ),
        "FILE_TYPES" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("file_types"),
            "TYPE" => "STRING",
            "DEFAULT" => "doc, txt"
        )
 )
);

<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 04.06.13
 * Time: 2:01
 * To change this template use File | Settings | File Templates.
 */

CopyDirFiles(
    WIZARD_ABSOLUTE_PATH."/site/modules/ceolab.calcmfo",
    $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/ceolab.calcmfo",
    true,
    true,
    false
);

include_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/ceolab.calcmfo/install/index.php";
$module = new ceolab_calcmfo();
if (!IsModuleInstalled($module->MODULE_ID))
    $module->InstallDB();


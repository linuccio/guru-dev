<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 04.06.13
 * Time: 1:24
 * To change this template use File | Settings | File Templates.
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if (!defined("WIZARD_SITE_ID"))
    return;

if (!defined("WIZARD_SITE_DIR"))
    return;


unlink(WIZARD_SITE_ROOT_PATH."/.left.menu_ext.php");

CopyDirFiles(
    WIZARD_ABSOLUTE_PATH."/site/public/".LANGUAGE_ID,
    WIZARD_SITE_PATH,
    true,
    true,
    false
);
$maxSumm = COption::GetOptionString("main", "toSumm", "10000");
$cParts = strlen($maxSumm)/3;
for($i = 1; $i <= $cParts; $i++)
{
    $start = strlen($maxSumm) - ($i*3)-1;

    $p[] = substr($maxSumm, $start, 3);
}
$p[] = substr($maxSumm, 0, $start+1);

$maxSumm = implode("&nbsp;", array_reverse($p));

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."icons.php",
    array(
        "max_summ" => $maxSumm,
        "max_term" => COption::GetOptionString("main", "toTerm", "16"),
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."contacts.php",
    array(
        "phone" => COption::GetOptionString("main", "phone", "8 800 700-43-44 "),
		"site_dir" => WIZARD_SITE_DIR
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."about.php",
    array(
        "min_summ" => COption::GetOptionString("main", "fromSumm", "1"),
        "max_summ" => COption::GetOptionString("main", "toSumm", "10 000"),
        "min_term" => COption::GetOptionString("main", "fromTerm", "1"),
        "max_term" => COption::GetOptionString("main", "toTerm", "16"),
        "from_age" => COption::GetOptionString("main", "fromAge", "16"),
        "to_age" => COption::GetOptionString("main", "toAge", "16"),
        "company_name" =>  COption::GetOptionString("main", "companyName", GetMessage("company_name")),
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."loan/howtoget.php",
    array(
        "from_age" => COption::GetOptionString("main", "fromAge", "21"),
        "to_age" => COption::GetOptionString("main", "toAge", "65"),
        "company_name" =>  COption::GetOptionString("main", "companyName", GetMessage("company_name")),
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."loan/howtopay.php",
    array(
        "company_name" =>  COption::GetOptionString("main", "companyName", GetMessage("company_name")),
        "owner_company" => COption::GetOptionString("main", "ownerCompany", GetMessage("owner_company")),
        "address" => COption::GetOptionString("main", "address", GetMessage("address")),
        "inn" => COption::GetOptionString("main", "inn", "00000000000"),
        "kpp" => COption::GetOptionString("main", "kpp", "00000000000"),
        "ogrn" => COption::GetOptionString("main", "ogrn", "00000000000"),
        "okpo" => COption::GetOptionString("main", "okpo", "00000000000"),
        "okato" => COption::GetOptionString("main", "okato", "00000000000"),
        "bank_account" => COption::GetOptionString("main", "bankAccount", "00000000000"),
        "branch_number" => COption::GetOptionString("main", "branchNumber", "000"),
        "bik" => COption::GetOptionString("main", "bik", "00000000"),
        "cash_account" => COption::GetOptionString("main", "cashAccount", "00000000"),
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."recomendations/repay.php",
    array(
        "from_term" => COption::GetOptionString("main", "fromTerm", "1"),
        "to_term" => COption::GetOptionString("main", "toTerm", "16"),
        "percent" =>  COption::GetOptionString("main", "percent", "2"),
        "company_name" =>  COption::GetOptionString("main", "companyName", GetMessage("company_name")),
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."install_index.php",
    array(
        "min_summ" => COption::GetOptionString("main", "fromSumm", "1000"),
        "max_summ" => COption::GetOptionString("main", "toSumm", "16000"),
        "percent" =>  COption::GetOptionString("main", "percent", "2"),
        "max_term" =>  COption::GetOptionString("main", "toTerm", "16"),
		"company_name" =>  COption::GetOptionString("main", "companyName", GetMessage("company_name")),
		"from_age" => COption::GetOptionString("main", "fromAge", "21"),
        "to_age" => COption::GetOptionString("main", "toAge", "65")
    )
);

$fromSumm = COption::GetOptionString("main", "fromSumm", "1000");
$toSumm = COption::GetOptionString("main", "toSumm", "16000");

if(empty($fromSumm))
    $fromSumm = 1000;

if(empty($toSumm))
    $toSumm = 16000;

$strValues = $currentVal = $fromSumm;
while(($currentVal = $currentVal + $fromSumm) <= $toSumm)
{
    $strValues .= ' '.$currentVal;
}
if($currentVal != $toSumm)
    $strValues .= ' '.$toSumm;

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."documents/rules.php",
    array(
        "from_term" => COption::GetOptionString("main", "fromTerm", "1"),
        "to_term" => COption::GetOptionString("main", "toTerm", "16"),
        "from_summ" => COption::GetOptionString("main", "fromSumm", "1000"),
        "to_summ" => COption::GetOptionString("main", "toSumm", "16 000"),
        "percent" =>  COption::GetOptionString("main", "percent", "2"),
        "company_name" =>  COption::GetOptionString("main", "companyName", GetMessage("company_name")),
        "owner_company" =>  COption::GetOptionString("main", "ownerCompany", GetMessage("owner_company")),
        "from_age" =>  COption::GetOptionString("main", "fromAge", "21"),
        "to_age" =>  COption::GetOptionString("main", "toAge", "65"),
        "loan_values" => $strValues
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."documents/personal-data.php",
    array(
        "owner_company" =>  COption::GetOptionString("main", "ownerCompany", GetMessage("owner_company")),
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."documents/affiliated-persons.php",
    array(
        "owner_company" =>  COption::GetOptionString("main", "ownerCompany", GetMessage("owner_company")),
        "ogrn" => COption::GetOptionString("main", "ogrn", "0000000000"),
        "inn" => COption::GetOptionString("main", "inn", "0000000000"),
    )
);


$certificate = COption::GetOptionString("main", "certificate");
if($certificate)
{
    $arFile = CFile::GetFileArray($certificate);
    $s = @copy($_SERVER['DOCUMENT_ROOT'].$arFile['SRC'], WIZARD_SITE_PATH."images/certificate.gif");
}

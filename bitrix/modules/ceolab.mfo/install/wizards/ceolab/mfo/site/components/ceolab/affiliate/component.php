<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 21.03.13
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$status = CModule::IncludeModuleEx('ceolab.mfo');

if($status != MODULE_NOT_FOUND)
{
    $arResult['status'] = $status;
    if($status != MODULE_DEMO_EXPIRED)
    {
        CModule::IncludeModule("iblock");

        $filter = array("IBLOCK_ID" => $arParams['IBLOCK_ID']);

        $res = CIBlockElement::GetList(array(), $filter);
        $keyGroup = "DESTINCT";
        while($r = $res->GetNextElement())
        {
            $props = $r->getProperties();


            foreach($props as $k => $v)
            {
                $properties['PROPERTY_'.$k] = $v['VALUE'];
            }

            $buttons = CIBlock::GetPanelButtons(
                $r->fields["IBLOCK_ID"],
                $r->fields["ID"],
                0,
                array("SECTION_BUTTONS"=>false, "SESSID"=>false)
            );

            $p['EDIT_LINK'] = $buttons["edit"]["edit_element"]["ACTION_URL"];
            $p['DELETE_LINK'] = $buttons["edit"]["delete_element"]["ACTION_URL"];

            $arResult['ITEMS'][] = array_merge($r->fields, $properties, $p);
        }

        if(CModule::IncludeModule("iblock"))
        {

            $arButtons = CIBlock::GetPanelButtons($arParams['IBLOCK_ID']);
            unset($arButtons['configure']['add_section']);


            if($APPLICATION->GetShowIncludeAreas())
                $this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

            if($arParams["SET_TITLE"])
            {

                $arTitleOptions = array(
                    'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_iblock"]["ACTION"],
                    'PUBLIC_EDIT_LINK' => "",
                    'COMPONENT_NAME' => $this->GetName(),
                );

            }
        }
        $this->includeComponentTemplate();
    }
    else
     echo "<font style='color: red'>".GetMessage("demo_expired")."</font>";

}
?>


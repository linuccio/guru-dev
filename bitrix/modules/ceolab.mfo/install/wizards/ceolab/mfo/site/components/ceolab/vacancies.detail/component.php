<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 21.03.13
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 */
CModule::IncludeModule("iblock");
$status = CModule::IncludeModuleEx('ceolab.mfo');

if($status != MODULE_NOT_FOUND)
{
    $arResult['status'] = $status;
    if($status != MODULE_DEMO_EXPIRED)
    {
        if(isset($_GET[$arParams['PAGE_ID']]))
        {
            $filter = array("IBLOCK_ID" => $arParams['IBLOCK_ID']);
            $filter['ID'] = $_GET[$arParams['PAGE_ID']];
            if($this->StartResultCache(false, array($filter)))
            {
                $res = CIBlockElement::GetList(array(), $filter);
                $r = $res->GetNextElement();
                $fields = $r->GetFields();
                $props = $r->GetProperties();

                foreach($props as $k => $v)
                {
                    $p[$k] = $v['VALUE'];
                }

                $arResult = array_merge($fields, $p);
                $this->includeComponentTemplate();
            }
        }
    }
    else
    {
        echo "<font style='color: red'>".GetMessage('demo_expired')."</font>";
    }
}
?>



    <?if($arResult['DATA']):?>
        <div class="mfo_vacancy_blocks">
            <?foreach($arResult['DATA'] as $res):?>

                <?
                    $this->AddEditAction($res['ID'], $res['EDIT_LINK'], CIBlock::GetArrayByID($res["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($res['ID'], $res['DELETE_LINK'], CIBlock::GetArrayByID($res["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="mfo_vacancy" id="<?=$this->GetEditAreaId($res['ID']);?>">
                    <div class="mfo_vacany_name">
                        <?if($res['DETAIL_LINK']):?>
                            <a href="<?=$res['DETAIL_LINK']?>"><?=$res['NAME']?></a>
                        <? else:?>
                            <?=$res['NAME']?>
                        <? endif?>
                    </div>
                    <span class="mfo_vacany_salary">
                        <?=$res['SALARY']?>
                    </span><br/>
                    <span class="mfo_vacancy_date">
                        <?=$res['ACTIVE_FROM']?>
                    </span>
                </div>
            <?endforeach?>
        </div>
        <div class="mfo_pagination">
            <?=$arResult["NAV_STRING"];?>
        </div>
    <?else:?>
        <p align="center">
            <?=GetMessage("no_data")?>
        </p>
    <?endif?>
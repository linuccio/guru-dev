<div style="font-size: 36px;">
    <?=$arResult['NAME']?>
</div>
<div style="margin-top: 20px; font-size: 14px;">
    <?=$arResult['SALARY']?> <span style="color: #2c88b8; font-size: 24px;">|</span> <?=$arResult['SHEDULE']?>
</div>
<div style="font-size: 12px; margin-top: 15px;">
    <?=GetMessage("experience")?>: <?=$arResult['EXPERIENCE']?>
</div>
<?if($arResult['APPLICANTS']): ?>
    <div style="font-size: 12px; margin-top: 5px; color: #999999;">
        (<?=$arResult['APPLICANTS']?>)
    </div>
<?endif?>
<div style="margin-top: 15px;">
    <span class="mfo_vacancy_date">
        <?=$arResult['ACTIVE_FROM']?>
    </span>
</div>
<div style="margin-top: 60px;">
    <?=$arResult['DETAIL_TEXT']?>
</div>
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 01.06.13
 * Time: 19:24
 * To change this template use File | Settings | File Templates.
 */

require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/install/wizard_sol/wizard.php");

class SelectSiteStep extends CSelectSiteWizardStep
{
    function InitStep()
    {
        parent::InitStep();

        $wizard =& $this->GetWizard();
        $wizard->solutionName = "ceolab.mfo";
    }
}

class SelectTemplateStep extends CSelectTemplateWizardStep
{
    function InitStep()
    {
        $wizard =& $this->GetWizard();
        $wizard->solutionName = "ceolab.mfo";
        parent::InitStep();

        $this->SetNextStep("site_settings");
    }
}

class SiteSettingsStep extends CSiteSettingsWizardStep
{
    private $fields = array(
        "companyName",
        "ownerCompany",
        "phone",
        "address",
        "inn",
        "kpp",
        "ogrn",
        "okpo",
        "okato",
        "bankAccount",
        "branchNumber",
        "bik",
        "cashAccount",
        "fromSumm" => array("style" => "width:10%"),
        "toSumm" => array("style" => "width:10%"),
        "fromTerm" => array("style" => "width:10%"),
        "toTerm" => array("style" => "width:10%"),
        "fromAge" => array("style" => "width:10%"),
        "toAge" => array("style" => "width:10%"),
        "percent"
    );

    private $text = array(
        'company_name_lang',
        'owner_company_lang',
        'phone_number',
        'loans',
        'summ',
        'fromSumm_lang',
        'toSumm_lang',
        'term',
        'fromTerm_lang',
        'toTerm_lang',
        'day',
        'currency_lang',
        'age',
        'fromAge_lang',
        'toAge_lang',
        'years',
        'percent_lang',
        'noCashData',
        'address_lang',
        'inn_lang',
        'kpp_lang',
        'ogrn_lang',
        'okpo_lang',
        'okato_lang',
        'bankAccount_lang',
        'branchNumber_lang',
        'bik_lang',
        'cashAccount_lang',
        'documents',
        'certificate_lang'
    );



    function InitStep()
    {
        $wizard =& $this->GetWizard();
        $wizard->solutionName = "itin.buyshop";
        parent::InitStep();

        $this->SetTitle(GetMessage("wiz_settings"));
        $this->SetNextStep("data_install");
        $this->SetNextCaption(GetMessage("wiz_install"));

        $siteID = $wizard->GetVar("siteID");


        $wizard->SetDefaultVars(
            Array(
                "siteName" => COption::GetOptionString("main", "site_personal_name", GetMessage("wiz_name"), $wizard->GetVar("siteID")),
                "copyright" => COption::GetOptionString("main", "site_copyright", str_replace("#CURRENT_YEAR#", date("Y") , GetMessage("wiz_copyright")), $wizard->GetVar("siteID")),
                "installDemoData" => COption::GetOptionString("main", "wizard_demo_data", "N"),
                "companyName" => GetMessage("company_name"),
                "ownerCompany" => GetMessage("owner_company"),
                "phone" => "8 800 700-43-44",
                "address" => GetMessage("address"),
                "inn" => "0000000000",
                "kpp"=> "0000000000",
                "ogrn"=> "0000000000",
                "okpo"=> "0000000000",
                "okato"=> "0000000000",
                "bankAccount"=> "0000000000",
                "branchNumber"=> "000",
                "bik" => "0000000000",
                "cashAccount" => "0000000000",
                "fromSumm" => "1000",
                "toSumm" => "16000",
                "fromTerm" => "1",
                "toTerm" => "16",
                "fromAge" => "21",
                "toAge" => "65",
                "percent" => "2"
            )
        );
    }

    function OnPostForm()
    {
        $wizard =& $this->GetWizard();

        if ($wizard->IsNextButtonClick())
        {
            foreach($this->fields as $key => $val)
            {

                if(is_array($val))
                    $val = $key;
                COption::SetOptionString("main", $val, htmlspecialchars($wizard->GetVar($val)));
            }
            $this->SaveFile("certificate", Array("max_file_size" => 5000*1024, "extensions" => "gif,jpg,png"));
            COption::SetOptionString("main", "certificate", $wizard->GetVar("certificate"));


        }
    }

    function ShowStep()
    {
        foreach($this->fields as $key => $val)
        {
            $style = "width:90%";
            if(is_array($val))
            {
                $style = $val['style'];
                $val = $key;
            }
            $replaceArr[$val] = $this->ShowInputField("text", $val, Array("id" => $val, "style" => $style));
        }

        foreach($this->text as $val)
        {
            $replaceArr[$val] = GetMessage($val);
        }

        $replaceArr['certificate'] = $this->ShowFileField("certificate", Array("id" => "certificate"));

        $this->content = $this->getTemplateForm(__DIR__."/wizardForms/siteSettings.php", $replaceArr);
    }

    private function getTemplateForm($path, Array $replaceArr = array())
    {
        $content = file_get_contents($path);
        if(!$content)
            return;

        foreach($replaceArr as $key => $val)
        {
            $content = str_replace("%".$key."%", $val, $content);
        }
        return $content;
    }
}

class DataInstallStep extends CDataInstallWizardStep
{
    function OnPostForm()
    {
        $result = parent::OnPostForm();
        unlink(WIZARD_SITE_PATH."/index.php");
        rename(WIZARD_SITE_PATH."install_index.php", WIZARD_SITE_PATH."/index.php");
        return $result;
    }
}

class FinishStep extends CFinishWizardStep
{
    function OnPostForm()
    {
        $wizard =& $this->GetWizard();
        if($wizard->IsNextButtonClick())
        {
            LocalRedirect("/");
        }
    }

}

<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!defined("WIZARD_DEFAULT_SITE_ID") && !empty($_REQUEST["wizardSiteID"]))
    define("WIZARD_DEFAULT_SITE_ID", $_REQUEST["wizardSiteID"]);


$arWizardDescription = Array(
    "NAME" => GetMessage("name"),
    "DESCRIPTION" => GetMessage("description"),
    "ICON" => "images/icon.gif",
    "IMAGE" => "images/".LANGUAGE_ID."/preview.png",	
    "VERSION" => "1.0.0",
    "START_TYPE" => "WINDOW",
    "WIZARD_TYPE" => "INSTALL",
    "PARENT" => "wizard_sol",
    "TEMPLATES" => Array(
        Array("SCRIPT" => "wizard_sol")
    ),
    "DEPENDENCIES" => Array(
        "main" => "6.0.5",
    ),
    "STEPS" => (defined("WIZARD_DEFAULT_SITE_ID") ?
        Array("SelectTemplateStep", "SiteSettingsStep", "DataInstallStep" ,"FinishStep") :
        Array("SelectSiteStep", "SelectTemplateStep", "SiteSettingsStep", "DataInstallStep" ,"FinishStep"))
);

?>

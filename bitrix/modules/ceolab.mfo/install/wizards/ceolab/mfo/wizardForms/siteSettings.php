<div>
    <table width="100%" cellspacing="10px">
        <tr>
            <td>
                %company_name_lang%:
            </td>
            <td>
                %companyName%
            </td>
        </tr>
        <tr>
            <td align="left">
                %owner_company_lang%:
            </td>
            <td align="left">
                %ownerCompany%
            </td>
        </tr>
        <tr>
            <td align="left">
                %phone_number%:
            </td>
            <td align="left">
                %phone%
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <p style="font-size: 18px;">
                    %loans%
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                %summ%
            </td>
        </tr>
        <tr>
            <td colspan="2">
                %fromSumm_lang% %fromSumm% %toSumm_lang% %toSumm% %currency_lang%
            </td>
        </tr>
        <tr>
            <td colspan="2">
                %term%
            </td>
        </tr>
        <tr>
            <td colspan="2">
                %fromTerm_lang% %fromTerm% %toTerm_lang% %toTerm% %day%
            </td>
        </tr>
        <tr>
            <td colspan="2">
                %age%
            </td>
        </tr>
        <tr>
            <td colspan="2">
                %fromAge_lang% %fromAge% %toAge_lang% %toAge% %years%
            </td>
        </tr>
        <tr>
            <td>
                %percent_lang%
            </td>
            <td>
                %percent%
            </td>
        </tr>


        <tr>
            <td colspan="2">
                <p style="font-size: 18px;">
                    %noCashData%
                </p>
            </td>
        </tr>
        <tr>
            <td>
                %address_lang%
            </td>
            <td>
                %address%
            </td>
        </tr>
        <tr>
            <td>
                %inn_lang%
            </td>
            <td>
                %inn%
            </td>
        </tr>
        <tr>
            <td>
                %kpp_lang%
            </td>
            <td>
                %kpp%
            </td>
        </tr>
        <tr>
            <td>
                %ogrn_lang%
            </td>
            <td>
                %ogrn%
            </td>
        </tr>
        <tr>
            <td>
                %okpo_lang%
            </td>
            <td>
                %okpo%
            </td>
        </tr>
        <tr>
            <td>
                %okato_lang%
            </td>
            <td>
                %okato%
            </td>
        </tr>
        <tr>
            <td>
                %bankAccount_lang%
            </td>
            <td>
                %bankAccount%
            </td>
        </tr>
        <tr>
            <td>
                %branchNumber_lang%
            </td>
            <td>
                %branchNumber%
            </td>
        </tr>
        <tr>
            <td>
                %bik_lang%
            </td>
            <td>
                %bik%
            </td>
        </tr>
        <tr>
            <td>
                %cashAccount_lang%
            </td>
            <td>
                %cashAccount%
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p style="font-size: 18px;">
                    %documents%
                </p>
            </td>
        </tr>
        <tr>
            <td>
                %certificate_lang%
            </td>
            <td>
                %certificate%
            </td>
        </tr>
    </table>

</div>
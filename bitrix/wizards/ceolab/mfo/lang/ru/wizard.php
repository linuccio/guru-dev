<?
$MESS["company_name"] = "МФО";
$MESS['owner_company'] = "ООО «МФО»";
$MESS['address'] = "г. Москва";

$MESS['company_name_lang'] = "Имя компании";
$MESS['owner_company_lang'] = "Владелец компании";
$MESS['phone_number'] = "Тел. горячей линии";
$MESS['loans'] = "О займах";
$MESS['summ'] = "Сумма";
$MESS['fromSumm_lang'] = "от";
$MESS['toSumm_lang'] = "до";
$MESS['term'] = "Срок";
$MESS['fromTerm_lang'] = "от";
$MESS['toTerm_lang'] = "до";
$MESS['day'] = "дней";
$MESS['currency_lang'] = "рублей";
$MESS['age'] = "Критерий возраста";
$MESS['fromAge_lang'] = "от";
$MESS['toAge_lang'] = "до";
$MESS['years'] = "лет";
$MESS['percent_lang'] = "Процент в день";
$MESS['noCashData'] = "Данные для безналичных переводов";
$MESS['address_lang'] = "Адрес";
$MESS['inn_lang'] = "ИНН";
$MESS['kpp_lang'] = "КПП";
$MESS['ogrn_lang'] = "ОГРН";
$MESS['okpo_lang'] = "ОКПО";
$MESS['okato_lang'] = "ОКАТО";
$MESS['bankAccount_lang'] = "р/с";
$MESS['branchNumber_lang'] = "в филиале №";
$MESS['bik_lang'] = "БИК";
$MESS['cashAccount_lang'] = "к/с";
$MESS['documents'] = "Документы";
$MESS['certificate_lang'] = "Свидетельство МФО";
?>

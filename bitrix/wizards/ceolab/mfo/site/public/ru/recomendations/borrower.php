<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Рекомендации заемщикам");
?>  <?$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "mfo_tabs",
    Array(
        "ROOT_MENU_TYPE" => "page",
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "page",
        "USE_EXT" => "Y",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => Array()
    )
);?>


<div class="how_to_get">
    <p style="font-size: 42px; ">
        Рекомендации заемщикам
    </p>
    <h1><span style="color: rgb(44, 136, 184);">-</span>&nbsp;Мы настоятельно рекомендуем</h1>

    <table>
        <tr>
            <td width="500px">
                <p>
                    Не брать займ на сумму<br/>
                    превышающую 50% суммы<br/>
                    Вашего ежемесячного дохода
                </p>
            </td>
            <td>
                <p>
                    Не брать займ, если Вы не уверены,<br/>
                    что сможете его погасить без<br/>
                    осложнений и прочего
                </p>
            </td>
        </tr>
    </table>
</div>

<div class="how_to_get">
    <h1><span style="color: rgb(44, 136, 184);">-</span>&nbsp;Вы можете погасить займ ДОСРОЧНО</h1>

    <table width="100%">
        <tr>
            <td width="500px">
                <p>
                    Микрозайм может быть погашен<br/>
                    любым из способов ранее срока<br/>
                    оплаты, указанного в договоре.<br/>
                    В этом случае производится перерасчет<br/>
                    процентов за пользование микрозаймом<br/>
                    на дату фактической оплаты.
                </p>
            </td>
            <td>
                <p>
                    За досрочное погашение займа не<br/>
                    взимается никаких штрафов.<br/>
                    Оплатить и оформить повторный<br/>
                    микрозайм Вы можете в любом удобном<br/>
                    для Вас офисе компании.
                </p>
            </td>
        </tr>
    </table>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
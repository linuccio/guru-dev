<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как получить займ");
?>  <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"mfo_tabs",
	Array(
		"ROOT_MENU_TYPE" => "page",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "page",
		"USE_EXT" => "Y",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => Array()
	)
);?> 
<div class="how_to_get"> 
  <h1><span style="color: rgb(44, 136, 184);">-</span>&nbsp;Как получить займ?</h1>
 
  <p>Для получения микрозайма в &laquo;#company_name#&raquo; необходимо соблюдение нескольких простых условий: </p>
 
  <p>- иметь постоянный доход;</p>
 
  <p>- проживать или работать в городе (области), где Вы хотите взять микрозайм;</p>
 
  <p>- и соответствовать возрастному критерию &mdash; от #from_age# до #to_age# лет
    <br />
  </p>
 </div>
 
<div class="how_to_get">
  <h1><span style="color: rgb(44, 136, 184);">-</span>&nbsp;Все что Вам нужно:</h1>
 
  <p>
    Найти удобный для Вас офис выдачи микрозаймов, предоставить свой паспорт и заполнить анкету
        <br />
    В случае одобрения Вы сразу получите деньги
  </p>
  <p>
      Оформить заявку, не выходя из дома — заполните анкету онлайн на сайте.<br/>
      В случае одобрения наши специалисты свяжутся с Вами в течение 2 часов и пригласят на оформление.<br/>
      Наши офисы приятно посещать, с нашими сотрудниками приятно общаться!
  </p>
</div>
 <?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
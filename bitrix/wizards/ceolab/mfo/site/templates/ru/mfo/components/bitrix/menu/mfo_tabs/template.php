<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="menu_tabs_mfo">
    <?if (!empty($arResult)):?>
        <? foreach($arResult as $arItem): ?>
            <?if($arItem["SELECTED"]):?>
                <a class="menu_tabs_mfo_active" href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a>
            <?else:?>
                <a class="menu_tabs_mfo" href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a>
            <?endif?>
        <? endforeach ?>
    <?endif?>
</div>
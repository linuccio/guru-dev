<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("calcmfo"),
    "DESCRIPTION" => 'Calc',
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "CeoLab",
        "CHILD" => array(
            "ID" => "calcmfo",
            "NAME" => GetMessage("calcmfo")
        )
    ),
);
?>

/*
 Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.plugins.setLang("a11yhelp","el",{title:" ",contents:" .  ESC  .",legend:[{name:"",items:[{name:" ",legend:" ${toolbarFocus}      .         TAB  Shift-TAB.          .    ENTER       ."},{name:"  ",
legend:"    ,  TAB        SHIFT + TAB     .  ENTER     .  ESC       .           ALT + F10       .          TAB  RIGHT ARROW.        SHIFT + TAB  LEFT ARROW.  SPACE  ENTER       ."},
{name:"  ",legend:" ${contextMenu}  APPLICATION KEY      .          TAB   .      SHIFT+TAB   .    ENTER      .           ENTER   .        ESC   .      ESC."},
{name:"  ",legend:"    ,      TAB   .      SHIFT + TAB   .    ENTER     .  ESC       ."},{name:"   ",legend:" ${elementsPathFocus}         .         TAB    .         SHIFT+TAB    .    ENTER       ."}]},
{name:"",items:[{name:"  ",legend:" ${undo}"},{name:"  ",legend:" ${redo}"},{name:"   ",legend:" ${bold}"},{name:"   ",legend:" ${italic}"},{name:"  ",legend:" ${underline}"},{name:"  ",legend:" ${link}"},{name:"   ",legend:" ${toolbarCollapse}"},{name:" Access previous focus space command",legend:"Press ${accessPreviousSpace} to access the closest unreachable focus space before the caret, for example: two adjacent HR elements. Repeat the key combination to reach distant focus spaces."},
{name:" Access next focus space command",legend:"Press ${accessNextSpace} to access the closest unreachable focus space after the caret, for example: two adjacent HR elements. Repeat the key combination to reach distant focus spaces."},{name:"  ",legend:" ${a11yHelp}"}]}]});
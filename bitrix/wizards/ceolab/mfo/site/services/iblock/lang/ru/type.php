<?
$MESS["REGIONS_TYPE_NAME"] = "Регионы";
$MESS["REGIONS_ELEMENT_NAME"] = "Регионы";
$MESS["REGIONS_SECTION_NAME"] = "Разделы";

$MESS["ADDRESS_TYPE_NAME"] = "Адреса";
$MESS["ADDRESS_ELEMENT_NAME"] = "Адреса";
$MESS["ADDRESS_SECTION_NAME"] = "Разделы";

$MESS["REVIEWS_TYPE_NAME"] = "Отзывы";
$MESS["REVIEWS_ELEMENT_NAME"] = "Отзывы";
$MESS["REVIEWS_SECTION_NAME"] = "Разделы";

$MESS["NEWS_TYPE_NAME"] = "Новости";
$MESS["NEWS_ELEMENT_NAME"] = "Новости";
$MESS["NEWS_SECTION_NAME"] = "Разделы";

$MESS["VACANCY_TYPE_NAME"] = "Вакансии";
$MESS["VACANCY_ELEMENT_NAME"] = "Вакансии";
$MESS["VACANCY_SECTION_NAME"] = "Разделы";

$MESS["AFFILIATTED_TYPE_NAME"] = "Аффилированные лица";
$MESS["AFFILIATTED_ELEMENT_NAME"] = "Аффилированные лица";
$MESS["AFFILIATTED_SECTION_NAME"] = "Разделы";

?>
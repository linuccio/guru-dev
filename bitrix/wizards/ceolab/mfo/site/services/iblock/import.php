<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 04.06.13
 * Time: 11:13
 * To change this template use File | Settings | File Templates.
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if(!CModule::IncludeModule("iblock"))
    return;

$iblocks = array(
    array(
        "ID" => "regions",
        "SECTIONS" => "Y",
        "IN_RSS" => "N",
        "SORT" => 100,
        "LANG" => Array(),
        "elements" => array(
            array(
                "file" => "regions.xml",
                "iblock" => "regions",
            )
        )
    ),
    array(
        "ID" => "address",
        "SECTIONS" => "Y",
        "IN_RSS" => "N",
        "SORT" => 100,
        "LANG" => Array(),
        "elements" => array(
            array(
                "file" => "address.xml",
                "iblock" => "offices"
            )
        )
    ),
    array(
        "ID" => "news",
        "SECTIONS" => "Y",
        "IN_RSS" => "N",
        "SORT" => 100,
        "LANG" => Array(),
        "elements" => array(
            array(
                "file" => "news.xml",
                "iblock" => "news"
            )
        )
    ),
    array(
        "ID" => "reviews",
        "SECTIONS" => "Y",
        "IN_RSS" => "N",
        "SORT" => 100,
        "LANG" => Array(),
        "elements" => array(
            array(
                "file" => "reviews.xml",
                "iblock" => "reviews"
            )
        )
    ),
    array(
        "ID" => "vacancy",
        "SECTIONS" => "Y",
        "IN_RSS" => "N",
        "SORT" => 100,
        "LANG" => Array(),
        "elements" => array(
            array(
                "file" => "vacancies.xml",
                "iblock" => "vacancies"
            )
        )
    ),
    array(
        "ID" => "affiliatted",
        "SECTIONS" => "Y",
        "IN_RSS" => "N",
        "SORT" => 100,
        "LANG" => Array(),
        "elements" => array(
            array(
                "file" => "affiliated.xml",
                "iblock" => "affiliated"
            )
        )
    )
);

$arLanguages = Array();
$rsLanguage = CLanguage::GetList($by, $order, array());
while($arLanguage = $rsLanguage->Fetch())
    $arLanguages[] = $arLanguage["LID"];

$iblockType = new CIBlockType;
foreach($iblocks as $iblock)
{
    $elements = $iblock['elements'];
    unset($iblock['elements']);

    $dbType = CIBlockType::GetList(Array(),Array("=ID" => $iblock["ID"]));
    if(!$dbType->Fetch())
    {
        foreach($arLanguages as $languageID)
        {
            WizardServices::IncludeServiceLang("type.php", $languageID);

            $code = strtoupper($iblock["ID"]);
            $iblock["LANG"][$languageID]["NAME"] = GetMessage($code."_TYPE_NAME");
            $iblock["LANG"][$languageID]["ELEMENT_NAME"] = GetMessage($code."_ELEMENT_NAME");

            if ($iblock["SECTIONS"] == "Y")
                $iblock["LANG"][$languageID]["SECTION_NAME"] = GetMessage($code."_SECTION_NAME");
        }

        $iblockType->Add($iblock);
    }



    foreach($elements as $element)
    {
        $iblockID = WizardServices::ImportIBlockFromXML(
            __DIR__."/xml/".LANGUAGE_ID."/".$element['file'],
            $element['iblock'],
            $iblock['ID'],
            WIZARD_SITE_ID
        );

        if(!$iblockID)
        {
             $rsIBlock = CIBlock::GetList(array(), array("CODE" => $element['iblock'], "TYPE" => $iblock['ID'], "SITE_ID" => WIZARD_SITE_ID))->Fetch();

            if(!$rsIBlock)
                $rsIBlock = CIBlock::GetList(array(), array("CODE" => $element['iblock'], "TYPE" => $iblock['ID']))->Fetch();

            if($rsIBlock['LID'] != WIZARD_SITE_ID)
            {

                $rsSites = CIBlock::GetSite($rsIBlock['ID']);
                $sites = array();
                while($s = $rsSites->Fetch())
                {
                    $sites[] = $s['LID'];
                }

                if($sites)
                {
                    $sites[] = WIZARD_SITE_ID;
                    $ib = new CIBlock();
                    $ib->Update($rsIBlock['ID'], array("SITE_ID" => $sites));
                }
            }

            $iblockID = $rsIBlock['ID'];
        }

        $replace[$element['iblock']] = $iblockID;
    }

}

$region = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $replace['regions']))->Fetch();
$reviewPropEmail = CIBlock::GetProperties($replace['reviews'], array(), array("CODE" => "EMAIL"))->Fetch();


CWizardUtil::ReplaceMacros(
    $_SERVER["DOCUMENT_ROOT"].BX_ROOT."/templates/".WIZARD_TEMPLATE_ID."/header.php",
    array(
        "regions" => $replace['regions'],
        "default_city" => $region['ID']
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."address.php",
    array(
        "regions" => $replace['offices'],
        "default_city" => $region['ID']
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."index_news.php",
    array(
        "reviews" => $replace['reviews'],
        "news" => $replace['news']
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."news.php",
    array(
        "news" => $replace['news']
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."news/detail.php",
    array(
        "news" => $replace['news']
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."reviews.php",
    array(
        "reviews" => $replace['reviews'],
        "reviews_field_email" => $reviewPropEmail['ID']
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."jobs/vacancies.php",
    array(
        "vacancies" => $replace['vacancies']
    )
);

CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH."documents/affiliated-persons.php",
    array(
        "affiliated" => $replace['affiliated']
    )
);


//reviews_field_email
//var_dump($replace);die;


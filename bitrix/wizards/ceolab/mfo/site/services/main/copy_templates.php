<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 04.06.13
 * Time: 1:39
 * To change this template use File | Settings | File Templates.
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if (!defined("WIZARD_TEMPLATE_ID"))
    return;


CopyDirFiles(
    $_SERVER["DOCUMENT_ROOT"].WizardServices::GetTemplatesPath(WIZARD_RELATIVE_PATH."/site")."/".WIZARD_TEMPLATE_ID,
    $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID,
    true,
    true,
    false
);

CWizardUtil::ReplaceMacros(
    $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID."/header.php",
    array(
        "site_dir" => WIZARD_SITE_DIR,
    )
);

$obSite = CSite::GetList($by = "def", $order = "desc", Array("LID" => WIZARD_SITE_ID));
if ($arSite = $obSite->Fetch())
{
    $arTemplates = Array();
    $found = false;
    $foundEmpty = false;
    $obTemplate = CSite::GetTemplateList($arSite["LID"]);
    while($arTemplate = $obTemplate->Fetch())
    {
        if(!$found && strlen(trim($arTemplate["CONDITION"]))<=0)
        {
            $arTemplate["TEMPLATE"] = WIZARD_TEMPLATE_ID;
            $found = true;
        }
        if($arTemplate["TEMPLATE"] == "empty")
        {
            $foundEmpty = true;
            continue;
        }
        $arTemplates[]= $arTemplate;
    }

    if (!$found)
        $arTemplates[]= Array("CONDITION" => "", "SORT" => 150, "TEMPLATE" => WIZARD_TEMPLATE_ID);

    $arFields = Array(
        "TEMPLATE" => $arTemplates,
        "NAME" => $arSite["NAME"],
    );

    $obSite = new CSite();
    $obSite->Update($arSite["LID"], $arFields);
}
<?php
$MESS['T_IBLOCK_DESC_LIST_TYPE'] = "Тип инфоблока";
$MESS['T_IBLOCK_DESC_LIST_ID'] = "Инфоблок";
$MESS['default_city'] = "Город по умолчанию";
$MESS['id_vacancy'] = "Идентификатор вакансии";
$MESS['count_elements'] = "Кол-во элементов на странице";
$MESS['set_session'] = "Устанавливать сессию";
$MESS['get_session'] = "Значение из сессии";
$MESS['region'] = "Регион";
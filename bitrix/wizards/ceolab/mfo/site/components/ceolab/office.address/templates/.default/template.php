<?
$address = current($arResult["ITEMS"]);
$address = $address[0];
?>

<script type="text/javascript">
    ymaps.ready(init);
    var myMap;

    function init(){
        myMap = new ymaps.Map ("map", {
            center: [55.76, 37.64],
            zoom: 15
        });

        myMap.controls.add(
                new ymaps.control.ZoomControl()
        );

        <?foreach($arResult['DATA'] as $res):?>
            <? if($res['COORD']):?>
                myMap.geoObjects.add(new ymaps.Placemark([<?=$res['COORD'];?>], { content: '', balloonContent: '' }));
            <? endif ?>
        <? endforeach ?>
        setObject('<?=$address['COORD']?>', '<?=implode(",",$address['BUS'])?>', '<?=implode(",",$address['TRAM'])?>', '<?=implode("<br/>", $address['SCHEDULE'])?>')
    }

    function setObject(coord, bus, tram, schedule)
    {
        var coord = coord.split(",");
        var bus = bus.split(",");
        var tram = tram.split(",");


        printTransport("office_address_bus", bus);
        printTransport("office_address_tram", tram);

        $("#office_address_schedule").empty();
        $('<span>'+schedule+'</span>').appendTo("#office_address_schedule");

        if(coord.length == 2)
        {
            myMap.setCenter(coord);
        }
    }

    function printTransport(htmlId, arr)
    {
        $("#"+htmlId).empty();
        var count = 0;
        var tr = $('<tr></tr>').appendTo("#"+htmlId);

        for(k in arr)
        {
            if(!arr[k].length)
                continue;
            $('<td align="center"><span class="c">'+arr[k]+'</span></td>').appendTo(tr);
            count++;
            if(count == 3)
            {
                tr = $('<tr></tr>').appendTo("#"+htmlId);
                count = 0;
            }
        }
    }


</script>

<?if($arResult['ITEMS']):?>
<table align="center" width="100%" cellspacing="30">
    <tr>
    <?
        $trOpen = false;

        foreach($arResult['ITEMS'] as $destinct => $item):
            $count++;
            if($count == 3)
            {
                echo "</tr><tr>";
                $count = 0;
            }

    ?>
        <td valign="top">
            <span class="area"><?=$destinct?></span>
            <br/>
            <br/>
            <?foreach($item as $address):
                $this->AddEditAction($address['ID'], $address['EDIT_LINK'], CIBlock::GetArrayByID($address["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($address['ID'], $address['DELETE_LINK'], CIBlock::GetArrayByID($address["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
                <a id="<?=$this->GetEditAreaId($address['ID']);?>" href="javascript:onclick()" onclick="setObject('<?=$address['COORD']?>', '<?=implode(",",$address['BUS'])?>', '<?=implode(",",$address['TRAM'])?>', '<?=implode("<br/>", $address['SCHEDULE'])?>')"><?=$address['ADDRESS']?></a><br/>
            <? endforeach ?>
        </td>
    <? endforeach ?>

    </tr>

</table>


<div class="map"  align="center">
    <div class="mapTopBlock mapHeader" align="right">

                        <span style="vertical-align: -30px; margin-right: 30px;">
                            <img src="/bitrix/components/ceolab/office.address/templates/.default/images/m.png"/>
                            <span style="vertical-align:35px; margin-left: -30px;"><?=GetMessage("offices")?></span>
                        </span>
    </div>
    <div class="mapBottomBlock">
        <table align="center" width="800px">
            <tr>
                <td valign="top">
                    <div id="map" style="width: 500px; height: 380px"></div>
                </td>
                <td colspan="2" width="400px" valign="top">
                    <table>
                        <tr>
                            <td colspan="2">

                            </td>
                        </tr>
                    </table>

                    <div class="itemTransport firstItemTransport">
                        <p><?=GetMessage("bus")?></p>
                        <div style="text-align:right;">
                            <table>
                                <tr>
                                    <td align="right">
                                        <img src="/bitrix/components/ceolab/office.address/templates/.default/images/bus.png"/>
                                    </td>
                                    <td valign="center">
                                        <table id="office_address_bus" cellspacing="10px">

                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="itemTransport mapTopBlock">
                        <p><?=GetMessage("tram")?></p>
                        <div style="text-align:right;">
                            <table>
                                <tr>
                                    <td align="right" width="100px">
                                        <img src="/bitrix/components/ceolab/office.address/templates/.default/images/tram.png"/>
                                    </td>
                                    <td valign="center">
                                        <table id="office_address_tram" cellspacing="10px">

                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="itemTransport mapBottomBlock weekly">
                        <p><?=GetMessage("shedule")?></p>
                        <span id="office_address_schedule">

                        </span>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div name="border">

    </div>
</div>
<?else:?>
    <p align="center">
        <?=GetMessage("no_data")?>
    </p>
<?endif?>
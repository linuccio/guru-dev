<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 21.03.13
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
$status = CModule::IncludeModuleEx('ceolab.mfo');

if($status != MODULE_NOT_FOUND)
{
    $arResult['status'] = $status;
    if($status != MODULE_DEMO_EXPIRED)
    {
        CModule::IncludeModule("iblock");

        $filter = array("IBLOCK_ID" => $arParams['IBLOCK_ID']);

        if($arParams['SET_GET'] && isset($_GET[$arParams['SET_GET']]))
            $cCity = $_GET[$arParams['SET_GET']];
        elseif(isset($arParams['GET_SESSION']) && $arParams['GET_SESSION'] && isset($_SESSION[$arParams['GET_SESSION']]))
            $cCity = $_SESSION[$arParams['GET_SESSION']];
        elseif(isset($arParams['DEFAULT_CITY']) && isset($city[$arParams['DEFAULT_CITY']]))
            $cCity = $arParams['DEFAULT_CITY'];


        $filter['PROPERTY_CITY'] = $cCity;


        $arNavParams = array(
            "nPageSize" =>  $arParams['COUNT_ELEMENTS'],
        );

        $arNavigation = CDBResult::GetNavParams($arNavParams);

        if($this->StartResultCache(false, array($cCity, $arNavigation)))
        {
            $res = CIBlockElement::GetList(array(), $filter, false, array('nPageSize' => $arParams['COUNT_ELEMENTS'], 'iNumPage' => $arNavigation['PAGEN']));

            while($r = $res->GetNextElement())
            {

                $p = array();
                $props = $r->GetProperties();
                $fields = $r->GetFields();

                foreach($props as $k => $v)
                {
                    $p[$k] = $v['VALUE'];
                }


                $buttons = CIBlock::GetPanelButtons(
                    $fields["IBLOCK_ID"],
                    $fields["ID"],
                    0,
                    array("SECTION_BUTTONS"=>false, "SESSID"=>false)
                );

                if($arParams['PAGE_ELEMENT_URL'])
                    $p['DETAIL_LINK'] = str_replace("{ELEMENT_ID}", $fields['ID'], $arParams['PAGE_ELEMENT_URL']);
                else
                    $p['DETAIL_LINK'] = false;

                $p['ID'] = $fields['ID'];
                $p['ACTIVE_FROM'] = date("d.m.Y", strtotime($fields['ACTIVE_FROM']));
                $p['NAME'] = $fields['NAME'];
                $p['IBLOCK_ID'] = $fields['IBLOCK_ID'];
                $p['EDIT_LINK'] = $buttons["edit"]["edit_element"]["ACTION_URL"];
                $p['DELETE_LINK'] = $buttons["edit"]["delete_element"]["ACTION_URL"];

                $arResult["DATA"][]  = $p;
            }


            $arResult["ITEMS"] = $data;


            $rsElements->nPageWindow = 3;
            @$arResult["NAV_STRING"] = $res->GetPageNavStringEx();

            $APPLICATION->AddHeadString("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js\"></script>");
            $this->includeComponentTemplate();
        }
        if(CModule::IncludeModule("iblock"))
        {

            $arButtons = CIBlock::GetPanelButtons($arParams['IBLOCK_ID']);
            unset($arButtons['configure']['add_section']);


            if($APPLICATION->GetShowIncludeAreas())
                $this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

            if($arParams["SET_TITLE"])
            {

                $arTitleOptions = array(
                    'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_iblock"]["ACTION"],
                    'PUBLIC_EDIT_LINK' => "",
                    'COMPONENT_NAME' => $this->GetName(),
                );

            }
        }
    }
    else
    {
        echo "<font style='color: red'>".GetMessage('demo_expired')."</font>";
    }
}

?>


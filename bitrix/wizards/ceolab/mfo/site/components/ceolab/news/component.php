<?php
/**
 * Created by JetBrains PhpStorm.
 * User: EvGenius
 * Date: 31.05.13
 * Time: 13:29
 * To change this template use File | Settings | File Templates.
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$status = CModule::IncludeModuleEx('ceolab.mfo');

if($status != MODULE_NOT_FOUND)
{
    $arResult['status'] = $status;
    if($status != MODULE_DEMO_EXPIRED)
    {
        $GLOBALS['regionFilter']['PROPERTY']['CITY'] = array($_SESSION['REGION_ID'], false);
        $arParams['FILTER_NAME'] = 'regionFilter';
        $this->includeComponentTemplate();
    }
    else
    {
        echo "<font style='color: red'>".GetMessage('demo_expired')."</font>";
    }
}


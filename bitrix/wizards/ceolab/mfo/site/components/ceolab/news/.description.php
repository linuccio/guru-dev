<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("T_IBLOCK_DESC_LIST"),
    "DESCRIPTION" => GetMessage("T_IBLOCK_DESC_LIST_DESC"),
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "CeoLab",
        "CHILD" => array(
            "ID" => "news",
            "NAME" => GetMessage("T_IBLOCK_DESC_NEWS")
        )
    ),
);
?>

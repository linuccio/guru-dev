<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("select_city"),
    "DESCRIPTION" => GetMessage("select_city"),
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "CeoLab",
        "CHILD" => array(
            "ID" => "officeaddress.selectcity",
            "NAME" => GetMessage("select_city")
        )
    ),
);
?>

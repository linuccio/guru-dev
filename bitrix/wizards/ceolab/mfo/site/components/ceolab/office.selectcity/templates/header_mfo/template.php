<div class="selectCity_header">
    <a href="<?=SITE_DIR?>address.php" style="text-decoration: none; border-bottom: none;"><?=GetMessage("address")?></a>
    <a href="javascript:onclick()" onclick="selectRegionShow('select_region_header')" ><?=GetMessage("city")?><?=$arResult["CURRENT_CITY"]['NAME']?></a>
</div>


<div id="select_region_header" class="selectRegion_header_wrapper" align="center">
    <div class="selectRegion_header">
        <div align="right">
            <a href="javascript:onclick()" onclick="selectRegionHide('select_region_header')">[x]</a>
        </div>
        <div class="regionList_header" align="left">
            <div style="display: inline-block;">
                <?foreach($arResult['CITY'] as $lether => $citys):?>
                <div class="selectRegion_header_block">
                    <div align="left" class="selectRegion_address_header_lether">
                        <?=$lether?>
                    </div>
                    <?foreach($citys as $cityId => $city):?>
                        <?
                            $this->AddEditAction($city['ID'], $city['EDIT_LINK'], CIBlock::GetArrayByID($city["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($city['ID'], $city['DELETE_LINK'], CIBlock::GetArrayByID($city["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <a id="<?=$this->GetEditAreaId($city['ID']);?>" href="?<?=$arParams['SET_GET']?>=<?=$cityId?>"><?=$city['NAME']?></a><br/>
                    <?endforeach?>
                </div>
                <?endforeach?>
            </div>
        </div>
    </div>
</div>

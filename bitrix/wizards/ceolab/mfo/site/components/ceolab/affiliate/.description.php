<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("affiliated_list"),
    "DESCRIPTION" => GetMessage("affiliated_list"),
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "CeoLab",
        "CHILD" => array(
            "ID" => "affiliated",
            "NAME" => GetMessage("affiliated_list")
        )
    ),
);
?>

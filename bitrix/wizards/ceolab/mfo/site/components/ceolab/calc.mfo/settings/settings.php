<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");

__IncludeLang($_SERVER['DOCUMENT_ROOT'].'/bitrix/components/ceolab/calc.mfo/lang/'.LANGUAGE_ID.'/settings.php');

if(isset($_REQUEST['CALC_ID']))
    $id = $_REQUEST['CALC_ID'];
else
    $id = false;

?>

<style>
    .formEditor
    {
        font-size: 13px!important;
        font-family: Arial,serif;
        margin:0 !important;
        padding:0 !important;
        position: relative;
        z-index: 1;
    }

    .formEditor input, select
    {
        border: 1px solid black !important;
        border-radius: 0px!important;
        padding: 0 !important;
    }
</style>

<script type="text/javascript" src="/bitrix/components/ceolab/calc.mfo/js/ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="/bitrix/components/ceolab/calc.mfo/js/settings.js"
        xmlns="http://www.w3.org/1999/html"></script>


<script>
    extend(formEditorElementInput, formEditorElement);
    extend(formEditorElementSpan, formEditorElement);
    extend(formEditorElementSelect, formEditorElement);
    extend(formEditorElementCheckbox, formEditorElement);

    var saveValidator = {
        "isInt" : function(element)
        {
            return parseInt(element.val());
        },
        "isNotEmpty" : function(element)
        {
            return element.val();
        }
    }

    var idForm = 0;
    var formsCalc = [];
    var formsEditorFrame = {};
    idField = 0;

    var formProperties = new formEditorProperties("#formProperties");

    var formValidators = new formEditorValidators();


    function showFormEditor()
    {
        var f = new formEditorFrame(idForm);
        formsEditorFrame[idForm] = f;
        f.Render(true);
        idForm++;
    }

    function saveData(id)
    {
        try
        {
            var oldData = JSON.parse(this.arParams.oInput.value);
        }
        catch(e)
        {
            var oldData =
            {
                calcs : [],
                validators : {}
            };
        }


        var validateFields = {
            "name" : "isNotEmpty",
            "min_summ" : "isInt",
            "max_summ" : "isInt",
            "step" : "isInt",
            "val_percent" : "isInt",
            "max_term" : "isInt",
            "currency" : "isNotEmpty"
        };

        var isValidate = true;
        for(var i in validateFields)
        {
            if(!saveValidator[validateFields[i]]($("#"+i)))
            {
                $("#"+i).attr("style", "border: 1px solid red");
                isValidate = false;
            }
            else
                $("#"+i).removeAttr("style");
        }

        if(isValidate && parseInt($("#min_summ").val()) >= parseInt($("#max_summ").val()))
        {
            $("#min_summ").attr("style", "border: 1px solid red");
            $("#max_summ").attr("style", "border: 1px solid red");
            isValidate = false;
        }

        if(!isValidate)
        {
            alert("<?=GetMessage("incorrectValues")?>");
            return;
        }

        var returnData = {
            forms:[],
            calc: {
                name:htmlspecialchars($("#name").val()),
                min_summ:parseInt($("#min_summ").val()),
                max_summ:parseInt($("#max_summ").val()),
                step:parseInt($("#step").val()),
                percent:parseInt($("#val_percent").val()),
                type_percent:$("#type_percent").val(),
                max_term:parseInt($("#max_term").val()),
                additionInfo: $("#additionInfo").val(),
                currency : $("#currency").val()
            },
            sendData:{
                templateText: CKEDITOR.instances["templateCalcEmail"].getData(),
                emails: $("#emailsCalc").val(),
                subject: $("#subjectEmailCalc").val(),
                from: $("#fromEmailCalc").val()
            }
        };

        for(var key in formsEditorFrame)
        {
            returnData.forms.push({
                data:formsEditorFrame[key].formCalc.data,
                fields:[]
            });

            for(var k in formsEditorFrame[key].formCalc.fields)
            {
                returnData.forms[key].fields.push({data: formsEditorFrame[key].formCalc.fields[k].data, type:formsEditorFrame[key].formCalc.fields[k].type});
            }
        }


        for(var key in formValidators.validators)
        {
            oldData.validators = formValidators.validators;
        }

        if(id || id===0)
        {
            returnData.calc.active = oldData.calcs[id].calc.active;
            oldData.calcs[id] = returnData;
        }
        else
        {
            if(!oldData.calcs)
            {
                oldData.calcs = [];
            }
            oldData.calcs.push(returnData);
        }

        this.arParams.oInput.value = JSON.stringify(oldData);

        window.jsPopup_calcEditor.Close(this.arParams.oInput.value);

        calcSettings(this.arParams);
    }

    function initForms(id)
    {
        try
        {
            var data = JSON.parse(this.arParams.oInput.value);
        }

        catch(e)
        {
            return;
        }


        if(id || id===0)
        {
            $("#name").val(data.calcs[id].calc.name);
            $("#min_summ").val(data.calcs[id].calc.min_summ);
            $("#max_summ").val(data.calcs[id].calc.max_summ);
            $("#step").val(data.calcs[id].calc.step);
            $("#val_percent").val(data.calcs[id].calc.percent);
            $("#type_percent").val(data.calcs[id].calc.type_percent);
            $("#max_term").val(data.calcs[id].calc.max_term);
            $("#additionInfo").val(data.calcs[id].calc.additionInfo);
            $("#currency").val(data.calcs[id].calc.currency);

            CKEDITOR.instances["templateCalcEmail"].setData(data.calcs[id].sendData.templateText);
            $("#emailsCalc").val(data.calcs[id].sendData.emails);
            $("#subjectEmailCalc").val(data.calcs[id].sendData.subject);
            $("#fromEmailCalc").val(data.calcs[id].sendData.from);
        }


        for(var i in data.calcs[id].forms)
        {

            f = new formEditorFrame(idForm);
            f.formCalc.data = data.calcs[id].forms[i].data;
            formsEditorFrame[idForm] = f;
            f.Render();

            for(var k in data.calcs[id].forms[i].fields)
            {
                switch (data.calcs[id].forms[i].fields[k].type)
                {
                    case 'input':
                        var field = new formEditorElementInput(formsEditorFrame[f.id]);
                        break;
                    case 'span':
                        var field = new formEditorElementSpan(formsEditorFrame[f.id]);
                        break;
                    case 'select':
                        var field = new formEditorElementSelect(formsEditorFrame[f.id]);
                        break;
                    case 'checkbox':
                        var field = new formEditorElementCheckbox(formsEditorFrame[f.id]);
                        break;
                }

                field.data = data.calcs[id].forms[i].fields[k].data;

                f.formCalc.addField(field);
            }

            idForm++;
        }


        for(var i in data.validators)
        {
            if(!formValidators.validators[i])
                formValidators.validators[i] = data.validators[i];
        }
    }

    function deleteFormFrame(id)
    {
        delete formsEditorFrame[id];
    }


    function formEditorFrame(id)
    {

        this.id = id;
        this.attrId = "formCalcFrame"+this.id;
        this.formCalc = new formEdittorForm(formProperties, this);


        this.Render = function(timeout)
        {
            thatF = this;

            var html = "<table id='"+this.attrId+"' width='100%'>" +
                    "<tr>" +
                        "<td>" +
                            "<input type='button' value='<?=GetMessage("addField")?>' onclick='formsEditorFrame["+thatF.id+"].formCalc.addField(new formEditorElementInput(formsEditorFrame["+thatF.id+"]));' /> " +
                            "<input type='button' value='<?=GetMessage("addText")?>' onclick='formsEditorFrame["+thatF.id+"].formCalc.addField(new formEditorElementSpan(formsEditorFrame["+thatF.id+"]));' />" +
                            "<input type='button' value='<?=GetMessage("addSelect")?>' onclick='formsEditorFrame["+thatF.id+"].formCalc.addField(new formEditorElementSelect(formsEditorFrame["+thatF.id+"]));' />" +
                            "<input type='button' value='<?=GetMessage("addCheckbox")?>' onclick='formsEditorFrame["+thatF.id+"].formCalc.addField(new formEditorElementCheckbox(formsEditorFrame["+thatF.id+"]));' />" +
                        "</td>" +
                    "</tr>" +
                    "<tr>" +
                        "<td width='50%' valign='top'>" +
                            "<div name='formEditor' class='formEditor'></div>" +
                            "<p name='properties' ><span name='size' ><input style='cursor: move;' type='button' value='<?=GetMessage("formSize")?>'/> </span></p>"+
                        "</td>" +

                    "</tr>";
            $("#calcFormsEditor").append(html);

            this.formCalc.Render("#"+this.attrId, timeout);
        }

        this.deleteFrame = function()
        {
            $("#formCalcFrame"+this.id).empty();
            deleteFormFrame(this.id);
        }

    }


    function formEditorValidators()
    {
        this.timestamp = <?=time()?>;

        thatValidator = this;

        this.validators =
            {
                "0" : {
                    name: "<?=GetMessage("validatorNotEmpty")?>",
                    value: ".{1,}",
                    isDefault: true
                },
                "1" : {
                    name: "<?=GetMessage("validatorEmail")?>",
                    value: "^.{1,}@.{1,}$",
                    isDefault: true
                },
                "3" : {
                    name: "<?=GetMessage("validatorOnlyNumbers")?>",
                    value: "^[0-9]{1,}$",
                    isDefault: true
                }
            };
        this.id = 0;


        this.add = function(name, regEx, notsave)
        {
            if(!name)
                name='default';
            if(!regEx)
                regEx = '.*';


            $("#calcFormsValidators").append(this.renderValidator(++this.timestamp, {value: regEx, name:name}));

            if(!notsave)
                this.save();
        }

        this.edit = function(id, type)
        {
            var td = $("#calcFormsValidators * #validator_"+id).children();
            var val = $(td[type]).text();

            $(td[type]).html("<input type='text' value='"+val+"' onblur='thatValidator.change(\""+id+"\", "+type+")'/>");
            $(td[type]).children("input").select();
        }

        this.Render = function()
        {
            for(var i in this.validators)
            {
                $("#calcFormsValidators").append(this.renderValidator(i, this.validators[i]));
            }
        }


        this.renderValidator = function(name, validator)
        {
            if(validator.isDefault)
                return "<tr id='validator_"+name+"'><td><span>"+validator.name+"</span></td><td><input type='button' value='<?=GetMessage("editValidator")?>' disabled='disabled'/></td><td><input type='button' value='<?=GetMessage("deleteValidator")?>' disabled='disabled'/> </td></tr>";
            else
                return "<tr id='validator_"+name+"'><td><span onclick='thatValidator.edit(\""+name+"\", 0)'>"+validator.name+"</span></td><td><span style='display:none'>"+validator.value+"</span><input onclick='thatValidator.edit(\""+name+"\", 1)' type='button' value='<?=GetMessage("editValidator")?>' /></td><td><input type='button' value='<?=GetMessage("deleteValidator")?>' onclick='thatValidator.deleteValidator(\""+name+"\")'/></td></tr>";
        }

        this.change = function(id, type)
        {
            var td = $("#calcFormsValidators * #validator_"+id).children();
            var val = $(td[type]).children("input").val();

            if(type == 1)
                $(td[1]).html("<span style='display:none'>"+val+"</span><input onclick='thatValidator.edit(\""+id+"\", 1)' type='button' value='<?=GetMessage("editValidator")?>' />");
            else
                $(td[type]).html("<span onclick='thatValidator.edit(\""+id+"\", "+type+")'>"+val+"</span>");

            this.save();
        }

        this.save = function()
        {
            thatValidator = this;
            this.validators = {};
            $("#calcFormsValidators * tr").each(
                    function(i, element)
                    {
                        var td = $(element).children("td");
                        if(td.length)
                        {
                            var key = $(element).attr("id").split("_")[1];
                            var name = $(td[0]).text();
                            var val = $(td[1]).text();

                            thatValidator.validators[key] =
                                {
                                    name : name,
                                    value : val
                                }
                        }
                    }
            )

            formProperties.updateValues();

        }

        this.deleteValidator = function(id)
        {
            if(confirm("<?=GetMessage("confirmDelete")?>"))
            {
                delete this.validators[id];
                $("#calcFormsValidators * #validator_"+id).remove();
            }
        }

    }




    function formEditorProperties(selector)
    {
        this.selector = selector;
        this.isMove = false;
        this.fields = [];


        this.position = {
            x: $(window).width()-300,
            y: ($(window).height()/2)-200
        }
        this.positionType = "fixed";

        if(window.ActiveXObject)
            this.positionType = "absolute";

        this.update = function()
        {
            $(thatP.selector).attr("style","border: 1px solid #000000; padding: 5px; background-color: white; z-index:100; position:"+thatP.positionType+"; left:"+thatP.position.x+"px; top:"+thatP.position.y+"px;")
        }

        this.updateValues = function()
        {
            for(var key in this.fields)
            {
                var value = this.currentElement.data[this.fields[key].field.path];
                this.fields[key].update(value);
            }
        }

        this.Render = function(element)
        {
            thatP = this;
            this.fields = [];

            this.currentElement = element;
            this.Empty();
            t = "";
            this.dataP = element.getData();

            for(var key in this.dataP)
            {
                if(this.dataP[key].type != "block")
                {
                    var f = new this.Fields[this.dataP[key].type](this.dataP[key]);
                    t += f.Render();
                    this.fields.push(f);
                }
                else
                {
                    t += "<tr><td><strong>"+this.dataP[key].name+"</strong></td></tr>";
                    for(var i in this.dataP[key].items)
                    {
                        var f = new this.Fields[this.dataP[key].items[i].type](this.dataP[key].items[i]);
                        t += f.Render();
                        this.fields.push(f);
                    }
                }
            }

            var posTypeBtn = "<input type='button' value='<?=GetMessage("propUnfixed")?>' name='fixed'/>";
            if(window.ActiveXObject)
                posTypeBtn = "";

            $(this.selector).append(
                    "<table >"+
                            "<tr>" +
                            "<td align='left'><?=GetMessage("properties")?></td><td align='right'><input type='button' value='<?=GetMessage("propPosition")?>' name='position'/>"+posTypeBtn+"</td>"+
                            "</tr>"+
                            t+
                    "</table>"
            );

            $('body').mousemove(
                    function(event)
                    {
                        if(thatP.isMove)
                        {
                            if(!correctPosForm)
                            {
                                correctPosForm = {
                                    x: (event.pageX - $("#windowEditCalc").offset().left)-($(thatP.selector).offset().left - $("#windowEditCalc").offset().left),
                                    y: (event.pageY - $("#windowEditCalc").offset().top)-($(thatP.selector).offset().top - $("#windowEditCalc").offset().top)
                                }
                            };

                            if(thatP.positionType == "absolute")
                            {
                                var x = (event.pageX - $("#windowEditCalc").offset().left)-correctPosForm.x;
                                var y = event.pageY - $("#windowEditCalc").offset().top - correctPosForm.y;
                            }
                            else
                            {
                                var x = event.clientX-correctPosForm.x;
                                var y = event.clientY-correctPosForm.y;
                            }

                            thatP.position.x = x;
                            thatP.position.y = y;
                            thatP.update();
                        }
                        else
                            correctPosForm = false;
                    }
            )

            $(this.selector+" * [name=position]").mousedown(
                    function(event)
                    {
                        thatP.isMove = true;
                    }
            )
            $(this.selector+" * [name=fixed]").click(
                    function(event)
                    {
                        if(thatP.positionType == 'absolute')
                        {
                            thatP.position.x = $(thatP.selector).offset().left - $(window).scrollLeft();
                            thatP.position.y = $(thatP.selector).offset().top - $(window).scrollTop();


                            thatP.positionType = 'fixed';
                            $(thatP.selector+" * [name=fixed]").val("<?=GetMessage("propUnfixed")?>");
                        }
                        else
                        {
                            var x = $(thatP.selector).offset().left - $("#windowEditCalc").offset().left;
                            var y = $(thatP.selector).offset().top - $("#windowEditCalc").offset().top;

                            if(($(thatP.selector).offset().left - $("#windowEditCalc").offset().left + $(thatP.selector).width()) > $("#windowEditCalc").width())
                                x = $("#windowEditCalc").width()-$(thatP.selector).width();
                            else if($(thatP.selector).offset().left - $("#windowEditCalc").offset().left < 0)
                                x = 0;

                            if(($(thatP.selector).offset().top - $("#windowEditCalc").offset().top + $(thatP.selector).height()) > $("#windowEditCalc").height())
                                y = $("#windowEditCalc").height()-$(thatP.selector).height();
                            else if($(thatP.selector).offset().top - $("#windowEditCalc").offset().top < 0)
                                y = 0;

                            thatP.position.x = x;
                            thatP.position.y = y;

                            thatP.positionType = 'absolute';
                            $(thatP.selector+" * [name=fixed]").val("<?=GetMessage("propFixed")?>");
                        }

                        thatP.update();
                    }
            )

            $('body').mouseup(
                    function(event)
                    {
                        thatP.isMove = false;
                    }
            )

            this.update();
        }

        this.Empty = function()
        {
            $(this.selector).attr("style", "diplay:none");
            $(this.selector).empty();
        }

        this.Fields = {
            input : function(field)
            {
                this.field = field;
                this.setValues = function(path)
                {
                    val = $(thatP.selector + " * [name = "+path+"]").val();

                    if(val == "default" || val.length == 0)
                        val = false;

                    var result = {};


                    result[path] = val;
                    thatP.currentElement.setData(result);
                }

                this.update = function(value)
                {
                    if(value!=undefined)
                    {
                        $(thatP.selector + " * [name = "+field.path+"]").val(value);
                    }
                }

                this.Render = function()
                {
                    thatField = this;
                    return "<tr><td>"+field.name+"</td><td><input name='"+field.path+"' type='text' onchange='thatField.setValues(\""+field.path+"\")' value='"+field.value+"'/></td></tr>";
                }
            },
            selectset: function(field)
            {
                this.id = 0;
                this.field = field;
                this.update = function(value){}
                this.setValues = function()
                {
                    var values = [];
                    $(thatP.selector + " * [name = "+field.path+"] * tr > td").each(
                            function(i, elem)
                            {
                                var type = $(elem).attr("id").split("_");
                                var index = type[1];
                                type = type[type.length-1];

                                if(!values[index])
                                    values[index] = {};
                                values[index][type] = $(elem).text();
                            }
                    )

                    var result = {};
                    result[field.path] = values;
                    thatP.currentElement.setData(result);
                }

                this.change = function(id)
                {
                    var type = id.split("_")[2];


                    var val = $(thatP.selector + " * [name = "+field.path+"] * #"+id+" > input").val();

                    if(type == "value" && !val)
                        val = "default";


                    if(type == "name" && !val)
                        $(thatP.selector + " * [name = "+field.path+"] * #"+id).parent("tr").remove();
                    else
                        $(thatP.selector + " * [name = "+field.path+"] * #"+id).html("<span onclick='thatFieldSelect.edit(\""+id+"\")'>"+val+"</span>")
                    this.setValues();
                }

                this.add = function(name, value)
                {
                    $(thatP.selector + " * [name = "+field.path+"]").append(this.getItemHtml(name, value));
                    this.setValues();
                }

                this.getItemHtml = function(name, value)
                {
                    if(!name)
                        name = 'default';

                    if(!value)
                        value = 'default';

                    thatFieldSelect = this;
                    var html = "<tr >" +
                            "<td id='option_"+this.id+"_name' >" +
                            "<span onclick='thatFieldSelect.edit(\"option_"+this.id+"_name\")'>"+name+"</span>"+
                            "</td>" +
                            "<td id='option_"+this.id+"_value' >" +
                            "<span onclick='thatFieldSelect.edit(\"option_"+this.id+"_value\")'>"+value+"</span>"+
                            "</td>" +
                            "</tr>"
                    this.id++;
                    return html;
                }

                this.edit = function(id, name)
                {
                    var val = $(thatP.selector + " * [name = "+field.path+"] * #"+id).text();
                    $(thatP.selector + " * [name = "+field.path+"] * #"+id).html("<input onblur='thatFieldSelect.change(\""+id+"\")' type='text' value='"+val+"' />");
                    $(thatP.selector + " * [name = "+field.path+"] * #"+id+" > input").select();
                }


                this.Render = function()
                {
                    thatFieldSelect = this;
                    var items = "";
                    if(field.value)
                    {

                        for(var i in field.value)
                        {
                            items+=this.getItemHtml(field.value[i].name, field.value[i].value);
                        }
                    }

                    return "<tr>" +
                                "<tr>"+
                                    "<td style='background: none !important;'>"
                                        +field.name+
                                    "</td>" +
                                    "<td>" +
                                        "<div style='overflow-y: auto; max-height: 100px'>"+
                                        "<table name='"+field.path+"'>"+
                                            "<th><?=GetMessage("name")?></th><th><?=GetMessage("value")?></th>"+
                                            items+
                                        "</table>"+
                                        "</div>"+
                                    "</td>" +
                                "</tr>"+
                                "<tr>"+
                                    "<td>"+
                                        "<input type='button' value='<?=GetMessage("add")?>' onclick='thatFieldSelect.add()'/>"+
                                    "</td>"+
                                "</tr>"+
                            "</tr>";
                }

            },
            select : function(field)
            {
                this.field = field;
                this.update = function(value){}
                this.setValue = function()
                {
                    var val = $(thatP.selector + " * [name = "+field.path+"]").val();
                    var result = {};

                    result[field.path] = val;
                    thatP.currentElement.setData(result);

                }

                this.Render = function()
                {
                    thatSelect = this;
                    var items = "<option></option>";

                    for(var i in field.items)
                    {
                        var selected = "";
                        if(i == field.value)
                        {
                            selected = "selected=selected"
                        }


                        items+="<option "+selected+" value='"+i+"'>"+field.items[i].name+"</option>";
                    }
                    return "<tr><td>"+field.name+"</td><td><select onchange='thatSelect.setValue()' name='"+field.path+"'>"+items+"</select></td></tr>"
                }
            },

            button : function(field)
            {
                this.field = field;
                this.update = function(value){}

                this.action = function()
                {
                    thatP.currentElement[field.action]();
                }

                this.Render = function()
                {
                    thatButton = this;
                    return "<tr><td><input type='button' value='"+field.name+"' onclick='thatButton.action()'/></td></tr>";
                }
            },

            checkbox : function(field)
            {
                this.field = field;
                this.setValues = function(path)
                {
                    var result = {};
                    if($(thatP.selector + " * [name = "+path+"]").attr("checked"))
                        result[path] = true;
                    else
                        result[path] = false;

                    console.debug(result);
                    thatP.currentElement.setData(result);
                }

                this.update = function(value)
                {

                }


                this.Render = function()
                {
                    thatField = this;
                    var checked = "";
                    if(field.value)
                        checked="checked=checked";

                    return "<tr><td>"+field.name+"</td><td><input name='"+field.path+"' type='checkbox' onchange='thatField.setValues(\""+field.path+"\")' "+checked+" /></tr>";
                }
            }
        }
    }




    function formEdittorForm(formProp, formFrame)
    {

        this.formProp = formProp;
        this.formFrame = formFrame;
        this.selectedElement = false;
        var that = this;
        this.data = {
            width: 800,
            height:300
        }
        this.tagName = "formCalc";
        this.fields = {};
        this.isSize = false;

        this.mousePos = {
            x: 0,
            y: 0
        };

        this.deleteForm = function()
        {
            if(confirm("<?=GetMessage("confirmDelete")?>?"))
                this.formFrame.deleteFrame();

        }

        this.getData = function()
        {
            return {
                actions:{
                    name:"<?=GetMessage("actions")?>",
                    type:"block",
                    items:{
                            deleteBtn:{
                            name:"<?=GetMessage("delete")?>",
                            type:"button",
                            action:"deleteForm"
                        }
                    }
                },
                width:{
                    type:"input",
                    name:"width",
                    value:this.data.width,
                    path: "width"
                },
                height:{
                    type:"input",
                    name:"height",
                    value:this.data.height,
                    path:"height"
                }
            }
        }

        this.setData = function(arr)
        {
            this.data = $.extend(this.data, arr);
            this.update();
        }

        this.update = function()
        {
            $(this.selector).attr("style", "overflow:hidden !important; font-size: 13px; font-family: Arial; position: relative; z-index: 1; width:"+this.data.width+"px; height:"+this.data.height+"px; border: 1px solid black;");
            $(this.propSelector).attr("style", "text-align:right!important; background-color: silver; margin: 0px!important; width:"+this.data.width+"px");
        }
        this.Render = function(selector, timeout)
        {
            this.selector = selector+" * [name = formEditor]";
            this.propSelector = selector+" * [name = properties]";
            $(this.selector).append("");



            $(this.selector).mousemove(function(event)
                    {
                        offset = $(this).offset();
                        that.mousePos = {
                            x: parseInt(event.pageX - offset.left),
                            y: parseInt(event.pageY - offset.top)
                        }
                        if(that.selectedElement)
                        {
                            if(!correctionPos)
                            {
                                correctionPos = {
                                    x:that.mousePos.x-that.selectedElement.data.positionX,
                                    y:that.mousePos.y-that.selectedElement.data.positionY
                                }
                            }
                            var x = that.mousePos.x-correctionPos.x;
                            var y = that.mousePos.y-correctionPos.y;

                            if(that.mousePos.x < that.data.width && that.mousePos.x >= 0 && that.mousePos.y < that.data.height && that.mousePos.y >= 0)
                            {
                                that.selectedElement.setData({positionX : x, positionY : y});
                            }
                        }
                        else
                            correctionPos = false;

                    }
            )

            $('body').mousemove(function(event)
                    {
                        if(that.isSize)
                        {
                            that.data.width = parseInt(event.pageX - offset.left);
                            that.data.height = parseInt(event.pageY - offset.top);
                            that.update();
                            formProperties.updateValues();
                        }
                    }
            );

            $(this.propSelector).mousedown(
                    function(event)
                    {
                        formProperties.Render(that);
                    }
            )


            $(this.propSelector+" > [name=size]").mousedown(
                    function(event)
                    {
                        that.isSize = true;
                    }
            );
            $('body').mouseup(
                    function(event)
                    {
                        that.isSize = false;
                    }
            );
            t = this;
            if(timeout)
            {
                setTimeout("t.update()", 100);
            }
            else
            {
                this.update();
            }
        }

        this.addField = function(field)
        {
            this.fields[field.id] = field;
            field.Render(this.selector);

        }

        this.deleteElement = function(id)
        {
            delete this.fields[id];
        }

    }


    function formEditorElement(baseForm)
    {
        this.propForm = baseForm.formProperties;
        this.formCalc = baseForm.formCalc;

        this.id = "field_"+(idField++);
        var that = this;
        this.isMoved = false;
        this.selector = "#"+this.id;

        this.data = {
            positionX : 100,
            positionY : 100
        };

        this.styleAttr = {
            position: "absolute",
            cursor: "move"
        }

        this.setData = function(arr)
        {
            arr = this.checkVal(arr);
            if(!arr)
                return;

            this.data = $.extend(this.data, arr);
            this.update();
        }



        this.Render = function(selector, self)
        {
            that.appendElement(selector);
            that.setAttr();

            $(that.selector).mousedown(function(event)
                    {
                        that.formCalc.selectedElement = that;
                        setTimeout(
                                function(){formProperties.Render(that)},
                                100
                        );



                        if(that.mouseDownFunc != undefined)
                            that.mouseDownFunc();
                    }
            );
            $("body").mouseup(function(event)
                    {
                        that.formCalc.selectedElement = false;
                        if(that.mouseUpFunc != undefined)
                            that.mouseUpFunc();
                    }
            );

            this.update();
        }

        this.deleteElement = function()
        {
            if(!confirm("<?=GetMessage("confirmDelete")?>"))
                return;

            formProperties.Empty();
            this.formCalc.deleteElement(this.id);
            $("#"+this.id).remove();
        }



        this.appendElement = function(selector)
        {
            $(selector).append("<input type='text' id='"+this.id+"'/>");
        }



        this.styleAttrToString = function()
        {
            str = "";
            for(var key in this.styleAttr)
            {
                if(this.styleAttr[key])
                    str += key+":"+this.styleAttr[key]+";";
            }
            return str;
        }

        this.setAttr = function()
        {
            $(this.selector).attr("style", this.styleAttrToString());
        }
    }

    formEditorElement.prototype.checkVal = function(arr)
    {
        var m = {
            positionX:"toInt",
            positionY:"toInt",
            width:"toInt",
            height:"toInt",
            name: "checkUnique"
        };

        for(var i in arr)
        {
            if(m[i] == "toInt")
            {
                if(isNaN(parseInt(arr[i])))
                    arr[i] = 0;
            }
            else if(m[i] == "checkUnique")
            {
                var re = new RegExp("^[a-zA-Z0-9]*$");
                if(!re.exec(arr[i]))
                {
                    alert("<?=GetMessage("wrongSymbols")?>");
                    return;
                }
                for(var key in formsEditorFrame)
                {
                    for(var k in formsEditorFrame[key].formCalc.fields)
                    {
                        if(formsEditorFrame[key].formCalc.fields[k].data.name == arr[i])
                        {
                            alert("<?=GetMessage("errorDuplicateTagName")?>");
                            return;
                        }
                    }
                }
            }

        }
        return arr;
    }

    formEditorElement.prototype.update = function()
    {
        this.styleAttr.top = parseInt(this.data.positionY)+"px";
        this.styleAttr.left = parseInt(this.data.positionX)+"px";
        this.setAttr();
        formProperties.updateValues();
    }

    formEditorElement.prototype.getData = function()
    {
        return {
            actions:{
                name: "<?=GetMessage("actions")?>",
                type: "block",
                items:{
                    deleteBtn: {
                        name:"<?=GetMessage("delete")?>",
                        type:"button",
                        action: "deleteElement"
                    }
                }
            },
            position : {
                name: "<?=GetMessage("position")?>",
                type : "block",
                items : {
                    x : {
                        name: "x",
                        type : "input",
                        value : this.data.positionX,
                        path: "positionX"
                    },
                    y : {
                        name: "y",
                        type : "input",
                        value : this.data.positionY,
                        path: "positionY"
                    }
                }
            }
        }
    }


    function formEditorElementInput(id)
    {
        this.type='input';
        formEditorElementInput.superclass.constructor.apply(this, new Array(id));

        this.data.width = 130;
        this.data.height = 20;


        this.getData = function()
        {
            var data = formEditorElementSpan.superclass.getData.apply(this, new Array());

            var validators = {};

            for(var i in formValidators.validators)
            {
                validators[i] = {
                    name: formValidators.validators[i].name
                };
            }
            var tagName = this.data.name ? this.data.name : "";
            data.name = {
                name: "<?=GetMessage("nameTag")?>",
                type: "input",
                value: tagName,
                path: "name"
            };

            data.width = {
                name:"<?=GetMessage("width")?>",
                type:"input",
                path:"width",
                value: this.data.width
            };

            data.height = {
                name:"<?=GetMessage("height")?>",
                type:"input",
                path:"height",
                value:this.data.height
            };

            data.validator = {
                name:"<?=GetMessage("validator")?>",
                type:"select",
                path:"validator",
                value:this.data.validator,
                items:validators
            }

            return data;
        }


        this.update = function()
        {
            if(this.data.width)
                this.styleAttr.width = parseInt(this.data.width)+"px";
            else
                this.styleAttr.width = false;

            if(this.data.height)
                this.styleAttr.height = parseInt(this.data.height)+"px";
            else
                this.styleAttr.height = false;

            formEditorElementSpan.superclass.update.apply(this, new Array());
        }
    }

    function formEditorElementSpan(id)
    {
        this.type='span';
        formEditorElementSpan.superclass.constructor.apply(this, new Array(id));

        this.data.text = "Text";

        this.getData = function()
        {
            var data = formEditorElementSpan.superclass.getData.apply(this, new Array());
            data.text = {
                name: "<?=GetMessage("text")?>",
                type: "input",
                value: this.data.text,
                path: "text"
            }
            return data;
        }

        this.update = function()
        {
            $(this.selector).empty();
            $(this.selector).append(this.data.text);
            formEditorElementSpan.superclass.update.apply(this, new Array());
        }

        this.appendElement = function(selector)
        {
            $(selector).append("<span id='"+this.id+"'>a</span>");
        }
    }


    function formEditorElementSelect(id)
    {
        this.type='select';
        this.lock = false;

        formEditorElementSelect.superclass.constructor.apply(this, new Array(id));

        this.data.width = 70;
        this.data.height = 30;

        this.appendElement = function(selector)
        {
            $(selector).append("<select id='"+this.id+"'></select>");
        }

        this.mouseDownFunc = function()
        {
            $(this.selector).empty();
            this.lock = true;
        }

        this.mouseUpFunc = function()
        {
            this.lock = false;
            this.appendValues();
        }

        this.getData = function()
        {
            var data = formEditorElementSpan.superclass.getData.apply(this, new Array());
            var tagName = this.data.name ? this.data.name : "";

            data.name = {
                name: "<?=GetMessage("nameTag")?>",
                type: "input",
                value: tagName,
                path: "name"
            };

            data.width = {
                name:"<?=GetMessage("width")?>",
                type:"input",
                path:"width",
                value: this.data.width
            };

            data.height = {
                name:"<?=GetMessage("height")?>",
                type:"input",
                path:"height",
                value:this.data.height
            };

            data.values = {
                name: "<?=GetMessage("values")?>",
                type: "selectset",
                value: this.data.values,
                path: "values"
            };

            return data;
        }

        this.update = function()
        {
            if(this.data.values)
            {
                if(!this.lock)
                    this.appendValues();
            }

            if(this.data.width)
                this.styleAttr.width = parseInt(this.data.width)+"px";
            else
                this.styleAttr.width = false;

            if(this.data.height)
                this.styleAttr.height = parseInt(this.data.height)+"px";
            else
                this.styleAttr.height = false;

            formEditorElementSelect.superclass.update.apply(this, new Array());
        }

        this.appendValues = function()
        {
            $(this.selector).empty();
            for(var i in this.data.values)
            {
                $(this.selector).append("<option>"+this.data.values[i].name+"</option>");
            }
        }
    }

    function formEditorElementCheckbox(id)
    {
        this.type='checkbox';
        formEditorElementCheckbox.superclass.constructor.apply(this, new Array(id));

        this.getData = function()
        {
            var data = formEditorElementCheckbox.superclass.getData.apply(this, new Array());


            var tagName = this.data.name ? this.data.name : "";
            var essential = this.data.essential ? true : false;
            data.name = {
                name: "<?=GetMessage("nameTag")?>",
                type: "input",
                value: tagName,
                path: "name"
            };

            data.essential = {
                name:"<?=GetMessage("essential")?>",
                type:"checkbox",
                value: essential,
                path: "essential"
            }

            return data;
        }


        this.appendElement = function(selector)
        {
            $(selector).append("<input type='checkbox' id='"+this.id+"'/>");
        }
    }


    function extend(Child, Parent)
    {
        var F = function() { }
        F.prototype = Parent.prototype

        Child.prototype = new F()
        Child.prototype.constructor = Child
        Child.superclass = Parent.prototype
     }


CKEDITOR.basePath = "/bitrix/components/ceolab/calc.mfo/js/ckeditor/";
CKEDITOR.replace('templateCalcEmail');
try{
    data = JSON.parse(this.sendDataParams.oInput.value);
}catch(e)
{
    data = {

    };
}



//showFormEditor()

</script>
<div id="windowEditCalc" style='position:relative'>
<div>
    <table width='100%'>
        <tr>
            <td valign='top' align='left'>
                <table name="calcFieldSet">
                    <tr>
                        <td>
                            <?=GetMessage("calcName")?>:
                        </td>
                        <td>
                            <input type="text" id="name" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?=GetMessage("minSum")?>:
                        </td>
                        <td>
                            <input type="text" id="min_summ" name="min_summ"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?=GetMessage("maxSum")?>:
                        </td>
                        <td>
                            <input type="text" id="max_summ" name="max_summ"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?=GetMessage("step")?>:
                        </td>
                        <td>
                            <input type="text" id="step" name="step"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?=GetMessage("maxTerm")?>:
                        </td>
                        <td>
                            <input type="text" id="max_term" name="max_term"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?=GetMessage("percent")?>
                        </td>
                        <td>
                            <input type="text" id="val_percent" name="percent"/>
                            <?=GetMessage("in")?>
                            <select id="type_percent" name="type_percent">
                                <option value="day"><?=GetMessage("day")?></option>
                                <option value="week"><?=GetMessage("week")?></option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <?=GetMessage("currency")?>
                        </td>
                        <td>
                            <input type="text" id="currency"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?=GetMessage("additionInfo")?>:
                        </td>
                        <td>
                            <textarea id="additionInfo">

                            </textarea>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign='top' align='right'>
                <div>
                    <strong>
                        <?=GetMessage("validators")?>
                    </strong>
                    <table id="calcFormsValidators" style='margin-top: 5px'>
                        <th><?=GetMessage("nameValidator")?></th>
                        <th><?=GetMessage("valValidator")?></th>

                    </table>
                    <input type="button" value="<?=GetMessage("addValidator")?>" onclick="formValidators.add()"/>
                </div>
            </td>
        </tr>
    </table>
</div>


<div align='right' id='formProperties' style='border: 1px solid #000000; display:block; background-color: white; position:absolute;'></div>
<div align="right">
    <h2 align='left'><?=GetMessage("settingsForm")?></h2>
    <input type="button" value="<?=GetMessage("addForm")?>" onclick="showFormEditor()" />
    <div id="calcFormsEditor" align="left">

    </div>

</div>
<div>
    <h2 align='left'><?=GetMessage("settingsSend")?></h2>
    <table width="100%">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <?=GetMessage("emailRecipient")?>:
                        </td>
                        <td>
                            <input type="type" id="emailsCalc" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?=GetMessage("emailSubject")?>:
                        </td>
                        <td>
                            <input type="type" id="subjectEmailCalc" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?=GetMessage("emailSender")?>:
                        </td>
                        <td>
                            <input type="type" id="fromEmailCalc" />
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <?=GetMessage("additionTags")?>:
                <table border="1px">
                    <tr>
                        <td>
                            <?=GetMessage("summTag")?>
                        </td>
                        <td>
                            %summ%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?=GetMessage("termTag")?>
                        </td>
                        <td>
                            %term%
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <?=GetMessage("percentTag")?>
                        </td>
                        <td>
                            %percent%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?=GetMessage("totalTag")?>
                        </td>
                        <td>
                            %total%
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <p><?=GetMessage("template")?></p>
    <textarea id="templateCalcEmail"></textarea>
</div>


    <input style='margin-top: 100px;' type="button" onclick="saveData(<?=$id?>)" value="<?=GetMessage("save")?>" />
    <?if($id !== false):?>
<script>
    initForms(<?=$id?>);
</script>
    <? endif ?>
<script>
    formValidators.Render();
</script>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Документация");
?> 
<div class="container"> 
  <h1>Документация</h1>
 
  <div class="doc-images"> 
    <div class="row"> 
      <div class="col-sm-3 " style="margin: 0px auto; float: none;"> 
        <div class="doc-images-element relative"> <a class="absolute" target="_blank" href="/upload/docs/express-svidetelstvo.pdf" > 
            <div class="img-wrapper"> <img src="/upload/medialibrary/d28/d28adcf47e82073952e1364ff9fbb6ad.jpg" title="sv.jpg" alt="sv.jpg" width="240" height="340"  /> </div>
           </a> <a href="express-svidetelstvo.pdf" >Свидетельство МФО</a> </div>
       </div>
     </div>
   
    <div class="row" style="margin-bottom: 50px;"> 
      <div class="col-sm-3"> 
        <div class="element-doc pdf relative"> <a class="absolute" target="_blank" href="/upload/docs/express-usloviya-dogovora.doc" ></a> 
          <div class="text"> <a href="/upload/docs/express-usloviya-dogovora.doc" >Общие условия договора займа</a> 
            <div class="gray light f14">DOC, 121 КБ</div>
           </div>
         </div>
       </div>
     
      <div class="col-sm-3"> 
        <div class="element-doc pdf relative"> <a class="absolute" target="_blank" href="/upload/docs/express-politika-obrabotki.docx" ></a> 
          <div class="text"> <a target="_blank" href="/upload/docs/express-politika-obrabotki.docx" >Политика по персональным данным</a> 
            <div class="gray light f14">DOCX, 48 КБ</div>
           </div>
         </div>
       </div>
     
      <div class="col-sm-3"> 
        <div class="element-doc pdf relative"> <a class="absolute" target="_blank" href="/upload/docs/express-raskritie-informacii.doc" ></a> 
          <div class="text"> <a target="_blank" href="/upload/docs/express-raskritie-informacii.doc" >Положение о раскрытии информации</a> 
            <div class="gray light f14">DOC, 59 КБ</div>
           </div>
         </div>
       </div>
     
      <div class="col-sm-3"> 
        <div class="element-doc pdf relative"> <a class="absolute" target="_blank" href="/upload/docs/express-pravila.doc" ></a> 
          <div class="text"> <a href="/upload/docs/express-pravila.doc" >Правила предоставления микрозаймов</a> 
            <div class="gray light f14">DOC, 102 КБ</div>
           </div>
         </div>
       </div>
     
      <div class="col-sm-12">
        <br />
      </div>
    
      <div class="col-sm-12">
        <br />
      </div>
    
      <div class="col-sm-12">Компания Экспресс-Деньги ведет свою деятельность в соответствии с действующим законодательством Российской Федерации, компания внесена в государственный реестр микрофинансовых организаций. 
        <div> </div>
       </div>
     </div>
   </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
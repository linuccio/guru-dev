<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список аффилированных лиц");
?>

    <div style="float: left" class="content_menu">
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "footer",
            Array(
                "ROOT_MENU_TYPE" => "footer",
                "MAX_LEVEL" => "2",
                "CHILD_MENU_TYPE" => "left",
                "USE_EXT" => "Y",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => Array()
            )
        );?>
        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/include_areas/left_banner.php",
                "EDIT_TEMPLATE" => ""
            ),
            false,
            array(
                "ACTIVE_COMPONENT" => "Y"
            )
        );?>

    </div>
<div class="content_text">
<p style="font-size: 42px; ">
    Список аффилированных лиц
</p>
<div align="center" class="mfo_affiliated_page">
    Список аффилированных лиц ООО «МФО»<br/>
    ОГРН 0000000000, ИНН 0000000000
</div><br><br>
<?$APPLICATION->IncludeComponent("ceolab:affiliate", ".default", array(
        "IBLOCK_TYPE" => "affiliatted",
        "IBLOCK_ID" => "6"
    ),
    false
);?> </div><div style="height: 500px"></div><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Готовый сайт для Вашей МФО | Как погасить займ?");
?>
    <div style="float: left" class="content_menu">
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "footer",
            Array(
                "ROOT_MENU_TYPE" => "footer",
                "MAX_LEVEL" => "2",
                "CHILD_MENU_TYPE" => "left",
                "USE_EXT" => "Y",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => Array()
            )
        );?>
        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/include_areas/left_banner.php",
                "EDIT_TEMPLATE" => ""
            ),
            false,
            array(
                "ACTIVE_COMPONENT" => "Y"
            )
        );?>

    </div>
    <div class="content_text">
<div class="how_to_get">
    <h1><span style="color: rgb(44, 136, 184);"></span>&nbsp;Как погасить займ? </h1>

            <p style="color: rgb(44, 136, 184); font-size: 20px;"> В любом офисе компании &laquo;МФО&raquo; </p>

            <div style="line-height: 25px;"> Если сумма к оплате превышает 15 000 рублей, при себе необходимо иметь паспорт. Если подходит срок оплаты, а у Вас нет возможности оплатить самостоятельно, Вы можете попросить друга или родственника, достигшего совершеннолетия, оплатить за Вас: ему необходимо иметь при себе паспорт и знать номер Вашего договора микрозайма. </div>

            <p style="color: rgb(44, 136, 184); font-size: 20px;"> В любом отделении любого банка </p>

            <div style="line-height: 25px;"> Безналичным переводом на расчетный счет нашей компании. Необходим паспорт, реквизиты компании и информация о договоре. </div>

            <br />

            <div style="color: silver; font-size: 12px;"> ООО &laquo;ООО «МФО»&raquo;
              <br />
             г. Москва
              <br />
             ИНН 0000000000
              <br />
             КПП 0000000000
              <br />
             ОГРН 0000000000
              <br />
             ОКПО 0000000000
              <br />
             ОКАТО 0000000000
              <br />
             р/с 0000000000
              <br />
             в филиале № 000
              <br />
             БИК 0000000000
              <br />
             к/с 0000000000
              <br />
             </div>



            <p style="color: rgb(44, 136, 184); font-size: 20px;"> Через QIWI терминал </p>

            <div style="line-height: 25px;"> Подробнее об оплате через терминалы QIWI можно прочитать здесь. </div>

</div></div><div style="height: 350px"></div>
 <?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
<!-- Slideshow :: start -->
<section class="slideshow slideshow-about">

    <!-- Title page :: start-->
    <div class="container title">
        <div class="row">
            <div class="col-lg-5 col-sm-7 col-xs-12">
                <h1 class="wow fadeInDown"><strong>О компании</strong></h1>
                <p>Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана.</p>
            </div>
        </div>
    </div>
    <!-- Title page :: end-->
</section>
<!-- Slideshow :: end -->
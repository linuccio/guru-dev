<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Готовый сайт для Вашей МФО | Адреса офисов");
?>

<main class="content">
    <div class="container">
        <div class="title-inline">
            <h1 class="h2">Адреса офисов</h1>

			<?$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"region",
				Array(
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "N",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_MODE" => "N",
					"IBLOCK_TYPE" => "regions",
					"IBLOCK_ID" => "1",
					"NEWS_COUNT" => "200",
					"SORT_BY1" => "ID",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array(),
					"PROPERTY_CODE" => array("TELEPHONE"),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "j F Y",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"CACHE_TYPE" => "N",
					"CACHE_TIME" => "3600",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N"
				)
			);?>
        </div>
	<?
	// не буду залезать в логику компонента
	if (!empty ($_SESSION["CITY_INFO"]["ID"]) && empty($_GET["city"])) {
		$_GET["city"] = $_SESSION["CITY_INFO"]["ID"];
	}
	?>	
	<?$APPLICATION->IncludeComponent("ceolab:office", "express", array(
		"IBLOCK_TYPE" => "address",
		"IBLOCK_ID" => "2",
		"DEFAULT_CITY" => "1",
		"SET_SESSION" => "",
		"GET_SESSION" => "REGION_ID",
		"SET_GET" => "city",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600"
		),
		false
	);?>
</main>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
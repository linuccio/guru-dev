<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Готовый сайт для Вашей МФО | Вакансии");
?>

<?$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "mfo_tabs",
    Array(
        "ROOT_MENU_TYPE" => "page",
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "page",
        "USE_EXT" => "Y",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => Array()
    )
);?>
<div style="margin-top: 50px;">
    <?$APPLICATION->IncludeComponent("ceolab:vacancies", ".default", array(
"IBLOCK_TYPE" => "vacancy",
"IBLOCK_ID" => "5",
"DEFAULT_CITY" => "1",
"PAGE_ID" => "id",
"COUNT_ELEMENTS" => "10",
"SET_SESSION" => "",
"GET_SESSION" => "REGION_ID",
"SET_GET" => "city",
"CACHE_TYPE" => "A",
"CACHE_TIME" => "3600"
),
false
);?>
</div>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
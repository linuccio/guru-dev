<div class="additiontext" align="center">
    <div class="item" align="left">
        <div class="left">
            <div class="icon">
                <img src="images/watch.png"/>
            </div>
        </div>
        <div class="right">
            <div class="titletext">
                Скорость рассмотрения<br/>
                заявки
            </div>
            <div class="text">
                Мы рассмотрим Вашу заявку
                в течении 20 минут и оповестим
                Вас о решении по телефону.
            </div>
        </div>
    </div>
    <div class="item" align="left">
        <div class="left">
            <div class="icon">
                <img src="images/earnings.png"/>
            </div>
        </div>
        <div class="right">
            <div class="titletext">
                Моментальная выдача<br/>
                денег
            </div>
            <div class="text">
                Вы получите деньги в тот же
                момент, как подпишите договор
                займа в офисе
            </div>
        </div>
    </div>
    <div class="item" align="left">
        <div class="left">
            <div class="icon">
                <img src="images/cross.png"/>
            </div>
        </div>
        <div class="right">
            <div class="titletext">
                Удобное погашение<br/>
                займа
            </div>
            <div class="text">
                Погашение или продление
                займа занимает не более
                10 минут.
            </div>
        </div>
    </div>
</div>
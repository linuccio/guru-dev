<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?> <section class="wrap"> <main class="content clearfix"> 
<!-- Content :: start -->
 
    <div class="container"> 
      <div class="about-goal"> 
        <div class="image hidden-xs"> <img src="<?= SITE_TEMPLATE_PATH?>/img/goal.png" class="img-responsive"  /> </div>
       
        <div class="text"> 
          <p> 						</p>
         
          <h5> 							<strong class="add-color"> 							<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/company_goal_title.php",
		"EDIT_TEMPLATE" => ""
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'Y'
)
);?>					 							</strong> 						</h5>
         					 
          <p></p>
         
          <p> 					<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/company_goal.php",
		"EDIT_TEMPLATE" => ""
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'Y'
)
);?> 					</p>
         </div>
       </div>
     </div>
   		<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"advantages",
	Array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "12",
		"NEWS_COUNT" => "3",
		"SORT_BY1" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(),
		"PROPERTY_CODE" => array(),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	)
);?> 
    <div class="slideshow-about-2"> 
      <div class="container"> 
        <h5><strong class="add-color">Как получить займ?</strong></h5>
       <a href="#" class="black-yellow" ><strong>Минимальные требования к заемщикам</strong></a> 
        <ul class="img-marker"> 					<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/advantages.php",
		"EDIT_TEMPLATE" => ""
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'Y'
)
);?> </ul>
       </div>
     </div>
   		 
<!-- инфоблок -->
 
    <div class="loan-repayment"> 
      <div class="container"> 
        <h5 class="text-center" id="how-to-pay"><strong>Как погасить займ?</strong></h5>
       
        <ul class="repayment text-center"> 				<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/methods.php",
		"EDIT_TEMPLATE" => ""
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'Y'
)
);?> </ul>
       </div>
     </div>
   		 		 
<!-- / инфоблок -->
 
    <div class="wall-red"> 			<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include_areas/about_order.php",
		"EDIT_TEMPLATE" => ""
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'Y'
)
);?>		 </div>
   
<!-- Content :: end -->
 </main> </section> <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
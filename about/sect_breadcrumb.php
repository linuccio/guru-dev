<section class="slideshow slideshow-about">
    <div class="container title">
        <div class="row">
            <div class="col-lg-5 col-sm-7 col-xs-12">
                <h1 class="wow fadeInDown"><strong><?= $APPLICATION->ShowTitle(false);?></strong></h1>
                <p>
				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_TEMPLATE_PATH . "/include_areas/about_about.php",
						"EDIT_TEMPLATE" => ""
					),
					false,
					array("ACTIVE_COMPONENT" => "Y")
				);?>				
				</p>
            </div>
        </div>
    </div>
</section>